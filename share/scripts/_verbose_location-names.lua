ardour {
	["type"]    = "EditorAction",
	name        = "Verbose Markers",
	license     = "MIT",
	author      = "Ardour Lua Task Force",
	description = [[Makes location names verbose. Also convenience parsing function for adaptation.]]
}

function factory () return function ()
	--This function parses the :flags() function attached to a location. This is a bitwise comparison.
	function parse(f)
		local flgs = ""
		if( (f & 1)   == 1)   then flgs = flgs .. "Marker "        end
		if( (f & 2)   == 2)   then flgs = flgs .. "Auto Punch"     end
		if( (f & 4)   == 4)   then flgs = flgs .. "Auto Loop"      end
		if( (f & 8)   == 8)   then flgs = flgs .. "Hidden "        end
		if( (f & 16)  == 16)  then flgs = flgs .. "CD Marker "     end
		if( (f & 32)  == 32)  then flgs = flgs .. "Range Marker "  end
		if( (f & 64)  == 64)  then flgs = flgs .. "Session Range " end
		if( (f & 128) == 128) then flgs = flgs .. "Skip "          end
		if( (f & 256) == 256) then flgs = flgs .. "Skipping " 	   end
		if( (f & 512) == 512) then flgs = flgs .. "Clock Origin "  end
		return flgs
	end

	local loc = Session:locations()
	print("Locations: ", "-------------------------------------------")
	for l in loc:list ():iter () do
		print("Name: ", l:name(), "|", "Flags: ", parse(l:flags()), "|", "Start: ", l:start(), " -- Length:", l:length())
		if not(l:is_session_range() or l:is_auto_loop()) then
			l:set_name(l:name() .. " | " .. parse(l:flags())  .. " | " .. l:start())
		end
	end
	print("----------------------------------------------------------")
end end
