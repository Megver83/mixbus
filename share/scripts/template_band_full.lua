ardour {
    ["type"]    = "EditorAction",
    name        = "Live Band Tracks",
    description = [[
This template helps create the tracks for a typical pop/rock band.

You will be prompted to assemble your session from a list of suggested tracks.

You may rename each track, and you may choose whether it is mono (default) or stereo.

Optionally, tracks may be assigned to sensible Groups ( vocals, guitars, drums ).  Each track will be pre-assigned a color corresponding to its group.

Optionally, VCA's will be created and assigned to the new tracks, depending on each track's type.

Optionally, mix-buses will be created, and the tracks will be assigned to the buses according to each track's type.

Optionally, tracks may be assigned appropriate Gate and Character plugins according to each track's type.

This script is developed in Lua, and can be duplicated and/or modified to meet your needs.
]]
}

function session_setup ()
    return true
end

function route_setup ()
    return
    {
        ['Insert_at'] = ARDOUR.PresentationInfo.max_order
    }
end

function factory (params) return function ()

   --at session load, params will be empty.  in this case we can do things that we -only- want to do if this is a new session
    if (not params) then
       Editor:set_toggleaction ("Rulers", "toggle-tempo-ruler", true)
       Editor:set_toggleaction ("Rulers", "toggle-meter-ruler", true)

       Editor:access_action ("Transport", "primary-clock-bbt")
       Editor:access_action ("Transport", "secondary-clock-minsec")

       Editor:set_toggleaction ("Rulers", "toggle-minsec-ruler", false)
       Editor:set_toggleaction ("Rulers", "toggle-timecode-ruler", false)
       Editor:set_toggleaction ("Rulers", "toggle-samples-ruler", false)

       Editor:set_toggleaction ("Rulers", "toggle-bbt-ruler", true)
    end

    local p         = params or route_setup ()
    local insert_at = p["insert_at"] or ARDOUR.PresentationInfo.max_order

    --prompt the user for the tracks they'd like to instantiate
    local dialog_options = {
        { type = "heading", title = "Track Type: ", align = "left", col=0, colspan = 1},
        { type = "heading", title = "Track Name:", align = "left", col=1, colspan = 1 },
        { type = "heading", title = "Stereo?", align = "left", col=2, colspan = 1 },

        { type = "checkbox", key = "check-basic-kit", default = false, title = "Basic Drum Mics", col=0 },
        { type = "label",  title = "Kick + Snare", col=1, colspan = 1, align = "left"},

        { type = "checkbox", key = "check-full-kit", default = false, title = "Full Drum Mics", col=0 },
        { type = "label",  title = "Hi-Hat, Hi-tom, Mid-Tom, Fl-tom", col=1, colspan = 1, align = "left"},

        { type = "checkbox", key = "check-overkill-kit", default = false, title = "Overkill Drum Mics", col=0 },
        { type = "label",  title = "Kick Beater, Snare Btm", col=1, colspan = 1, align = "left"},

        { type = "checkbox", key = "check-overhead", default = false, title = "Drum Overheads", col=0 },
        { type = "label",  title = "OH {OH L, OH R}", col=1, colspan = 1, align = "left"},
        { type = "checkbox", key = "stereo-overhead", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-room", default = false, title = "Drum Room", col=0 },
        { type = "label",  title = "Room {Rm L, Rm R}", col=1, colspan = 1, align = "left"},
        { type = "checkbox", key = "stereo-room", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-bass", default = false, title = "Bass", col=0 },
        { type = "entry",    key = "name-bass",   default = "Bass", title = "", col=1 },
        { type = "checkbox", key = "stereo-bass", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-electric-guitar", default = false, title = "Electric Guitar", col=0 },
        { type = "entry",    key = "name-electric-guitar",   default = "E Gtr", title = "", col=1 },
        { type = "checkbox", key = "stereo-electric-guitar", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-solo-guitar", default = false, title = "Lead Guitar", col=0 },
        { type = "entry",    key = "name-solo-guitar",   default = "Ld Gtr", title = "", col=1 },
        { type = "checkbox", key = "stereo-solo-guitar", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-accoustic-guitar", default = false, title = "Acoustic Guitar", col=0 },
        { type = "entry",    key = "name-accoustic-guitar",   default = "Ac Gtr", title = "", col=1 },
        { type = "checkbox", key = "stereo-accoustic-guitar", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-piano", default = false, title = "Piano", col=0 },
        { type = "entry",    key = "name-piano",   default = "Piano", title = "", col=1 },
        { type = "checkbox", key = "stereo-piano", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-electric-piano", default = false, title = "Electric Piano", col=0 },
        { type = "entry",    key = "name-electric-piano",   default = "E Piano", title = "", col=1 },
        { type = "checkbox", key = "stereo-electric-piano", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-organ", default = false, title = "Organ", col=0 },
        { type = "entry",    key = "name-organ",   default = "Organ", title = "", col=1 },
        { type = "checkbox", key = "stereo-organ", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-ldvox",  default = false,        title = "Lead Vocal", col=0 },
        { type = "entry",    key = "name-ldvox",   default = "Ld Vox", title = "", col=1 },
        { type = "checkbox", key = "stereo-ldvox", default = false,        title = "", col=2 },

        { type = "checkbox", key = "check-bgvox", default = false, title = "Background Vocals", col=0 },
        { type = "label",  title = "BGV1, BGV2, BGV3", col=1, colspan = 1, align = "left"},
        { type = "checkbox", key = "stereo-bgvox", default = false,        title = "", col=2 },

        { type = "hseparator", title="", col=0, colspan = 3},

        { type = "checkbox", key = "group", default = false, title = "Group Track(s)?", col=0 },
        { type = "checkbox", key = "vca", default =   false, title = "Add VCA master(s)?", col = 1},
        { type = "checkbox", key = "mb", default =    false, title = "Route to Mixbuses?", col = 0},
        { type = "checkbox", key = "mon", default =   false, title = "Enable Monitor?", col=1 },

        { type = "hseparator", title="", col=0, colspan = 3},
    }

    --- check for available plugins
    -- gates
    local xt_eg = not ARDOUR.LuaAPI.new_plugin_info ("XT-EG Expander Gate (Mono)", ARDOUR.PluginType.LV2):isnil ()
    local xt_tg = not ARDOUR.LuaAPI.new_plugin_info ("XT-TG Tom Gate (Mono)", ARDOUR.PluginType.LV2):isnil ()
    -- master
    local xt_me = not ARDOUR.LuaAPI.new_plugin_info ("XT-ME Mastering Equalizer (Stereo)", ARDOUR.PluginType.LV2):isnil ()
    local x_eq  = not ARDOUR.LuaAPI.new_plugin_info ("x42-eq - Parametric Equalizer Stereo", ARDOUR.PluginType.LV2):isnil ()
    local dyno  = not ARDOUR.LuaAPI.new_plugin_info ("Dyno-Mite (Stereo)", ARDOUR.PluginType.LV2):isnil ()
    local xt_mc = not ARDOUR.LuaAPI.new_plugin_info ("XT-MC Multiband Compressor (Stereo)", ARDOUR.PluginType.LV2):isnil ()
    local xt_sc = not ARDOUR.LuaAPI.new_plugin_info ("XT-SC Spectral Compressor (Stereo)", ARDOUR.PluginType.LV2):isnil ()
    -- char
    local xt_tg = not ARDOUR.LuaAPI.new_plugin_info ("XT-BC Bass Character (Mono)", ARDOUR.PluginType.LV2):isnil ()
    local xt_vc = not ARDOUR.LuaAPI.new_plugin_info ("XT-VC Vocal Character (Mono)", ARDOUR.PluginType.LV2):isnil ()

    if (xt_eg and xt_tg) then
        table.insert (dialog_options,
        { type = "checkbox", key = "gates", default = false, title = "Add Gate(s)?", col=0 }
        )
    end
    if (xt_tg and xt_vc) then
        table.insert (dialog_options,
        { type = "checkbox", key = "char", default = false, title = "Add Character Plugin(s)?", col=1 }
        )
    end
    if (xt_me and x_eq and dyno and xt_mc and xt_sc) then
        table.insert (dialog_options,
        { type = "checkbox", key = "master", default = false, title = "Add Mastering Chain?", col=0 }
        )
    end

    table.insert (dialog_options,
    { type = "hseparator", title="", col=0, colspan = 3}
    )

    local dlg = LuaDialog.Dialog ("Template Setup", dialog_options)
    local rv = dlg:run()
    if (not rv) then
        return
    end
    --[[helper function to add lv2 plugs (all plugs in this script) easy]]
    function add_lv2_plugin (track, pluginname, position)
        local p = ARDOUR.LuaAPI.new_plugin(Session, pluginname, ARDOUR.PluginType.LV2, "")
        if not p:isnil () then
            track:add_processor_by_index(p, position, nil, true)
            return p
        end
    end

    --[[helper function to reference processors takes a track (t)
    and a string (s) as arguments, returns valid processor reference (if there is one)]]
    function processor(t, s)
        local i = 0
        local proc = t:nth_processor(i)
            repeat
                if ( proc:display_name() == s ) then
                    return proc
                else
                    i = i + 1
                end
                proc = t:nth_processor(i)
            until proc:isnil()
        end

    if rv['vca'] then --Check what VCA's and Mixbuses we need to create
        --@ToDo: What follows is really dumb, there has to be a better way to check these
        if (rv['check-basic-kit'] or rv['check-full-kit'] or rv['check-overkill-kit'] or rv['check-overhead'] or rv['check-room']) then
            Session:vca_manager():create_vca (1, "Percussion")
            perc_vca = true
        end
        if (rv['check-electric-guitar'] or rv['check-solo-guitar'] or rv['check-accoustic-guitar']) then
            gtr_vca = true
        end
        if (rv['check-piano'] or rv['check-electric-piano'] or rv['check-organ']) then
            key_vca = true
        end
        if (rv['check-ldvox'] or rv['check-bgvox']) then
            vox_vca = true
        end
        if (rv['check-bass']) then
            bass_vca = true
        end
        if (bass_vca and gtr_vca) then
            Session:vca_manager():create_vca (1, "Bass + Gtr")
            bassgtr_vca = true
        end
        if (vox_vca and gtr_vca) then
            Session:vca_manager():create_vca (1, "Vox + Gtrs")
            voxgtr_vca = true
        end
        if (vox_vca and key_vca) then
            Session:vca_manager():create_vca (1, "Vox + Keys")
            voxkey_vca = true
        end
        if (gtr_vca and key_vca) then
            Session:vca_manager():create_vca (1, "Gtr + Keys")
            gtrkey_vca = true
        end
    end

    if rv['group'] then
        if (rv['check-basic-kit'] or rv['check-full-kit'] or rv['check-overkill-kit'] or rv['check-overhead'] or rv['check-room']) then
            drum_group = Session:new_route_group("Percussion")
            drum_group:set_rgba(0x425CADff)
            d_g = true
        end
        if (rv['check-electric-guitar'] or rv['check-solo-guitar'] or rv['check-accoustic-guitar']) then
            guitar_group = Session:new_route_group("Guitars")
            guitar_group:set_rgba(0xB475CBff)
            g_g = true
        end
        if (rv['check-piano'] or rv['check-electric-piano'] or rv['check-organ']) then
            key_group = Session:new_route_group("Keys")
            key_group:set_rgba(0xDA8032ff)
            k_g = true
        end
        if (rv['check-ldvox'] or rv['check-bgvox']) then
            vox_group = Session:new_route_group("Vox")
            vox_group:set_rgba(0xC54249ff)
            v_g = true
        end
        if (rv['check-bass']) then
            bass_group = Session:new_route_group("Bass")
            bass_group:set_rgba(0x1AE54Eff)
            b_g = true
        end
    end

    local channel_count = 0
    if rv['check-basic-kit'] then
        local names = {"Kick", "Snare"}
        for i = 1, #names do --#names is how many items are in the table 'names'
            local tl = Session:new_audio_track (1, 1, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(0)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if d_g then drum_group:add(track) end
                if perc_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Percussion")) end
                if rv['gates'] then
                    add_lv2_plugin (track, "XT-EG Expander Gate (Mono)", 0)
                end
            end
        end
        channel_count = channel_count + #names
    end

    if rv['check-full-kit'] then
        local names = {"Hi-Hat", "Hi-tom", "Mid-tom", "Fl-tom"}
        for i = 1, #names do
            local tl = Session:new_audio_track (1, 1, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(0)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if d_g then drum_group:add(track) end
                if perc_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Percussion")) end
                if rv['gates'] then
                    if string.find(track:name(), '-tom') then
                        add_lv2_plugin (track, "XT-TG Tom Gate (Mono)", 0)
                    else
                        add_lv2_plugin (track, "XT-EG Expander Gate (Mono)", 0)
                    end
                end
            end
        end
        channel_count = channel_count + #names
    end

    if rv['check-overkill-kit'] then
        local names = {"Kick Beater", "Snare Btm"}
        for i = 1, #names do
            local tl = Session:new_audio_track (1, 1, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(0)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if d_g then drum_group:add(track) end
                if perc_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Percussion")) end
                if rv['gates'] then
                    if string.find(track:name(), '-tom') then
                        add_lv2_plugin (track, "XT-TG Tom Gate (Mono)", 0)
                    else
                        add_lv2_plugin (track, "XT-EG Expander Gate (Mono)", 0)
                    end
                end
            end
        end
        channel_count = channel_count + #names
    end

    if rv['check-overhead'] then
        local names = { "OH L", "OH R"}
        local pan = {-90, 90}
        local ch = 1
        if rv["stereo-overhead"] then
            ch = 2
        end
        for i = 1, #names do
            local tl = Session:new_audio_track (1, 1, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(0)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                local ctrl = track:pan_azimuth_control()
                if not(ctrl:isnil()) then
                    ctrl:set_value(pan[i], PBD.GroupControlDisposition.NoGroup)
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if d_g then drum_group:add(track) end
                if perc_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Percussion")) end
            end
        end
        channel_count = channel_count + ch
    end


    if rv['check-room'] then
        local names = { "RM L", "RM R"}
        local pan = {-90, 90}
        local ch = 1
        if rv["stereo-room"] then
            ch = 2
        end
        for i = 1, #names do
            local tl = Session:new_audio_track (1, 1, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(0)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                local ctrl = track:pan_azimuth_control()
                if not(ctrl:isnil()) then
                    ctrl:set_value(pan[i], PBD.GroupControlDisposition.NoGroup)
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if d_g then drum_group:add(track) end
                if perc_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Percussion")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-bass'] then
        local names = { rv["name-bass"] }
        local ch = 1
        if rv["stereo-bass"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(1)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if b_g then bass_group:add(track) end
                if bassgtr_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Bass + Gtr")) end
                if rv['char'] then
                    add_lv2_plugin (track, "XT-BC Bass Character (Mono)", 0)
                end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-electric-guitar'] then
        local names = { rv["name-electric-guitar"] }
        local ch = 1
        if rv["stereo-electric-guitar"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(2)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if g_g then guitar_group:add(track) end
                if bassgtr_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Bass + Gtr")) end
                if gtrkey_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Gtr + Keys")) end
                if voxgtr_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Gtrs")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-solo-guitar'] then
        local names = { rv["name-solo-guitar"] }
        local ch = 1
        if rv["stereo-solo-guitar"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(2)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if g_g then guitar_group:add(track) end
                if bassgtr_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Bass + Gtr")) end
                if gtrkey_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Gtr + Keys")) end
                if voxgtr_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Gtrs")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-accoustic-guitar'] then
        local names = { rv["name-accoustic-guitar"] }
        local ch = 1
        if rv["stereo-accoustic-guitar"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(2)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if g_g then guitar_group:add(track) end
                if bassgtr_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Bass + Gtr")) end
                if gtrkey_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Gtr + Keys")) end
                if voxgtr_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Gtrs")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-piano'] then
        local names = { rv["name-piano"] }
        local ch = 1
        if rv["stereo-piano"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(3)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if k_g then key_group:add(track) end
                if voxkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Keys")) end
                if gtrkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Gtr + Keys")) end
            end
        end

        channel_count = channel_count + ch
    end

    if rv['check-electric-piano'] then
        local names = { rv["name-electric-piano"] }
        local ch = 1
        if rv["stereo-electric-piano"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(3)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if k_g then key_group:add(track) end
                if voxkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Keys")) end
                if gtrkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Gtr + Keys")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-organ'] then
        local names = { rv["name-organ"] }
        local ch = 1
        if rv["stereo-organ"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    local ctrl = track:send_enable_controllable(3)
                    if not(ctrl:isnil()) then
                        ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if k_g then key_group:add(track) end
                if voxkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Keys")) end
                if gtrkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Gtr + Keys")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-ldvox'] then
        local names = { rv["name-ldvox"] }
        local ch = 1
        if rv["stereo-ldvox"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track ( ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    for _, ctrl in pairs({
                        track:send_enable_controllable(4),
                        track:send_enable_controllable(5)
                    }) do
                        if not(ctrl:isnil()) then
                            ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                        end
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if v_g then vox_group:add(track) end
                if voxgtr_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Gtrs")) end
                if voxkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Keys")) end
                if rv['char']  then
                    add_lv2_plugin (track, "XT-VC Vocal Character (Mono)", 0)
                end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['check-bgvox'] then
        local names = { "BGV 1", "BGV 2", "BGV 3"  }
        local ch = 1
        if rv["stereo-bgvox"] then ch = 2 end --stereo
        for i = 1, #names do
            local tl = Session:new_audio_track (ch, ch, nil, 1, names[i],  insert_at, ARDOUR.TrackMode.Normal, true)
            for track in tl:iter() do
                if rv['mb'] then
                    for _, ctrl in pairs({
                        track:send_enable_controllable(4),
                        track:send_enable_controllable(5)
                    }) do
                        if not(ctrl:isnil()) then
                            ctrl:set_value(1, PBD.GroupControlDisposition.NoGroup)
                        end
                    end
                end
                if rv['rec'] then track:rec_enable_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
                if v_g then vox_group:add(track) end
                if voxgtr_vca  then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Gtrs")) end
                if voxkey_vca then track:to_slavable():assign (Session:vca_manager():vca_by_name("Vox + Keys")) end
            end
        end
        channel_count = channel_count + ch
    end

    if rv['mb'] then
        for x = 0,7 do
            local bus = Session:get_mixbus(x)
            if x == 0 then -- 0 corresponds to aux 1 (mixbus 1)
                bus:set_name("Drum Bus")
                bus:presentation_info_ptr():set_color(0x425CADff)
            end
            if x == 1 then -- 1 corresponds to aux 2 (mixbus 2)
                bus:set_name("Bass Bus")
                bus:presentation_info_ptr():set_color(0x1AE54Eff)
            end
            if x == 2 then -- 2 corresponds to aux 3 (mixbus 3)
                bus:set_name("Guitar Bus")
                bus:presentation_info_ptr():set_color(0xB475CBff)
            end
            if x == 3 then -- 3 corresponds to aux 4 (mixbus 4)
                bus:set_name("Keys Bus")
                bus:presentation_info_ptr():set_color(0xDA8032ff)
            end
            if x == 4 then -- 4 corresponds to aux 5 (mixbus 5)
                bus:set_name("Vox Bus")
                bus:presentation_info_ptr():set_color(0xC54249ff)
            end
            if x == 5 then -- 5 corresponds to aux 6 (mixbus 6)
                bus:set_name("Short Verb")
                bus:presentation_info_ptr():set_color(0x6D45A3ff)
            end
        end
    end

    if rv['master'] then
        local master = Session:route_by_name ("Master")
        local xt_me = add_lv2_plugin (master, "XT-ME Mastering Equalizer (Stereo)", 0)
        ARDOUR.LuaAPI.set_processor_param (xt_me, 0, 6.0) --Master Trim
        ARDOUR.LuaAPI.set_processor_param (xt_me, 1, 1.0) --Master LPF In
        ARDOUR.LuaAPI.set_processor_param (xt_me, 3, 1.0) --Master HPF In
        local x_eq = add_lv2_plugin (master, "x42-eq - Parametric Equalizer Stereo", 1)
        ARDOUR.LuaAPI.set_processor_param (x_eq, 2, -103.67950439453) --Master Enable
        ARDOUR.LuaAPI.set_processor_param (x_eq, 3, 1.0) --Master Gain
        ARDOUR.LuaAPI.set_processor_param (x_eq, 5, 20.0) --Master Reset Peak Hold
        ARDOUR.LuaAPI.set_processor_param (x_eq, 6, 0.69999998807907) --Master Highpass
        ARDOUR.LuaAPI.set_processor_param (x_eq, 7, 0.0) --Master Highpass Frequency
        ARDOUR.LuaAPI.set_processor_param (x_eq, 8, 20000.0) --Master HighPass Resonance
        ARDOUR.LuaAPI.set_processor_param (x_eq, 9, 1.0) --Master Lowpass
        ARDOUR.LuaAPI.set_processor_param (x_eq, 10, 1.0) --Master Lowpass Frequency
        ARDOUR.LuaAPI.set_processor_param (x_eq, 11, 80.0) --Master LowPass Resonance
        ARDOUR.LuaAPI.set_processor_param (x_eq, 13, 0.0) --Master Lowshelf Frequency
        ARDOUR.LuaAPI.set_processor_param (x_eq, 15, 165.08087158203) --Master Lowshelf Gain
        ARDOUR.LuaAPI.set_processor_param (x_eq, 16, 0.5) --Master Section 1
        ARDOUR.LuaAPI.set_processor_param (x_eq, 17, 0.0) --Master Frequency 1
        ARDOUR.LuaAPI.set_processor_param (x_eq, 18, 1.0) --Master Bandwidth 1
        ARDOUR.LuaAPI.set_processor_param (x_eq, 19, 318.72119140625) --Master Gain 1
        ARDOUR.LuaAPI.set_processor_param (x_eq, 20, 0.60151249170303) --Master Section 2
        ARDOUR.LuaAPI.set_processor_param (x_eq, 21, -3.1999998092651) --Master Frequency 2
        ARDOUR.LuaAPI.set_processor_param (x_eq, 22, 1.0) --Master Bandwidth 2
        ARDOUR.LuaAPI.set_processor_param (x_eq, 23, 1250.0001220703) --Master Gain 2
        ARDOUR.LuaAPI.set_processor_param (x_eq, 24, 0.60151249170303) --Master Section 3
        ARDOUR.LuaAPI.set_processor_param (x_eq, 25, 0.0) --Master Frequency 3
        ARDOUR.LuaAPI.set_processor_param (x_eq, 26, 1.0) --Master Bandwidth 3
        ARDOUR.LuaAPI.set_processor_param (x_eq, 27, 5220.3159179688) --Master Gain 3
        ARDOUR.LuaAPI.set_processor_param (x_eq, 28, 0.60151249170303) --Master Section 4
        ARDOUR.LuaAPI.set_processor_param (x_eq, 29, 3.0) --Master Frequency 4
        ARDOUR.LuaAPI.set_processor_param (x_eq, 30, 1.0) --Master Bandwidth 4
        ARDOUR.LuaAPI.set_processor_param (x_eq, 31, 7999.9995117188) --Master Gain 4
        ARDOUR.LuaAPI.set_processor_param (x_eq, 32, 1.0139591693878) --Master Highshelf
        ARDOUR.LuaAPI.set_processor_param (x_eq, 33, 0.0) --Master Highshelf Frequency
        ARDOUR.LuaAPI.set_processor_param (x_eq, 34, 0.0) --Master Highshelf Bandwidth
        local dyno = add_lv2_plugin (master, "Dyno-Mite (Stereo)", 2)
        ARDOUR.LuaAPI.set_processor_param (dyno, 0, 0.25390625) --Master Sustain
        local xt_mc = add_lv2_plugin (master, "XT-MC Multiband Compressor (Stereo)", 3)
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 10, 3.0) --Master depth1
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 11, 3.0) --Master depth2
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 12, 3.0) --Master depth3
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 17, 100.0) --Master freq0
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 18, 600.0) --Master freq1
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 19, 3600.0) --Master freq2
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 20, 0.7960000038147) --Master attk
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 21, 8.4519996643066) --Master rel
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 22, 14.961000442505) --Master cross
        ARDOUR.LuaAPI.set_processor_param (xt_mc, 23, 2.9880027770996) --Master makup
        local xt_sc = add_lv2_plugin (master, "XT-SC Spectral Compressor (Stereo)", 3)
        ARDOUR.LuaAPI.set_processor_param (xt_sc, 7, -3.2400012016296) --Master Fdr4
        ARDOUR.LuaAPI.set_processor_param (xt_sc, 8, 20.0) --Master Fdr5
        ARDOUR.LuaAPI.set_processor_param (xt_sc, 10, 512.0) --Master Olap
    end

    if rv['mon'] then
        if Session:monitor_out():isnil() then
            ARDOUR.config():set_use_monitor_bus (true)
            Editor:access_action("Common", "ToggleMonitorSection")
        end
    end

    --fit all tracks on the screen
    Editor:access_action("Editor","fit_all_tracks")
    Session:save_state("")

    -- determine the number of channels we can record
    local e = Session:engine()
    local _, t = e:get_backend_ports ("", ARDOUR.DataType("audio"), ARDOUR.PortFlags.IsOutput | ARDOUR.PortFlags.IsPhysical, C.StringVector())  -- from the engine's POV readable/capture ports are "outputs"
    local num_inputs = t[4]:size() -- table 't' holds argument references. t[4] is the C.StringVector (return value)

    if num_inputs < channel_count then
        -- warn the user if there are less physical inputs than created tracks
        LuaDialog.Message ("Session Creation",
            "Created " .. (channel_count - num_inputs) .. " more tracks than there are physical inputs on the soundcard.",
            LuaDialog.MessageType.Info, LuaDialog.ButtonType.Close):run ()
    end
collectgarbage ()
end end
