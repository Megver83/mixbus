ardour {
	["type"] = "EditorAction",
	name = "Drum Replacer",
	author = "Nik",
	description = [[
]]
} 

function factory (unused_params) 
	return function ()

		--Session:save_state("(Backup) Before drum_replace")
		local vamp = ARDOUR.LuaAPI.Vamp("libardourvampplugins:qm-onsetdetector", Session:nominal_frame_rate())
		vamp:plugin ():setParameter ("dftype", 1);
		vamp:plugin ():setParameter ("sensitivity", 40);
		vamp:plugin ():setParameter ("whiten", 1);

		
		local pos1 = Session:transport_frame() --playhead starting locations
		ARDOUR.LuaAPI.usleep(250000)
		Editor:access_action("Editor", "playhead-to-next-region-boundary") -- get the region end frame
		ARDOUR.LuaAPI.usleep(250000)
		local r_end = Session:transport_frame()
		ARDOUR.LuaAPI.usleep(250000)
		Session:request_locate(pos1)
		
		print("Starting:", pos1)
		print("Region end at", r_end)
		
		local lt = false
		
		repeat
			
			local l_frame = Session:transport_frame() -- last frame
			Editor:access_action("Editor", "alternate-tab-to-transient-forwards")
			ARDOUR.LuaAPI.usleep(250000)
			local c_frame = Session:transport_frame() -- new frame
			Editor:access_action("Editor", "select-next-route")
					
			if ( l_frame == c_frame ) or (c_frame >= r_end ) then
				print("Reached last transient at:", c_frame)
				Session:request_locate(pos1)
				lt = true
				else
				--Editor:access_action("Editor", "editor-paste")
			end
			
			Editor:access_action("Editor", "select-prev-route")
			print("Pasting sample at:", c_frame)

		until lt
	
		print(c_frame, l_frame)
	end
end
