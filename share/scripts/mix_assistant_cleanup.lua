ardour {
    ["type"] = "EditorAction",
    name = "Mix Assistant - Cleanup",
    author = "Nikolaus Gullotta",
    description = [[
Reorders tracks (Drums->Bass->Guitars...), groups them by category, assigns colors, performs basic mixing actions,
adds gates and sends to custom busses.

** WARNING ** This is my own template and WILL trash your
current mixing on this session. Save a backup (and a backup of that) or else.
]]
}

function action_params ()
    return
    {
        ["selectionpref"] = { title = "Work on all tracks regardless of user selection? (yes/no)", default = "no"},
        ["deactivationpref"] = { title = "Deactivate gates upon instantiation? (yes/no)", default = "yes"},
        ["deselectionpref"] = { title = "Deselect tracks upon completion? (yes/no)", default = "yes"},
    }
end

function factory (params)
    return function ()
        
        Session:save_state("(Backup) Before mixing_cleanup_template")
        
        --init track tables
        local kick = {}
        local snare = {}
        local hat = {}
        local tom = {}
        local front = {}
        local oh = {}
        local room = {}
        local bass = {}
        local gtr = {}
        local key = {}
        local str = {}
        local vox = {}
        
        --track-name tables
        local kick_n = {"kick"}
        local snare_n = {"snare", "clap"}
        local hat_n = {"hat", "cym"}
        local tom_n = {"tom"}
        local oh_n= {"oh", "ovhd"}
        local front_n = {"front"}
        local room_n = {"room"}
        local bass_n = {"bass"}
        local gtr_n = {"guitar", "gtr", "amp"}
        local key_n = {"key", "piano"}
        local str_n = {"string", "cello", "violin"}
        local vox_n = {"vox", "voc", "harm", "bg"}
        
        local p = params or {}
        local selpref = p["selectionpref"] or "no"
        local deacpref = p["deactivationpref"] or "yes"
        local deselpref = p["deselectionpref"] or "yes"
        
        if selpref ~= "no" then
            Editor:deselect_all()
            Editor:select_all_tracks()
        end
        
        Session:cancel_all_solo()
        local selection = Editor:get_selection()
        for t in selection.tracks:routelist():iter() do
            
            local new_order = ARDOUR.ProcessorList();
            local order = {};
            local unknown = {};
            
            if ( not t:is_monitor() and not t:is_auditioner() ) then    
                t:gain_control():set_value(1, PBD.GroupControlDisposition.NoGroup)
                t:trim_control():set_value(1, PBD.GroupControlDisposition.NoGroup)
                t:mute_control():set_value(0, PBD.GroupControlDisposition.NoGroup)
                
                local w=0
                local i=0
                local proc = t:nth_processor (i)
                repeat
                    if ( proc:display_name () == "PRE" ) then
                        ARDOUR.LuaAPI.reset_processor_to_default(proc)
                        order[0] = proc
                    elseif ( proc:display_name () == "EQ" ) then
                        ARDOUR.LuaAPI.reset_processor_to_default(proc)
                        order[1] = proc
                    elseif  ( proc:display_name () == "Comp" ) then
                        ARDOUR.LuaAPI.reset_processor_to_default (proc)
                        order[2] = proc
                    elseif ( proc:display_name () == "POST" ) then
                        ARDOUR.LuaAPI.reset_processor_to_default (proc)
                        order[3] = proc
                    elseif ( proc:display_name () == "Fader" ) then
                        ARDOUR.LuaAPI.reset_processor_to_default (proc)
                        order[4] = proc
                    else
                        unknown[w] = proc
                    end
                    i = i + 1
                    
                    if unknown[w] then
                        w = w + 1
                    end
                    proc = t:nth_processor (i)
                until proc:isnil()
                
                for x = 0,4 do
                    new_order:push_back(order[x])
                    --print("Proc order:", order[x]:display_name())
                end
                
                for w = 0, w-1 do
                    t:remove_processor(unknown[w], nil, true)
                    --print("About to delete:", unknown[w]:display_name())
                end
                t:reorder_processors(new_order, nil)
            end
        end
        
        for t in selection.tracks:routelist():iter() do
        
            for _, v in pairs(kick_n) do
                if string.find (string.lower(t:name()), v)  then
                    kick[#kick + 1] = t
                    --print("Track:", kick[#kick]:name(), "Matched:", v, "Put in: kick Table")
                end
            end
            
            for _, v in pairs(snare_n) do
                if string.find (string.lower(t:name()), v)  then
                    snare[#snare + 1] = t
                    --print("Track:", snare[#snare]:name(), "Matched:", v, "Put in: snare Table")
                end
            end
            
            for _, v in pairs(hat_n) do
                if string.find (string.lower(t:name()), v)  then
                    hat[#hat + 1] = t
                    --print("Track:", hat[#hat]:name(), "Matched:", v, "Put in: hat Table")
                end
            end
            
            for _, v in pairs(tom_n) do
                if string.find (string.lower(t:name()), v)  then
                    tom[#tom + 1] = t
                    --print("Track:", tom[#tom]:name(), "Matched:", v, "Put in: tom Table")
                end
            end
            
            for _, v in pairs(front_n) do
                if string.find (string.lower(t:name()), v)  then
                    front[#front + 1] = t
                    --print("Track:", front[#front]:name(), "Matched:", v, "Put in: front Table")
                end
            end
            
            for _, v in pairs(oh_n) do
                if string.find (string.lower(t:name()), v)  then
                    oh[#oh + 1] = t
                    --print("Track:", oh[#oh]:name(), "Matched:", v, "Put in: oh Table")
                end
            end
            
            for _, v in pairs(room_n) do
                if string.find (string.lower(t:name()), v)  then
                    room[#room + 1] = t
                    --print("Track:", room[#room]:name(), "Matched:", v, "Put in: room Table")
                end
            end
            
            for _, v in pairs(bass_n) do
                if string.find (string.lower(t:name()), v)  then
                    bass[#bass + 1] = t
                    --print("Track:", bass[#bass]:name(), "Matched:", v, "Put in: bass Table")
                end
            end
            
            for _, v in pairs(gtr_n) do
                if string.find (string.lower(t:name()), v)  then
                    gtr[#gtr + 1] = t
                    --print("Track:", gtr[#gtr]:name(), "Matched:", v, "Put in: gtr Table")
                end
            end
            
            for _, v in pairs(key_n) do
                if string.find (string.lower(t:name()), v)  then
                    key[#key + 1] = t
                    --print("Track:", key[#key]:name(), "Matched:", v, "Put in: key Table")
                end
            end
            
            for _, v in pairs(str_n) do
                if string.find (string.lower(t:name()), v)  then
                    str[#str + 1] = t
                    --print("Track:", str[#str]:name(), "Matched:", v, "Put in: str Table")
                end
            end

            for _, v in pairs(vox_n) do
                if string.find (string.lower(t:name()), v)  then
                    vox[#vox + 1] = t
                    --print("Track:", vox[#vox]:name(), "Matched:", v, "Put in: vox Table")
                end
            end
        end
        
        --helper functions to find processor locations
        function pre_process(track)
            local i = 0
            local proc = track:nth_processor(i)
                repeat
                    if ( proc:display_name() == "PRE" ) then
                        return proc
                    else
                        i = i + 1
                    end
                    proc = track:nth_processor(i)
                until proc:isnil()
            end
            
        function eq_process(track)
            local i = 0
            local proc = track:nth_processor(i)
                repeat
                    if ( proc:display_name() == "EQ" ) then
                        eqpos = i
                        return proc
                    else
                        i = i + 1
                    end
                    proc = track:nth_processor(i)
                until proc:isnil()
            end
            
        function comp_process(track)
            local i = 0
            local proc = track:nth_processor(i)
                repeat
                    if ( proc:display_name() == "Comp" ) then
                        return proc
                    else
                        i = i + 1
                    end
                    proc = track:nth_processor(i)
                until proc:isnil()
            end
            
        function bus_process(track)
            local i = 0
            local proc = track:nth_processor(i)
                repeat
                    if ( proc:display_name() == "POST" ) then
                        return proc
                    else
                        i = i + 1
                    end
                    proc = track:nth_processor(i)
                until proc:isnil()
            end
                

        local drum_g = Session:new_route_group("Drums")
        drum_g:set_color(false)
        local bass_g = Session:new_route_group("Bass")
        bass_g:set_color(false)
        local gtr_g = Session:new_route_group("Guitars")
        gtr_g:set_color(false)
        local key_g = Session:new_route_group("Keys")
        key_g:set_color(false)
        local str_g = Session:new_route_group("Strings")
        str_g:set_color(false)
        local vox_g = Session:new_route_group("Vox")
        vox_g:set_color(false)
        
        
        local i = 1
        for x = 1,#kick do
            drum_g:add(kick[x])
            local eq = eq_process(kick[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 20) -- hp frequency
            local bus = bus_process(kick[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 3, 1) -- aux 1 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 4, 0) -- aux 1 level
            ARDOUR.LuaAPI.set_processor_param (bus, 17, 1) -- aux 8 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 18, -75) -- aux 8 level
            local gate = ARDOUR.LuaAPI.new_plugin(Session, "XT-EG Expander Gate (Mono)", ARDOUR.PluginType.LV2, "")
            ARDOUR.LuaAPI.set_processor_param (gate, 3, -30) -- thresh
            ARDOUR.LuaAPI.set_processor_param (gate, 4, 30) -- depth
            ARDOUR.LuaAPI.set_processor_param (gate, 8, 1.05) -- attack
            ARDOUR.LuaAPI.set_processor_param (gate, 9, 50) -- release
            kick[x]:add_processor_by_index(gate, 1, nil, true)
            kick[x]:set_presentation_order(i)
            kick[x]:presentation_info_ptr():set_color(0x425CADff)
            i = i + 1
            if deacpref ~= "yes" then
                gate:activate()
                else
                gate:deactivate() -- deactivate by default
            end
        end
        
        for x = 1,#snare do
            drum_g:add(snare[x])
            local eq = eq_process(snare[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 60) -- hp frequency
            local bus = bus_process(snare[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 3, 0) -- aux 1 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 4, 0) -- aux 1 level
            ARDOUR.LuaAPI.set_processor_param (bus, 17, 1) -- aux 8 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 18, -75) -- aux 8 level
            ARDOUR.LuaAPI.set_processor_param (bus, 2, 20) -- channel_post_pan (-90/left 90/right) Audience Perspective stuff
            local gate = ARDOUR.LuaAPI.new_plugin(Session, "XT-EG Expander Gate (Mono)", ARDOUR.PluginType.LV2, "")
            ARDOUR.LuaAPI.set_processor_param (gate, 3, -16) -- thresh
            ARDOUR.LuaAPI.set_processor_param (gate, 4, 40) -- depth
            ARDOUR.LuaAPI.set_processor_param (gate, 5, 6) -- gate hysterisis
            ARDOUR.LuaAPI.set_processor_param (gate, 6, 115) -- hold
            ARDOUR.LuaAPI.set_processor_param (gate, 7, 5) -- lookahead
            ARDOUR.LuaAPI.set_processor_param (gate, 8, 3.5) -- attack
            ARDOUR.LuaAPI.set_processor_param (gate, 9, 55) -- release
            snare[x]:set_presentation_order(i)
            snare[x]:presentation_info_ptr():set_color(0x425CADff)
            i = i + 1
            if deacpref ~= "yes" then
                gate:activate()
                else
                gate:deactivate()
            end
        end
        
        for x = 1,#hat do
            drum_g:add(hat[x])
            local eq = eq_process(hat[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 400) -- hp frequency
            local bus = bus_process(hat[x])
            if string.find ( string.lower( hat[x]:name() ), "ride" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -50) -- channel_post_pan (-90/left 90/right) Audience Perspective stuff
            end
            ARDOUR.LuaAPI.set_processor_param (bus, 3, 1) -- aux 1 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 4, 0) -- aux 1 level
            ARDOUR.LuaAPI.set_processor_param (bus, 17, 1) -- aux 8 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 18, -75) -- aux 8 level
            ARDOUR.LuaAPI.set_processor_param (bus, 2, 30) -- channel_post_pan (-90/left 90/right) Audience Perspective stuff
            local gate = ARDOUR.LuaAPI.new_plugin(Session, "XT-EG Expander Gate (Mono)", ARDOUR.PluginType.LV2, "")
            ARDOUR.LuaAPI.set_processor_param (gate, 3, 30) -- thresh
            ARDOUR.LuaAPI.set_processor_param (gate, 4, 38) -- depth
            ARDOUR.LuaAPI.set_processor_param (gate, 6, 42) -- hold
            ARDOUR.LuaAPI.set_processor_param (gate, 8, 0.2) -- attack
            ARDOUR.LuaAPI.set_processor_param (gate, 9, 60) -- release
            hat[x]:add_processor_by_index(gate, 1, nil, true)
            hat[x]:set_presentation_order(i)
            hat[x]:presentation_info_ptr():set_color(0x425CADff)
            i = i + 1
            if deacpref ~= "yes" then
                gate:activate()
                else
                gate:deactivate()
            end
        end

        for x = 1,#tom do
            drum_g:add(tom[x])
            local eq = eq_process(tom[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 60) -- hp frequency
            local bus = bus_process(tom[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 3, 1) -- aux 1 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 4, 0) -- aux 1 level
            ARDOUR.LuaAPI.set_processor_param (bus, 17, 1) -- aux 8 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 18, -75) -- aux 8 level
            if string.find ( string.lower( tom[x]:name() ), "low.tom" ) or string.find ( string.lower( tom[x]:name() ), "lo.tom" ) or string.find ( string.lower( tom[x]:name() ), "l.tom" ) or string.find ( string.lower(tom[x]:name() ), "floor.tom" ) or string.find ( string.lower( tom[x]:name() ), "fl.tom" ) or string.find ( string.lower( tom[x]:name() ), "ride.tom" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -40) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( tom[x]:name() ), "middle.tom" ) or string.find ( string.lower( tom[x]:name() ), "mid.tom" ) or string.find ( string.lower( tom[x]:name() ), "m.tom" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 10) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( tom[x]:name() ), "high.tom" ) or string.find ( string.lower( tom[x]:name() ), "hi.tom" ) or string.find ( string.lower( tom[x]:name() ), "h.tom" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 15) -- channel_post_pan (-90/left 90/right)
            end
            local gate = ARDOUR.LuaAPI.new_plugin(Session, "XT-TG Tom Gate (Mono)", ARDOUR.PluginType.LV2, "")
            -- ARDOUR.LuaAPI.set_processor_param (gate, 0, 1) -- learn tom
            -- ARDOUR.LuaAPI.set_processor_param (gate, 0, 1) -- learn spill
            ARDOUR.LuaAPI.set_processor_param (gate, 2, .25) -- thresh
            ARDOUR.LuaAPI.set_processor_param (gate, 3, 200) -- hold
            ARDOUR.LuaAPI.set_processor_param (gate, 4, 250) -- release
            ARDOUR.LuaAPI.set_processor_param (gate, 5, 30) -- depth
            tom[x]:add_processor_by_index(gate, 1, nil, true)
            tom[x]:set_presentation_order(i)
            tom[x]:presentation_info_ptr():set_color(0x425CADff)
            i = i + 1
            if deacpref ~= "yes" then
                gate:activate()
                else
                gate:deactivate()
            end
        end
        
        for x = 1,#oh do
            drum_g:add(oh[x])
            local eq = eq_process(oh[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 500) -- hp frequency
            local bus = bus_process(oh[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 3, 1) -- aux 1 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 4, 0) -- aux 1 level
            ARDOUR.LuaAPI.set_processor_param (bus, 17, 1) -- aux 8 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 18, -75) -- aux 8 level
            oh[x]:set_presentation_order(i)
            oh[x]:presentation_info_ptr():set_color(0x425CADff)
            i = i + 1
            if string.find ( string.lower( oh[x]:name() ), "overhead.l" ) or string.find ( string.lower(oh[x]:name() ), "l.overhead" ) or string.find ( string.lower(oh[x]:name() ), "front.l" ) or string.find ( string.lower(oh[x]:name() ), "l.front" ) or string.find ( string.lower(oh[x]:name() ), "oh.l" ) or string.find ( string.lower(oh[x]:name() ), "ohl" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -90) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( oh[x]:name() ), "overhead.r" ) or string.find ( string.lower(oh[x]:name() ), "r.overhead" ) or string.find ( string.lower(oh[x]:name() ), "front.r" ) or string.find ( string.lower(oh[x]:name() ), "r.front" ) or string.find ( string.lower(oh[x]:name() ), "oh.r" ) or string.find ( string.lower(oh[x]:name() ), "ohr" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 90) -- channel_post_pan (-90/left 90/right)
            end
        end
        
        for x = 1,#room do
            local bus = bus_process(room[x])
            local comp = comp_process(room[x])
            ARDOUR.LuaAPI.set_processor_param (comp, 1, 1)  --enable
            ARDOUR.LuaAPI.set_processor_param (comp, 2, -28)  --thresh
            ARDOUR.LuaAPI.set_processor_param (comp, 3, 15)  --speed
            ARDOUR.LuaAPI.set_processor_param (comp, 4, 1)  --mode
            ARDOUR.LuaAPI.set_processor_param (comp, 5, 0)  --makeup
            ARDOUR.LuaAPI.set_processor_param (comp, 6, 10)  --redux
            room[x]:gain_control():set_value(.125, PBD.GroupControlDisposition.NoGroup) -- lower fader by 12 db
            room[x]:set_presentation_order(i)
            room[x]:presentation_info_ptr():set_color(0x425CADff)
            i = i + 1
            if string.find ( string.lower( room[x]:name() ), "room.l" ) or string.find ( string.lower(room[x]:name() ), "left.room" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -90) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( room[x]:name() ), "room.r" ) or string.find ( string.lower(room[x]:name() ), "right.room" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 90) -- channel_post_pan (-90/left 90/right)
            end
        end
        
        for x = 1,#bass do
            bass_g:add(bass[x])
            local eq = eq_process(bass[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 40) -- hp frequency
            local comp = comp_process(bass[x])
            ARDOUR.LuaAPI.set_processor_param (comp, 1, 1)  --enable
            ARDOUR.LuaAPI.set_processor_param (comp, 2, -28)  --thresh
            ARDOUR.LuaAPI.set_processor_param (comp, 3, 15)  --speed
            ARDOUR.LuaAPI.set_processor_param (comp, 4, 1)  --mode
            ARDOUR.LuaAPI.set_processor_param (comp, 5, 3)  --makeup
            local bus = bus_process(bass[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 5, 1) -- aux 2 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 6, 0) -- aux 2 level
            bass[x]:set_presentation_order(i)
            bass[x]:presentation_info_ptr():set_color(0x1AE54Eff)
            i = i + 1
        end
        
        for x = 1,#gtr do
            gtr_g:add(gtr[x])
            local eq = eq_process(gtr[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 120) -- hp frequency
            ARDOUR.LuaAPI.set_processor_param (eq, 5, 2500)  --mid freq
            ARDOUR.LuaAPI.set_processor_param (eq, 6, -3)  --gain
            local bus = bus_process(gtr[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 7, 1) -- aux 3 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 8, 0) -- aux 3 level
            gtr[x]:set_presentation_order(i)
            gtr[x]:presentation_info_ptr():set_color(0xB475CBff)
            i = i + 1
            if string.find ( string.lower( gtr[x]:name() ), "accoustic.guitar.l" ) or string.find ( string.lower( gtr[x]:name() ), "accoustic.gtr.l" ) or string.find ( string.lower( gtr[x]:name() ), "acc.gtr.l" ) or string.find ( string.lower( gtr[x]:name() ), "gtr.l" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -90) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( gtr[x]:name() ), "accoustic.guitar.r" ) or string.find ( string.lower( gtr[x]:name() ), "accoustic.gtr.r" ) or string.find ( string.lower( gtr[x]:name() ), "acc.gtr.r" ) or string.find ( string.lower( gtr[x]:name() ), "gtr.r" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 90) -- channel_post_pan (-90/left 90/right)
            end
        end
        
        for x = 1,#key do
            key_g:add(key[x])
            local eq = eq_process(key[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 63) -- hp frequency
            ARDOUR.LuaAPI.set_processor_param (eq, 7, 400)  --low freq
            ARDOUR.LuaAPI.set_processor_param (eq, 8, -3)  --gain
            local bus = bus_process(key[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 9, 1) -- aux 4 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 10, 0) -- aux 4 level
            key[x]:set_presentation_order(i)
            key[x]:presentation_info_ptr():set_color(0xDA8032ff)
            i = i + 1
            if string.find ( string.lower( key[x]:name() ), "keys.l" ) or string.find ( string.lower( key[x]:name() ), "key.l" ) or string.find ( string.lower( key[x]:name() ), "l.key" ) or string.find ( string.lower( key[x]:name() ), "piano.l" ) or string.find ( string.lower( key[x]:name() ), "l.piano" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -90) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( key[x]:name() ), "keys.r" ) or string.find ( string.lower( key[x]:name() ), "key.r" ) or string.find ( string.lower( key[x]:name() ), "r.key" ) or string.find ( string.lower( key[x]:name() ), "piano.r" ) or string.find ( string.lower( key[x]:name() ), "r.piano" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 90) -- channel_post_pan (-90/left 90/right)
            end
        end
        
        for x = 1,#str do
            str_g:add(str[x])
            local eq = eq_process(str[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 100) -- hp frequency
            ARDOUR.LuaAPI.set_processor_param (eq, 3, 7900)  --hi freq
            ARDOUR.LuaAPI.set_processor_param (eq, 4, 3)  --gain
            local bus = bus_process(str[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 9, 1) -- aux 4 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 10, 0) -- aux 4 level
            str[x]:set_presentation_order(i)
            str[x]:presentation_info_ptr():set_color(0x368D91ff)
            i = i + 1
            if string.find ( string.lower( str[x]:name() ), "strings.l" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, -90) -- channel_post_pan (-90/left 90/right)
            end
            if string.find ( string.lower( str[x]:name() ), "strings.r" ) then
                ARDOUR.LuaAPI.set_processor_param (bus, 2, 90) -- channel_post_pan (-90/left 90/right)
            end
        end
        
        for x = 1,#vox do
            vox_g:add(vox[x])
            local eq = eq_process(vox[x])
            ARDOUR.LuaAPI.set_processor_param (eq, 1, 1) -- enable
            ARDOUR.LuaAPI.set_processor_param (eq, 2, 200) -- hp frequency
            ARDOUR.LuaAPI.set_processor_param (eq, 5, 2500)  --mid freq
            ARDOUR.LuaAPI.set_processor_param (eq, 6, 3)  --gain
            local bus = bus_process(vox[x])
            ARDOUR.LuaAPI.set_processor_param (bus, 11, 1) -- aux 5 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 12, 0) -- aux 5 level
            ARDOUR.LuaAPI.set_processor_param (bus, 13, 1) -- aux 6 assign
            ARDOUR.LuaAPI.set_processor_param (bus, 14, 0) -- aux 6 level
            vox[x]:set_presentation_order(i)
            vox[x]:presentation_info_ptr():set_color(0xC54249ff)
            i = i + 1
        end

        for x = 0,7 do
            local bus = Session:get_mixbus(x)
            if x == 0 then -- 0 corresponds to aux 1 (mixbus 1)
                bus:set_name("Drum Bus")
                bus:presentation_info_ptr():set_color(0x425CADff)
            end
            if x == 1 then -- 1 corresponds to aux 2 (mixbus 2)
                bus:set_name("Bass Bus")
                bus:presentation_info_ptr():set_color(0x1AE54Eff)
            end
            if x == 2 then -- 2 corresponds to aux 3 (mixbus 3)
                bus:set_name("Guitar Bus")
                bus:presentation_info_ptr():set_color(0xB475CBff)
            end
            if x == 3 then -- 3 corresponds to aux 4 (mixbus 4)
                bus:set_name("Keys Bus")
                bus:presentation_info_ptr():set_color(0xDA8032ff)
            end
            if x == 4 then -- 4 corresponds to aux 5 (mixbus 5)
                bus:set_name("Vox Bus")
                bus:presentation_info_ptr():set_color(0xC54249ff)
            end
            if x == 5 then -- 5 corresponds to aux 6 (mixbus 6)
                bus:set_name("Short Verb")
                bus:presentation_info_ptr():set_color(0x6D45A3ff)
            end
            if x == 6 then -- 6 corresponds to aux 7 (mixbus 7)
                bus:set_name("Long Verb")
                bus:presentation_info_ptr():set_color(0xF05858ff)
            end
            if x == 7 then -- 8 corresponds to aux 8 (mixbus 8)
                bus:set_name("LA Comp")
                bus:presentation_info_ptr():set_color(0xFFBD87ff)
            end
        end
        
        if deselpref ~= "no" then
            Editor:deselect_all()
        end
    end
end