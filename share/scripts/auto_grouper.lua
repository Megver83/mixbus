ardour {
	["type"]    = "EditorHook",
	name        = "Auto Grouper",
	author = "Nikolaus Gullotta",
	description = [[Groups new tracks categorically]]
}

function signals ()
	local s = LuaSignal.Set()
	s:add (
		{
			[LuaSignal.RouteAdded] = true,
		}
	)
	return s
end

function factory (params)
	return function (signal, ref, ...)
		
		local drum = {"kick", "snare", "hat", "tom", "oh", "ovhd"}
		local bass = {"bass"}
		local gtr  = {"guitar", "gtr", "git", "solo"}
		local key  = {"key", "piano"}
		local vox  = {"vox", "vocal"}
		
		--http://manual.ardour.org/lua-scripting/class_reference/#ARDOUR:RouteGroup
		local drum_g = Session:new_route_group("Drums")
		drum_g:set_color(false)
		local bass_g = Session:new_route_group("Basses")
		bass_g:set_color(false)
		local gtr_g  = Session:new_route_group("Guitars")
		gtr_g:set_color(false)
		local key_g  = Session:new_route_group("Keys")
		key_g:set_color(false)
		local vox_g  = Session:new_route_group("Vox")
		vox_g:set_color(false)
		
		assert(signal == LuaSignal.RouteAdded)
		local added_routes  = ...
		
		for t in added_routes:iter() do 
			--print(t:name())
			for _, v in pairs(drum) do
				if string.find (string.lower(t:name()), v)  then
					drum_g:add(t)
					--print("Track:", t:name(), "Matched:", v)
				end
			end
			
			for _, v in pairs(bass) do
				if string.find (string.lower(t:name()), v)  then
					bass_g:add(t)
					--print("Track:", t:name(), "Matched:", v)
				end
			end
			
			for _, v in pairs(gtr) do
				if string.find (string.lower(t:name()), v)  then
					gtr_g:add(t)
					--print("Track:", t:name(), "Matched:", v)
				end
			end
			
			for _, v in pairs(key) do
				if string.find (string.lower(t:name()), v)  then
					key_g:add(t)
					--print("Track:", t:name(), "Matched:", v)
				end
			end
			
			for _, v in pairs(vox) do
				if string.find (string.lower(t:name()), v)  then
					vox_g:add(t)
					--print("Track:", t:name(), "Matched:", v)
				end
			end
		end
	end
end
