ardour {
	["type"] = "EditorAction",
	name = "Automation Done Quick",
	author = "Nikolaus Gullotta",
	description = [[For the selected track show automation for a number of common parameters and switches them to a writeable state.
	Playing engages after a user-defined variable number of seconds (1 second = 1,000,000 μSeconds)
]]
}
-- todo https://github.com/Ardour/ardour/blob/f7b005ebf10982c4e3c9f3462e2b4d5b5e99c0fb/gtk2_ardour/automation_time_axis.cc
-- want to spill automation regions on demand
function action_params ()
	return
	{
		["sleep"]   = { title = "How many seconds before playing begins? (default 1 second)", default = "1"},
		["param1"]   = { title = "Spill post-pan automation? (yes,no)", default = "no"},
		["param2"]   = { title = "Spill fader automation? (yes,no)", default = "no"},
		["param3"]   = { title = "Spill mute automation? (yes,no)", default = "no"},
		["param4"]   = { title = "Spill trim automation? (yes,no)", default = "no"},
		["returnparam"]   = { title = "Return playhead to original position before the script ran? (yes,no)", default = "no"},
	}
end

function factory (params)
	return function ()
		
		Session:save_state("(Backup) Before Quick Automation")
		
		local playhead = Session:transport_frame ()
		local p = params or {}
		local sleep = p["sleep"] or "1"
		local param1 = p["param1"] or "no"
		local param2 = p["param2"] or "no"
		local param3 = p["param3"] or "no"
		local param4 = p["param4"] or "no"
		local returnparam = p["returnparam"] or "no"
		local usleep = sleep * 1000000 --conversion to seconds

		if param1 ~= "no" then
			auto_pan = true
			else
			auto_pan = false
		end

		if param2 ~= "no" then
			auto_fader = true
			else
			auto_fader = false
		end
		
		if param3 ~= "no" then
			auto_mute = true
			else
			auto_mute = false
		end
		
		if param4 ~= "no" then
			auto_trim = true
			else
			auto_trim = false
		end
	
		if returnparam ~= "no" then
			Session:cfg():set_auto_return(true)
		end
		
		
		
		local selection = Editor:get_selection()
	
		
		for t in selection.tracks:routelist():iter() do
			
			if auto_pan == true then
				t:pan_azimuth_control():set_automation_state(ARDOUR.AutoState.Write) --change write to touch, latch, or manual depending
			end
			
			if auto_fader == true then
				t:gain_control():set_automation_state(ARDOUR.AutoState.Write)
			end
			
			if auto_mute == true then
				t:mute_control():set_automation_state(ARDOUR.AutoState.Write)
			end
			
			if auto_trim == true then
				t:trim_control ():set_automation_state(ARDOUR.AutoState.Write)
			end
		
		end -- for selection
	
	ARDOUR.LuaAPI.usleep(usleep)
	Editor:play_with_preroll()
	
	end  -- return function

end --factory
