ardour {
    ["type"] = "EditorAction",
    name = "Session Organizer",
    author = "Nik Gullotta, Owen Bollig, Ben Loftis",
    description = [[Quick session organizer. Offers the ability to change Track Names, Groups, Comments & Colors]]
}

function factory (params) return function ()

   function get_current_group ( in_track, preferred_group )
          for group in Session:route_groups():iter() do
   			for route in group:route_list():iter() do
   				if route:name() == in_track:name() then
   					return group:name()
   				end
   			end
   		end
       return preferred_group
   end

   --track reference tables
   local kick, snare, hat, tom, front, oh, room, bass, gtr, key, str, vox = {}

   --track-name tables
  local kick_n = {"kick", "kik"}
  local snare_n = {"snare", "clap"}
  local hat_n = {"hat", "cym"}
  local tom_n = {"tom"}
  local oh_n= {"oh", "ovhd"}
  local front_n = {"front"}
  local room_n = {"room"}
  local bass_n = {"bass"}
  local gtr_n = {"guitar", "gtr", "amp"}
  local key_n = {"key", "piano"}
  local str_n = {"string", "cello", "violin"}
  local vox_n = {"vox", "voc", "harm", "bg"}

	--now  starting to build our dialog
   local dialog_options = {
      { type = "heading", title = "NAME", col=0, colspan = 1  },
      { type = "heading", title = "GROUP", col=1, colspan = 1  },
      { type = "heading", title = "COLOR", col=2, colspan = 1},
      { type = "heading", title = "COMMENT", col=3, colspan = 1}
   }

	local j = 0
	function build_group_menu ( some_track, some_group, some_color )
		
		table.insert(dialog_options,
			{ type = "entry", key = "track_name " .. j,   default = some_track:name(), title = "", col=0 })
		
		table.insert(dialog_options,
			{type = "dropdown", key = "track_group " .. j, title = "", col=1, values =
				{
					[some_group] = some_group , [" - - "] = "None", ["Drums"] = "Drums", ["Bass"] = "Bass", ["Guitars"] = "Guitars", ["Keys"] = "Keys", ["Strings"] = "Strings", ["Vox"] = "Vox"
				},
			default = some_group
			})
		
		table.insert(dialog_options,
			{type = "dropdown", key = "track_color " .. j, title = "", col=2, values =
				{
					["Orange"] = "0xA68C4FFF", ["Yellow"] = "0xA5AE42FF", ["Green"] = "0x3F6D4CFF", ["Blue"] = "0x4278AEFF", ["Purple"] = "0x745692FF", ["Pink"] = "0xC76D63FF"
				},
			default = "Orange"  --Green{["Current"] = some_track:presentation_info_ptr():color()}
			})
		
		table.insert(dialog_options,
			{ type = "entry", key = "comment " .. j,   default = some_track:comment(), title = "", col=3 } )
		
		j = j + 1
	end

	function group_color_name ( group )
		local group_color_name = ""
		if (group:rgba() == 1064127743) then
			group_color_name = "Green"

		elseif (group:rgba() == 2794213375) then
			group_color_name = "Orange"

		elseif (group:rgba() == 2488286719) then
			group_color_name = "Red"

		elseif (group:rgba() == 2779661055) then
			group_color_name = "Yellow"

		elseif (group:rgba() == 1115205375) then
			group_color_name = "Blue"

		elseif (group:rgba() == 1951830783) then
			group_color_name = "Purple"

		elseif (group:rgba() == 3345835007) then
			group_color_name = "Pink"
		else
			group_color_name = " - - "
		end
		return group_color_name
	end
		
	for t in Session:get_tracks():iter() do
			local found = false
				for _, v in pairs(kick_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", kick[#kick]:name(), "Matched:", v, "Put in: kick Table")
					end
				end

				for _, v in pairs(snare_n) do
					if string.find (string.lower(t:name()), v)  then
					   local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
					   found = true
						--print("Track:", snare[#snare]:name(), "Matched:", v, "Put in: snare Table")
					end
				end

				for _, v in pairs(hat_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", hat[#hat]:name(), "Matched:", v, "Put in: hat Table")
					end

				end

				for _, v in pairs(tom_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", tom[#tom]:name(), "Matched:", v, "Put in: tom Table")
					end
				end

				for _, v in pairs(front_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", front[#front]:name(), "Matched:", v, "Put in: front Table")
					end
				end

				for _, v in pairs(oh_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", oh[#oh]:name(), "Matched:", v, "Put in: oh Table")
					end
				end

				for _, v in pairs(room_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Drums")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", room[#room]:name(), "Matched:", v, "Put in: room Table")
					end
				end

				for _, v in pairs(bass_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Bass")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", bass[#bass]:name(), "Matched:", v, "Put in: bass Table")
					end

				end

				for _, v in pairs(gtr_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Guitars")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", gtr[#gtr]:name(), "Matched:", v, "Put in: gtr Table")
					end
				end

				for _, v in pairs(key_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Keys")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", key[#key]:name(), "Matched:", v, "Put in: key Table")
					end
				end

				for _, v in pairs(str_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Strings")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", str[#str]:name(), "Matched:", v, "Put in: str Table")
					end
				end

				for _, v in pairs(vox_n) do
					if string.find (string.lower(t:name()), v)  then
						local preferred_group = get_current_group(t, "Vox")
						build_group_menu( t, preferred_group )
						found = true
						--print("Track:", vox[#vox]:name(), "Matched:", v, "Put in: vox Table")
					end
				end

				if not found then
					local preferred_group = get_current_group(t, " - - ")
					build_group_menu( t, preferred_group )
				end
        end

		local dlg = LuaDialog.Dialog ("Template Setup", dialog_options)
		local rv = dlg:run()
		if (not rv) then
			return
		end

	local drum = {}
	local drum_group = Session:new_route_group("Drums")
	local bass = {}
	local bass_group = Session:new_route_group("Bass")
	local guitars = {}
	local guitars_group = Session:new_route_group("Guitars")
	local keys = {}
	local keys_group = Session:new_route_group("Keys")
	local strings = {}
	local strings_group = Session:new_route_group("Strings")
	local vox = {}
	local vox_group = Session:new_route_group("Vox")

	local d = 0
	for t in Session:get_tracks():iter() do

		sel_group = rv['track_group ' .. d]

		t:set_name(rv['track_name ' .. d])

		clr = rv['track_color ' .. d]
		if ( clr > 0 ) then
			t:presentation_info_ptr():set_color(rv['track_color ' .. d])
		end
		
		t:set_comment(rv['comment ' .. d], nil)

		--build_group_menu( t, "track_group " .. d )
		--print(rv['track_group ' .. d])
		if not (sel_group ==  " - - ") then
			if sel_group == "Drums" then
				drum_group:add(t)
				drum[#drum + 1] = t

			elseif sel_group == "Bass" then
				bass_group:add(t)
				bass[#bass + 1] = t

			elseif sel_group == "Guitars" then
				guitars_group:add(t)
				guitars[#guitars + 1] = t

			elseif sel_group == "Keys" then
				keys_group:add(t)
				keys[#keys + 1] = t

			elseif sel_group == "Strings" then
				strings_group:add(t)
				strings[#strings + 1] = t

			elseif sel_group == "Vox" then
				vox_group:add(t)
				vox[#vox + 1] = t
			end
		end
		d = d + 1
   end

 
end end
