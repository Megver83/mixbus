ardour {
	["type"] = "EditorAction",
	name = "Track Stain",
	author = "Nik",
	description = [[Group, color organize]]
}

function factory (unused_params) 
	return function ()
	
		local drum = {}
		local bass = {}
		local gtr = {}
		local key = {}
		local vox = {} 
		
		local drum_n = {"kick", "snare", "hat", "tom", "front", "oh", "room"}
		local bass_n = {"bass"}
		local gtr_n = {"guitar", "gtr"}
		local key_n = {"key", "piano"}
		local vox_n = {"vox", "vocal", "harm"}
		
		for t in Session:get_routes():iter() do
			for _, v in pairs(drum_n) do
				if string.find (string.lower(t:name()), v)  then
					drum[#drum + 1] = t
					--print("Track:", drum[#drum]:name(), "Matched:", v, "Put in: drum Table")
				end
			end
			
			for _, v in pairs(bass_n) do
				if string.find (string.lower(t:name()), v)  then
					bass[#bass + 1] = t
					--print("Track:", bass[#bass]:name(), "Matched:", v, "Put in: bass Table")
				end
			end
			
			for _, v in pairs(gtr_n) do
				if string.find (string.lower(t:name()), v)  then
					gtr[#gtr + 1] = t
					--print("Track:", gtr[#gtr]:name(), "Matched:", v, "Put in: gtr Table")
				end
			end
			
			for _, v in pairs(key_n) do
				if string.find (string.lower(t:name()), v)  then
					key[#key + 1] = t
					--print("Track:", key[#key]:name(), "Matched:", v, "Put in: key Table")
				end
			end
			
			for _, v in pairs(vox_n) do
				if string.find (string.lower(t:name()), v)  then
					vox[#vox + 1] = t
					--print("Track:", vox[#vox]:name(), "Matched:", v, "Put in: vox Table")
				end
			end
		end
		
		local i = 0
		for l = 1, #drum do
			drum[l]:set_presentation_order(i)
			drum[l]:presentation_info_ptr():set_color(0x425CADff)
			i = i + 1
		end
		
		for l = 1, #bass do
			bass[l]:set_presentation_order(i)
			bass[l]:presentation_info_ptr():set_color(0x1AE54Eff)
			i = i + 1
		end
		
		for l = 1, #gtr do
			gtr[l]:set_presentation_order(i)
			gtr[l]:presentation_info_ptr():set_color(0xB475CBff)
			i = i + 1
		end
		
		for l = 1, #key do
			key[l]:set_presentation_order(i)
			key[l]:presentation_info_ptr():set_color(0xDA8032ff)
			i = i + 1
		end
		
		for l = 1, #vox do
			vox[l]:set_presentation_order(i)
			vox[l]:presentation_info_ptr():set_color(0xC54249ff)
			i = i + 1
		end
	end
end
