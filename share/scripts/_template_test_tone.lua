ardour {
	["type"]    = "EditorAction",
	name        = "Test Tones",
	description = [[Add as many mono audio tracks to the new session as there are physical I/O ports.
	Then, add a noise generator, sine wave generator, or both, to the track.]]
}

function session_setup ()
	return true
end

function route_setup ()
	return
	{
		['Insert_at'] = ARDOUR.PresentationInfo.max_order
	}
end

function factory (params) return function ()

	local e = Session:engine()
	-- from the engine's POV readable/capture ports are "outputs"
	local _, t = e:get_backend_ports ("", ARDOUR.DataType("audio"), ARDOUR.PortFlags.IsOutput | ARDOUR.PortFlags.IsPhysical, C.StringVector())
	-- table 't' holds argument references. t[4] is the C.StringVector (return value)
	local tracks = t[4]:size();

	local dialog_options = {
		{ type = "heading",  title = "Customize Session: " .. Session:name () },
		{ type = "number",   key = "tracks", title = "Create Tracks",  min = 1, max = 128, step = 1, digits = 0, default = tracks },
		{
			type = "radio", key = "select", title = "RadioBtn", values =
			{
				["Sine Wave Generator"] = 1, ["Noise Generator"] = 2
			},
			default = "Noise Generator"
		},
		{ type = "checkbox", key = "mutepref", default = true, title = "Mute tracks? (Warning: disabling this could be VERY loud)" },
		{ type = "checkbox", key = "deacpref", default = false, title = "Deactivate generators upon instantiation?" },
	}

	local dlg = LuaDialog.Dialog ("Template Setup", dialog_options)
	local rv = dlg:run()
	if (not rv or rv['tracks'] == 0) then
		return
	end

	-- create tracks
	local tl = Session:new_audio_track (1, 2, nil, rv['tracks'], "",  ARDOUR.PresentationInfo.max_order, ARDOUR.TrackMode.Normal)
	local g = Session:new_route_group("TEST")
  	g:set_active(true, nil)
	g:set_rgba(0x425CADff)
  	g:set_monitoring(false)

	-- and optionally add generators/mute tracks
	if rv['select'] == 2 then
		for track in tl:iter() do
			g:add(track)
			if rv['mutepref'] then track:mute_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
			local a = ARDOUR.LuaAPI.new_luaproc(Session, "NoiseGen");
			if (not a:isnil()) then
				--print("Adding: ", a:display_name(), " to track:", i) --debug
				track:add_processor_by_index(a, 0, nil, true)
				if rv['deacpref'] then a:deactivate() end
				a = nil -- explicitly drop shared-ptr reference
			end
		end
	end

	if rv['select'] == 1 then
		for track in tl:iter() do
			g:add(track)
			if rv['mutepref'] then track:mute_control ():set_value (1, PBD.GroupControlDisposition.NoGroup) end
			local a = ARDOUR.LuaAPI.new_luaproc(Session, "SinGen");
			if (not a:isnil()) then
				--print("Adding: ", a:display_name(), " to track:", i) --debug
				track:add_processor_by_index(a, 0, nil, true)
				if rv['deacpref'] then a:deactivate() end
				a = nil -- explicitly drop shared-ptr reference
			end
		end
	end

	Session:save_state("");
end end
