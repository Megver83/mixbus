ardour {
	["type"] = "EditorAction",
	name = "Batch Rename",
	author = "Nikolaus Gullotta",
	license = "MIT",
	description = [[Removes user-defined text from track names, and replaces them (if applicable).]]
} 

function factory (params) 
	return function ()
		local dialog_options = {
			{ type = "entry", key = "text", title = "Text to be removed", default = "Audio" },
			{ type = "entry", key = "replacement", title = "Replacement text (if any)", default = "" },
		}
		local rv = LuaDialog.Dialog ("Batch Rename Tracks", dialog_options):run ()
		if not rv then return end -- cancel

		Session:save_state("(Backup) Before batch_rename")
		
		local text = rv["text"] or nil
		local repl = rv["replacement"] or ""
		
		local digest = string.gsub(text, "(%W)","%%%1") -- tags characters that are not alphanumeric for gsub to handle later
		
		for t in Session:get_tracks():iter() do
            local name = t:name()
            local new_name = string.gsub(name, digest, repl)
            t:set_name(new_name)
        end -- end for all tracks
	
	end -- end function

end --end factory
