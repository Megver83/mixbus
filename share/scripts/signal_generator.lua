ardour {
	["type"]    = "dsp",
	name        = "Signal Generator",
	category    = "Instrument",
	license     = "MIT",
	author      = "Christopher Hickman",
	description = [[Lua Signal Generator - v1.0]]
}

--@ToDo: Cleanup wave functions to work under one function
--@ToDo: Cleanup var names
--
--------------------------------------------------------------------------------
-- XXX THIS SCRIPT IS INCLUDES SOME BEGINNER DSP MISTAKES
-- DO NOT COPY ANY OF THIS CODE AS EXAMPLE.
-- (robin)
--------------------------------------------------------------------------------
--
local phase, sweepFreq = 0, 0
local logIndex = 0
local twoPI = (2*math.pi)
local r = 0 -- TODO cleanup, overloaded sample-rate
local txt = nil
local f_old, a_old, signal_old, toggle_old = 0, 0, 0, 0
local pulse = 1
local draw = 0

local gen_white, gen_pink, gen_gauss;

function dsp_init (rate)
	self:shmem():allocate(1)
	r = rate
	gen_white = ARDOUR.DSP.Generator() gen_white:set_type (ARDOUR.DSP.NoiseType.UniformWhiteNoise)
	gen_pink = ARDOUR.DSP.Generator()  gen_pink:set_type (ARDOUR.DSP.NoiseType.PinkNoise)
	gen_gauss = ARDOUR.DSP.Generator() gen_gauss:set_type (ARDOUR.DSP.NoiseType.GaussianWhiteNoise)
end

function map_signal_type(enum)
	if enum == 0 then return "Sine Wave"            end
	if enum == 1 then return "Uniform"              end
	if enum == 2 then return "Gaussian"             end
	if enum == 3 then return "Pink"                 end
	if enum == 4 then return "Impulse Train"        end
	if enum == 5 then return "Square Wave"          end
	if enum == 6 then return "Triangle Wave"        end
	if enum == 7 then return "Sawtooth Wave"        end
	return "ERR"
end

function map_pattern_type(enum)
	if enum == 0 then return "Continuous" end
	if enum == 1 then return "Sweep"      end
	if enum == 2 then return "Log Sweep"  end
	return "ERR"
end

function generate_sine_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
	local output = {}
	local amp = amp_coeff or 1
	local inc = (freq/samplerate)
	if pattern == 0 then
		for sample = 1, n_samples do
			output[sample] = amp * math.sin(twoPI * phase)
			phase = phase + inc -- increment phase
			if phase >= 1 then phase = phase - 1 end
		end return output
	end
	if pattern == 1 then
		if sweepFreq <= 0 then -- initialize sweepFreq
			sweepFreq = freq
		end
		local p_inc = 2 * math.pi * (sweepFreq / samplerate) -- calculate initial phase increment value
		local f_inc = (freq_end - freq)/ (samplerate * period) -- calculate frequency increment value
		for s = 1, n_samples do --fill table with fragments of a sweeping sine wave
			output[s] = amp * math.sin(phase)

			phase = phase + p_inc
			if phase > (2*math.pi) then phase = phase - (2*math.pi) end

			sweepFreq = sweepFreq + f_inc -- increment frequency

			if (freq_end >= freq)
			then
				if (sweepFreq >= freq_end) then sweepFreq = freq end -- once ending freq is reached reset to starting freq
			end

			if (freq_end < freq) then -- if this is the case sweep will proceed from high to low freq
				if (sweepFreq <= freq_end) then sweepFreq = freq end
			end
			p_inc = 2 * math.pi * (sweepFreq / samplerate)	-- recalculate phase increment for newly incremented frequency
		end
		return output
	end
	if pattern == 2 then
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		local w0 = 2*math.pi*f0
		local w1 = 2*math.pi*f1

		local v1 = math.log(f1/f0)

		for s = 1, n_samples do
			t = logIndex/(r * T)
			v2 = math.exp(t * v1) - 1

			output[s] = amp * math.sin((w0 * (r * period) * v2)/(r * v1))

			logIndex = logIndex + 1
			if t > 1 then logIndex = 1 end
		end
		return output
	end
end

function generate_square_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
	local out = {}
	local amp = amp_coeff or 1
	local inc = freq/samplerate
	if pattern == 0 then
		for smpl = 1, n_samples do
			phase = phase + inc

			local val = math.sin(phase * twoPI)

			if val >= 0 then
				out[smpl] = amp * 1
			else
				out[smpl] = amp * -1
			end
		end return out
	elseif pattern == 1 then
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		if sweepFreq <= 0 then -- initialize sweepFreq
			sweepFreq = freq
		end
		local p_inc = twoPI * (sweepFreq / r) -- calculate initial phase increment value
		local f_inc = (f1 - f0)/ (r * T) -- calculate frequency increment value
		for smpl = 1, n_samples do --fill table with fragments of a sweeping sine wave
			local val = math.sin(phase)

			if val >= 0 then
				out[smpl] = amp * 1
			else
				out[smpl] = amp * -1
			end

			phase = phase + p_inc
			if phase > twoPI then phase = phase - twoPI end

			sweepFreq = sweepFreq + f_inc -- increment frequency

			if (f1 >= f0)
			then
				if (sweepFreq >= f1) then sweepFreq = f0 end -- once ending freq is reached reset to starting freq
			end

			if (freq_end < f0) then -- if this is the case sweep will proceed from high to low freq
				if (sweepFreq <= f1) then sweepFreq = f0 end
			end
			p_inc = twoPI * (sweepFreq / r)	-- recalculate phase increment for newly incremented frequency
		end return out
	else -- pattern == 2
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		local w0 = 2*math.pi*f0
		local w1 = 2*math.pi*f1

		local v1 = math.log(f1/f0)

		for smpl = 1, n_samples do
			t = logIndex/(r * T)
			v2 = math.exp(t * v1) - 1

			local val = math.sin((w0 * (r * period) * v2)/(r * v1))

			if val >= 0 then
				out[smpl] = amp * 1
			else
				out[smpl] = amp * -1
			end

			logIndex = logIndex + 1
			if t > 1 then logIndex = 1 end
		end return out
	end
end

function generate_triangle_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
	local out = {}
	local amp = amp_coeff or 1
	local inc = freq/samplerate
	local f0, r = freq , samplerate
	if pattern == 0 then
		for smpl = 1, n_samples do
			if phase < math.pi then
				out[smpl] = amp * (-1 + ((2/math.pi) * phase))
			else
				out[smpl] = amp * (3 - ((2/math.pi) * phase))
			end
			phase = phase + ((2 * math.pi * f0)/r)
			if phase > (2 * math.pi) then
				phase = phase - (2 * math.pi)
			end
		end return out
	elseif pattern == 1 then
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		if sweepFreq <= 0 then -- initialize sweepFreq
			sweepFreq = freq
		end
		local p_inc = twoPI * (sweepFreq  / r) -- calculate initial phase increment value
		local f_inc = (f1 - f0)/ (r * T) -- calculate frequency increment value
		for smpl = 1, n_samples do
			phase = phase + p_inc
			if phase < math.pi then
				out[smpl] = amp * (-1 + ((2/math.pi) * phase))
			else
				out[smpl] = amp * (3 - ((2/math.pi) * phase))
			end
			if phase > twoPI then phase = phase - twoPI end

			sweepFreq = sweepFreq + f_inc -- increment frequency

			if (f1 >= f0)
			then
				if (sweepFreq >= f1) then sweepFreq = f0 end -- once ending freq is reached reset to starting freq
			end

			if (freq_end < f0) then -- if this is the case sweep will proceed from high to low freq
				if (sweepFreq <= f1) then sweepFreq = f0 end
			end
			p_inc = twoPI * (sweepFreq / r)	-- recalculate phase increment for newly incremented frequency

		end return out
	else -- pattern == 2
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		local w0 = twoPI*f0
		local w1 = twoPI*f1
		local inc = (twoPI * f0)/r

		local v1 = math.log(f1/f0)

		for smpl = 1, n_samples do
			t = logIndex/(r * T)
			v2 = math.exp(t * v1) - 1
			local b = (w0 * (r * period) * v2)/(r * v1)
			out[smpl] = amp * (math.asin(math.cos(b))/1.5708)
			--out[smpl] = amp * (math.atan(math.tan(b))/1.5708)
			logIndex = logIndex + 1
			if t > 1 then logIndex = 1 end
		end return out
	end
end

function generate_sawtooth_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
	local out = {}
	local amp = amp_coeff or 1
	local inc = (twoPI * freq)/samplerate
	local f0, r = freq , samplerate
	if pattern == 0 then
		for smpl = 1, n_samples do
			out[smpl] = amp * (1 - (1/math.pi * phase))
			phase = phase + inc
			if phase >= twoPI then phase = phase - twoPI end
		end return out
	elseif pattern == 1 then
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		if sweepFreq <= 0 then -- initialize sweepFreq
			sweepFreq = freq
		end
		local p_inc = twoPI * (sweepFreq  / r) -- calculate initial phase increment value
		local f_inc = (f1 - f0)/ (r * T) -- calculate frequency increment value
		for smpl = 1, n_samples do
			phase = phase + p_inc
			out[smpl] = amp * (1 - (1/math.pi * phase))
			if phase > twoPI then phase = phase - twoPI end

			sweepFreq = sweepFreq + f_inc -- increment frequency

			if (f1 >= f0)
			then
				if (sweepFreq >= f1) then sweepFreq = f0 end -- once ending freq is reached reset to starting freq
			end

			if (freq_end < f0) then -- if this is the case sweep will proceed from high to low freq
				if (sweepFreq <= f1) then sweepFreq = f0 end
			end
			p_inc = twoPI * (sweepFreq / r)	-- recalculate phase increment for newly incremented frequency

		end return out
	else -- pattern == 2
		local f0, f1 = freq, freq_end
		local T, r = period, samplerate
		local w0 = math.pi*f0
		local w1 = math.pi*f1
		local inc = (twoPI * f0)/r

		local v1 = math.log(f1/f0)

		for smpl = 1, n_samples do
			t = logIndex/(r * T)
			v2 = math.exp(t * v1) - 1
			local b = (w0 * (r * period) * v2)/(r * v1)
			--local b = (w0 * (r * period) * v2)/(r * v1)
			--out[smpl] = amp * (math.asin(math.cos(b))/1.5708)
			out[smpl] = amp * (math.atan(math.tan(b))/1.5708)
			logIndex = logIndex + 1
			if t > 1 then logIndex = 1 end
		end return out
	end
end

function generate_impulse(amp_coeff, n_samples, period, p_secs)
	local amp = amp_coeff or 1
	local cfg = self:shmem():to_float(0):array()
	phase = phase + n_samples
	if phase >= period then
		cfg[1]=1
		phase = 0
		pulse = 1
		return {amp*1}
	else
		cfg[1] = pulse - (pulse/(p_secs*25))
		pulse = cfg[1]
		self:queue_draw()
	end
end

function dsp_params ()
	return
	{
		-- CtrlPorts:array()[1]
		{ ["type"] = "input", name = "Pattern", min = 0, max = 2, default = 0, enum = true, scalepoints =
			{
				["Continuous"]   = 0,
				["Linear Sweep"] = 1,
				["Log Sweep"]    = 2,
			}
		},
		--  CtrlPorts:array()[2]
		{ ["type"] = "input", name = "Signal", min = 0, max = 7, default = 0, enum = true, scalepoints =
			{
				["Sine wave"]     = 0,
				["Uniform"]       = 1,
				["Gaussian"]      = 2,
				["Pink"]          = 3,
				["Impulse Train"] = 4,
				["Square Wave"]   = 5,
				["Triangle Wave"] = 6,
				["Sawtooth Wave"] = 7,
			}
		},
		-- CtrlPorts:array()[3]
		{ ["type"] = "input", name = "Frequency/Sweep Start", min = 20, max = r/2, default = 440, unit="Hz", logarithmic = true },
		-- CtrlPorts:array()[4]
		{ ["type"] = "input", name = "Sweep End", min = 20, max = r/2, default = 1000, unit="Hz", logarithmic = true },
		-- CtrlPorts:array()[5]
		{ ["type"] = "input", name = "Sweep Period", min = 0.1, max = 15, default = 0.5, unit="seconds"},
		-- CtrlPorts:array()[6]
		{ ["type"] = "input", name = "Impulse Period", min = 0.1, max = 15, default = 1, unit="seconds"},
		-- CtrlPorts:array()[7]
		{ ["type"] = "input", name = "Gain", min = -90, max = 0, default = -18, unit="dB", logarithmic = true },
		-- CtrlPorts:array()[8]
		{ ["type"] = "input", name = "Simple Display/Waveform Display", min = 0, max = 1, default = 0, toggled = true },
	}
end

function dsp_ioconfig ()
	return { [1] = { audio_in = -1, audio_out = -1}, }
end

function dsp_run (ins, outs, n_samples)
	local ctrl = CtrlPorts:array()

	local output
	local pattern = math.floor (ctrl[1] + .5)
	local signal = math.floor (ctrl[2] + .5)
	local freq = ctrl[3]
	local freq_end = ctrl[4]
	local period  = ctrl[5]
	local impulse_period = ctrl[6]
	local samplerate = r
	local amp_coeff = ARDOUR.DSP.dB_to_coefficient(ctrl[7])

	if #outs > 0 then
		if signal == 5 then
			local output = generate_square_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
			outs[1]:set_table(output, #output)
		elseif signal == 6 then
			local output = generate_triangle_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
			outs[1]:set_table(output, #output)
		elseif signal == 7 then
			local output = generate_sawtooth_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
			outs[1]:set_table(output, #output)
		elseif signal == 3 then
			gen_pink:run (outs[1], n_samples)
			ARDOUR.DSP.apply_gain_to_buffer (outs[1], n_samples, amp_coeff);
			draw = draw + n_samples
		elseif signal == 2 then
			gen_gauss:run (outs[1], n_samples)
			ARDOUR.DSP.apply_gain_to_buffer (outs[1], n_samples, amp_coeff);
			draw = draw + n_samples
		elseif signal == 1 then
			gen_white:run (outs[1], n_samples)
			ARDOUR.DSP.apply_gain_to_buffer (outs[1], n_samples, amp_coeff);
			draw = draw + n_samples
		elseif signal == 4 then
			ARDOUR.DSP.apply_gain_to_buffer (outs[1], n_samples, 0); -- silence
			-- THIS IS FUBAR. the impulse should be equally spaced, regardless of buffersize
			local output = generate_impulse(amp_coeff, n_samples, impulse_period*samplerate, impulse_period)
			if output then
				outs[1]:set_table(output, #output)
			end
		else -- signal == 0
			local output = generate_sine_wave(freq, samplerate, amp_coeff, n_samples, pattern, freq_end, period)
			outs[1]:set_table(output, #output)
		end
	end

	-- copy signal to output channels
	for c = 2,#outs do
		ARDOUR.DSP.copy_vector (outs[c], outs[1], n_samples)
	end

	if (freq ~= f_old) or (ctrl[7] ~= a_old) or (signal ~= signal_old) or (ctrl[8] ~= toggle_old) then
		self:queue_draw()
	end

	if (draw > (r/15)) then
		self:queue_draw()
		draw = 0
	end
	f_old, a_old, signal_old, toggle_old = ctrl[3], ctrl[7], signal, ctrl[8]
end

function render_sine_wave(ctx, w, h, amp, freq)
	ctx:set_line_width(2)
	ctx:set_source_rgba(0.8, 0.8, 0.8, 1.0)
	local inc = 1/w
	local f = freq / 1000
	if f < 0.5 then f = 0.5 end
	if f > 8 then f  = 8 end
	p = 0
	l_x = 0
	l_y = 0
	for x = 0,w do
		y = amp * math.sin(f * twoPI * p)
		yc = 0.5 * h + ((-0.5 * h) * y)
		ctx:move_to (x, yc)
		ctx:line_to (l_x, l_y)
		l_x = x
		l_y = yc
		ctx:stroke()
		p = p + inc
	end
end

function render_square_wave(ctx, w, h, amp, freq)
	ctx:set_line_width(2)
	ctx:set_source_rgba(0.8, 0.8, 0.8, 1.0)
	local inc = 1/w
	local f = freq / 1000
	if f < 0.5 then f = 0.5 end
	if f > 8 then f  = 8 end
	p = 0
	l_x = 0
	l_y = 0
	for x = 0,w do
		local val = math.sin(f * twoPI * p)
		if val >= 0 then
			y = amp * 1
		else
			y = amp * -1
		end
		yc = 0.5 * h + ((-0.5 * h) * y)
		ctx:move_to (x, yc)
		ctx:line_to (l_x, l_y)
		l_x = x
		l_y = yc
		ctx:stroke()
		p = p + inc
	end
end

function render_sawtooth_wave(ctx, w, h, amp, freq)
	ctx:set_line_width(2)
	ctx:set_source_rgba(0.8, 0.8, 0.8, 1.0)
	local inc = 1/w
	local f = freq / 1000
	if f < 0.5 then f = 0.5 end
	if f > 8 then f  = 8 end
	p = 0
	l_x = 0
	l_y = 0
	for x = 0,w do
		--val =  ((f*x/w) % 1)
		y = amp * (math.atan(math.tan(f*twoPI*p))/1.5708)
		yc = 0.5 * h + ((-0.5 * h) * y)
		ctx:move_to (x, yc)
		ctx:line_to (l_x, l_y)
		l_x = x
		l_y = yc
		ctx:stroke()
		p = p + inc
	end
end

function render_triangle_wave(ctx, w, h, amp, freq)
	ctx:set_line_width(2)
	ctx:set_source_rgba(0.8, 0.8, 0.8, 1.0)
	local inc = 1/w
	local f = freq / 1000
	if f < 1 then f = 1 end
	if f > 8 then f  = 8 end
	p = 0
	l_x = 0
	l_y = 0
	for x = 0,w do
		--val =  ((f*x/w) % 1)
		y = amp * -1*(math.asin(math.cos(f*twoPI*p))/1.5708)
		yc = 0.5 * h + ((-0.5 * h) * y)
		ctx:move_to (x, yc)
		ctx:line_to (l_x, l_y)
		l_x = x
		l_y = yc
		ctx:stroke()
		p = p + inc
	end
end

function render_noise(ctx, w, h, amp_coeff, pink)
	p = 0
	inc = 0
	ycy = 0.5
	if pink ~= 1 then
		pink = false
	end
	if pink then inc = 0.7/w end

	ctx:set_line_width(2.0)
	ctx:set_source_rgba(0.8, 0.8, 0.8, 1.0)
	h = h - 1;

	if pink then ycy = 0.3 * h else ycy = 0.5 * h end --slant slightly like an actual pink noise spectrum

	l_x = 0
	l_y = ycy
	for x = 0,w do
		y = amp_coeff * (math.random() - 0.5) - p
		yc = math.floor (ycy - 0.5 * h * y) + .5
		ctx:move_to (x, yc)
		ctx:line_to (l_x, l_y)
		l_x = x
		l_y = yc
		ctx:stroke()
		p = p + inc
	end
end

function render_blip(ctx, w, h)
	state = self:shmem():to_float(0):array()
	color = state[1]
	ctx:set_source_rgba(color, color, color, color)
	ctx:translate(w/2, h/2)
	ctx:arc(0, 0, state[1]*15, 0, (twoPI))
	ctx:stroke()
end

function render_mapped_signal_type(ctx, w, h, signal)
	ctx:rectangle(0, 0, w, h)
	ctx:set_source_rgba(.2, .2, .2, 1.0)
	ctx:fill()
	if not txt then txt = Cairo.PangoLayout(ctx, "Mono 8px") end
	tw, th = txt:get_pixel_size()
	txt:set_text(map_signal_type(signal))
	local h0 = th + h
	if (h0 > 45) then h0 = 45 end
	ctx:set_source_rgba (.8, .8, .8, 1.0)
	ctx:move_to (.5 * (w - tw), .5 * (h0 - th))
	txt:show_in_cairo_context(ctx)
end

function render_inline (ctx, w, max_h)
	local ctrl = CtrlPorts:array () -- get control ports
	local h = 45
	local freq = ctrl[3]
	local amp_coeff = ARDOUR.DSP.dB_to_coefficient(ctrl[7]) * .9
	local signal = math.floor (ctrl[2] + .5)

	if ctrl[8] == 0 then
		render_mapped_signal_type(ctx, w, h, signal)
	else
		if signal == 0 then
			render_sine_wave(ctx, w, h, amp_coeff, freq)
		elseif signal == 5 then
			render_square_wave(ctx, w, h, amp_coeff, freq)
		elseif signal == 7 then
			render_sawtooth_wave(ctx, w, h, amp_coeff, freq)
		elseif signal == 6 then
			render_triangle_wave(ctx, w, h, amp_coeff, freq)
		elseif signal == 3 then
			render_noise(ctx, w, h, amp_coeff, 1)
		elseif signal == 2 or signal == 1 then
			render_noise(ctx, w, h, amp_coeff, 0)
		elseif signal == 4 then
			render_blip(ctx, w, h)
		else
			ctx:rectangle(0, 0, w, h)
			ctx:set_source_rgba(0, 0, 0, 1.0)
			ctx:fill()
		end
	end
	return {w, h}
end
