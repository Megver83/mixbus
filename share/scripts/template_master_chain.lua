ardour {
	["type"]    = "EditorAction",
	name        = "Master Chain",
	description = [[
	Add a chain of mastering plugins to the session's Master bus.

	It relies on the following plugins (in order):
	* XT-ME
	* XT-MC
	* XT-SC
	* x42 Digital Peak Limiter

	This script is developed in Lua, and can be duplicated and/or modified to
	meet your needs.
    ]]
}

function session_setup ()
	return false
end

function route_setup ()
	return
	{
		['Insert_at'] = ARDOUR.PresentationInfo.max_order
	}
end

function factory (params) return function ()
	local master = Session:route_by_name("Master")
	if master:isnil() or not(master:is_master()) then
		for route in Session:get_routes():iter() do
			if route:is_master() then
				master = route
				break
			end
		end
	end

	local cont = not(master:isnil())

	-- You can find LV2 URIs in the manifest of every LV2 plugin's
	-- folder. E.g for these pre-installed Mixbus LV2's they can be
	-- found under under 'lib/LV2/(plugin)/manifest.ttl'
	local uris = {
		"http://harrisonconsoles.com/lv2/xt-me/stereo",
		"http://harrisonconsoles.com/lv2/xt-mc/stereo",
		"http://harrisonconsoles.com/lv2/SpectComp/stereo",
		"http://gareus.org/oss/lv2/dpl#stereo",
	}

	-- Ensure that these plguins are accessible
	for _, uri in pairs(uris) do
		cont = not(
			ARDOUR.LuaAPI.new_plugin_info(uri, ARDOUR.PluginType.LV2)
		):isnil()
		if not(cont) then
			break
		end
	end

	local plugins = {
		-- XT-ME
		[ARDOUR.LuaAPI.new_plugin(
			Session,
			uris[1],
			ARDOUR.PluginType.LV2,
			""
		)] = {
			[1] = 1.000000, -- LPF In
			[3] = 1.000000, -- HPF In
			[10] = 2.385321, -- Band 6
			[11] = 2.079511, -- Band 7
			[12] = 2.079511, -- Band 8
			[15] = -1.328090, -- Band 1
			[19] = 2.123197, -- Band 15
			[20] = 2.123197, -- Band 16
			[21] = 1.817388, -- Band 17
			[24] = 0.200962, -- Band 20
			[25] = 0.419397, -- Band 21
			[26] = 0.812582, -- Band 22
			[27] = 2.429008, -- Band 23
			[28] = 2.429008, -- Band 24
			[29] = 2.429008, -- Band 25
			[30] = 2.429008, -- Band 26
			[31] = 2.429008, -- Band 27
			[32] = 2.429008, -- Band 28
			[33] = 2.429008, -- Band 29
			[34] = 2.429008, -- Band 30
			[35] = 2.429008, -- Band 31
		},

		-- XT-MC
		[ARDOUR.LuaAPI.new_plugin(
			Session,
			uris[2],
			ARDOUR.PluginType.LV2,
			""
		)] = {
			[0] = -31.679991, -- Mthresh
			[2] = -1.200000, -- thresh1
			[7] = 0.000000, -- in2
			[10] = 4.440000, -- depth1
			[11] = 3.000000, -- depth2
			[12] = 0.012000, -- depth3
			[17] = 90.531563, -- freq0
			[18] = 600.000000, -- freq1
			[19] = 3736.823242, -- freq2
			[20] = 5.819869, -- attk
			[21] = 23.820972, -- rel
			[22] = 10.338001, -- cross
			[23] = 2.016000, -- makup
		},

		-- XT-SC
		[ARDOUR.LuaAPI.new_plugin(
			Session,
			uris[3],
			ARDOUR.PluginType.LV2,
			""
		)] = {
			[2] = -2.975994, -- Bands
			[3] = -11.040001, -- Fdr0
			[4] = -11.472000, -- Fdr1
			[7] = -28.800009, -- Fdr4
		},

		-- X42 Digital Peak Limiter
		[ARDOUR.LuaAPI.new_plugin(
			Session,
			uris[4],
			ARDOUR.PluginType.LV2,
			""
		)] = {
			[2] = -0.300000, -- Enable
			[3] = 0.251189, -- Input Gain
		},
	}

	if not(master:isnil()) and cont then
		for plugin, params in pairs(plugins) do
			print(plugin, params)
			if not(plugin:isnil()) then
				master:add_processor_by_index(plugin, -1, nil, true)
				for i,v in pairs(params) do
					print(plugin:name(), i, v)
					ARDOUR.LuaAPI.set_processor_param(plugin, i, v)
				end
			end
		end
		-- End of script for a normal run-through
		return
	end

	LuaDialog.Message("Session Creation",
		"You don't have the right plugins installed!",
		LuaDialog.MessageType.Info, LuaDialog.ButtonType.Close):run()
end end
