ardour { ["type"] = "Snippet", name = "Dump MIDI Region" }

function factory () return function ()
	
	local m_names = {"Intro",
	"Verse 1",
	"Pre-Chorus",
	"Chorus",
	"Verse 2",
	"Pre-Chorus 2",
	"Chorus 2",
	"Chorus 3",
	"Verse 3",
	"Final Chorus",
	"Outro"
	}
	
	local sel = Editor:get_selection ()
	for r in sel.regions:regionlist ():iter () do
		local mr = r:to_midiregion ()
		if mr:isnil () then goto next end
		print (r:name (), "Pos:", r:position (), "Start:", r:start ())
		
		local bfc = ARDOUR.BeatsFramesConverter (Session:tempo_map (), r:position ())
		local nl = ARDOUR.LuaAPI.note_list (mr:model ())
		local i = 1
		for n in nl:iter () do
			local loc = bfc:to(n:time ())
			print (" Note @", loc)
			Session:request_locate(loc)
			ARDOUR.LuaAPI.usleep(185759) 
			Editor:add_location_from_playhead_cursor(m_names[i])
			i = i + 1
		end
		::next::
	end
end end