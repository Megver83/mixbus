ardour {
	["type"]    = "SessionInit",
	name        = "Podcast - lua helper",
	description = [[ Prioritize the min:sec clocks for a podcast session ]]
}

function session_setup ()
	return true
end

function factory () return function ()

    Editor:set_toggleaction ("Rulers", "toggle-tempo-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-meter-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-bbt-ruler", false)
    
    Editor:set_toggleaction ("Rulers", "toggle-minsec-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-timecode-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-samples-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-minsec-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-cd-marker-ruler", false)

    --enable "loop&range related" rulers
    Editor:set_toggleaction ("Rulers", "toggle-range-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-marker-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-loop-punch-ruler", true)

    Editor:access_action ("Transport", "primary-clock-minsec")
    Editor:access_action ("Transport", "secondary-clock-samples")

    --fit all tracks on the screen
    Editor:access_action("Editor","fit_all_tracks")
    Session:save_state("")

collectgarbage ()
end end
