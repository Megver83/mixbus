ardour {
	["type"]    = "SessionInit",
	name        = "lua helper",
	description = [[ Prioritize the BBT clocks for a MIDI session ]]
}

function session_setup ()
	return true
end

function factory () return function ()

    --enable "music related" rulers
    Editor:set_toggleaction ("Rulers", "toggle-tempo-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-meter-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-bbt-ruler", true)
    
    Editor:set_toggleaction ("Rulers", "toggle-minsec-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-timecode-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-samples-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-minsec-ruler", false)
    Editor:set_toggleaction ("Rulers", "toggle-cd-marker-ruler", false)
    
    --enable "loop&range related" rulers
    Editor:set_toggleaction ("Rulers", "toggle-range-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-marker-ruler", true)
    Editor:set_toggleaction ("Rulers", "toggle-loop-punch-ruler", true)

    Editor:access_action ("Transport", "primary-clock-bbt")
    Editor:access_action ("Transport", "secondary-clock-minsec")

    --fit all tracks on the screen
    Editor:access_action("Editor","fit_all_tracks")
    Session:save_state("")

collectgarbage ()
end end
