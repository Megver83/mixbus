/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_COMP_SWITCHER__
#define __MB_COMP_SWITCHER__

#include <string>
#include <vector>

#include <pangomm/fontdescription.h>

#include <canvas/canvas.h>
#include <canvas/container.h>
#include <canvas/item.h>
#include <canvas/fill.h>
#include <canvas/outline.h>

#include <gtkmm/layout.h>

#include <ardour/route.h>
#include <ardour/vca.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/automation_control.h>

#include "mixbus_ctrls_switcher.h"
#include "mixbus_widgets.h"
#include "mixbus_strips.h"

#include "route_ui.h"

class MixbusKnob;
class MixbusToggle;
class MixbusCompSwitcher;

class MixbusCompSwitcherCanvas : public MixbusSwitcherCanvas
{
public:
	MixbusCompSwitcherCanvas(RouteUI::RUIMessage vis_message);

	float natural_height() {return NATURAL_SWITCHER_HEIGHT;}
};

//----------------------------------


class MixbusCompSwitcher : public MixbusSwitcher
{
public:

	MixbusCompSwitcher (ArdourCanvas::Item *, RouteUI::RUIMessage vis_message);
	~MixbusCompSwitcher ();
};

#endif
