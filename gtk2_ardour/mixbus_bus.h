/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_BUS__
#define __MB_BUS__

#include "mixbus_strips.h"

#ifdef MIXBUS32C
# define NATURAL_BUS_HEIGHT (235)
#else
# define NATURAL_BUS_HEIGHT (190)
#endif

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class Mixbus_BusLabel;
class MixbusMeterDot;
class MixbusPointerMeter;

class MixbusBusStrip;

//input knobs
class MixbusBusStripCanvas : public MixbusSwitcherCanvas
{
public:
	MixbusBusStripCanvas();

	float natural_height();
};

class MixbusBusStrip : public MixbusStrip
{
public:

	MixbusBusStrip (ArdourCanvas::Item *);
	~MixbusBusStrip ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void meter();

	void route_property_changed (const PBD::PropertyChange&);

private:
	void set_button_names () {}

	MixbusKnob *bus_knobs[NUM_MIXBUSES];
	Mixbus_BusLabel *bus_labels[NUM_MIXBUSES];
	Mixbus_BusToggle *bus_asgns[NUM_MIXBUSES];

	MixbusKnob *mb_fx_knobs[4];
	Mixbus_BusLabel *mb_fx_labels[NUM_MIXBUSES];
	Mixbus_BusToggle *mb_fx_asgns[NUM_MIXBUSES];

	MixbusToggle *bus_eq_in;
	MixbusToggle *bus_filt_in;

	MixbusKnob *bus_eq_low_gain;
	MixbusKnob *bus_eq_mid_gain;
	MixbusKnob *bus_eq_hi_gain;

	MixbusKnob *mstr_eq_low_gain;
	MixbusKnob *mstr_eq_mid_gain;
	MixbusKnob *mstr_eq_hi_gain;

	MixbusPointerMeter	*sat_meter;
	MixbusKnob		    *drive;
	MixbusToggle		*sat_ylw;
	MixbusToggle		*sat_org;

	MixbusKnob		*bus_filter_freq;

	sigc::connection meter_connection;

	PBD::ScopedConnectionList _config_connection;
	void config_changed (std::string);
};

#endif
