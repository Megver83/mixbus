/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"

#include "mixbus_bus.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

#define NUM_FX_BUSES 0

//an MBCanvas for input knobs
MixbusBusStripCanvas::MixbusBusStripCanvas ()
{
	_knobs = new MixbusBusStrip(root());
}

float
MixbusBusStripCanvas::natural_height ()
{
	float h = NATURAL_BUS_HEIGHT;

	MixbusStrip *child = (MixbusStrip *) _root.items ().front ();
	if (child && child->stripable() && !child->stripable()->is_input_strip()) {
		h = 124;  //mixbus and master-bus tone/sat is smaller than 8 send knobs
	}

	return h;
}

//-------------------------


MixbusBusStrip::~MixbusBusStrip ()
{
	meter_connection.disconnect();
}

MixbusBusStrip::MixbusBusStrip (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	std::string bt[7] = { "", "|", "|", "", "|", "|", "" };
	for (int i = 0; i < 8; i++) {
		bus_knobs[i] = new MixbusKnob(this);
		widgets.push_front(bus_knobs[i]);
		bus_knobs[i]->set_color("mixbus bus knob");
		bus_knobs[i]->set_ticks(bt);
	}
	std::string lt[7] = { "|", "|", "|", "", "|", "|", "|" };
	bus_knobs[7]->set_ticks(lt);

	for (int i = 0; i < 8; i++) {
		bus_asgns[i] = new Mixbus_BusToggle(this);
		widgets.push_front(bus_asgns[i]);
		bus_asgns[i]->set_color("mixbus bus toggle");
		bus_asgns[i]->set_mixbus_asgn_num(i+1);
	}

	bus_eq_in = new MixbusToggle(this);
	widgets.push_front(bus_eq_in);
	bus_eq_in->set_text("EQ");

//bus Tone
	std::string bgt[NUM_KNOB_TICKS] = { "-9", "|", "|", "|", "|", "|", "9"  };
	bus_eq_low_gain = new MixbusKnob(this);
	widgets.push_front(bus_eq_low_gain);
	bus_eq_low_gain->set_color( "mixbus tone low gain knob");
	bus_eq_low_gain->set_ticks(bgt);
	bus_eq_low_gain->set_tooltip( _("Lo Gain (300Hz)") );

	bus_eq_mid_gain = new MixbusKnob(this);
	widgets.push_front(bus_eq_mid_gain);
	bus_eq_mid_gain->set_color( "mixbus tone mid gain knob");
	bus_eq_mid_gain->set_ticks(bgt);
	bus_eq_mid_gain->set_tooltip( _("Mid Gain (800Hz)") );

	bus_eq_hi_gain = new MixbusKnob(this);
	widgets.push_front(bus_eq_hi_gain);
	bus_eq_hi_gain->set_color( "mixbus tone hi gain knob");
	bus_eq_hi_gain->set_ticks(bgt);
	bus_eq_hi_gain->set_tooltip( _("High Gain (2kHz)") );

//mstr Tone
	std::string mgt[NUM_KNOB_TICKS] = { "-6", "|", "|", "|", "|", "|", "6"  };
	mstr_eq_low_gain = new MixbusKnob(this);
	widgets.push_front(mstr_eq_low_gain);
	mstr_eq_low_gain->set_color( "mixbus tone low gain knob");
	mstr_eq_low_gain->set_ticks(mgt);
	mstr_eq_low_gain->set_tooltip( _("Low Gain (90Hz)") );

	mstr_eq_mid_gain = new MixbusKnob(this);
	widgets.push_front(mstr_eq_mid_gain);
	mstr_eq_mid_gain->set_color( "mixbus tone mid gain knob");
	mstr_eq_mid_gain->set_ticks(mgt);
	mstr_eq_mid_gain->set_tooltip( _("Mid Gain (300Hz)") );

	mstr_eq_hi_gain = new MixbusKnob(this);
	widgets.push_front(mstr_eq_hi_gain);
	mstr_eq_hi_gain->set_color( "mixbus tone hi gain knob");
	mstr_eq_hi_gain->set_ticks(mgt);
	mstr_eq_hi_gain->set_tooltip( _("High Gain (4kHz)") );

//saturation-, k- & correlation-meters
	sat_meter = new MixbusPointerMeter(this);
	widgets.push_front(sat_meter);
	sat_meter->set_tooltip( _("Tape Saturation Meter") );

	mb_eq_filter_freq = new MixbusKnob(this);
	mb_eq_filter_freq->set_color("mixbus pan knob");
	widgets.push_front(mb_eq_filter_freq);
	std::string mbt[NUM_KNOB_TICKS] = { "M", "|", "|", "|", "|", "|", "St" };
	mb_eq_filter_freq->set_ticks( mbt );
	mb_eq_filter_freq->set_tooltip( _("Stereo Width") );

	drive = new MixbusKnob(this);
	drive->set_color( "mixbus drive knob");
	widgets.push_front(drive);
	drive->set_tooltip( _("Tape Saturation Drive") );

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	//TODO:  maybe consolidate this at some higher level, now that meter is in 2+ widgets
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusBusStrip::meter));

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusBusStrip::config_changed, this, _1), gui_context());
}

static const float eql = -230.0;;  ///this is the eq space we saved


void
MixbusBusStrip::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scale factor

	const float toggle_height = h*18.0;
	const float knob_sz = h*21;

// ------------ track EQ knobs

	float l_offs = midp + (Wide ? -42*h : -35*h);
	float m_offs = (midp - knob_sz/2.0) + (Wide ? 0 : 1);
	float r_offs = midp + (Wide ? 22*h : 16*h);

	l_offs = -h*28;
	r_offs = h*1;

	ArdourCanvas::Duple pos(midp, h*1);  //top offset
	for (int i = 0; i < 8; i++) {
		bus_knobs[i]->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));

		pos.x = midp + (Wide ? 27*h : 15*h);
		bus_knobs[i]->set_position(pos);

		pos.y = pos.y + h*23;
		if (i==3)
			pos.y += h*3;
	}

	//mixbus assigns
	pos.y = h*2;
	for (int i = 0; i < 8; i++) {

		float button_w = Wide ? 70*h: 47*h;

		bus_asgns[i]->set_size(ArdourCanvas::Rect (0, 0, button_w, toggle_height));

		pos.x = midp - button_w + (Wide ? 21*h : 10*h) ;
		bus_asgns[i]->set_position(pos);

		pos.y = pos.y + h*23;
		if (i==3)
			pos.y += h*3;
	}


	float moffs = 9;

//bus meters
	sat_meter->set_size(ArdourCanvas::Rect (0, 0, inWidth-12*h, 45*h));
	sat_meter->set_position(ArdourCanvas::Duple ( 6, (56+moffs)*h));

	drive->set_size(ArdourCanvas::Rect (0, 0, 23*h, h*23));
	drive->set_position(ArdourCanvas::Duple ( midp - 11.5*h, (89+moffs)*h));

	//left, mid, right offset
	l_offs = midp + (Wide ? -42*h : -35*h);
	m_offs = (midp - knob_sz/2.0) + (Wide ? 0 : 1);
	r_offs = midp + (Wide ? 22*h : 16*h);

	bus_eq_in->set_size(ArdourCanvas::Rect (0, 0, h*34, toggle_height));
	bus_eq_in->set_position(ArdourCanvas::Duple (l_offs - 4*h, h*4));

	mb_eq_filter_freq->set_size(ArdourCanvas::Rect (0, 0, 23*h, 23*h));
	mb_eq_filter_freq->set_position(ArdourCanvas::Duple ( r_offs, h*(36)));

//bus Tone
	bus_eq_hi_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_eq_hi_gain->set_position(ArdourCanvas::Duple (r_offs, h*8));

	bus_eq_mid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_eq_mid_gain->set_position(ArdourCanvas::Duple (m_offs, h*20));

	bus_eq_low_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_eq_low_gain->set_position(ArdourCanvas::Duple (l_offs, h*32));

//master Tone
	mstr_eq_hi_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	mstr_eq_hi_gain->set_position(ArdourCanvas::Duple (r_offs, h*8));

	mstr_eq_mid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	mstr_eq_mid_gain->set_position(ArdourCanvas::Duple (m_offs, h*20));

	mstr_eq_low_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	mstr_eq_low_gain->set_position(ArdourCanvas::Duple (l_offs, h*32));

	end_visual_change();
}

void
MixbusBusStrip::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	int mixbus_num = rt->mixbus();
	bool is_mixbus = mixbus_num != 0;
	bool is_master = rt->is_master();
	bool is_track = !is_master && !is_mixbus;

	_bg_pattern_type = is_mixbus ? bg_pattern_bus_mixbus : (is_master ? bg_pattern_bus_master :  bg_pattern_bus );

	//now show and re-connect based on route type ?
	if ( rt->is_master() ) {

		bus_eq_in->show();
		bus_eq_in->set_route( rt, rt->eq_enable_controllable() );

		mstr_eq_low_gain->show();
		mstr_eq_low_gain->set_route( rt, rt->eq_gain_controllable(0) );

		mstr_eq_mid_gain->show();
		mstr_eq_mid_gain->set_route( rt, rt->eq_gain_controllable(1) );

		mstr_eq_hi_gain->show();
		mstr_eq_hi_gain->set_route( rt, rt->eq_gain_controllable(2) );

		sat_meter->show();
		sat_meter->set_route( rt, rt->tape_drive_mtr_controllable() );

		drive->show();
		drive->set_route( rt, rt->tape_drive_controllable() );

	} else if ( rt->mixbus() ) {

		bus_eq_in->show();
		bus_eq_in->set_route( rt, rt->eq_enable_controllable() );

		bus_eq_low_gain->show();
		bus_eq_low_gain->set_route( rt, rt->eq_gain_controllable(0) );

		bus_eq_mid_gain->show();
		bus_eq_mid_gain->set_route( rt, rt->eq_gain_controllable(1) );

		bus_eq_hi_gain->show();
		bus_eq_hi_gain->set_route( rt, rt->eq_gain_controllable(2) );

		sat_meter->show();
		sat_meter->set_route( rt, rt->tape_drive_mtr_controllable() );

		drive->show();
		drive->set_route( rt, rt->tape_drive_controllable() );

		mb_eq_filter_freq->show();
		mb_eq_filter_freq->set_route( rt, rt->pan_width_control() );

	} else {

		for (int i = 0; i < NUM_MIXBUSES; i++) {

			bus_knobs[i]->show();
			bus_knobs[i]->set_route( rt, rt->send_level_controllable(i) );

			bus_asgns[i]->show();
			bus_asgns[i]->set_route( rt, rt->send_enable_controllable(i) );
			bus_asgns[i]->set_bus_route( rt->session().get_mixbus(i) );
		}

	}

	end_visual_change();
}

void
MixbusBusStrip::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;


	float midp = width / 2;
//	float w = width / NATURAL_STRIP_WIDTH;  //this  scales all our values "as if" the strip were NATURAL_BUS_WIDTH pix wide
	float h = ScreenSizer::user_scale();  //scale factor

	int tw,  th;  //text width and height

	bool flat = UIConfiguration::instance().get_flat_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );


	float drad = 5;  //decorator radius

	//EQ Text
	cr->set_source_rgba (fg_r, fg_g, fg_b, LEGEND_BRIGHTNESS);
	if ( false && !is_track) {
		layout->set_text( "TONE" );
		layout->get_pixel_size( tw,th );
		m = cr->get_matrix();
		cr->translate ( floor(midp - (tw/2.0)), floor(h*5) );
		layout->show_in_cairo_context (cr);
		cr->set_matrix(m);
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//Saturation slate
	if ( (is_mixbus || is_master) ) {

		float moffs = 9;

		//Tape text
		if (Wide) {
			cr->set_source_rgba (fg_r*0.7, fg_g*0.7, fg_b*0.7,LEGEND_BRIGHTNESS);
			layout->set_text( Wide ? "DRIVE" : "Drv");
			m = cr->get_matrix();
			cr->translate ( floor(7*h), floor((112)*h) );
			layout->show_in_cairo_context (cr);
			cr->set_matrix(m);
		}
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 8, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 8, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false &&!flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 7);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 8 );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -mixbus_num*25, -mixbus_num*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//special case for VU meter widgets;  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
MixbusBusStrip::meter()
{
	if (!_route)
		return;

	//saturation meter
	sat_meter->meter();
}

void
MixbusBusStrip::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {}
}

void
MixbusBusStrip::config_changed (string p)
{
}
