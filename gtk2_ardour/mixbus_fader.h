/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_FADER__
#define __MB_FADER__

#include "mixbus_strips.h"

# define NATURAL_FADER_HEIGHT (288)

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusSquareToggle;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;
class MixbusSpillButton;

class MixbusStripFader;

#define MAIN_METER_CNT 18
#define THRESH_METER_CNT 6

//input knobs
class MixbusStripFaderCanvas : public MixbusStripCanvas
{
public:
	MixbusStripFaderCanvas(bool small = false );

	float natural_height();

	void set_hold_count (int cnt);
	void display_slate_text (std::string txt);

private:
	bool	_small;
};

class MixbusStripFader : public MixbusStrip
{
public:

	MixbusStripFader (ArdourCanvas::Item *);
	~MixbusStripFader ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void set_hold_count (int cnt);
	void display_slate_text (std::string txt);
	void meter();

	void route_color_changed ();

	void route_property_changed (const PBD::PropertyChange&);

protected:
	void set_button_names () {}

	uint32_t hold_cnt;
	float hold_val, hold_val_r;

	MixbusToggle *mute;
	MixbusToggle *solo;

#ifdef MIXBUS32C
	MixbusToggle *rec_toggle;
	MixbusToggle *input_toggle;
#endif

	sigc::connection meter_connection;

	MixbusSlate *slate;

	MixbusKnob *trim;
	MixbusKnob *makeup;
	MixbusKnob *speed;
	MixbusFader *fader;

	MixbusThresh *thresh;
	MixbusThresh *gate_thresh;

	MixbusMeterBar *strip_meter;
	MixbusReduxDot *comp_redux_meterDots[THRESH_METER_CNT];
	MixbusReduxDot *gate_redux_meterDots[THRESH_METER_CNT];

	MixbusToggle *sidech_in;

	MixbusToggle *comp_in;
	MixbusCompLegend *speed_leg;
	MixbusModeSelector *comp_disp;

	MixbusToggle *drive_in;

	MixbusSpillButton *_spill_button;
	MixbusActionToggle *_monitor_section_button;

	bool mb_solo_press(GdkEventButton*);
	bool mb_solo_release(GdkEventButton*);
	bool mb_mute_press(GdkEventButton*);
	bool mb_mute_release(GdkEventButton*);
};

class MixbusStripFader_Small : public MixbusStripFader
{
public:
	MixbusStripFader_Small (ArdourCanvas::Item *);
	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);
};

#endif
