/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_COMP__
#define __MB_COMP__

#include "mixbus_strips.h"

#ifdef MIXBUS32C
# define NATURAL_COMP_HEIGHT (235)
#else
# define NATURAL_COMP_HEIGHT (94)
#endif

#define COMP_METER_CNT 10
#define COMP_THRESH_CNT 6

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;

class MixbusComp;

class MixbusCompCanvas : public MixbusSwitcherCanvas
{
public:
	MixbusCompCanvas();

	float natural_height();

	void set_hold_count (int cnt);
};

class MixbusComp : public MixbusStrip
{
public:

	MixbusComp (ArdourCanvas::Item *);
	~MixbusComp ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void route_property_changed (const PBD::PropertyChange&);

	void meter();

private:
	void set_button_names () {}

	MixbusKnob *comp_attk;
	MixbusKnob *comp_rels;
	MixbusKnob *comp_ratio;
	MixbusKnob *comp_emph;
	MixbusKnob *comp_gain;

	MixbusModeSelector *comp_mode;

	MixbusThresh *comp_thresh;
	MixbusKnob *comp_thresh_knob;

	MixbusToggle *comp_in;

	MixbusMeterDot *input_meterDots[COMP_METER_CNT];
	MixbusReduxDot *redux_meterDots[COMP_THRESH_CNT];

	PBD::ScopedConnectionList _config_connection;
	void config_changed (std::string);

	sigc::connection meter_connection;
};

#endif
