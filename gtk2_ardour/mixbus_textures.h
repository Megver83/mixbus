
/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_TEXTURES__
#define __MB_TEXTURES__

#include <canvas/canvas.h>

#include <gtkmm/layout.h>

class MixbusTextures
{
public:
	MixbusTextures();

	static Glib::RefPtr<Gdk::Pixbuf> mb_strip_texture;
	static Glib::RefPtr<Gdk::Pixbuf> mb_logo_texture;
	static Glib::RefPtr<Gdk::Pixbuf> mb_scratch_texture;
};

#endif
