/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_EQ__
#define __MB_EQ__

#include "mixbus_strips.h"

#ifdef MIXBUS32C
# define NATURAL_EQ_HEIGHT (235)
#else
# define NATURAL_EQ_HEIGHT (94)
#endif

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;

class MixbusEQ;

class MixbusEQCanvas : public MixbusSwitcherCanvas
{
public:
	MixbusEQCanvas();

	float natural_height() {return NATURAL_EQ_HEIGHT;}

	void set_hold_count (int cnt);
};

class MixbusEQ : public MixbusStrip
{
public:

	MixbusEQ (ArdourCanvas::Item *);
	~MixbusEQ ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void route_property_changed (const PBD::PropertyChange&);

private:
	void set_button_names () {}

	MixbusKnob *hp_freq;
#ifdef MIXBUS32C
	MixbusKnob *lp_freq;

	Mixbus_EQ_Knob *eq_low_gain;
	Mixbus_EQ_Knob *eq_lomid_gain;
	Mixbus_EQ_Knob *eq_himid_gain;
	Mixbus_EQ_Knob *eq_hi_gain;
	Mixbus_EQ_Knob *eq_low_freq;
	Mixbus_EQ_Knob *eq_lomid_freq;
	Mixbus_EQ_Knob *eq_himid_freq;
	Mixbus_EQ_Knob *eq_hi_freq;
#else
	MixbusKnob *eq_low_gain;
	MixbusKnob *eq_mid_gain;
	MixbusKnob *eq_hi_gain;
	MixbusKnob *eq_low_freq;
	MixbusKnob *eq_mid_freq;
	MixbusKnob *eq_hi_freq;
#endif

	MixbusToggle *eq_in;
#ifdef MIXBUS32C
	MixbusToggle *filt_in;
	MixbusToggle *eq_lo_bell_in;
	MixbusToggle *eq_hi_bell_in;
#endif

	PBD::ScopedConnectionList _config_connection;
	void config_changed (std::string);
};

#endif
