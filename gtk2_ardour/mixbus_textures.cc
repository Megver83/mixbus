
/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <pbd/compose.h>

#include <canvas/canvas.h>
#include <canvas/debug.h>
#include <canvas/utils.h>

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

Glib::RefPtr<Gdk::Pixbuf> MixbusTextures::mb_strip_texture;
Glib::RefPtr<Gdk::Pixbuf> MixbusTextures::mb_logo_texture;
Glib::RefPtr<Gdk::Pixbuf> MixbusTextures::mb_scratch_texture;

MixbusTextures::MixbusTextures()
{
#ifdef MIXBUS32C
		mb_strip_texture = ::get_icon ("strip_texture32c");
		mb_logo_texture = ::get_icon ("mb32c_bug");
#else
		mb_strip_texture = ::get_icon ("strip_texture");
#endif
		mb_scratch_texture = ::get_icon ("strip_scratch");
}

//Glib::RefPtr<Gdk::Pixbuf> channel_strip_sw_texture() {return channel_logo_sw_texture;}
//Glib::RefPtr<Gdk::Pixbuf> channel_logo_sw_texture() {return channel_strip_sw_texture;}
//Glib::RefPtr<Gdk::Pixbuf> channel_scratch_sw_texture() {return channel_scratch_sw_texture;}
