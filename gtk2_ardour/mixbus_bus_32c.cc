/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_k_meter.h"

#include "mixbus_bus.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

#define NUM_FX_BUSES 4

//an MBCanvas for input knobs
MixbusBusStripCanvas::MixbusBusStripCanvas ()
{
	_knobs = new MixbusBusStrip(root());
}

float
MixbusBusStripCanvas::natural_height ()
{
	float h = NATURAL_BUS_HEIGHT;

	MixbusStrip *child = (MixbusStrip *) _root.items ().front ();
	if (child && child->stripable()&& child->stripable()->is_master()) {
		h += -NATURAL_K14_HEIGHT;  //master leaves room for K-meter
	}

	return h;
}

//-------------------------


MixbusBusStrip::~MixbusBusStrip ()
{
	meter_connection.disconnect();
}

MixbusBusStrip::MixbusBusStrip (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	std::string lt[NUM_KNOB_TICKS] = { "|", "|", "|", "|", "|", "|", "|", "|", "*", "|", "|" };
	for (int i = 0; i < NUM_MIXBUSES; i++) {
		bus_knobs[i] = new MixbusSendKnob(this, i);
		widgets.push_front(bus_knobs[i]);
		if (i>7)
			bus_knobs[i]->set_color("mixbus fx knob");
		else
			bus_knobs[i]->set_color("mixbus bus knob");
		bus_knobs[i]->set_ticks(lt);
	}

	for (int i = 0; i < NUM_MIXBUSES; i++) {
		bus_labels[i] = new Mixbus_BusLabel(this);
		bus_labels[i]->set_mixbus_asgn_num(i+1);
		widgets.push_front(bus_labels[i]);
	}

	for (int i = 0; i < NUM_MIXBUSES; i++) {
		bus_asgns[i] = new Mixbus_BusToggle(this);
		bus_asgns[i]->set_mixbus_asgn_num(i+1);
		widgets.push_front(bus_asgns[i]);
	}

	//new FX bus sends
	{
		std::string lt[NUM_KNOB_TICKS] = { "|", "|", "|", "|", "|", "|", "|", "|", "*", "|", "|" };
		for (int i = 0; i < NUM_FX_BUSES; i++) {
			mb_fx_knobs[i] = new MixbusSendKnob(this, i);
			widgets.push_front(mb_fx_knobs[i]);
			mb_fx_knobs[i]->set_color("mixbus fx knob");
			mb_fx_knobs[i]->set_ticks(lt);
		}

		for (int i = 0; i < NUM_FX_BUSES; i++) {
			mb_fx_labels[i] = new Mixbus_BusLabel(this);
			widgets.push_front(mb_fx_labels[i]);
		}

		for (int i = 0; i < NUM_FX_BUSES; i++) {
			mb_fx_asgns[i] = new Mixbus_BusToggle(this);
			widgets.push_front(mb_fx_asgns[i]);
		}
	}
	
	bus_eq_in = new MixbusToggle(this);
	widgets.push_front(bus_eq_in);
	bus_eq_in->set_text("EQ");
	bus_eq_in->set_tooltip( _("EQ enable") );

	bus_filt_in = new MixbusToggle(this);
	widgets.push_front(bus_filt_in);
	bus_filt_in->set_text("HPF");
	bus_filt_in->set_tooltip( _("High-Pass Filter enable") );

//bus Tone
	std::string bgt[NUM_KNOB_TICKS] = { "|", "-9", "|", "|", "|", "|", "|", "|", "|", "9", "|"  };
	bus_eq_low_gain = new MixbusKnob(this);
	widgets.push_front(bus_eq_low_gain);
	bus_eq_low_gain->set_color( "mixbus tone low gain knob");
	bus_eq_low_gain->set_ticks(bgt);
	bus_eq_low_gain->set_tooltip( _("Lo Gain (300Hz)") );

	bus_eq_mid_gain = new MixbusKnob(this);
	widgets.push_front(bus_eq_mid_gain);
	bus_eq_mid_gain->set_color( "mixbus tone mid gain knob");
	bus_eq_mid_gain->set_ticks(bgt);
	bus_eq_mid_gain->set_tooltip( _("Mid Gain (800Hz)") );

	bus_eq_hi_gain = new MixbusKnob(this);
	widgets.push_front(bus_eq_hi_gain);
	bus_eq_hi_gain->set_color( "mixbus tone hi gain knob");
	bus_eq_hi_gain->set_ticks(bgt);
	bus_eq_hi_gain->set_tooltip( _("High Gain (2kHz)") );

//mstr Tone
	std::string mgt[NUM_KNOB_TICKS] = { "|", "-6", "|", "|", "|", "|", "|",  "|", "|", "6", "|" };
	mstr_eq_low_gain = new MixbusKnob(this);
	widgets.push_front(mstr_eq_low_gain);
	mstr_eq_low_gain->set_color( "mixbus tone low gain knob");
	mstr_eq_low_gain->set_ticks(mgt);
	mstr_eq_low_gain->set_tooltip( _("Low Gain (90Hz)") );

	mstr_eq_mid_gain = new MixbusKnob(this);
	widgets.push_front(mstr_eq_mid_gain);
	mstr_eq_mid_gain->set_color( "mixbus tone mid gain knob");
	mstr_eq_mid_gain->set_ticks(mgt);
	mstr_eq_mid_gain->set_tooltip( _("Mid Gain (300Hz)") );

	mstr_eq_hi_gain = new MixbusKnob(this);
	widgets.push_front(mstr_eq_hi_gain);
	mstr_eq_hi_gain->set_color( "mixbus tone hi gain knob");
	mstr_eq_hi_gain->set_ticks(mgt);
	mstr_eq_hi_gain->set_tooltip( _("High Gain (4kHz)") );
	
//saturation-, k- & correlation-meters
	sat_meter = new MixbusPointerMeter(this);
	widgets.push_front(sat_meter);
	sat_meter->set_tooltip( _("Tape Saturation Meter") );

	bus_filter_freq = new MixbusKnob(this);
	bus_filter_freq->set_color("mixbus hp filter freq knob");
	widgets.push_front(bus_filter_freq);
	std::string ft[NUM_KNOB_TICKS] = { "|", "20", "|", "|", "|", "|", "|", "|", "|", "3k", "|" };
	bus_filter_freq->set_ticks(ft);
	bus_filter_freq->set_tooltip( _("HPF Freq") );

	drive = new MixbusKnob(this);
	drive->set_color( "mixbus drive knob");
	widgets.push_front(drive);
	drive->set_tooltip( _("Tape Saturation Drive") );

	sat_ylw = new MixbusButton(this, 1);
	sat_ylw->set_color( "mixbus sat ylw button");
	widgets.push_front(sat_ylw);
	sat_ylw->set_tooltip( _("Tape Saturation Yellow") );

	sat_org = new MixbusButton(this, 2);
	sat_org->set_color( "mixbus sat org button");
	widgets.push_front(sat_org);
	sat_org->set_tooltip( _("Tape Saturation Orange") );

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	//TODO:  maybe consolidate this at some higher level, now that meter is in 2+ widgets
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusBusStrip::meter));

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusBusStrip::config_changed, this, _1), gui_context());
}

static const float eql = -230.0;;  ///this is the eq space we saved


void
MixbusBusStrip::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scale factor

	const float toggle_height = h*17.0;
	const float knob_sz = h*21;

// ------------ track EQ knobs

	float l_offs = midp + (Wide ? -41*h : -35*h);
	float m_offs = (midp - knob_sz/2.0) + (Wide ? 0 : 1);
	float r_offs = midp + (Wide ? 21*h : 16*h);

//mixbus send knobs

	l_offs = -h*28;
	r_offs = h*1;

	ArdourCanvas::Duple pos( 4*h, (3)*h);
	for (int i = 0; i < NUM_MIXBUSES; i++) {
		bus_knobs[i]->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));

		if ((i%2) == 0) {
			bus_knobs[i]->set_position( Duple(pos.x, pos.y) );
		} else {
			bus_knobs[i]->set_position( Duple(pos.x + 17*h, pos.y)  );
		}

		pos.y += 18*h;

		if (i == 3)
			pos.y += 6*h;

		if (i == 7)
			pos.y += 6*h;

	}

	//mixbus assigns
	pos.x = 3*h;
	pos.y = (5)*h;
	for (int i = 0; i < NUM_MIXBUSES; i++) {
		bus_asgns[i]->set_size(ArdourCanvas::Rect (0, 0, toggle_height, toggle_height));

		if ((i%2) == 0) {
			bus_asgns[i]->set_position( Duple(pos.x + 22*h, pos.y)  );
		} else {
			bus_asgns[i]->set_position( Duple(pos.x, pos.y) );
		}
		pos.y += 18*h;

		if (i == 3)
			pos.y += 6*h;

		if (i == 7)
			pos.y += 6*h;
	}

	//mixbus labels
	pos.x = 42*h;
	pos.y = (4)*h;
	for (int i = 0; i < NUM_MIXBUSES; i++) {
		bus_labels[i]->set_size(ArdourCanvas::Rect (0, 0, 56*h, toggle_height));

		if ((i%2) == 0) {
			bus_labels[i]->set_position( Duple(pos.x, pos.y)  );
		} else {
			bus_labels[i]->set_position( Duple(pos.x, pos.y) );
		}
		pos.y += 18*h;

		if (i == 3)
			pos.y += 6*h;

		if (i == 7)
			pos.y += 6*h;
	}

//------------  NEW FX KNOBS

	{
		ArdourCanvas::Duple pos( 4*h, (67+65+27)*h);
		for (int i = 0; i < NUM_FX_BUSES; i++) {
			mb_fx_knobs[i]->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));

			if ((i%2) == 0) {
				mb_fx_knobs[i]->set_position( Duple(pos.x, pos.y) );
			} else {
				mb_fx_knobs[i]->set_position( Duple(pos.x + 17*h, pos.y)  );
			}

			pos.y += 18*h;

			if (i == 3)
				pos.y += 6*h;

			if (i == 7)
				pos.y += 6*h;

		}

		//mixbus assigns
		pos.x = 3*h;
		pos.y = (67+66+27)*h;
		for (int i = 0; i < NUM_FX_BUSES; i++) {
			mb_fx_asgns[i]->set_size(ArdourCanvas::Rect (0, 0, toggle_height, toggle_height));

			if ((i%2) == 0) {
				mb_fx_asgns[i]->set_position( Duple(pos.x + 22*h, pos.y)  );
			} else {
				mb_fx_asgns[i]->set_position( Duple(pos.x, pos.y) );
			}
			pos.y += 18*h;

			if (i == 3)
				pos.y += 6*h;

			if (i == 7)
				pos.y += 6*h;
		}

		//mixbus labels
		pos.x = 42*h;
		pos.y = (67+66+27)*h;
		for (int i = 0; i < NUM_FX_BUSES; i++) {
			mb_fx_labels[i]->set_size(ArdourCanvas::Rect (0, 0, 56*h, toggle_height));

			if ((i%2) == 0) {
				mb_fx_labels[i]->set_position( Duple(pos.x, pos.y)  );
			} else {
				mb_fx_labels[i]->set_position( Duple(pos.x, pos.y) );
			}
			pos.y += 18*h;

			if (i == 3)
				pos.y += 6*h;

			if (i == 7)
				pos.y += 6*h;
		}
	}
//------------


	float moffs = 9+20;

//bus meters
	sat_meter->set_size(ArdourCanvas::Rect (0, 0, inWidth-12*h, 45*h));
	sat_meter->set_position(ArdourCanvas::Duple ( 6, (58+moffs)*h));

	drive->set_size(ArdourCanvas::Rect (0, 0, 21*h, h*21));
	drive->set_position(ArdourCanvas::Duple ( midp - 10.5*h, (104+moffs)*h));

	sat_ylw->set_size(ArdourCanvas::Rect (0, 0, 12*h, h*12));
	sat_ylw->set_position(ArdourCanvas::Duple ( midp + 17*h, (105+moffs)*h));

	sat_org->set_size(ArdourCanvas::Rect (0, 0, 12*h, h*12));
	sat_org->set_position(ArdourCanvas::Duple ( midp + 30*h, (105+moffs)*h));

	//left, mid, right offset
	l_offs = midp + (Wide ? -42*h : -35*h);
	m_offs = (midp - knob_sz/2.0) + (Wide ? 0 : 1);
	r_offs = midp + (Wide ? 22*h : 16*h);

	bus_filter_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_filter_freq->set_position(ArdourCanvas::Duple ( l_offs+6*h, 5*h));

	bus_filt_in->set_size(ArdourCanvas::Rect (0, 0, h*40, toggle_height));
//	bus_filt_in->set_position(ArdourCanvas::Duple (36*h, 3*h));
	bus_filt_in->set_position(ArdourCanvas::Duple (inWidth - toggle_height - 24*h, 70*h));

	bus_eq_in->set_size(ArdourCanvas::Rect (0, 0, h*40, toggle_height));
	bus_eq_in->set_position(ArdourCanvas::Duple (inWidth - toggle_height - 24*h, 56*h));

//bus Tone
	bus_eq_hi_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_eq_hi_gain->set_position(ArdourCanvas::Duple (r_offs-h*2, h*13));

	bus_eq_mid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_eq_mid_gain->set_position(ArdourCanvas::Duple (m_offs, h*28));

	bus_eq_low_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	bus_eq_low_gain->set_position(ArdourCanvas::Duple (l_offs+h*2, h*43));

//master Tone
	mstr_eq_hi_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	mstr_eq_hi_gain->set_position(ArdourCanvas::Duple (r_offs-h*2, h*13));

	mstr_eq_mid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	mstr_eq_mid_gain->set_position(ArdourCanvas::Duple (m_offs, h*28));

	mstr_eq_low_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	mstr_eq_low_gain->set_position(ArdourCanvas::Duple (l_offs+h*2, h*43));

	end_visual_change();
}

void
MixbusBusStrip::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	int mixbus_num = rt->mixbus();
	bool is_mixbus = mixbus_num != 0;
	bool is_master = rt->is_master();
	bool is_input = !is_master && !is_mixbus;

	_bg_pattern_type = is_mixbus ? bg_pattern_sends_mixbus : (is_master ? bg_pattern_sends_master :  bg_pattern_sends );
	if (mixbus_num > 8) {
		_bg_pattern_type = bg_pattern_sends_fxbus;
	}

	//now show and re-connect based on route type ?
	if ( rt->is_master() ) {

		bus_eq_in->show();
		bus_eq_in->set_route( rt, rt->eq_enable_controllable() );

		mstr_eq_low_gain->show();
		mstr_eq_low_gain->set_route( rt, rt->eq_gain_controllable(0) );

		mstr_eq_mid_gain->show();
		mstr_eq_mid_gain->set_route( rt, rt->eq_gain_controllable(1) );

		mstr_eq_hi_gain->show();
		mstr_eq_hi_gain->set_route( rt, rt->eq_gain_controllable(2) );

		sat_meter->show();
		sat_meter->set_route( rt, rt->tape_drive_mtr_controllable() );

		drive->show();
		drive->set_route( rt, rt->tape_drive_controllable() );

	} else if ( rt->mixbus() ) {

		bus_eq_in->show();
		bus_eq_in->set_route( rt, rt->eq_enable_controllable() );

		bus_filt_in->show();
		bus_filt_in->set_route( rt, rt->filter_enable_controllable(true) );

		bus_eq_low_gain->show();
		bus_eq_low_gain->set_route( rt, rt->eq_gain_controllable(0) );

		bus_eq_mid_gain->show();
		bus_eq_mid_gain->set_route( rt, rt->eq_gain_controllable(1) );

		bus_eq_hi_gain->show();
		bus_eq_hi_gain->set_route( rt, rt->eq_gain_controllable(2) );

		sat_meter->show();
		sat_meter->set_route( rt, rt->tape_drive_mtr_controllable() );

		drive->show();
		drive->set_route( rt, rt->tape_drive_controllable() );
		
		sat_ylw->show();
		sat_ylw->set_route( rt, rt->tape_drive_mode_controllable() );

		sat_org->show();
		sat_org->set_route( rt, rt->tape_drive_mode_controllable() );

		bus_filter_freq->show();
		bus_filter_freq->set_route( rt, rt->filter_freq_controllable(true) );

		if (rt->mixbus() < 9) {
			for (int i = 0; i < NUM_FX_BUSES; i++) {

				mb_fx_knobs[i]->show();
				mb_fx_knobs[i]->set_route( rt, rt->send_level_controllable(i) );

				mb_fx_knobs[i]->set_concentric_controllables(  rt->send_pan_azimuth_enable_controllable(i), rt->send_pan_azimuth_controllable(i)  );

				mb_fx_asgns[i]->show();
				mb_fx_asgns[i]->set_route( rt, rt->send_enable_controllable(i) );
				mb_fx_asgns[i]->set_bus_route( rt->session().get_mixbus(i+8) );

				mb_fx_labels[i]->show();
				mb_fx_labels[i]->set_route( rt, rt->send_enable_controllable(i) );
				mb_fx_labels[i]->set_bus_route( rt->session().get_mixbus(i+8) );
			}
		}
		
	} else {

		for (int i = 0; i < NUM_MIXBUSES; i++) {

			bus_knobs[i]->show();
			bus_knobs[i]->set_route( rt, rt->send_level_controllable(i) );

			bus_knobs[i]->set_concentric_controllables(  rt->send_pan_azimuth_enable_controllable(i), rt->send_pan_azimuth_controllable(i)  );

			bus_asgns[i]->show();
			bus_asgns[i]->set_route( rt, rt->send_enable_controllable(i) );
			bus_asgns[i]->set_bus_route( rt->session().get_mixbus(i) );

			bus_labels[i]->show();
			bus_labels[i]->set_route( rt, rt->send_enable_controllable(i) );
			bus_labels[i]->set_bus_route( rt->session().get_mixbus(i) );
		}

	}

	end_visual_change();
}

void
MixbusBusStrip::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();

#ifdef MIXBUS32C
	Wide = true;
#endif

	Cairo::Matrix m;

	int mixbus_num = _route->mixbus();
	bool is_mixbus = mixbus_num != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	float midp = width / 2;
//	float w = width / NATURAL_STRIP_WIDTH;  //this  scales all our values "as if" the strip were NATURAL_BUS_WIDTH pix wide
	float h = ScreenSizer::user_scale();  //scale factor

	int tw,  th;  //text width and height

	bool flat = UIConfiguration::instance().get_flat_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );


	float drad = 5;  //decorator radius

	//EQ Text
	cr->set_source_rgba (fg_r, fg_g, fg_b, LEGEND_BRIGHTNESS);
	if ( false && !is_track) {
		layout->set_text( "TONE" );
		layout->get_pixel_size( tw,th );
		m = cr->get_matrix();
		cr->translate ( midp - (tw/2.0), h*5 );
		layout->show_in_cairo_context (cr);
		cr->set_matrix(m);
	}
	
	SVAModifier bord = UIConfiguration::instance().modifier ("strip border alpha");
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double puddle_r,puddle_g,puddle_b;
	Gtkmm2ext::color_to_rgba( puddle_color, puddle_r, puddle_g, puddle_b, unused);

	if (is_track) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,1.0);
		cr->move_to( 2*h, (79+77)*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to( 13*h, 0);  //slant start
		cr->rel_line_to( 7*h, 3*h);  //slant end
		cr->rel_line_to( 65*h, 0*h);
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 65*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -65*h, 0 );   //slant (right side)
//		cr->rel_line_to( -7*h, 3*h );  //slant
//		cr->rel_line_to( -14*h, 0 );  //slant (middle)
		cr->rel_line_to( -7*h, -3*h );  //slant
		cr->rel_line_to( -13*h, 0 );  
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -65*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		//cr->stroke();
//		cr->stroke_preserve ();
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		cr->fill();

		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		cr->move_to( 6*h, 76*h ); //start in the top left
		cr->rel_line_to( 12*h, 0 );   //slant (right side)
		cr->rel_line_to( 7*h, 3*h );  //slant
		cr->rel_line_to( 67*h, 0 );
		cr->stroke();

		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		cr->move_to( 6*h, 156*h ); //start in the top left
		cr->rel_line_to( 12*h, 0 );   //slant (right side)
		cr->rel_line_to( 7*h, 3*h );  //slant
		cr->rel_line_to( 67*h, 0 );
		cr->stroke();
	}

	//new FX bus knobs
	if ( is_mixbus && (mixbus_num < 9) ) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,1.0);
		cr->move_to( 2*h, 129*h+27*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to( 13*h, 0);  //slant start
		cr->rel_line_to( 7*h, 3*h);  //slant end
		cr->rel_line_to( 65*h, 0*h);
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 65*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -65*h, 0 );   //slant (right side)
//		cr->rel_line_to( -7*h, 3*h );  //slant
//		cr->rel_line_to( -14*h, 0 );  //slant (middle)
		cr->rel_line_to( -7*h, -3*h );  //slant
		cr->rel_line_to( -13*h, 0 );  
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -65*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		//cr->stroke();
//		cr->stroke_preserve ();
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		cr->fill();

		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,1.0);
		cr->move_to( 6*h, 128*h + 27*h ); //start in the top left
		cr->rel_line_to( 12*h, 0 );   //slant (right side)
		cr->rel_line_to( 7*h, 3*h );  //slant
		cr->rel_line_to( 67*h, 0 );
		cr->stroke();
	}
	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//EQ ranges
	if ( (is_mixbus || is_master) ) {

		cr->set_source_rgba (fg_r*0.7, fg_g*0.7, fg_b*0.7,LEGEND_BRIGHTNESS);

		cr->set_identity_matrix();
		layout->set_text( "Low");
		layout->get_pixel_size (tw, th);
		cr->translate ( midp-29*h-tw*0.5, 60*h );
		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->set_source_rgba (fg_r*0.7, fg_g*0.7, fg_b*0.7,LEGEND_BRIGHTNESS);
		layout->set_text( "Mid");
		layout->get_pixel_size (tw, th);
		cr->translate ( midp-tw*0.5, 46*h );
		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->set_source_rgba (fg_r*0.7, fg_g*0.7, fg_b*0.7,LEGEND_BRIGHTNESS);
		layout->set_text( "Hi");
		layout->get_pixel_size (tw, th);
		cr->translate ( midp+31*h-tw*0.5, 32*h );
		layout->show_in_cairo_context (cr);
	}

	cr->set_identity_matrix();

	//Saturation slate
	if ( (is_mixbus || is_master) ) {

		float moffs = 9;

		//Tape text
		if (Wide) {
			cr->set_source_rgba (fg_r*0.7, fg_g*0.7, fg_b*0.7,LEGEND_BRIGHTNESS);
			layout->set_text( Wide ? "Drive" : "Drv");
			cr->translate ( 7*h, (moffs+103+20)*h );
			layout->get_pixel_size (tw, th);
			layout->show_in_cairo_context (cr);
		}
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 6*h, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false &&!flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 7);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 8 );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//section bounds
	if (true) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width-2*h, height-2*h, 5*h);
		cr->stroke ();
	}

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -mixbus_num*25, -mixbus_num*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//special case for VU meter widgets;  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
MixbusBusStrip::meter()
{
	if (!_route)
		return;

	//saturation meter
	sat_meter->meter();
}

void
MixbusBusStrip::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {}
}

void
MixbusBusStrip::config_changed (string p)
{
}
