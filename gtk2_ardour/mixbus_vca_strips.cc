/*
    Copyright (C) 2014 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include <pbd/compose.h>

#include "ardour/utils.h"
#include "ardour/amp.h"
#include "ardour/vca.h"
#include "ardour/meter.h"
#include "ardour/dB.h"
#include "ardour/route_group.h"
#include "ardour/automation_control.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/types.h"
#include "canvas/debug.h"
#include "canvas/utils.h"
#include "canvas/canvas.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "ardour_message.h"
#include "context_menu_helper.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_vca_strips.h"

#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"
#include "pbd/controllable.h"

#include "timers.h"

using namespace std;
using namespace PBD;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------

//an MBCanvas for input knobs
MixbusVcaStripCanvas::MixbusVcaStripCanvas (std::shared_ptr<ARDOUR::VCA> vca )
{
	_knobs = new MixbusVcaStrip (root(), vca);
}

MixbusVcaStripCanvas::~MixbusVcaStripCanvas ()
{
	delete _knobs;
}


//-------------------------


MixbusVcaStrip::~MixbusVcaStrip ()
{
	_vca.reset ();
	delete _fader;
	delete _solo;
	delete _mute;
	delete _spill;
}

MixbusVcaStrip::MixbusVcaStrip (ArdourCanvas::Item *p, std::shared_ptr<ARDOUR::VCA> vca)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_vca;

	_vca = vca;
	_vca->DropReferences.connect (_vca_connections, invalidator (*this), boost::bind (&MixbusVcaStrip::drop_references, this), gui_context());
	_vca->PropertyChanged.connect (prop_connections, invalidator (*this), boost::bind (&MixbusVcaStrip::vca_property_changed, this, _1), gui_context());

	set_tooltip("");

	_fader = new MixbusVcaFader(this);
	_fader->set_vca_controllable(vca->gain_control(), _vca);
	widgets.push_front(_fader);

	_label = new MixbusLabel(this);
	_label->set_text(_vca->name());
	widgets.push_front(_label);

	_mute = new MixbusBigToggle(this, false);
	_mute->set_controllable (_vca, _vca->mute_control());
	widgets.push_front(_mute);
	_mute->set_color("mute button");
	_mute->set_text ("M");
	_mute->set_tooltip(_("Mute"));

	_solo = new MixbusBigToggle(this, true);
	_solo->set_controllable (_vca, _vca->solo_control());
	widgets.push_front(_solo);
	_solo->set_color("solo button");
	_solo->set_text ("S");
	_solo->set_tooltip (_("Solo"));

	_spill = new MixbusSpillButton(this);
	_spill->set_stripable (_vca);
	widgets.push_front (_spill);
	_spill->set_text ("Spill");
	_spill->set_tooltip (_("Filter the channel list to show only input strips\nthat are assigned to this bus."));
	_spill->set_color("mixbus spill button");
}

void
MixbusVcaStrip::vca_property_changed (PropertyChange const & what_changed)
{
	if (_vca) {
		_label->set_text(_vca->name());
		_label->redraw();
	}
}

void
MixbusVcaStrip::drop_references ()
{
	_vca.reset ();
	_mute->set_controllable(_vca, std::shared_ptr<ARDOUR::AutomationControl> ());
	_solo->set_controllable(_vca, std::shared_ptr<ARDOUR::AutomationControl> ());
	_fader->set_vca_controllable (std::shared_ptr<ARDOUR::AutomationControl> (), _vca);
	_spill->set_stripable(std::shared_ptr<ARDOUR::Stripable> ());
}


void
MixbusVcaStrip::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	float mute_w = 22;

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scale factor

	_fader->set_size(ArdourCanvas::Rect (0, 0, 32*h, h*(305)));
	_fader->set_position(ArdourCanvas::Duple (midp - 12*h, (28+28)*h));
	_fader->show();

	_label->set_size(ArdourCanvas::Rect (0, 0, 48*h, h*(14)));
	_label->set_position(ArdourCanvas::Duple (midp-24*h, inHeight-16*h));
	_label->show();

	_mute->set_size(ArdourCanvas::Rect (0, 0, h*mute_w, h*22));
	_mute->set_position(ArdourCanvas::Duple (midp-24*h, (4+28)*h));
	_mute->show();

	_solo->set_size(ArdourCanvas::Rect (0, 0, h*mute_w, h*22));
	_solo->set_position(ArdourCanvas::Duple (midp+4*h, (4+28)*h));
	_solo->show();

	_spill->set_size(ArdourCanvas::Rect (0, 0, inWidth-4*h, 22*h));
	_spill->set_position(ArdourCanvas::Duple (2*h, 4*h));
	_spill->show();

	//end_visual_change();
}

void
MixbusVcaStrip::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	MixbusStrip::render(area, cr);
}

void
MixbusVcaStrip::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	bool flat = false;//UIConfiguration::instance().get_flat_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	float scale = ScreenSizer::user_scale();

	float offs = 5;
#ifdef MIXBUS32C
	offs= 146;
#endif

	cr->set_identity_matrix();

	if (false && !flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 8, 0);
		left_pattern->add_color_stop_rgba (0, 0, 0, 0, 0.3);
		left_pattern->add_color_stop_rgba (1, 0, 0, 0, 0.0);
		cr->set_source (left_pattern);
		cr->rectangle (0, 0, 8, height);
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false && !flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 8);
		shine_pattern->add_color_stop_rgba (0, 0, 0, 0, 0.4);
		shine_pattern->add_color_stop_rgba (1, 0, 0, 0, 0.0);
		cr->set_source (shine_pattern);
		cr->rectangle (0, 0, width, 7);
		cr->fill ();

	}
	
	if (false) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
		btm_pattern->add_color_stop_rgba (0, 0, 0, 0, 0.0);
		btm_pattern->add_color_stop_rgba (1, 0, 0, 0, 0.1);
		cr->set_source (btm_pattern);
		cr->rectangle (0, 0, width, height);
		cr->fill ();
	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha (mod.a() * 0.40);
		cr->begin_new_path();
	}

	//scratches
	if (false) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_vca->number()*25, -_vca->number()*10);  //offset the scratches
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha (mod.a() * 0.10);
		cr->begin_new_path();
	}

	cr->set_identity_matrix();

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++) {
		if ((*i)->visible()) {
			cr->set_identity_matrix();
			(*i)->render_bg(cr, Wide);
		}
	}
}

//------------------------------

MixbusSpillCanvas::MixbusSpillCanvas ()
{
	_knobs = new MixbusSpillBox(root());
}

//------------------------------


MixbusSpillBox::~MixbusSpillBox ()
{
}

MixbusSpillBox::MixbusSpillBox (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_spiller;

	_spill = new MixbusSpillButton(this);
	widgets.push_front(_spill);
	_spill->set_text( "Spill" );
	_spill->set_tooltip( _("Filter the channel list to show only input strips\nthat are assigned to this bus.") );
	_spill->set_color("mixbus spill button");

	_monitor_section_button = new MixbusActionToggle (this);
	widgets.push_front(_monitor_section_button);
	_monitor_section_button->set_color ( "mixbus monitor button" );
	_monitor_section_button->set_text ("Monitor >");
	_monitor_section_button->hide ();
	_monitor_section_button->button_release_event.connect (sigc::mem_fun(*this, &MixbusSpillBox::mon_button_release));

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusSpillBox::config_changed, this, _1), gui_context());
}

void
MixbusSpillBox::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	float h = ScreenSizer::user_scale();  //scale factor

	_spill->set_size(ArdourCanvas::Rect (0, 0, inWidth-6*h, inHeight-6*h));
	_spill->set_position(ArdourCanvas::Duple (3*h,4*h));

	_monitor_section_button->set_size(ArdourCanvas::Rect (0, 0, inWidth-6*h, inHeight-6*h));
	_monitor_section_button->set_position(ArdourCanvas::Duple ( 3*h,4*h ));

	end_visual_change();
}

void
MixbusSpillBox::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	if (rt->is_master()) {
		_spill->hide();
		config_changed ("use-monitor-bus"); // show/hide depending on mon-section.
	} else {
		_monitor_section_button->hide();
		_spill->set_stripable( rt );
		_spill->show();
	}

	end_visual_change();
}

void
MixbusSpillBox::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	MixbusStrip::render(area, cr);
}

void
MixbusSpillBox::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	bool flat = UIConfiguration::instance().get_flat_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	cr->set_identity_matrix();
	
//match the bottom color at the bus canvas
	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.10);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 8, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 8, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (false) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//special case for VU meter widgets (K-meter);  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
MixbusSpillBox::config_changed (string p)
{
	if (p == "use-monitor-bus") {
		if (_route && _route->is_master()) {
			update_monitor_section_button ();
			_monitor_section_button->show();
		}
	}
}

void
MixbusSpillBox::update_monitor_section_button ()
{
	if (ARDOUR::Config->get_use_monitor_bus()) {
		Glib::RefPtr<Action> act = ActionManager::get_action ("Mixer", "ToggleMonitorSection");
		_monitor_section_button->set_related_action (act);
		_monitor_section_button->set_tooltip (_("Show/Hide Monitoring Section"));
	} else {
		_monitor_section_button->set_related_action (Glib::RefPtr<Action> ());
		_monitor_section_button->set_tooltip (_("Add Monitoring Section"));
	}
}

bool
MixbusSpillBox::mon_button_release (GdkEventButton* ev)
{
	if (ev->button == 3) {
		using namespace Gtk::Menu_Helpers;
		Gtk::Menu* m = ARDOUR_UI_UTILS::shared_popup_menu ();
		MenuList& items = m->items ();
		/* there is no confirmation dialog using the context menu */
		if (ARDOUR::Config->get_use_monitor_bus()) {
			items.push_back (MenuElem (_("Remove Monitor Section"), []() {
						ARDOUR::Config->set_use_monitor_bus (false);
						ActionManager::get_toggle_action (X_("Monitor"), X_("UseMonitorSection"))->set_active (false);
						}));
		} else {
			items.push_back (MenuElem (_("Add Monitor Section"), []() {
						ARDOUR::Config->set_use_monitor_bus (true);
						ActionManager::get_toggle_action (X_("Monitor"), X_("UseMonitorSection"))->set_active (true);
						ActionManager::get_toggle_action (X_("Mixer"), X_("ToggleMonitorSection"))->set_active (true);
						}));
		}

		m->popup (ev->button, ev->time);
		return true;
	}

	if (ARDOUR::Config->get_use_monitor_bus()) {
		return false;
	}

	ArdourMessageDialog msg (_("Add monitoring section"),
                         false, MESSAGE_QUESTION, BUTTONS_YES_NO);
	msg.set_default_response (RESPONSE_YES);

	if (msg.run () == RESPONSE_YES) {
		ARDOUR::Config->set_use_monitor_bus (true);
		ActionManager::get_toggle_action (X_("Monitor"), X_("UseMonitorSection"))->set_active (true);
		ActionManager::get_toggle_action (X_("Mixer"), X_("ToggleMonitorSection"))->set_active (true);
	}
	return true;
}
