/*
    Copyright (C) 2014 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_VCA_STRIPS__
#define __MB_VCA_STRIPS__

#include <string>
#include <vector>

#include <pangomm/fontdescription.h>

#include <canvas/canvas.h>
#include <canvas/container.h>
#include <canvas/item.h>
#include <canvas/fill.h>
#include <canvas/outline.h>

#include <gtkmm/layout.h>

#include <ardour/route.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/automation_control.h>

#include "mixbus_widgets.h"
#include "mixbus_strips.h"
#include "route_ui.h"

class MixbusActionToggle;
class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;
class MixbusSpillButton;
class MixbusPhaseMeter;

class MixbusVcaStrip;
class MixbusVcaStripCanvas;

#define NATURAL_VCA_WIDTH 60
#define NATURAL_VCA_HEIGHT (348+28)

//input knobs
class MixbusVcaStripCanvas : public MixbusStripCanvas
{
public:
	MixbusVcaStripCanvas (std::shared_ptr<ARDOUR::VCA> vca);
	~MixbusVcaStripCanvas ();

	float natural_height() {return NATURAL_VCA_HEIGHT;}
};

class MixbusVcaStrip : public MixbusStrip
{
public:

	MixbusVcaStrip (ArdourCanvas::Item *, std::shared_ptr<ARDOUR::VCA> vca);
	~MixbusVcaStrip ();

	void scale_strip_to (float inWidth, float inHeight, bool wide);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void drop_references ();

	PBD::ScopedConnectionList _vca_connections;

	void route_property_changed (const PBD::PropertyChange&) {}

private:

	PBD::ScopedConnectionList   prop_connections;
	void vca_property_changed (PBD::PropertyChange const & what_changed);

	std::shared_ptr<ARDOUR::VCA> _vca;

	MixbusSpillButton *_spill;

	MixbusLabel *_label;

	MixbusVcaFader *_fader;
	MixbusToggle *_mute;
	MixbusToggle *_solo;

	MixbusSlate *slate;
};


//--------------

class MixbusSpillCanvas : public MixbusStripCanvas
{
public:
	MixbusSpillCanvas();

	float natural_height() {return NATURAL_SWITCHER_HEIGHT*2;}
};

class MixbusSpillBox : public MixbusStrip
{
public:

	MixbusSpillBox (ArdourCanvas::Item *);
	~MixbusSpillBox ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

private:
	void route_property_changed (const PBD::PropertyChange&) {}

	bool mon_button_release (GdkEventButton* ev);
	void update_monitor_section_button ();

	MixbusSpillButton* _spill;
	MixbusActionToggle* _monitor_section_button;

	PBD::ScopedConnectionList _config_connection;
	void config_changed (std::string);
};


#endif
