/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "ardour_message.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_panner.h"

#include "mixbus_bus_switcher.h"
#include "mixbus_comp_switcher.h"
#include "mixbus_eq_switcher.h"
#include "mixbus_gate_switcher.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------

//an MBCanvas for input knobs
MixbusStripPannerCanvas::MixbusStripPannerCanvas ()
{
	_knobs = new MixbusStripPanner(root());
}

float
MixbusStripPannerCanvas::natural_height ()
{
	float h = NATURAL_PANNER_HEIGHT;

	return h;
}


//-------------------------


MixbusStripPanner::~MixbusStripPanner ()
{
}

MixbusStripPanner::MixbusStripPanner (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_panner;

	master_pan = new MixbusPanKnob(this);
	widgets.push_front(master_pan);
	master_pan->set_color("mixbus pan knob");
#ifdef MIXBUS32C
	std::string pt[NUM_KNOB_TICKS] = { "L", "|", "|", "|", "|", "C", "|", "|", "|", "|", "R" };
#else
	std::string pt[NUM_KNOB_TICKS] = { "L", "|", "|", "|", "|", "|", "R" };
#endif
	master_pan->set_ticks( pt );
	master_pan->set_tooltip(_("Pan Left/Right"));

	master_asgn = new Mixbus_MasterToggle(this);
	widgets.push_front(master_asgn);
	master_asgn->set_text( "Mstr" );
	master_asgn->set_tooltip(_("Master Bus Assign"));

	stereo_width = new MixbusWidthKnob(this);
	widgets.push_front(stereo_width);
	stereo_width->set_tooltip(_("Stereo Width"));

	bus_switcher = new MixbusBusSwitcher(this, ToggleVisibility_MixerBus);
	bus_switcher->set_tooltip(_("Show Sends"));

	comp_switcher = new MixbusCompSwitcher(this, ToggleVisibility_MixerComp);
	comp_switcher->set_tooltip(_("Show Compressor"));

	//we like the bus comp switcher in a slightly different location, on the buses
	b_comp_switcher = new MixbusCompSwitcher(this, ToggleVisibility_MixerComp);
	b_comp_switcher->set_tooltip(_("Show Compressor"));

	eq_switcher = new MixbusEQSwitcher(this, ToggleVisibility_MixerEQ);
	eq_switcher->set_tooltip(_("Show EQ"));

	gate_switcher = new MixbusGateSwitcher(this, ToggleVisibility_MixerGate);
	gate_switcher->set_tooltip(_("Show Gate/Expander"));

	ctrls_switcher = new MixbusCtrlSwitcher(this, ToggleVisibility_MixerCtrls);
	ctrls_switcher->set_tooltip(_("Show Input Controls"));

	mb_master_asgn = new Mixbus_MasterToggle(this);
	widgets.push_front(mb_master_asgn);
	mb_master_asgn->set_text( "Mstr" );
	mb_master_asgn->set_tooltip(_("Master Bus Assign"));

//mstr limiter
	lim_in = new MixbusToggle(this);
	widgets.push_front(lim_in);
	lim_in->set_text("Limit");
	lim_in->set_tooltip( _("Limiter In/Out") );

//master limiter meter
	float met = -0.2;
	for (int i = 0 ; i< LIM_METER_CNT; i++) {
		lim_meter[i] = new MixbusReduxDot(this);
		lim_meter[i]->set_color( rgba_to_color (1,1,0,1) );
		widgets.push_front(lim_meter[i]);
		lim_meter[i]->set_thresh( dB_to_coefficient(met) );
		met -= 1.0;
	}

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusStripPanner::meter));
}

void
MixbusStripPanner::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scale factor

	const float toggle_height = h*17.0;
	const float knob_sz = h*23;

	//limiter controls
#ifdef MIXBUS32C
	lim_in->set_size(ArdourCanvas::Rect (0, 0, 48*h, toggle_height));
	lim_in->set_position(ArdourCanvas::Duple ( floor(5*h), floor(inHeight - 18*h)));

	float lw = (Wide ? floor(h*5) : floor(h*4) );  //led width
	float ls = lw+1;  //led stride
	for (int i = 0 ; i< LIM_METER_CNT; i++) {
		lim_meter[i]->set_size(ArdourCanvas::Rect (0, 0, lw, floor (h*9)));
		lim_meter[i]->set_position(ArdourCanvas::Duple ( floor(8*h + ls*i), floor(inHeight - 28*h)));
	}
#else
	lim_in->set_size(ArdourCanvas::Rect (0, 0, 48*h, toggle_height));
	lim_in->set_position(ArdourCanvas::Duple ( midp + 4*h, 49*h));

	float lw = (Wide ? floor(h*6) : floor(h*4) );  //led width
	float ls = lw+1;  //led stride
	for (int i = 0 ; i< LIM_METER_CNT; i++) {
		lim_meter[i]->set_size(ArdourCanvas::Rect (0, 0, lw, floor (h*9)));
		lim_meter[i]->set_position(ArdourCanvas::Duple ( floor(2*h + midp-LIM_METER_CNT*ls + ls*i), floor(h*(16))));
	}
#endif
	master_pan->set_size(ArdourCanvas::Rect (0, 0, knob_sz+3*h, knob_sz+3*h));
	master_pan->set_position(ArdourCanvas::Duple ( 13*h, (17)*h));

	master_asgn->set_size(ArdourCanvas::Rect (0, 0, 42*h, toggle_height));
	master_asgn->set_position(ArdourCanvas::Duple ( 5*h, (58)*h ));

	stereo_width->set_size(ArdourCanvas::Rect (0, 0, 33*h, toggle_height-4));
	stereo_width->set_position(ArdourCanvas::Duple ( 9*h, 47*h));

	mb_master_asgn->set_size(ArdourCanvas::Rect (0, 0, 48, toggle_height));
	mb_master_asgn->set_position(ArdourCanvas::Duple ( 4*h, (58)*h ));

	int switcher_height = 12;

	b_comp_switcher->set_size(ArdourCanvas::Rect (0, 0, 47*h, switcher_height*h));
	b_comp_switcher->set_position(ArdourCanvas::Duple ( floor(inWidth - 47*h - 4*h), inHeight -2*h - 14*h * 5 ));

	ctrls_switcher->set_size(ArdourCanvas::Rect (0, 0, 47*h, switcher_height*h));
	ctrls_switcher->set_position(ArdourCanvas::Duple ( floor(inWidth - 47*h - 4*h), inHeight -2*h - 14*h * 5 ));

	gate_switcher->set_size(ArdourCanvas::Rect (0, 0, 47*h, switcher_height*h));
	gate_switcher->set_position(ArdourCanvas::Duple ( floor(inWidth - 47*h - 4*h), inHeight -2*h - 14*h * 4 ));

	comp_switcher->set_size(ArdourCanvas::Rect (0, 0, 47*h, switcher_height*h));
	comp_switcher->set_position(ArdourCanvas::Duple ( floor(inWidth - 47*h - 4*h), inHeight -2*h - 14*h * 3 ));

	eq_switcher->set_size(ArdourCanvas::Rect (0, 0, 47*h, switcher_height*h));
	eq_switcher->set_position(ArdourCanvas::Duple ( floor(inWidth - 47*h - 4*h), inHeight -2*h - 14*h * 2 ));

	bus_switcher->set_size(ArdourCanvas::Rect (0, 0, 47*h, switcher_height*h));
	bus_switcher->set_position(ArdourCanvas::Duple ( floor(inWidth - 47*h - 4*h), inHeight -2*h - 14*h * 1 ));

	end_change();  //TODO ?
}

void
MixbusStripPanner::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	int mixbus_num = rt->mixbus();
	bool is_mixbus = mixbus_num != 0;
	bool is_master = rt->is_master();
	bool is_input = !is_master && !is_mixbus;

	_bg_pattern_type = is_mixbus ? bg_pattern_panner_bus : (is_master ? bg_pattern_panner_master :  bg_pattern_panner );

	MixbusStrip::set_strip_route(rt, mixer_owned);

	if (is_input) {
		bus_switcher->show();
		bus_switcher->set_strip_route(rt, mixer_owned);  //Ben TODO:  set the vis message here, too  (inputs, buses or trigger window?)

		comp_switcher->show();
		comp_switcher->set_strip_route(rt, mixer_owned);

		b_comp_switcher->hide();
	} else {
		comp_switcher->hide();

		b_comp_switcher->show();
		b_comp_switcher->set_strip_route(rt, mixer_owned);
	}

	ctrls_switcher->hide();
	eq_switcher->hide();
	gate_switcher->hide();

	if (is_input) {
		ctrls_switcher->show();
		ctrls_switcher->set_strip_route(rt, mixer_owned);

		eq_switcher->show();
		eq_switcher->set_strip_route(rt, mixer_owned);

		gate_switcher->show();
		gate_switcher->set_strip_route(rt, mixer_owned);

		master_pan->show();
		master_pan->set_route( rt, rt->pan_azimuth_control() );

		master_asgn->show();
		master_asgn->set_route( rt, rt->master_send_enable_controllable() );
	}

	if (is_mixbus) {
		stereo_width->show();
		stereo_width->set_route( rt, rt->pan_width_control() );

		master_pan->show();
		master_pan->set_route( rt, rt->pan_azimuth_control() );

		master_asgn->show();
		master_asgn->set_route( rt, rt->master_send_enable_controllable() );
	}

	if (is_master) {
		lim_in->show();
		lim_in->set_route( rt, rt->master_limiter_enable_controllable() );

		for (int i = 0 ; i< LIM_METER_CNT; i++) {
			lim_meter[i]->show();
			lim_meter[i]->set_route( rt, rt->master_limiter_mtr_controllable() );
		}
	}
	end_visual_change();  //TODO ?
}


void
MixbusStripPanner::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scale factor

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	bool is_master = _route->is_master();
	bool is_mixbus = _route->mixbus() != 0;
	bool is_input = !is_master && !is_mixbus;

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	/* puddle color */
	SVAModifier bord = UIConfiguration::instance().modifier ("strip border alpha");
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double puddle_r,puddle_g,puddle_b;
	Gtkmm2ext::color_to_rgba( puddle_color, puddle_r, puddle_g, puddle_b, unused);

	//panner puddle
	if (true) {
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		Gtkmm2ext::rounded_rectangle (cr, 0, 0, width, height, boxy ? 0 : 5*h);
		cr->fill ();
	}

	//Width puddle
	if (false /*is_mixbus*/) {
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		Gtkmm2ext::rounded_rectangle (cr, 50*h, -8*h, 64*h, 56*h, 8*h);
		cr->fill ();
	}

	//Width decorator
	if (false /*is_mixbus*/) {
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Width" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(width - 2*h - 21*h - (tw/2.)), floor(30*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS*0.6);
		layout->show_in_cairo_context (cr);
	}

	cr->set_identity_matrix();

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 6*h, height );
		cr->fill ();
	}

	//drop-shadow at top
	if (false /*!flat  && !is_input*/) {
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 8*h);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 7*h );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//section bounds
	cr->set_line_width (1.0*h);
	cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
	Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width-2*h, height-2*h, 5*h);
	cr->stroke ();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat &&  is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//special case for VU meter widgets;  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
MixbusStripPanner::meter ()
{
	//limiter meters
	for (int i = 0 ; i< LIM_METER_CNT; i++) {
		lim_meter[i]->meter();
	}
}

void
MixbusStripPanner::route_property_changed (const PBD::PropertyChange& what_changed)
{

}
