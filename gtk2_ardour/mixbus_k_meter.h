/*
    Copyright (C) 2014 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_K14__
#define __MB_K14__

#include <string>
#include <vector>

#include <pangomm/fontdescription.h>

#include <canvas/canvas.h>
#include <canvas/container.h>
#include <canvas/item.h>
#include <canvas/fill.h>
#include <canvas/outline.h>

#include <gtkmm/layout.h>

#include <ardour/route.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/automation_control.h>

#include "mixbus_widgets.h"
#include "mixbus_strips.h"
#include "route_ui.h"

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;
class MixbusPhaseMeter;
class MixbusSpillButton;
class MixbusPhaseMeter;

class MixbusK14;
class MixbusK14Canvas;

#define NATURAL_K14_HEIGHT (80)

//--------------

class Mixbus_K_Canvas : public MixbusStripCanvas
{
public:
	Mixbus_K_Canvas();

	float natural_height() {return NATURAL_K14_HEIGHT;}
};

class Mixbus_K_Meter : public MixbusStrip
{
public:

	Mixbus_K_Meter (ArdourCanvas::Item *);
	~Mixbus_K_Meter ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void route_property_changed (const PBD::PropertyChange&) {}

	MixbusPointerMeter	*k_meter;

	MixbusPhaseMeter	*c_meter;

	void meter();

private:
	sigc::connection meter_connection;
};


#endif
