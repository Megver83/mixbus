/*
    Copyright (C) 2014 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include "pbd/compose.h"
#include "pbd/convert.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/meter.h"
#include "ardour/solo_isolate_control.h"
#include "ardour/utils.h"
#include "ardour/value_as_string.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"
#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "timers.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_fader.h"

#include "pbd/i18n.h"

using namespace std;
using namespace PBD;
using namespace ArdourCanvas;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;
using namespace ARDOUR;

#ifdef PLATFORM_WINDOWS
#define random() rand()
#endif

//#define LAYOUT_HELPER 1

static const float TWO_PI = (M_PI*2.0);

#define CAPTION_BRIGHTNESS 0.80

Glib::RefPtr<Gdk::Pixbuf> meter_texture;
Glib::RefPtr<Gdk::Pixbuf> harrison_bug_img;

float
f_range_check( float min, float test, float max)
{
	if (test < min) return min;
	if (test > max) return max;
	return test;
}

MixbusWidget::MixbusWidget (ArdourCanvas::Item *c) : Item ( c )
{
	_highlight = false;
	_grabbed = false;
}

MixbusWidget::~MixbusWidget ()
{
	ClearConnections();
}

void
MixbusWidget::ClearConnections ()
{
	_connection.disconnect();

//	binding_proxy.set_controllable(NULL);
	_auto_ctrl.reset();
	_ro_ctrl.reset();
	_stripable.reset();
}

void
MixbusWidget::set_size (ArdourCanvas::Rect const & area)
{
	if (_rect != area) {
		begin_change ();
		_rect = area;
		set_bbox_dirty ();
		end_change ();
	}
}

void
MixbusWidget::update_display ()
{
	cout << "MixbusWidget::update_display  ... some widget needs to override this\n" << endl;
}

void
MixbusWidget::compute_bounding_box () const
{
	if (!_rect.empty()) {
		_bounding_box = _rect;
	}
	set_bbox_clean ();
}



// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

MixbusVcaFader::MixbusVcaFader ( Item* c)
	: MixbusWidget ( c )
{
	Event.connect (sigc::mem_fun (*this, &MixbusVcaFader::vca_event));
}

MixbusVcaFader::~MixbusVcaFader ()
{
	_connection.disconnect();
	_auto_ctrl.reset();
	//_stripable.reset();
}

void
MixbusVcaFader::set_vca_controllable( std::shared_ptr<ARDOUR::AutomationControl> ctrl, std::shared_ptr<VCA> v )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_auto_ctrl = ctrl;

	_vca = v;

	binding_proxy.set_controllable(_auto_ctrl);

	//get initial value
	if (_auto_ctrl ) {
		_normal = _auto_ctrl->internal_to_interface ( _auto_ctrl->normal () );  //weird for Amps
		_last_val = _auto_ctrl->get_interface ();
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusVcaFader::update_display, this), gui_context ());
		redraw();
	}
}

void
MixbusVcaFader::update_display(  )
{
//PBD::stacktrace(cout);

	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}

}


bool
MixbusVcaFader::vca_event ( GdkEvent* event)
{
	switch (event->type) {
		case GDK_SCROLL:
			{

				GdkEventScroll *ev = (GdkEventScroll *)event;

				/* ignore left/right scrolling */
				if ( event->scroll.direction == GDK_SCROLL_LEFT )
					return true;
				if ( event->scroll.direction == GDK_SCROLL_RIGHT )
					return true;
				//if (ev->state & Keyboard::TertiaryModifier) { return false; }

				float scale = 0.02;  //by default, we step in 1/50th of the knob travel
				if (ev->state & Keyboard::GainFineScaleModifier) {
					if (ev->state & Keyboard::GainExtraFineScaleModifier) {
						scale *= 0.01;
					} else {
						scale *= 0.10;
					}
				}

				std::shared_ptr<PBD::Controllable> c = _auto_ctrl;//binding_proxy.get_controllable();
				if (c) {
					//fake a psedo-Latch mode by switching to write
					if (_auto_ctrl->automation_state() == Touch)
						_auto_ctrl->set_automation_state(Write);

					float val = c->get_interface();

					if ( ev->direction == GDK_SCROLL_UP )
						val += scale;
					else
						val -= scale;

					c->set_interface(val);
				}

				return true;

			}
			break;

		case GDK_BUTTON_PRESS:
			{
				GdkEventButton *ev = (GdkEventButton*) event;

				if (binding_proxy.button_press_handler (ev)) {
					return true;
				}

				start_touch(_auto_ctrl);
				grab();

				_grabbed_y = ev->y;
				_grabbed = true;
				_highlight = true;

				// set_active_state (Gtkmm2ext::ExplicitActive);

				return true;
			}
			break;

		case GDK_2BUTTON_PRESS:
			{
				/* double-click to reset to default */
				GdkEventButton *ev = (GdkEventButton*) event;
				if (_auto_ctrl &&  ev->type == GDK_2BUTTON_PRESS) {  //double click
					_auto_ctrl->set_value (_auto_ctrl->normal(), Controllable::NoGroup);
					return true;
				}
			}
			break;

		case GDK_BUTTON_RELEASE:
			if (_grabbed) {
				ungrab();
				stop_touch(_auto_ctrl);
				_grabbed = false;
				redraw();
			}
			return true;
			break;

		case GDK_ENTER_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> (_auto_ctrl));
			}
			//update_slate ();
			_highlight = true;
			redraw();
			break;

		case GDK_LEAVE_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> ());
			}
			_highlight = false;
			redraw();
			break;
		case GDK_MOTION_NOTIFY:
			if (_grabbed) {

				GdkEventMotion *ev = (GdkEventMotion *)event;

				//scale the adjustment based on keyboard modifiers
				float scale = 0.0025;
				if (ev->state & Keyboard::GainFineScaleModifier) {
					if (ev->state & Keyboard::GainExtraFineScaleModifier) {
						scale *= 0.10;
					} else {
						scale *= 0.10;
					}
				}

				//calculate the travel of the mouse
				int y_delta = 0;
				y_delta = _grabbed_y - ev->y;
				_grabbed_y = ev->y;
				if (y_delta == 0) return TRUE;

				//step the value of the controllable
				float val = 0.0;
				std::shared_ptr<PBD::Controllable> c = _auto_ctrl; //binding_proxy.get_controllable();
				if (c) {
					val = c->get_interface();
					val += y_delta * scale;
				}

				if (val < 0.0) val = 0.0;  //range check;  especially important for logarithmic or fader-type controls!

				c->set_interface(val);
			}

			return true;
			break;
		default:
			break;
	}
	return false;
}

void
MixbusVcaFader::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{
#ifdef LAYOUT_HELPER
	return;
#endif

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	Distance height = self.height();
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

	/* draw background */

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	float fader_width = width-10*h;
	float fader_height = fader_width*1.4;
	float travel = height - fader_height - 4*h;

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//draw scale (?)
	float scale_vals[9] = { 6, 3, 0, -3, -6, -12, -24, -40, -200 };
	const char* scale_txt[9] = { "6", "3", "0", "-3", "-6", "12", "24", "40", "90" };
	cr->set_line_width (0.75);
	for (int i =0; i< 9; i++) {
		float val = dB_to_coefficient(scale_vals[i]);
		val = gain_to_slider_position_with_max (val, Config->get_max_gain ());
		val = 2*h+fader_height/2 + travel*(1.0-val);

		cr->move_to( 0, val );
		cr->line_to( width-2*h, val );
		float bscale = scale_vals[i] == 0 ? 0.9 : 1.2;
		cr->set_source_rgba (fg_r*bscale, fg_g*bscale, fg_b*bscale, 1.0 );
		cr->stroke();

		//only show the text if we are "wide"
//		if (wide ) {
			//text
			layout->set_text (scale_txt[i]);
			int tw,  th;
			layout->get_pixel_size (tw, th);

			Cairo::Matrix m = cr->get_matrix();
			cr->translate( floor(-1*h-tw), floor(val- th/2));
			layout->show_in_cairo_context (cr);
			cr->set_matrix(m);
//		}
	}

	//slot
	bool flat = false;//UIConfiguration::instance().get_flat_buttons();
	if (!flat) {
		cr->translate( 1,1);
		cr->set_source_rgba (1,1,1,0.1);
		Gtkmm2ext::rounded_rectangle (cr, 2 -3 + fader_width/2, -2 + fader_height/2, 6, travel + 10, 3);
		cr->fill ();
	}
	cr->translate( -1,-1);
	cr->set_source_rgba (0,0,0,1.0);
	Gtkmm2ext::rounded_rectangle (cr, 2 -3 + fader_width/2, -2 + fader_height/2, 6, travel + 10, 3);
	cr->fill ();

	cr->set_identity_matrix();
}

void
MixbusVcaFader::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
//	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	float fader_width = width-10*h;
	float fader_height = fader_width*1.4;
	float _corner_radius = width*0.05;
	float travel = height - fader_height - 4*h;

	float val = f_range_check(0, _last_val, 1.0);
	float dist = travel * (1.0-val);

//did this already in bg_render

	bool flat = UIConfiguration::instance().get_flat_buttons();

	cr->translate (0, dist+2*h);

	//shadow
	if (!flat) {
		cr->translate (4*h, 4*h);
		cr->set_source_rgba (0, 0, 0, 0.4);
		Gtkmm2ext::rounded_rectangle (cr, 2*h, 0, fader_width, fader_height, _corner_radius+2*h);
		cr->fill ();
		cr->translate (-4*h, -4*h);
	}

	//fader cap
	if ( _vca && UIConfiguration::instance().get_use_route_color_widely() ) {
		double color_r,color_g,color_b,unused;
		Gtkmm2ext::color_to_rgba( _vca->presentation_info().color (), color_r, color_g, color_b, unused);
		cr->set_source_rgba( (color_r+0.5)/1.5, (color_g+0.5)/1.5, (color_b+0.5)/1.5, 1 );
	} else {
		uint32_t generic_color = UIConfiguration::instance().color ("mixbus fader: fill");
		double color_r,color_g,color_b,unused;
		Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);
		cr->set_source_rgba (color_r, color_g, color_b, 1);
	}
	Gtkmm2ext::rounded_rectangle (cr, 2*h, 0, fader_width, fader_height, _corner_radius);
	cr->fill ();
	cr->set_line_width (2*h);
	cr->set_source_rgba (0,0,0,1);
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_line_width (3*h);
		cr->set_source_rgba (0.7, 0.7, 0.7,1);
	}
	Gtkmm2ext::rounded_rectangle (cr, 2*h, 0, fader_width, fader_height, _corner_radius);
	cr->stroke ();

	cairo_t *ct = cr->cobj();

	if (!flat) {
		//gradient
		cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0.0, 0.0,  fader_height);  //note we have to offset the gradient from our centerpoint
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.4);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.25, 0,0,0, 0.4);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.5, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.0);
		cairo_set_source (ct, shade_pattern);
		cairo_rectangle (ct, 2*h, fader_height/4, fader_width, fader_height/4);
		cairo_fill (ct);
		cairo_pattern_destroy (shade_pattern);

		shade_pattern = cairo_pattern_create_linear (0.0, 0.0, 0.0,  fader_height);  //note we have to offset the gradient from our centerpoint
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.74, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.75, 0,0,0, 0.2);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.6);
		cairo_set_source (ct, shade_pattern);
		cairo_rectangle (ct, 2*h, fader_height*0.75, fader_width, fader_height/4);
		cairo_fill (ct);
		cairo_pattern_destroy (shade_pattern);
	}

	//mid line
	cairo_set_source_rgba (ct, 0,0,0, 1);
	cairo_set_line_width (ct, 0.75);
	cairo_move_to (ct, 2*h, fader_height*0.5 );
	cairo_line_to (ct, 2*h+fader_width, fader_height*0.5);
	cairo_stroke (ct);
}

void
MixbusVcaFader::start_touch (std::shared_ptr<ARDOUR::AutomationControl> ctrl)
{
	ctrl->start_touch (timepos_t (ctrl->session().transport_sample()));
}

void
MixbusVcaFader::stop_touch (std::shared_ptr<ARDOUR::AutomationControl> ctrl)
{
	ctrl->stop_touch (timepos_t (ctrl->session().transport_sample()));
}

// ---------------------------------------------------------------------
MixbusKnob::MixbusKnob ( Item* c)
	: MixbusWidget ( c )
{
	Event.connect (sigc::mem_fun (*this, &MixbusKnob::knob_event));
	_color = "mixbus widget knob default";

	_rotary = true;

	int randr = random() % 255;
	_blue_desat = 1.0;
	if (randr < 64) _blue_desat = .98;
	if (randr < 32) _blue_desat = .95;
	if (randr < 4) _blue_desat = .88;

	for (int i = 0; i <NUM_KNOB_TICKS; i++)
		ticks[i] = "";

	UIConfiguration::instance().ColorsChanged.connect (sigc::mem_fun (*this, &MixbusKnob::color_handler));
	color_handler();

	_last_concentric_enable_val = _last_concentric_knob_val = 0.0;
}

MixbusKnob::~MixbusKnob ()
{
	_concentric_enable_connection.disconnect();
	_concentric_enable_ctrl.reset();

	_concentric_knob_connection.disconnect();
	_concentric_knob_ctrl.reset();

	_connection.disconnect();
	_auto_ctrl.reset();
	_stripable.reset();
}

void
MixbusKnob::ClearConnections ()
{
	MixbusWidget::ClearConnections ();

	_concentric_enable_ctrl.reset();
	_concentric_knob_ctrl.reset();
}

void
MixbusKnob::color_handler ()
{
	redraw();
}


void
MixbusKnob::set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;
	_auto_ctrl = ctrl;

	binding_proxy.set_controllable(_auto_ctrl);

	//get initial value
	if (_auto_ctrl ) {
		_normal = _auto_ctrl->internal_to_interface(_auto_ctrl->normal());
		_last_val = _auto_ctrl->get_interface (_rotary);
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusKnob::update_display, this), gui_context ());
		redraw();
	}
}

void
MixbusKnob::set_controllable( std::shared_ptr<ARDOUR::Stripable> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;
	_auto_ctrl = ctrl;

	binding_proxy.set_controllable(_auto_ctrl);

	//get initial value
	if (_auto_ctrl ) {
		_normal = _auto_ctrl->internal_to_interface ( _auto_ctrl->normal () );  //weird for Amps
		_last_val = _auto_ctrl->get_interface (_rotary);
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusKnob::update_display, this), gui_context ());
		redraw();
	}


}

//#include "pbd/stacktrace.h"

void
MixbusKnob::update_display(  )
{
//PBD::stacktrace(cout);

	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface (_rotary);
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
	if (_concentric_knob_ctrl && _concentric_enable_ctrl) {
		float val = _concentric_knob_ctrl->get_interface (_rotary);
		if ( val != _last_concentric_knob_val ) {
			_last_concentric_knob_val = val;
			redraw();
		}
		val = _concentric_enable_ctrl->get_interface (_rotary);
		if ( val != _last_concentric_enable_val ) {
			_last_concentric_enable_val = val;
			redraw();
		}
	}

}

void
MixbusKnob::update_slate (std::shared_ptr<ARDOUR::AutomationControl> ac)
{
	MixbusStrip *parent = dynamic_cast<MixbusStrip *> (Item::parent());
	if (!ac) {
		ac = _auto_ctrl;
	}
	if (!ac) {
		return;
	}
	// TODO: use ac->get_user_string ()
	if (ac == _concentric_knob_ctrl) {
		float val = ac->get_value ();
		char theBuf[32];
		theBuf[0] = 0;
		sprintf(theBuf, "%2.1f", -90 + 180*val);
		parent->set_slate_text (theBuf);
	} else {
		float val = ac->get_value ();
		// XXX: we should really use AutomationControl::get_user_string here,
		// but we'd first need to move slate_conversion_func() there,
		// which isn't really possible (knob-type is used to idenify parameter)
		switch (_auto_ctrl->desc().type) {
			case BusSendLevel:
			case GainAutomation:
			case TrimAutomation:
			val = accurate_coefficient_to_dB (val);
			break;
			default:
			break;
		}
		char theBuf[32];
		theBuf[0] = 0;
		slate_conversion_func(theBuf, val);
		parent->set_slate_text (theBuf);
	}
}

void
MixbusKnob::start_touch ( std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	if (ctrl) {
		ctrl->start_touch (timepos_t (ctrl->session().transport_sample()));
	}
}

void
MixbusKnob::stop_touch (  std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	if (ctrl) {
		ctrl->stop_touch (timepos_t (ctrl->session().transport_sample()));
	}
}

bool
MixbusKnob::knob_event ( GdkEvent* event)
{
	switch (event->type) {
		case GDK_SCROLL:
			{
				GdkEventScroll *ev = (GdkEventScroll *)event;
					// XXX hack allow to override group
					bool override_group =  (Keyboard::the_keyboard().key_is_down (GDK_Shift_R) || Keyboard::the_keyboard().key_is_down (GDK_Shift_L));

				/* ignore left/right scrolling */
				if ( event->scroll.direction == GDK_SCROLL_LEFT )
					return true;
				if ( event->scroll.direction == GDK_SCROLL_RIGHT )
					return true;
				// if (ev->state & Keyboard::TertiaryModifier) { return false; }

				float scale = 0.02;  //by default, we step in 1/50th of the knob travel
				if (ev->state & Keyboard::GainFineScaleModifier) {
					if (ev->state & Keyboard::GainExtraFineScaleModifier) {
						scale *= 0.01;
					} else {
						scale *= 0.10;
					}
				}

				std::shared_ptr<ARDOUR::AutomationControl> c = _auto_ctrl;

				/* For concentric-knobs, "TertiaryModifier" isn't a group, but rather the concentric panner control */
				if (override_group && _concentric_knob_ctrl) {
					c = _concentric_knob_ctrl;
				}

				if (c) {
					/* fake a psedo-Latch mode by switching to write */
					if (c->automation_state() == Touch)
						c->set_automation_state(Write);

					float val = c->get_interface(_rotary);

					if ( ev->direction == GDK_SCROLL_UP )
						val += scale;
					else
						val -= scale;

					if ( dynamic_cast<MixbusFader*>(this) ) { // special case fader for track-groups
						if (val < 0.0) val = 0.0;
						_auto_ctrl->set_value (_auto_ctrl->interface_to_internal(val), override_group ? Controllable::NoGroup : Controllable::UseGroup);
					} else if (c) {
						c->set_interface(val, _rotary);
					}

					update_slate (c);
				}
				return true;

			}
			break;

		case GDK_BUTTON_PRESS:
			{
				GdkEventButton *ev = (GdkEventButton*) event;

				if (binding_proxy.button_press_handler (ev)) {
					return true;
				}

				std::shared_ptr<ARDOUR::AutomationControl> c = _auto_ctrl;
				if (_concentric_knob_ctrl && event->button.button == 1 && (ev->state & Keyboard::TertiaryModifier)) {
					c = _concentric_knob_ctrl;
				}
				start_touch (c);

				grab();

				_grabbed_y = ev->y;
				_grabbed = true;
				_highlight = true;

				// set_active_state (Gtkmm2ext::ExplicitActive);

				update_slate (c);

				return true;
			}
			break;

		case GDK_2BUTTON_PRESS:
			{
				GdkEventButton *ev = (GdkEventButton*) event;

				/* shift+double-click to enable/disable concentric knob */
				if ( event->button.button == 1 && (ev->state & Keyboard::TertiaryModifier) ) {
					if (_concentric_enable_ctrl) {
						std::shared_ptr<PBD::Controllable> c = _concentric_enable_ctrl;
						float val = _concentric_enable_ctrl->get_value() > 0.5 ? 0.0 : 1.0;
						_concentric_enable_ctrl->set_value(val, PBD::Controllable::NoGroup);
						update_slate (_concentric_enable_ctrl);
					}
					return true;
				} else if (_auto_ctrl) {  //double click to reset to default
					_auto_ctrl->set_value (_auto_ctrl->normal(), Controllable::NoGroup);
					update_slate ();
					return true;
				}
			}
			break;

		case GDK_BUTTON_RELEASE:
			{
				GdkEventButton *ev = (GdkEventButton*) event;
				if (_grabbed) {
					ungrab();
					if ( _concentric_knob_ctrl && event->button.button == 1 && (ev->state & Keyboard::TertiaryModifier) ) {
						stop_touch(_concentric_knob_ctrl);
					} else {
						stop_touch(_auto_ctrl);
					}
					_grabbed = false;
					redraw();
				}
				return true;
			}
			break;

		case GDK_ENTER_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> (_auto_ctrl));
			}
			//update_slate ();
			_highlight = true;
			redraw();
			break;

		case GDK_LEAVE_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> ());
			}
			_highlight = false;
			redraw();
			break;

		case GDK_MOTION_NOTIFY:
			if (_grabbed) {

				GdkEventMotion *ev = (GdkEventMotion *)event;

				//scale the adjustment based on keyboard modifiers
				float scale = 0.0025;
				if (ev->state & Keyboard::GainFineScaleModifier) {
					if (ev->state & Keyboard::GainExtraFineScaleModifier) {
						scale *= 0.10;
					} else {
						scale *= 0.10;
					}
				}

				//calculate the travel of the mouse
				int y_delta = 0;
				y_delta = _grabbed_y - ev->y;
				_grabbed_y = ev->y;
				if (y_delta == 0) return TRUE;

				//step the value of the controllable
				float val = 0.0;
				std::shared_ptr<ARDOUR::AutomationControl> c = _auto_ctrl; //binding_proxy.get_controllable();
				if (c) {
					val = c->get_interface(_rotary);
					float delta = y_delta * scale;
					val += delta;
				}

				if (val < 0.0) val = 0.0;  //range check;  especially important for logarithmic or fader-type controls!

				if ( dynamic_cast<MixbusFader*>(this) ) {  //this is the fader case
					// XXX hack allow to override group
					bool override_group =  (Keyboard::the_keyboard().key_is_down (GDK_Shift_R) || Keyboard::the_keyboard().key_is_down (GDK_Shift_L));
					_auto_ctrl->set_value (_auto_ctrl->interface_to_internal(val), override_group ? Controllable::InverseGroup : Controllable::UseGroup);
				} else if (_concentric_knob_ctrl && ev->state & Keyboard::TertiaryModifier) {
					c = _concentric_knob_ctrl;
					if (c) {
						val = c->get_interface(_rotary);
						val += y_delta * scale;
						if (val < 0.0) val = 0.0;  //range check;  especially important for logarithmic or fader-type controls!
						c->set_interface(val, _rotary);
					}
				} else if (c) {
					c->set_interface(val, _rotary);
				}

				update_slate (c);
			}
			return true;
			break;
		default:
			break;
	}
	return false;
}

#ifdef MIXBUS32C

void
MixbusKnob::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{
	//look up my color
	uint32_t generic_color = UIConfiguration::instance().color (string_compose ("%1: fill", _color));
	double color_r,color_g,color_b,unused;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);

	bool flat = UIConfiguration::instance().get_flat_buttons();

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	Distance height = self.height();
	Distance halfh = height/2;

	cr->set_identity_matrix();

#ifdef LAYOUT_HELPER
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, self.width(), height);
	cr->stroke ();
	return;
#endif

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b,unused);

	//setup for text
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//these text offsets make sure the hard left side is right-justified, the top tick is center justified, and the hard right side is left-justified
#ifdef MIXBUS32C
	float horz_offset[NUM_KNOB_TICKS] = { 0.65, 0.75, 1.0, 0.85, 0.65, 0.5, 0.35, 0.15, 0.0, 0.25, 0.35};
#else
	float horz_offset[NUM_KNOB_TICKS] = { 0.75, 1.0, 0.65, 0.5, 0.35, 0.0, 0.25};
#endif

	//tickmarks
	cr->set_line_width(1);
	cr->set_source_rgba (fg_r,fg_g,fg_b, LEGEND_BRIGHTNESS);
	for (int r = 0; r<NUM_KNOB_TICKS; r++) {

		bool show_tick = (ticks[r] == "|");
		if (show_tick) {
			cr->set_identity_matrix();
			cr->translate (self.x0-0.5 + halfh, self.y0-0.5 + halfh);  //locate to center of knob

			double angle = TWO_PI*0.125 + TWO_PI*((float)r/(float)(NUM_KNOB_TICKS-1))*0.75;
			cr->rotate(angle);

			cr->move_to (0, halfh*0.8 );
			cr->line_to (0, halfh*1.1);

			cr->stroke ();
		}

		if (ticks[r] == "*") {
			cr->set_identity_matrix();
			cr->translate (self.x0-0.5 + halfh, self.y0-0.5 + halfh);  //locate to center of knob

			cr->set_line_width(1.5);
			cr->move_to (0, 0 );
			cr->line_to ( halfh*1.2, 0 );
			cr->stroke ();
			cr->set_line_width(1.0);
		}

		bool show_text = (ticks[r] != "" && ticks[r] != "|" && ticks[r] != "*");
		if (show_text) {
			cr->set_identity_matrix();
			cr->translate (self.x0-0.5 + halfh, self.y0-0.5 + halfh);  //locate to center of knob
			layout->set_text (ticks[r]);
			int w,  h;
			layout->get_pixel_size (w, h);
			cr->translate ( -w*horz_offset[r], -h/2.0 );

			double angle = TWO_PI*0.125 + TWO_PI*((float)r/(float)(NUM_KNOB_TICKS-1))*0.75;
			cr->rotate(angle);
			cr->translate (0, halfh*1.25);
			cr->rotate(-angle);
			layout->show_in_cairo_context (cr);
		}

	}

	if (!flat) {
		//shadow
		cr->set_identity_matrix();
		cr->translate (self.x0+height*0.08, self.y0-0.5+height*0.18);
		cr->set_source_rgba (0, 0, 0, 0.3);
		cr->arc(halfh, halfh, halfh*1.0, 0, TWO_PI );
		cr->fill ();
	}

	cr->set_identity_matrix();
}

void
MixbusKnob::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	//look up my color
	uint32_t generic_color = UIConfiguration::instance().color (string_compose ("%1: fill", _color));
	double color_r,color_g,color_b,unused;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);
	color_b *= _blue_desat;

	uint32_t line_color = UIConfiguration::instance().color (string_compose ("%1: line", _color));
	double line_color_r,line_color_g,line_color_b;
	Gtkmm2ext::color_to_rgba( line_color, line_color_r, line_color_g, line_color_b, unused);

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();

	bool flat = UIConfiguration::instance().get_flat_buttons();

	bool do_concentric = _concentric_enable_ctrl && _concentric_enable_ctrl->get_value() > 0.5;

	float kval = _last_val;

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, self.width(), self.height());
	cr->stroke ();
	return;
#endif

	/* locate to knob, for remainder  */
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	//highlight
	if ( (UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.2);
		cr->arc(halfh, halfh, halfh*0.99, 0, TWO_PI );
		cr->fill ();
	}

	//black outline
//	cr->set_line_width( OUTLINE_WIDTH );
	cr->set_source_rgba (0, 0, 0, 1);
	cr->arc(halfh, halfh, halfh*0.87, 0, TWO_PI );
	cr->fill ();

	//knob projections
	if ( !do_concentric ) {
		float brightness;
		float fract = f_range_check(0, kval, 1.0);
		double angle = TWO_PI*0.125 + TWO_PI*fract*0.75;
		angle += M_PI;

		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(height/2., height/2.);
		cr->rotate(angle);
		cr->translate(-height*.15, -height*0.97/2);
		Gtkmm2ext::rounded_rectangle (cr, 0, 0, height*0.3, height*0.1, height*0.05);
		brightness =  0.1 + 0.1*(1.0 - fabs(angle - (1.75*M_PI)));
		cr->set_source_rgba (brightness, brightness, brightness, 1);
		cr->fill ();

		angle += M_PI;
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(height/2., height/2.);
		cr->rotate(angle);
		cr->translate(-height*.15, -height*0.97/2);
		Gtkmm2ext::rounded_rectangle (cr, 0, 0, height*0.3, height*0.1, height*0.05);
		float b_angle = angle < 2.75*M_PI ? angle : angle-2.*M_PI;
		brightness =  0.1 + 0.2*(1.0 - fabs(b_angle - (1.75*M_PI)));
		cr->set_source_rgba (brightness, brightness, brightness, 1);
		cr->fill ();
	}

	/* locate to knob, for remainder  */
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	//concentric
	if (do_concentric ) {

		float pan = _concentric_knob_ctrl->get_interface(_rotary);
		float fract = f_range_check(0, pan, 1.0);
		double angle = TWO_PI*0.125 + TWO_PI*fract*0.75;
		angle += M_PI;

		//black concentric knob projection
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(height/2., height/2.);
		cr->rotate(angle);
		cr->translate(-width*.15, -width*0.92/2);
		Gtkmm2ext::rounded_rectangle (cr, 0, 0, width*0.3, width*0.1, width*0.05);
		cr->set_source_rgba (0, 0, 0, 1);
		cr->fill ();

		//draw white line
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(height/2., height/2.);
		cr->rotate(angle);
		cr->translate(-width*.05, -width*0.88/2);
		Gtkmm2ext::rounded_rectangle (cr, 0, 0, width*0.1, width*0.1, width*0.05);
		cr->set_source_rgba (1, 1, 1, 1);
		cr->fill ();

		//black inner circle
//		cr->set_identity_matrix();
//		cr->translate (self.x0, self.y0-0.5);
//		cr->translate(height/2., height/2.);
//		cr->set_source_rgba (0, 0, 0, 1);
//		cr->arc(halfh, halfh, halfh*0.79, 0, TWO_PI );
//		cr->fill ();

	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	float rad = halfh;
	if (do_concentric)
		rad *= 0.85;

	//knob base
	cr->set_source_rgb (color_r, color_g, color_b);
	cr->arc(halfh, halfh, rad*0.70, 0, TWO_PI );
	cr->fill ();

	if (!flat) {
			//lower shadow
			Cairo::RefPtr<Cairo::RadialGradient> shine_pattern = Cairo::RadialGradient::create ( halfh*1.2, halfh*1.85, 1, halfh*1.2, halfh*1.85, halfh*1.3  );
			shine_pattern->add_color_stop_rgba ( 0.0, 0,0,0, 0.40);
			shine_pattern->add_color_stop_rgba ( 1.0, 0,0,0, 0.0);
			cr->set_source (shine_pattern);
			cr->arc(halfh, halfh, rad*1.1, 0, TWO_PI );
			cr->fill ();

			//upper shine
			shine_pattern = Cairo::RadialGradient::create ( halfh*0.80, halfh*0.35, 1, halfh*0.80, halfh*0.35, halfh*1.1  );
			shine_pattern->add_color_stop_rgba ( 0.0, 1,1,1, 0.25);
			shine_pattern->add_color_stop_rgba ( 1.0, 1,1,1, 0.0);
			cr->set_source (shine_pattern);
			cr->arc(halfh, halfh, rad*.85, 0, TWO_PI );
			cr->fill ();
	}

	//flatter top
	cr->set_source_rgba (color_r, color_g, color_b, 0.3);
	cr->arc(halfh, halfh, rad*0.6, 0, TWO_PI );
	cr->fill ();

	//line
	{
		//don't draw the "line" if we aren't assigned to anything.
		if( !_stripable )
			return;

		float fract = f_range_check(0, kval, 1.0);
		double angle = TWO_PI*0.125 + TWO_PI*fract*0.75;

		//draw dark line
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(width/2, width/2);
		cr->translate(0.5, 0.5);
		cr->set_source_rgba (0, 0, 0, 0.4);
		cr->rotate(angle);
		cr->move_to (0, width/6);
		cr->line_to (0.0, width/2.4);
		cr->set_line_width ( width/18 );
		cr->stroke ();

		//draw white line
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(width/2, width/2);
		cr->translate(-0.5, -0.5);
		cr->set_source_rgba (1, 1, 1, 0.2);
		cr->rotate(angle);
		cr->move_to (0, width/6);
		cr->line_to (0.0, do_concentric ? width / 2.9: width/2.3);
		cr->set_line_width ( width/18 );
		cr->stroke ();

		//draw colored line
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->translate(width/2, width/2);
		//draw line
		if ( fabs(_last_val-_normal) < 0.005 )
			cr->set_source_rgba (line_color_r, line_color_g, line_color_b, 0.3);
		else
			cr->set_source_rgba (line_color_r, line_color_g, line_color_b, 1);
		cr->rotate(angle);
		cr->move_to (0, width/6);
		cr->line_to (0.0, width/2.9);
		cr->set_line_cap  (Cairo::LINE_CAP_ROUND);
		cr->set_line_width ( width/18 );
		cr->stroke ();

	}



	cr->set_identity_matrix ();

}


#else

void
MixbusKnob::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{
	//look up my color
	uint32_t generic_color = UIConfiguration::instance().color (string_compose ("%1: fill", _color));
	double color_r,color_g,color_b,unused;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	cr->set_identity_matrix();

#ifdef LAYOUT_HELPER
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	//tickmarks
	cr->set_line_width(1);
	cr->set_source_rgba (1, 1, 1, LEGEND_BRIGHTNESS);
	for (int r = 0; r<NUM_KNOB_TICKS; r++) {
		bool show_tick = (ticks[r] == "|");
		show_tick |= ( !wide && (r==0) && (ticks[0] != "") );
		show_tick |= ( !wide && (r==6) && (ticks[6] != "") );

		if (show_tick) {
			cr->set_identity_matrix();
			cr->translate (self.x0 + halfh, self.y0-0.5 + halfh);  //locate to center of knob

			double angle = TWO_PI*0.125 + TWO_PI*((float)r/6.0)*0.75;
			cr->rotate(angle);

			cr->move_to (0, halfh );
			cr->line_to (0, halfh + 2);

			cr->stroke ();
		}
	}
	cr->set_identity_matrix();

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//start text (if any)
	if (wide) {
		cr->set_source_rgba (1, 1, 1, LEGEND_BRIGHTNESS);
		if (ticks[0] != "" && ticks[0] != "|") {
			layout->set_text (ticks[0]);

			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0 + height);  //bottom left corner

			int w,  h;
			layout->get_pixel_size (w, h);
			cr->translate ( floor(-w/2.0), floor(-h/2.0) );
			layout->show_in_cairo_context (cr);

			cr->set_identity_matrix();
		}

		//end text (if any)
		cr->set_source_rgba (1, 1, 1, LEGEND_BRIGHTNESS);
		if (ticks[6] != "" && ticks[6] != "|") {
			layout->set_text (ticks[6]);

			cr->set_identity_matrix();
			cr->translate (self.x0+width, self.y0 + height);  //bottom left corner

			int w,  h;
			layout->get_pixel_size (w, h);
			cr->translate ( floor(-w/2.0), floor(-h/2.0) );
			layout->show_in_cairo_context (cr);

			cr->set_identity_matrix();
		}
	}

	cr->set_identity_matrix();
}

void
MixbusKnob::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	//look up my color
	uint32_t generic_color = UIConfiguration::instance().color (string_compose ("%1: fill", _color));
	double color_r,color_g,color_b,unused;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);

	uint32_t line_color = UIConfiguration::instance().color (string_compose ("%1: line", _color));
	double line_color_r,line_color_g,line_color_b;
	Gtkmm2ext::color_to_rgba( line_color, line_color_r, line_color_g, line_color_b, unused);

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();

	bool flat = UIConfiguration::instance().get_flat_buttons();

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	/* locate to knob, for remainder  */
	cr->translate (self.x0, self.y0-0.5);

	/* locate to knob, for remainder  */
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

//	cr->push_group ();

	//shadow
	if (true) {
		cr->translate (2, 2.5);
		cr->set_source_rgba (0, 0, 0, 0.4);  //TODO:  use a radial gradient so it's not so "hard"
		cr->arc(halfh, halfh, halfh-1, 0, TWO_PI );  //TODO:  this goes outside our bounding box, so will probably have to be drawn by the parent background?
		cr->fill ();
		cr->translate (-2, -2.5);
	}

	//knob base
	cr->set_source_rgb (color_r, color_g, color_b);
	cr->arc(halfh, halfh, halfh-2, 0, TWO_PI );
	cr->fill ();

	//knob base
	cr->set_source_rgba (color_r*1.1, color_g*1.1, color_b*1.1, 0.7);
	cr->arc(halfh, halfh, halfh-2, 0, TWO_PI );
	cr->fill ();

	//black outline
	cr->set_line_width( OUTLINE_WIDTH );
	cr->set_source_rgba (0, 0, 0, 1);
	cr->arc(halfh, halfh, halfh-2, 0, TWO_PI );
	cr->stroke ();

	if (!flat) {
			//lower shadow
			Cairo::RefPtr<Cairo::RadialGradient> shine_pattern = Cairo::RadialGradient::create ( halfh*1.2, halfh*1.85, 1, halfh*1.2, halfh*1.85, halfh*1.3  );
			shine_pattern->add_color_stop_rgba ( 0.0, 0,0,0, 0.35);
			shine_pattern->add_color_stop_rgba ( 1.0, 0,0,0, 0.0);
			cr->set_source (shine_pattern);
			cr->arc(halfh, halfh, halfh+1, 0, TWO_PI );
			cr->fill ();

			//upper shine
			shine_pattern = Cairo::RadialGradient::create ( halfh*0.80, halfh*0.35, 1, halfh*0.80, halfh*0.35, halfh*1.1  );
			shine_pattern->add_color_stop_rgba ( 0.0, 1,1,1, 0.25);
			shine_pattern->add_color_stop_rgba ( 1.0, 1,1,1, 0.0);
			cr->set_source (shine_pattern);
			cr->arc(halfh, halfh, halfh-1.5, 0, TWO_PI );
			cr->fill ();
	}

	//flatter top
	cr->set_source_rgba (color_r, color_g, color_b, 0.6);
	cr->arc(halfh, halfh, halfh-4.0, 0, TWO_PI );
	cr->fill ();

	//highlight
	if ( (UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.2);
		cr->arc(halfh, halfh, halfh-2.5, 0, TWO_PI );
		cr->fill ();
	}

	//line

	//don't draw the "line" if we aren't assigned to anything.
	if( !_stripable )
		return;

	float fract = f_range_check(0, _last_val, 1.0);
	double angle = TWO_PI*0.125 + TWO_PI*fract*0.75;

	//move to center of knob
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);
	cr->translate(width/2, width/2);
	cr->translate(0, 1);

	//draw dark line
	cr->set_source_rgba (0, 0, 0, 0.3);
	cr->rotate(angle);
	cr->move_to (0, width/6);
	cr->line_to (0.0, width/2.4);
	cr->set_line_width (2.0);
	cr->stroke ();

	//move to center of knob
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);
	cr->translate(width/2, width/2);

//printf("adj: %3.8f, %3.8f, increm: %3.8f\n", adjustment.get_value(), initial_val, adjustment.get_step_increment());

	//draw circle around knob to give feel of "movement"
	Cairo::Matrix mat = cr->get_matrix();
	cr->set_source_rgba (1, 1, 1, 0.05);
	for (int i = 1; i < 12; i++) {
		cr->rotate(((2*M_PI)/12)*i + TWO_PI*0.125 + TWO_PI*fract*0.75);
		cr->move_to (0, width*0.47);
		cr->line_to (0.0, width*0.42);
//		std::valarray< double > dashes(2); dashes[0] = 2.0; dashes[1] = 2.0;
//		cr->set_dash ( dashes, 0.0 );
		cr->set_line_width ( 1.0);
		cr->stroke();
		cr->set_matrix(mat);
	}


	//draw line
	if ( fabs(_last_val-_normal) < 0.005 )
		cr->set_source_rgba (line_color_r*0.4, line_color_g*0.4, line_color_b*0.4, 1);
	else
		cr->set_source_rgba (line_color_r, line_color_g, line_color_b, 1);
	cr->rotate(angle);
	cr->move_to (0, width/6);
	cr->line_to (0.0, width/2.4);
	cr->set_line_cap  (Cairo::LINE_CAP_ROUND);
	cr->set_line_width (2.0);
	cr->stroke ();

//	cr->pop_group_to_source ();
//	cr->paint();

	cr->set_identity_matrix ();

	/* draw ticks + text */

//	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
//	if (_font_description) {
//		layout->set_font_description (*_font_description);
//	}

//printf("render B\n");


	//NOTE:  need to destroy the cairo patterns!!
}

#endif

void
MixbusKnob::set_concentric_controllables ( std::shared_ptr<ARDOUR::AutomationControl> enbl, std::shared_ptr<ARDOUR::AutomationControl> knb )
{
	_concentric_enable_connection.disconnect();
	_concentric_enable_ctrl.reset();
	_concentric_enable_ctrl = enbl;
	if (_concentric_enable_ctrl)
		_concentric_enable_ctrl->Changed.connect (_concentric_enable_connection, invalidator (*this), boost::bind (&MixbusKnob::update_display, this), gui_context ());

	_concentric_knob_connection.disconnect();
	_concentric_knob_ctrl.reset();
	_concentric_knob_ctrl = knb;
	if (_concentric_knob_ctrl)
		_concentric_knob_ctrl->Changed.connect (_concentric_knob_connection, invalidator (*this), boost::bind (&MixbusKnob::update_display, this), gui_context ());

	update_display();
}

//------------------------------------

MixbusWidthKnob::MixbusWidthKnob ( Item* c ) : MixbusKnob ( c )
{
	_rotary = true;
}

void
MixbusWidthKnob::slate_conversion_func( char* buf, float val ) const
{
	int percent = round(val*100.0);

	sprintf(buf, "%d%%", percent);
}

void
MixbusWidthKnob::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{

}


void
MixbusWidthKnob::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_t *ct = cr->cobj();

	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : halfh;

//	if (!_auto_ctrl) return;

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

   { //text

		Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
		layout->set_font_description ( ScreenSizer::GetFontDesc() );
		char buf[32];
		slate_conversion_func(buf,_last_val);
		layout->set_text (buf);

		cairo_save (ct);
		cairo_rectangle (ct, 0, 0, width, height);
		cairo_clip(ct);

		cairo_new_path (ct);

		int tw,  th;
		layout->get_pixel_size (tw, th);
		Cairo::Matrix m = cr->get_matrix();
		cairo_translate (ct, floor(width/2.-(tw/2.)), 0*h);
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
		cr->set_matrix(m); //locate back to widget pos

		cairo_restore (ct); //release clip rect

		//highlight
		if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
			cr->set_source_rgba (1,1,1, 0.2);
			Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : (height-2)/2.);
			cr->fill ();
		}
   }

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}

//------------------------------------

MixbusPanKnob::MixbusPanKnob ( Item* c ) : MixbusKnob ( c )
{
	_rotary = true;
}

void
MixbusPanKnob::slate_conversion_func( char* buf, float val ) const
{
	sprintf(buf, "%2.1f", -90.0 + val*180.0);
}

//------------------------------------

MixbusSpeedKnob::MixbusSpeedKnob ( Item* c ) : MixbusKnob ( c )
{
}

void
MixbusSpeedKnob::slate_conversion_func( char* buf, float val ) const
{
	std::shared_ptr<ARDOUR::AutomationControl> ctrl;
	ctrl = std::dynamic_pointer_cast<ARDOUR::AutomationControl>(_stripable->comp_mode_controllable());
	if (ctrl) {
		int mode  = (int)( 0.1+ctrl->get_value() );

		switch (mode) {
		case 0: {  //leveler mode;  print the attack time
			val = 20*exp( 1.609437912*val);
			sprintf(buf, "Attk: %3.0fms", val);
		} break;
		case 1: {  //compressor mode;  print the ratio
			float B = 0.5*log((float)10);
			float a = 10*exp(-B);
			val = a*exp(B*val);
			sprintf(buf, "Ratio: %3.1f:1", val);
		} break;
		case 2: {  //limiter mode:  print the release time
			float B = log((float)15);
			float a = 15;
			val = a*exp(B*val);
			sprintf(buf, "Rels: %3.0fms", val);
		} break;
		case 3: {  //sidechain mode:  print the release time
			val = 316.227*exp(1.844439*val);
			sprintf(buf, "Rels: %3.0fms", val);
		} break;
		default:
			assert (0);
			break;
		}
	}
}

//------------------------------------

Mixbus_EQ_Knob::Mixbus_EQ_Knob ( Item* c ) : MixbusKnob ( c )
{
}

//======================



Mixbus_Filter_Knob::Mixbus_Filter_Knob (ArdourCanvas::Item *c, int idx) : MixbusKnob ( c )
{
	_band_idx = idx;
}

void
Mixbus_Filter_Knob::slate_conversion_func( char* buf, float val ) const
{
	if (_auto_ctrl) {
		sprintf(buf, "%s", _auto_ctrl->get_user_string().c_str());
	}
}

// -----------------------------------


MixbusGainKnob::MixbusGainKnob ( Item* c ) : MixbusKnob ( c )
{
}

void
MixbusGainKnob::slate_conversion_func( char* buf, float val ) const
{
	sprintf(buf, "%s", _auto_ctrl->get_user_string().c_str());
}

// -----------------------------------


MixbusSendKnob::MixbusSendKnob ( Item* c, int n ) : MixbusKnob ( c )
{
	_send_number = n;
}

void
MixbusSendKnob::slate_conversion_func( char* buf, float val ) const
{
	sprintf(buf, "%3.1f", val);
}

// -----------------------------------


MixbusModeSelector::MixbusModeSelector ( Item* c)	: MixbusWidget ( c )
{
	_theme = "mixbus drive knob";
	event_connection = Event.connect (sigc::mem_fun (*this, &MixbusModeSelector::click_handler));

	comp_mode_menu = 0;
}

MixbusModeSelector::~MixbusModeSelector ()
{
	delete comp_mode_menu;
	comp_mode_menu = 0;
}

void
MixbusModeSelector::update_display()
{
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_value ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
}


void
MixbusModeSelector::set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;
	_auto_ctrl = ctrl;

	binding_proxy.set_controllable(_auto_ctrl);

	//get initial value
	if (_auto_ctrl ) {
		_normal = _auto_ctrl->normal();
		_last_val = _auto_ctrl->get_value ();
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusModeSelector::update_display, this), gui_context ());
		redraw();
	}
}

bool
MixbusModeSelector::click_handler ( GdkEvent* ev)
{
	switch (ev->type) {
		case GDK_SCROLL:
			{
				GdkEventScroll *event = (GdkEventScroll *)ev;

				/* ignore left/right scrolling */
				if ( ev->scroll.direction == GDK_SCROLL_LEFT )
					return false;
				if ( ev->scroll.direction == GDK_SCROLL_RIGHT )
					return false;
				// if (event->state & Keyboard::TertiaryModifier) { return false; }

				std::shared_ptr<PBD::Controllable> c = _auto_ctrl;//binding_proxy.get_controllable();
				if (c) {
					float val = 0.1+c->get_value();

					if ( event->direction == GDK_SCROLL_UP )
						val += 1;
					else
						val -= 1;

					/* range of settings differs for each channel type */
					int num_modes = 3;

					if (val >= num_modes)
						val = 0;
					if (val < 0)
						val = (float) num_modes-0.1;

					c->set_value(floor(val), Controllable::NoGroup);

					// update_slate ();
				}
			}
			break;

		case GDK_BUTTON_PRESS:
			{
				comp_mode_button_press(ev);  //show menu
			}
			break;

		case GDK_2BUTTON_PRESS:
			break;

		case GDK_BUTTON_RELEASE:
			//show menu
			break;

		case GDK_ENTER_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> (_auto_ctrl));
			}
			_highlight = true;
			redraw();
			break;

		case GDK_LEAVE_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> ());
			}
			_highlight = false;
			redraw();
			break;

		case GDK_MOTION_NOTIFY:
			break;

		default:
			break;
	}

	return true;
}

	static const char* chan_comp_modes[4] = {
		"Leveler",
#ifdef MIXBUS32C
		"Compressor",
#else
		"Comprs"
#endif
		"Limiter",
		"UNDEF"
	};

	static const char* mixb_comp_modes[4] = {
		"Leveler",
#ifdef MIXBUS32C
		"Compressor",
#else
		"Comprs"
#endif
		"Limiter",
		"SideCh"
	};

	static const char* mstr_comp_modes[4] = {
		"Leveler",
#ifdef MIXBUS32C
		"Compressor",
#else
		"Comprs"
#endif
		"Limiter",
		"UNDEF"
	};


void
MixbusModeSelector::set_comp_mode (int mode)
{
	if (_stripable == NULL) return;

	std::shared_ptr<ARDOUR::AutomationControl> c = _stripable->comp_mode_controllable();

	if (c)
		c->set_value (mode, PBD::Controllable::NoGroup);
}

void
MixbusModeSelector::build_comp_mode_menu ()
{
	using namespace Gtk::Menu_Helpers;
	comp_mode_menu = new Gtk::Menu;
	comp_mode_menu->set_name ("ArdourContextMenu");

	comp_mode_menu->items().push_back (MenuElem (_("Leveler"),
							  bind (mem_fun (*this, &MixbusModeSelector::set_comp_mode), 0)));
	comp_mode_menu->items().push_back (MenuElem (_("Compressor"),
							  bind (mem_fun (*this, &MixbusModeSelector::set_comp_mode), 1)));
	comp_mode_menu->items().push_back (MenuElem (_("Limiter"),
							  bind (mem_fun (*this, &MixbusModeSelector::set_comp_mode), 2)));
}

gboolean
MixbusModeSelector::comp_mode_button_press (GdkEvent* ev)
{
	if ( comp_mode_menu != 0 )
		delete comp_mode_menu;

	build_comp_mode_menu();

	comp_mode_menu->popup (1, ev->button.time);

	return true;
}

void
MixbusModeSelector::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cairo_t *ct = cr->cobj();
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
//	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	/* draw background */
	uint32_t generic_color = UIConfiguration::instance().color (string_compose ("%1: fill", _theme));
	double color_r,color_g,color_b,unused;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);


	//shadow
	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : 5*h;

	if (!flat) {
		cr->translate (1.2, 1.2);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, width-1, height-1, _corner_radius);
		cr->fill ();
		cr->translate (-2, -2);
	}

	//button rect
	cairo_set_source_rgba (ct, color_r, color_g, color_b, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
	cairo_fill (ct);

	static const char** mode_strings;

	if (_stripable->is_master())
		mode_strings = mstr_comp_modes;
	else if (_stripable->mixbus())
		mode_strings = mixb_comp_modes;
	else
		mode_strings = chan_comp_modes;

	if (!flat) {
		//lower gradient
		cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  height);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.7);
		cairo_set_source (ct, shade_pattern);
		Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.0);
		cairo_fill (ct);
		cairo_pattern_destroy (shade_pattern);

		//upper shine
		shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  height/2);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 1,1,1, 0.3);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 1,1,1, 0.0);
		cairo_set_source (ct, shade_pattern);
		Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.0);
		cairo_fill (ct);
		cairo_pattern_destroy (shade_pattern);
	}

	//flatter top
	cairo_set_source_rgba (ct, color_r, color_g, color_b, 0.75);
	Gtkmm2ext::rounded_rectangle (ct, 3, 3, width-6, height-6, _corner_radius - 1.5);
	cairo_fill (ct);

	//text
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//range of settings differs for each channel type
	int num_modes = 3;
	if (_stripable->mixbus())
		num_modes = 4;

	//get current setting & range check
	float val = 0;
	if (_auto_ctrl)
		val = _auto_ctrl->get_value ()+0.1;
	int mode = floor (val);
	if (mode < 0) mode = 0;
	if (mode > num_modes-1) mode = num_modes-1;

	//calculate the text size
	layout->set_text (mode_strings[mode]);
	int tw, th;

	//text clipping rect
	cairo_save (ct);
	cairo_rectangle (ct, 2, 1, width-4, height-2);
	cairo_clip(ct);

	//render the text
	if (mode==3)  //sidechain
		cr->set_source_rgba (1, 0.1, 0.1, CAPTION_BRIGHTNESS * 1.1);
	else
		cr->set_source_rgba (fg_r, fg_g, fg_b, CAPTION_BRIGHTNESS);
	Cairo::Matrix m = cr->get_matrix();
	layout->get_pixel_size (tw, th);
	cr->translate( floor(width/2-(tw/2)), floor(height/2-(th/2)));
	layout->show_in_cairo_context (cr);
	cr->set_matrix(m);

	cairo_restore (ct);

	//black border
	cairo_set_line_width(ct, OUTLINE_WIDTH);
	cairo_set_source_rgba (ct, 0, 0, 0, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
	cairo_stroke (ct);

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.2);
		Gtkmm2ext::rounded_rectangle (cr, 2, 2, width - 4, height-4, boxy ? 0 : 2);
		cr->fill ();
	}
}

//----------------------------------------------------


MixbusCompLegend::MixbusCompLegend ( Item* c)	: MixbusModeSelector ( c )
{
	event_connection.disconnect ();
}

void
MixbusCompLegend::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance width = self.width();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//black border...this should be in draw_bg
//	set_source_rgba (cr, rgba_to_color (1,0,1,1));
//	cr->set_line_width(1);
//	cr->rectangle(0, 0, width, height);
//	cr->stroke ();
	cr->begin_new_path();

	//range of settings differs for each channel type
	int num_modes = 3;
	if (_stripable->mixbus())
		num_modes = 4;

	//get current setting & range check
	float val = 0;
	if (_auto_ctrl)
		val = _auto_ctrl->get_value ()+0.1;
	int mode = floor (val);
	if (mode < 0) mode = 0;
	if (mode > num_modes-1) mode = num_modes-1;

	//text
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//calculate the text size
	int w,  h;

	//render the text
	if (mode==0)  //leveler
		layout->set_text ("Attk");
	else if (mode==1)  //comp
		layout->set_text ("Ratio");
	else if (mode==2)  //limiter
		layout->set_text ("Rels");
	else if (mode==3)  //sidech
		layout->set_text ("Rels");
	layout->get_pixel_size (w, h);
	cr->translate( floor(width/2- (w/2)), 0);
	cr->set_source_rgba (fg_r,fg_g,fg_b, LEGEND_BRIGHTNESS);
	layout->show_in_cairo_context (cr);
}


MixbusHystLegend::MixbusHystLegend ( Item* c) : MixbusModeSelector ( c )
{
	event_connection.disconnect ();
}


void
MixbusHystLegend::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance width = self.width();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//get current setting & range check
	float val = 0;
	if (_auto_ctrl)
		val = _auto_ctrl->get_value ()+0.1;
	int mode = floor (val);

	//text
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//calculate the text size
	int tw,  th;

	//render the text
	cr->begin_new_path();
	if (mode == 0) {
		layout->set_text("Hyst");
	} else if (mode == 1) {
		layout->set_text("Knee");
	}
	layout->get_pixel_size (tw, th);
	cr->translate( floor(width/2.- (tw/2.)), 0);
	cr->set_source_rgba (fg_r,fg_g,fg_b, LEGEND_BRIGHTNESS);
	layout->show_in_cairo_context (cr);
}

// --------------------

MixbusHoldLegend::MixbusHoldLegend ( Item* c) : MixbusModeSelector ( c )
{
	event_connection.disconnect ();
}


void
MixbusHoldLegend::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance width = self.width();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//get current setting & range check
	float val = 0;
	if (_auto_ctrl)
		val = _auto_ctrl->get_value ()+0.1;
	int mode = floor (val);

	//text
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//calculate the text size
	int tw,  th;

	//render the text
	cr->begin_new_path();
	if (mode == 0) {
		layout->set_text("Hold");
	} else if (mode == 1) {
		layout->set_text("Ratio");
	}
	layout->get_pixel_size (tw, th);
	cr->translate( floor(width/2- (tw/2)), 0);
	cr->set_source_rgba (fg_r,fg_g,fg_b, LEGEND_BRIGHTNESS);
	layout->show_in_cairo_context (cr);
}

// --------------------

MixbusLabel::MixbusLabel ( Item* c) : MixbusWidget ( c )
{
}


void
MixbusLabel::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance width = self.width();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//get current setting & range check
	float val = 0;
	if (_auto_ctrl)
		val = _auto_ctrl->get_value ()+0.1;
	int mode = floor (val);

	//text
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetBigBoldFontDesc() );

	//calculate the text size
	int tw,  th;

	//render the text
	cr->begin_new_path();
	layout->set_text(_label);
	layout->get_pixel_size (tw, th);
	cr->translate( floor(width/2- (tw/2)), 0);
	cr->set_source_rgba (fg_r,fg_g,fg_b, LEGEND_BRIGHTNESS);
	layout->show_in_cairo_context (cr);
}

// -----------------------------------


MixbusFader::MixbusFader ( Item* c)	: MixbusKnob ( c )
{
	_rotary = false;

	_tickmarks.ticks.push_back(KnobTickMark(6, "6"));
	_tickmarks.ticks.push_back(KnobTickMark(3, "3"));
	_tickmarks.ticks.push_back(KnobTickMark(0, "0"));
	_tickmarks.ticks.push_back(KnobTickMark(-3, "-3"));
	_tickmarks.ticks.push_back(KnobTickMark(-6, "-6"));
	_tickmarks.ticks.push_back(KnobTickMark(-12, "12"));
	_tickmarks.ticks.push_back(KnobTickMark(-24, "24"));
	_tickmarks.ticks.push_back(KnobTickMark(-40, "40"));
	_tickmarks.ticks.push_back(KnobTickMark(-200, "90"));
}

void
MixbusFader::update_display()
{
	// Even though they're functionally identical, don't
	// be tempted to remove this override and defer to the
	// underlying 'MixbusKnob::parameter_changed()' (when
	// connecting signal handlers for this derived class).
	// 'boost::bind' gets very unhappy if we attempt that!
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
}

void
MixbusFader::set_fader_route( std::shared_ptr<ARDOUR::Route> rt )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;

	//find our automated control knob
	_auto_ctrl = rt->amp()->gain_control();

	binding_proxy.set_controllable(_auto_ctrl);

	//get initial value
	if (_auto_ctrl ) {
		_normal = _auto_ctrl->internal_to_interface ( _auto_ctrl->normal () );
		_last_val = _auto_ctrl->get_interface (_rotary);
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusFader::update_display, this), gui_context ());
		redraw();
	}

}

void
MixbusFader::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{
#ifdef LAYOUT_HELPER
	return;
#endif

#ifdef MIXBUS32C
	wide = true;
#endif


	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	Distance height = self.height();
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

	/* draw background */

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	float fader_width = width-12*h;
	float fader_height = fader_width*1.4;
	float travel = height - fader_height - 4*h;

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//draw scale (?)
	cr->set_line_width (0.75);
	for (int i =0; i< _tickmarks.ticks.size(); i++) {
		float val = dB_to_coefficient(_tickmarks.ticks[i].first);
		val = gain_to_slider_position_with_max (val, Config->get_max_gain ());
		val = 2+fader_height/2 + travel*(1.0-val);

		cr->move_to( 2*h, floor(val) );
		cr->line_to( width-2*h, floor(val) );
		cr->set_source_rgba (fg_r,fg_g,fg_b, _tickmarks.ticks[i].first == 0 ? 0.9 : 0.7);
		cr->stroke();

		//only show the text if we are "wide"
		if (wide ) {
			//text
			layout->set_text (_tickmarks.ticks[i].second);
			int tw,  th;
			layout->get_pixel_size (tw, th);

			Cairo::Matrix m = cr->get_matrix();
			cr->translate( floor(1*h-tw), floor(val- th/2));
			layout->show_in_cairo_context (cr);
			cr->set_matrix(m);
		}
	}

	//slot
	bool flat = UIConfiguration::instance().get_flat_buttons();
	cr->translate( 3*h,0);
	if (!flat) {
		cr->translate( 1*h,1*h);
		cr->set_source_rgba (1,1,1,0.1);
		Gtkmm2ext::rounded_rectangle (cr, -1+ fader_width/2, -2*h + fader_height/2, 6*h, travel + 10, 3*h);
		cr->fill ();
	}
	cr->translate( -1*h,-1*h);
	cr->set_source_rgba (0,0,0,1.0);
	Gtkmm2ext::rounded_rectangle (cr, -1*h + fader_width/2, -2*h + fader_height/2, 6*h, travel + 10*h, 3*h);
	cr->fill ();

	cr->set_identity_matrix();
}

void
MixbusFader::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
//	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	float fader_width = width-12*h;
	float fader_height = fader_width*1.4;
	float _corner_radius = width*0.05;
	float travel = height - fader_height - 4;

	float val = f_range_check(0, _last_val, 1.0);
	float dist = travel * (1.0-val);

//did this already in bg_render

	bool flat = UIConfiguration::instance().get_flat_buttons();

	cr->translate (3*h, dist+2);

	//shadow
	if (!flat) {
		cr->translate (4*h, 4*h);
		cr->set_source_rgba (0, 0, 0, 0.4);
		Gtkmm2ext::rounded_rectangle (cr, 2*h, 0, fader_width, fader_height, _corner_radius+2*h);
		cr->fill ();
		cr->translate (-4*h, -4*h);
	}

	//fader cap
	if (UIConfiguration::instance().get_use_route_color_widely ()) {
		double color_r,color_g,color_b,unused;
		Gtkmm2ext::color_to_rgba( _route_color, color_r, color_g, color_b, unused);
		cr->set_source_rgba( (color_r+0.5)/1.5, (color_g+0.5)/1.5, (color_b+0.5)/1.5, 1 );
	} else {
		uint32_t generic_color = UIConfiguration::instance().color ("mixbus fader: fill");
		double color_r,color_g,color_b,unused;
		Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);
		cr->set_source_rgba (color_r, color_g, color_b, 1);
	}
	Gtkmm2ext::rounded_rectangle (cr, 2*h, 0, fader_width, fader_height, _corner_radius);
	cr->fill ();
	cr->set_line_width (2);
	cr->set_source_rgba (0,0,0,1);
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_line_width (3);
		cr->set_source_rgba (0.7, 0.7, 0.7,1);
	}
	Gtkmm2ext::rounded_rectangle (cr, 2*h, 0, fader_width, fader_height, _corner_radius);
	cr->stroke ();

	cairo_t *ct = cr->cobj();

	if (!flat) {
		//gradient
		cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0.0, 0.0,  fader_height);  //note we have to offset the gradient from our centerpoint
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.4);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.25, 0,0,0, 0.4);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.5, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.0);
		cairo_set_source (ct, shade_pattern);
		cairo_rectangle (ct, 2, fader_height/4, fader_width, fader_height/4);
		cairo_fill (ct);
		cairo_pattern_destroy (shade_pattern);

		shade_pattern = cairo_pattern_create_linear (0.0, 0.0, 0.0,  fader_height);  //note we have to offset the gradient from our centerpoint
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.74, 0,0,0, 0.0);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 0.75, 0,0,0, 0.2);
		cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.6);
		cairo_set_source (ct, shade_pattern);
		cairo_rectangle (ct, 2, fader_height*0.75, fader_width, fader_height/4);
		cairo_fill (ct);
		cairo_pattern_destroy (shade_pattern);
	}

	//mid line
	cairo_set_source_rgba (ct, 0,0,0, 1);
	cairo_set_line_width (ct, 0.75);
	cairo_move_to (ct, 2*h, fader_height*0.5 );
	cairo_line_to (ct, 2*h+fader_width, fader_height*0.5);
	cairo_stroke (ct);
}

// -----------------------------------


MixbusThresh::MixbusThresh ( Item* c)	: MixbusKnob ( c )
{
}

void
MixbusThresh::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

	float fader_height = width-2*h;
	float fader_width = width;
	float travel = height - fader_height - 4*h;

	//slot
	bool flat = UIConfiguration::instance().get_flat_buttons();
	if (!flat) {
		cr->translate( 1*h,1*h);
		cr->set_source_rgba (1,1,1,0.1);
		Gtkmm2ext::rounded_rectangle (cr, fader_width/2., -2*h + fader_height/2., 4*h, travel + 10*h, 2*h);
		cr->fill ();
	}
	cr->translate( -1*h,-1*h);
	cr->set_source_rgba (0,0,0,0.9);
	Gtkmm2ext::rounded_rectangle (cr, fader_width/2, -2 + fader_height/2., 4*h, travel + 10*h, 2*h);
	cr->fill ();
}

void
MixbusThresh::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);

	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	/* draw background */

	float fader_width = width*0.5;
	float fader_height = fader_width*0.75;
	float travel = height - fader_height - 4*h;

	float val = f_range_check(0, _last_val, 1.0);
	float dist = travel * (1.0-val);

	bool flat = UIConfiguration::instance().get_flat_buttons();

	cr->translate (width*0.25, dist+2*h);

	if (!flat) {
		//shadow
		cr->translate (3*h, 3*h);
		cr->set_source_rgba (0, 0, 0, 0.4);
			cr->move_to( 0*h, 0*h ); //start in the top left
			cr->rel_line_to( fader_width, fader_height/2. );
			cr->rel_line_to( -fader_width, fader_height/2. );
			cr->rel_line_to( 0, -fader_height );
		cr->fill ();
		cr->translate (-2*h, -2*h);
	}

	//fader cap color
	uint32_t generic_color = UIConfiguration::instance().color ("mixbus fader: fill");
	double color_r,color_g,color_b,unused;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);
	cr->set_source_rgba (color_r, color_g, color_b, 1);

	bool high = (UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed;

	//fader cap fill
	{
		cr->move_to( 0*h, 0*h ); //start in the top left
		cr->rel_line_to( 4*h, 0 );
		cr->rel_line_to( fader_width-4*h, fader_height/2. );
		cr->rel_line_to( -fader_width+4*h, fader_height/2. );
		cr->rel_line_to( -4*h, 0 );
		cr->rel_line_to( 0, -fader_height );
		if (high) {
			cr->set_source_rgba (0,0,0,1);
		} else {
			cr->set_source_rgba (color_r,color_g,color_b,1);
		}
		cr->fill_preserve ();
		if (high) {
			cr->set_line_width (1*h);
			cr->set_source_rgba (1,1,1,1);
		} else {
			cr->set_line_width (2*h);
			cr->set_source_rgba (0,0,0,1);
		}
		cr->stroke ();
	}
}

void
MixbusThresh::update_display ()
{
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
}


// ---------------------------------------------------------------------

MixbusBigToggle::MixbusBigToggle (Item* c, bool soloer)
	: MixbusButton (c)
{
	_soloer = soloer;
}

void
MixbusBigToggle::set_controllable( std::shared_ptr<ARDOUR::Stripable> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	_route_connections.drop_connections();

	MixbusToggle::set_controllable(rt, ctrl);

	if (!rt) {
		return;
	}

	if (rt->solo_control ())
		rt->solo_control ()->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusBigToggle::update_solo_display, this), gui_context());
	if (rt->solo_isolate_control ())
		rt->solo_isolate_control ()->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusBigToggle::update_solo_display, this), gui_context());
	if (rt->solo_safe_control ())
		rt->solo_safe_control ()->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusBigToggle::update_solo_display, this), gui_context());
	if (rt->mute_control ())
		rt->mute_control ()->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusBigToggle::update_solo_display, this), gui_context());
	rt->session().SoloChanged.connect (_route_connections, invalidator (*this), boost::bind (&MixbusBigToggle::update_solo_display, this), gui_context());
}

void
MixbusBigToggle::update_solo_display ()
{
	redraw();
}

int
MixbusBigToggle::active_state () const
{
	if (!_stripable) return 0;

	int state = 0;
	if (_soloer) {
		std::shared_ptr<SoloControl> sc = _stripable->solo_control();
		if (!sc || !sc->can_solo()) {
			return 0;
		}
		if (sc->self_soloed()) {
			state=2;  //explicit active
		} else if (sc->soloed_by_others()) {
			state=1;  //implicit active
		}
	} else {
		std::shared_ptr<MuteControl> mc = _stripable->mute_control();
		if (!mc) {
			return 0;
		}
		if (Config->get_show_solo_mutes() && !Config->get_solo_control_is_listen_control ()) {
			if (mc->muted_by_self ()) {
				/* full mute */
				state=2;  //explicit active
			} else if (mc->muted_by_others_soloing () || mc->muted_by_masters ()) {
				state=1;  //implicit active
			}
		} else {
			if (mc->muted_by_self()) {
				state=2;  //explicit active
			} else if (mc->muted_by_masters ()) {
				state=1;  //implicit active
			}
		}
	}
	return state;
}

void
MixbusBigToggle::update_display ()
{
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
}


//--------------------------------------------------------------------

MixbusButton::MixbusButton (Item* c, float toggle_value)
	: MixbusToggle (c)
{
	_tog_value = toggle_value;
}

void
MixbusButton::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_t *ct = cr->cobj();

	//show implicit/explicit states based on solo/mute logic
	const int state = active_state ();

	//shadow
	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : 5.0;

	//shadow
	if ( !flat) {
		cr->translate (2*h, 2*h);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, width, height, _corner_radius);
		cr->fill ();
		cr->translate (-2*h, -2*h);
	}

	//implicit or explicit active
	if (state == 2) {
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-2*h, height-2*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
	} else if (state==1) {  //implicit active
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-2*h, height-2*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 2*h, 2*h, width-4*h, height-4*h, _corner_radius - 1.5*h);
		cairo_set_line_width (ct, 2.0*h);
		cairo_stroke (ct);
	} else  {
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-2*h, height-2*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
	}

	if (!flat) {

		if (state) {
			//gradient
			cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  height);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.5);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.0);
			cairo_set_source (ct, shade_pattern);
			Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-2*h, height-2*h, _corner_radius - 1.5*h);
			cairo_fill (ct);
			cairo_pattern_destroy (shade_pattern);
		} else {
			//gradient
			cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  height);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.0);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.7);
			cairo_set_source (ct, shade_pattern);
			Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-2*h, height-2*h, _corner_radius - 1.5*h);
			cairo_fill (ct);
			cairo_pattern_destroy (shade_pattern);
		}
	}

	if (state==2) {  //halo
		cairo_pattern_t* led_halo_pattern;
		led_halo_pattern = cairo_pattern_create_radial ( width/2., height*0.7, height*0.1,
		                                                 width/2., height*0.7, height*0.45);
		cairo_pattern_add_color_stop_rgba (led_halo_pattern, 0, _led_r*2, _led_g*2, _led_b*2, 0.5);
		cairo_pattern_add_color_stop_rgba (led_halo_pattern, 1, _led_r, _led_g, _led_b, 0.0);

		cairo_arc (ct, width/2., height*0.55, height*0.45, 0, 2 * M_PI);
		cairo_set_source (ct, led_halo_pattern);
		cairo_fill (ct);

		cairo_pattern_destroy(led_halo_pattern);
	}

	if (!state) {
		//flatter top
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 4*h, 4*h, width-8*h, height-8*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
	}

   { //text --------------

		Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
		if ( _stripable )
			layout->set_font_description ( ScreenSizer::GetBigBoldFontDesc() );
		else
			layout->set_font_description ( ScreenSizer::GetFontDesc() );
		layout->set_text (_text);

		int tw,  th;
		layout->get_pixel_size (tw, th);

		cairo_save (ct);
		cairo_rectangle (ct, 2*h, 1*h, width-4*h, height-2*h);
		cairo_clip(ct);

		cairo_new_path (ct);
		if (state==2) {  //Ben TODO this is ugly
			cairo_set_source_rgba (ct, 1, 1, 1, 0.4);
			cairo_move_to (ct, floor(width/2 - tw/2 +1*h), floor(height/2.0 - th/2.0+1*h));
		} else {
			cairo_set_source_rgba (ct, 0,0,0, 0.4);
			cairo_move_to (ct, floor(width/2 - tw/2 -1*h), floor(height/2.0 - th/2.0-1*h));
		}
		layout->show_in_cairo_context (cr);

		cairo_new_path (ct);
		if (state==2)
			cairo_set_source_rgba (ct, 0,0,0, 0.7);
		else
			cairo_set_source_rgba (ct, 1, 1, 1, 0.7);

		cairo_move_to (ct, floor(width/2 - tw/2), floor(height/2.0 - th/2.0));
		layout->show_in_cairo_context (cr);

		cairo_restore (ct);
	} //-----------------------------


	//black border
	cairo_set_line_width(ct, OUTLINE_WIDTH);
	cairo_set_source_rgba (ct, 0, 0, 0, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-2*h, height-2*h, _corner_radius - 1.5*h);
	cairo_stroke (ct);

	//user is currently pressing the button. dark outline helps to indicate this
	if (_grabbed ) {
		Gtkmm2ext::rounded_rectangle (ct, 2*h, 2*h, width - 4*h, height - 4*h, _corner_radius);
		cairo_set_line_width(ct, 3*h);
		cairo_set_source_rgba (ct, 0.0, 0.0, 0.0, 0.5);
		cairo_stroke (ct);
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 2*h, 2*h, width-4*h, height-4*h, _corner_radius-1.5*h);
		cr->fill ();
	}

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}

int
MixbusButton::active_state () const
{
	if (_auto_ctrl && _auto_ctrl->get_value() == _tog_value) {
		return 2;  //ExplicitActive
	}
	return false;
}

//--------------------------------------------------------------------

MixbusActionToggle::MixbusActionToggle (Item* c)
	: MixbusButton (c)
{
	button_release_event.connect (sigc::mem_fun(*this, &MixbusActionToggle::button_release));
}

void
MixbusActionToggle::set_related_action (Glib::RefPtr<Gtk::Action> act)
{
	Gtkmm2ext::Activatable::set_related_action (act);
	if (_action) {
		Glib::RefPtr<Gtk::ToggleAction> tact = Glib::RefPtr<Gtk::ToggleAction>::cast_dynamic (_action);

		if (tact) {
			tact->signal_toggled().connect (sigc::mem_fun (*this, &MixbusActionToggle::update_display));
		}
		_action->connect_property_changed ("sensitive", sigc::mem_fun (*this, &MixbusActionToggle::update_display));
	}

	update_display();
}

int
MixbusActionToggle::active_state () const
{
	Glib::RefPtr<Gtk::ToggleAction> tact = Glib::RefPtr<Gtk::ToggleAction>::cast_dynamic (_action);
	if (tact && tact->get_active()) {
		return 1;
	}
	return 0;
}

void
MixbusActionToggle::update_display ()
{
	redraw ();
}

bool
MixbusActionToggle::button_release (GdkEventButton* ev)
{
	if (ev->button != 1) {
		return false;
	}

	Glib::RefPtr<Gtk::ToggleAction> tact = Glib::RefPtr<Gtk::ToggleAction>::cast_dynamic (_action);
	if (tact) {
		_action->activate ();
		return true;
	}
	return false;
}

// ---------------------------------------------------------------------


Mixbus_BusToggle::Mixbus_BusToggle ( Item* c)
#ifdef MIXBUS32C
	: MixbusTinyToggle ( c )
#else
	: MixbusToggle ( c )
#endif
{
	_text = "Mixbus #";
}

Mixbus_BusToggle::~Mixbus_BusToggle ()
{
	_bus_connections.drop_connections();
	_bus_route.reset();
}

void
Mixbus_BusToggle::set_bus_route( std::shared_ptr<ARDOUR::Route> rt )
{
	_bus_connections.drop_connections();

	_bus_route = rt;

	//listen for bus name, color changes
	_bus_route->presentation_info().PropertyChanged.connect (_bus_connections, invalidator (*this), boost::bind (&Mixbus_BusToggle::route_gui_changed, this, _1), gui_context ());
	_bus_route->PropertyChanged.connect (_bus_connections, invalidator (*this), boost::bind (&Mixbus_BusToggle::property_changed, this, _1), gui_context());

	//get initial values
	route_gui_changed (Properties::color);
	PropertyChange ch; ch.add (ARDOUR::Properties::name);
	property_changed(ch);
}

void
Mixbus_BusToggle::color_handler ()
{
	MixbusToggle::color_handler();

	if (_bus_route) {
		Gtkmm2ext::Color c = _bus_route->presentation_info().color();
		double unused;
		Gtkmm2ext::color_to_rgba (c, _led_r, _led_g, _led_b, unused);
	}
	redraw();
}

void
Mixbus_BusToggle::route_gui_changed (PropertyChange const& what_changed)
{
	if (what_changed.contains (Properties::color)) {
		color_handler();
	}
}

void
Mixbus_BusToggle::property_changed (const PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {
		_text = _bus_route->name();

		redraw();
	}
}

void
Mixbus_BusToggle::update_display ()
{
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
}

// -------------------------------

Mixbus_BusLabel::Mixbus_BusLabel ( Item* c)
	: Mixbus_BusToggle ( c )
{
}

void
Mixbus_BusLabel::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{

}


void
Mixbus_BusLabel::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_t *ct = cr->cobj();

	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : halfh;

//	if (!_auto_ctrl) return;

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

   { //text --------------   TODO

		Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
		layout->set_font_description ( ScreenSizer::GetFontDesc() );
		layout->set_text (_text);

		int tw,  th;
		layout->get_pixel_size (tw, th);

		cairo_save (ct);
		cairo_rectangle (ct, 0, 0, width-2, height);
		cairo_clip(ct);

		cairo_new_path (ct);

		if (_text.rfind("Mixbus ",0)==0) {
			cairo_set_source_rgba (ct, fg_r*0.6,fg_g*0.6,fg_b*0.6, LEGEND_BRIGHTNESS);
		} else {
			cairo_set_source_rgba (ct, fg_r,fg_g,fg_b, LEGEND_BRIGHTNESS);
		}

		cairo_translate (ct, floor(3*h), floor(height/2.0-th/2.0));
		layout->show_in_cairo_context (cr);

		cairo_restore (ct);
	} //-----------------------------

	//user is currently pressing the button. dark outline helps to indicate this
	if (_grabbed ) {
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-4*h, height-2*h, 4*h);
		cairo_set_line_width(ct, 2*h);
		cairo_set_source_rgba (ct, 0.0, 0.0, 0.0, 0.6);
		cairo_stroke (ct);
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, width-4*h, height-2*h, 4*h );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}

// ---------------------------------------------------------------------


Mixbus_MasterToggle::Mixbus_MasterToggle ( Item* c)
	: MixbusToggle ( c )
{
	_mixbus_asgn_num = 0;
}

void
Mixbus_MasterToggle::update_display ()
{
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_interface ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}
}


//--------------------------------------------


MixbusToggle::MixbusToggle ( Item* c)
	: MixbusWidget ( c )
{
	Event.connect (sigc::mem_fun (*this, &MixbusToggle::event_handler));
	_led_r = _led_g = _led_b = 1.0;

	_mixbus_asgn_num = -1;

	_tog_value = 1.0;

	_color = "mixbus toggle";
	UIConfiguration::instance().ColorsChanged.connect (sigc::mem_fun (*this, &MixbusToggle::color_handler_callback));
	color_handler();
}

MixbusToggle::~MixbusToggle ()
{
}

void
MixbusToggle::color_handler_callback ()
{
	color_handler();  //note: this func is overridden in BusToggle, for example
}

void
MixbusToggle::color_handler ()
{
	double unused = 1.0;

	//look up fill color
	uint32_t fill_color = UIConfiguration::instance().color (string_compose ("%1: fill", _color));
	Gtkmm2ext::color_to_rgba( fill_color, fill_r, fill_g, fill_b, unused);

	//look up my LED color
	uint32_t led_color = UIConfiguration::instance().color (string_compose ("%1: led active", _color));
	Gtkmm2ext::color_to_rgba( led_color, _led_r, _led_g, _led_b, unused);

	redraw();
}

void
MixbusToggle::set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;
	_auto_ctrl = ctrl;

	binding_proxy.set_controllable(_auto_ctrl);

	//get initial value & subscribe for the future
	if (_auto_ctrl ) {
		_last_val = _auto_ctrl->get_value ();
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusToggle::update_display, this), gui_context ());
		redraw();
	}
}

void
MixbusToggle::set_controllable( std::shared_ptr<ARDOUR::Stripable> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;

	//find our automated control knob
	_auto_ctrl = ctrl;

	binding_proxy.set_controllable (_auto_ctrl);

	//get initial value & subscribe for the future
	if (_auto_ctrl ) {
		_last_val = _auto_ctrl->get_value ();
		_auto_ctrl->Changed.connect (_connection, invalidator (*this), boost::bind (&MixbusToggle::update_display, this), gui_context ());
		redraw();
	}
}

void
MixbusToggle::update_display(  )
{
	if (_auto_ctrl) {
		float val = _auto_ctrl->get_value ();
		if ( val != _last_val ) {
			_last_val = val;
			redraw();
		}
	}

}

bool
MixbusToggle::event_handler ( GdkEvent* ev)
{
	Route* rt = dynamic_cast<Route*>( _stripable.get() );

	switch (ev->type) {
		case GDK_SCROLL:
			{
				/* ignore left/right scrolling */
				if ( ev->scroll.direction == GDK_SCROLL_LEFT )
					return false;
				if ( ev->scroll.direction == GDK_SCROLL_RIGHT )
					return false;
				// if (event->state & Keyboard::TertiaryModifier) { return false; }

			}
			break;

		case GDK_BUTTON_PRESS:
			{
				GdkEventButton *event = (GdkEventButton *)ev;
				if (button_press_event (event)) { // emit signal
					return true;
				}
				if (binding_proxy.button_press_handler (event)) {
					return true;
				}

				grab();
				_grabbed = true;
				redraw();
			}
			break;
		case GDK_2BUTTON_PRESS:
			break;

		case GDK_BUTTON_RELEASE:
			{
				GdkEventButton *event = (GdkEventButton *)ev;
				if (button_release_event (event)) { // emit signal
					ungrab();
					_grabbed = false;
					redraw();
					return true;
				}

				ungrab();
				_grabbed = false;

				if (!_auto_ctrl) {
					redraw();
					break;
				}

				_auto_ctrl->start_touch (timepos_t (_auto_ctrl->session().transport_sample()));

				float val = _auto_ctrl->get_value ();
				float new_val = (val != _tog_value ? _tog_value : 0.0);

				bool set = false;

				//set group override state
				bool override_group = Keyboard::is_group_override_event(event);

				if (rt && rt->is_input_strip() && _mixbus_asgn_num >= 0 ) {
					if (Keyboard::modifier_state_equals (event->state, Keyboard::ModifierMask (Keyboard::PrimaryModifier|Keyboard::TertiaryModifier))) {
						rt->session().set_all_chan_bussing (_mixbus_asgn_num, new_val);
						set = true;
					} else if (Keyboard::modifier_state_contains (event->state, Keyboard::ModifierMask (Keyboard::PrimaryModifier))) {
						rt->set_bus_exclusively (_mixbus_asgn_num, Controllable::UseGroup);
						set = true;
					} else if (!override_group) {
						RouteGroup* rg = rt->route_group();
						if (rg) {
							_auto_ctrl->set_value (new_val, Controllable::UseGroup);
							set = true;
						}
					}
				}

				if ( !set && _auto_ctrl ) {
					_auto_ctrl->set_value (new_val, Controllable::NoGroup);
				}
				redraw();

			}
			break;

		case GDK_ENTER_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> (_auto_ctrl));
			}
			_highlight = true;
			redraw();
			break;

		case GDK_LEAVE_NOTIFY:
			if (_auto_ctrl) {
				PBD::Controllable::GUIFocusChanged (std::weak_ptr<PBD::Controllable> ());
			}
			_highlight = false;
			redraw();
			break;

		case GDK_MOTION_NOTIFY:
			break;
		default:
			break;
	}

	return true;
}

void
MixbusToggle::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool wide) const
{
/*	
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	cairo_t *ct = cr->cobj();

	//shadow
	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : halfh;

	if (!flat) {
		cr->translate (2, 2);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, width, height, _corner_radius);
		cr->fill ();
		cr->translate (-2, -2);
	}

	//colored button
	cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1);
	cairo_fill (ct);  */
}


void
MixbusToggle::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	cairo_t *ct = cr->cobj();

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : 4*h;

	int state = (_last_val == _tog_value) ? 2 : 0;  //comparing floats is not a great idea, but let's assume these will be small real numbers

	float box_size = height - 6*h;

	cr->translate (3*h, 3*h);

	//shadow
	if (!flat) {
		cr->translate (2*h, 2*h);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, box_size, box_size, _corner_radius);
		cr->fill ();
		cr->translate (-2*h, -2*h);
	}

	//implicit or explicit active
	if (state == 2) {
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, box_size-2*h, box_size-2*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
	} else if (state==1) {  //implicit active
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, box_size-2*h, box_size-2*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 2*h, 2*h, box_size-4*h, box_size-4*h, _corner_radius - 1.5*h);
		cairo_set_line_width (ct, 2.0*h);
		cairo_stroke (ct);
	} else  {
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, box_size-2*h, box_size-2*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
	}

	if (!flat) {
		if (state==2) {
			//gradient
			cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  box_size);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.5);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.0);
			cairo_set_source (ct, shade_pattern);
			Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, box_size-2*h, box_size-2*h, _corner_radius - 1.5*h);
			cairo_fill (ct);
			cairo_pattern_destroy (shade_pattern);
		} else {
			//gradient
			cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  box_size);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.0);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.7);
			cairo_set_source (ct, shade_pattern);
			Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, box_size-2*h, box_size-2*h, _corner_radius - 1.5*h);
			cairo_fill (ct);
			cairo_pattern_destroy (shade_pattern);
		}
	}

	if (state==2) {  //halo
		cairo_pattern_t* led_halo_pattern;
		led_halo_pattern = cairo_pattern_create_radial ( box_size/2., box_size*0.7, box_size*0.1,
		                                                 box_size/2., box_size*0.7, box_size*0.45);
		cairo_pattern_add_color_stop_rgba (led_halo_pattern, 0, _led_r*2, _led_g*2, _led_b*2, 0.5);
		cairo_pattern_add_color_stop_rgba (led_halo_pattern, 1, _led_r, _led_g, _led_b, 0.0);

		cairo_arc (ct, box_size/2., box_size*0.55, box_size*0.45, 0, 2 * M_PI);
		cairo_set_source (ct, led_halo_pattern);
		cairo_fill (ct);

		cairo_pattern_destroy(led_halo_pattern);
	}

	if (state==0) {
		//flatter top
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 4*h, 4*h, box_size-8*h, box_size-8*h, _corner_radius - 1.5*h);
		cairo_fill (ct);
	}

	//black outline
	cairo_set_line_width (ct, 1);  //not scaled; as fine as we can get it
	cairo_set_source_rgba (ct, 0, 0, 0, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1*h, 1*h, box_size-2*h, box_size-2*h, _corner_radius - 1.5*h);
	cairo_stroke (ct);

	cr->translate (-3*h, -3*h);

	float _diameter = box_size;
	float offs = 2*h;
	float toffs = 4*h;

   { //text --------------   TODO

		Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
		layout->set_font_description ( ScreenSizer::GetFontDesc() );
		layout->set_text (_text);

		int tw,  th;
		layout->get_pixel_size (tw, th);

		cairo_save (ct);
		cairo_rectangle (ct, 1*h, 1*h, width-4*h, height-2*h);
		cairo_clip(ct);

		cairo_new_path (ct);
		cairo_set_source_rgba (ct, fg_r,fg_g,fg_b, CAPTION_BRIGHTNESS);

		cairo_translate (ct, floor(_diameter + toffs), floor(height/2.0-th/2.0+1*h));
		layout->show_in_cairo_context (cr);

		cairo_restore (ct);
	} //-----------------------------

	//user is currently pressing the button. dark outline helps to indicate this
	if (_grabbed ) {
		Gtkmm2ext::rounded_rectangle (ct, 3*h, 3*h, width-6*h, height-6*h, _corner_radius - 3*h);
		cairo_set_line_width(ct, 2);
		cairo_set_source_rgba (ct, 0.0, 0.0, 0.0, 0.6);
		cairo_stroke (ct);
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 2*h, 2*h, width-4*h, height-4*h, _corner_radius - 2*h);
		cr->fill ();
	}

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}

// -------------------------------------------------------

MixbusSquareToggle::MixbusSquareToggle(ArdourCanvas::Item *i, int32_t tog_value) : MixbusToggle(i)
{
	_tog_value = tog_value;
}

void
MixbusSquareToggle::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

	cairo_t *ct = cr->cobj();

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	bool state = (_last_val == _tog_value);

	{ //indicator led --------------
		cairo_pattern_t* led_inset_pattern;
		if (!flat) {
			led_inset_pattern = cairo_pattern_create_linear (0.0, 0.0, 0.0, height);
			cairo_pattern_add_color_stop_rgba (led_inset_pattern, 0, 0,0,0, 0.4);
			cairo_pattern_add_color_stop_rgba (led_inset_pattern, 1, 1,1,1, 0.5);

			//inset
			cairo_rectangle (ct, 0, 0, width, height);
			cairo_set_source (ct, led_inset_pattern);
			cairo_fill (ct);
		}

		//black ring
		cairo_set_source_rgb (ct, 0, 0, 0);
		cairo_rectangle (ct, 1, 1, width-2*h, height-2*h);
		cairo_fill(ct);

		//led color
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, state ? 1.0 : 0.30);
		cairo_rectangle (ct, 2, 2, width-4*h, height-4*h);
		cairo_fill(ct);

	} //-----------------------------

	if (state) {  //halo
		cairo_pattern_t* led_halo_pattern;
		led_halo_pattern = cairo_pattern_create_radial ( width/2., height*0.7, height*0.2,
		                                                 width/2., height*0.7, height*0.6);
		cairo_pattern_add_color_stop_rgba (led_halo_pattern, 0, _led_r*2, _led_g*2, _led_b*2, 0.5);
		cairo_pattern_add_color_stop_rgba (led_halo_pattern, 1, _led_r, _led_g, _led_b, 0.0);

		cairo_arc (ct, width/2., height*0.7, height*0.7, 0, 2 * M_PI);
		cairo_set_source (ct, led_halo_pattern);
		cairo_fill (ct);

		cairo_pattern_destroy(led_halo_pattern);
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.3);
		cairo_rectangle (ct, 0, 0, width, height);
		cr->fill ();
	}

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}

// ---------------------------------------------------------------------

MixbusTinyToggle::MixbusTinyToggle (ArdourCanvas::Item *i) : MixbusToggle(i)
{
}

void
MixbusTinyToggle::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_t *ct = cr->cobj();

	bool flat = UIConfiguration::instance().get_flat_buttons();

//	if (!_auto_ctrl) return;

	bool state = (_last_val == _tog_value);

	float _diameter = height-6;

	{ //indicator led --------------
		cairo_pattern_t* led_inset_pattern;

		cairo_save (ct);
		cairo_translate (ct, width/2.0, height/2.0);

		if (!flat) {
			led_inset_pattern = cairo_pattern_create_linear (0.0, 0.0, 0.0, _diameter);
			cairo_pattern_add_color_stop_rgba (led_inset_pattern, 0, 0,0,0, 0.4);
			cairo_pattern_add_color_stop_rgba (led_inset_pattern, 1, 1,1,1, 0.7);

			//inset
			cairo_arc (ct, 0, 0, _diameter/2, 0, 2 * M_PI);
			cairo_set_source (ct, led_inset_pattern);
			cairo_fill (ct);
		}

		//black ring
		cairo_set_source_rgb (ct, 0, 0, 0);
		cairo_arc (ct, 0, 0, _diameter/2-1, 0, 2 * M_PI);
		cairo_fill(ct);

		//led color
		if (true) {
			cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, state ? 1.0 : 0.30);
			cairo_arc (ct, 0, 0, _diameter/2-3, 0, 2 * M_PI);
			cairo_fill(ct);
		}

		//reflec
		if (!flat && (UIConfiguration::instance().get_mixbus_strip_scale() > 0.89) ) {
			Cairo::RefPtr<Cairo::RadialGradient> shine_pattern = Cairo::RadialGradient::create ( -halfh/8, -halfh/4, 1, -halfh/8, -halfh/4, halfh*0.25  );
			shine_pattern->add_color_stop_rgba ( 0.0, 0,0,0, 0.0);
			shine_pattern->add_color_stop_rgba ( 1.0, 0,0,0, state ? 0.3 : 0.5);
			cr->set_source (shine_pattern);
			cr->arc(0, 0, _diameter/2-3, 0, 2 * M_PI );
			cr->fill();
		}
//		cairo_arc (ct, 0, 0, _diameter/2-2, 0, 2 * M_PI);
//		cairo_set_source (ct, reflection_pattern);
//		cairo_fill (ct);

		cairo_restore (ct);
	} //-----------------------------

	//user is currently pressing the button. dark outline helps to indicate this
	if (_grabbed ) {
		Gtkmm2ext::rounded_rectangle (ct, 3, 3, width-6, height-6, (height-6)/2);
		cairo_set_line_width(ct, 2);
		cairo_set_source_rgba (ct, 0.0, 0.0, 0.0, 0.6);
		cairo_stroke (ct);
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 2, 2, width-4, height-4, (height-4)/2);
		cr->fill ();
	}

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}


// ---------------------------------------------------------------------
MixbusSlate::MixbusSlate (ArdourCanvas::Item *i) : MixbusWidget(i)
{
	Event.connect (sigc::mem_fun (*this, &MixbusSlate::event_handler));
}

MixbusSlate::~MixbusSlate ()
{
	_route_connections.drop_connections();
}

void
MixbusSlate::set_route (std::shared_ptr<ARDOUR::Route> rt)
{
	_route = rt;

	if (_route->is_track()) {
		_rec_enable_ctrl = _route->rec_enable_control();
		_rec_enable_ctrl->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusSlate::something_changed, this), gui_context());
	}
}

void
MixbusSlate::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	float h = ScreenSizer::user_scale();  //scale factor

	Distance height = self.height();
	Distance width = self.width();

	const int track_number = _route->track_number();
	const int mb_number = _route->mixbus();
	char buf[256];
	if ( mb_number ) {
		sprintf(buf, "%d", mb_number);
	} else {
		sprintf(buf, "%d", track_number);
	}

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	bool boxy = UIConfiguration::instance().get_boxy_buttons();
	bool recording = false;

	cairo_t *ct = cr->cobj();

	//white box
//	Gtkmm2ext::rounded_rectangle( ct, 1, 1, width-2, height-2, 4*h);
	cairo_rectangle( ct, 0, 0, width, height);
	cairo_set_source_rgba(ct, 0.85,0.85,0.82 ,1.0);  //TODO slate bg color
	cr->fill_preserve ();
//	set_source_rgba(ct, _color);
//	cr->set_line_width(1);
//	cr->stroke ();
	cairo_new_path (ct);

	//noise
	if (true){
		cairo_save (ct);

		//clip rect
		cairo_rectangle (ct, 1, 1, width, height);
		cairo_clip(ct);
		cairo_new_path (ct);

		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( 0.2 + 0.5*mod.a() );
		cairo_restore (ct);
	}

	//color bar
//	Gtkmm2ext::rounded_rectangle( ct, 1, 1, width-2, height-2, 4*h);
	cairo_new_path (ct);
	cairo_rectangle( ct, 0, height-4*h, width, 4*h);
	set_source_rgba(ct, _color);
	cr->fill_preserve ();
//	set_source_rgba(ct, _color);
//	cr->set_line_width(1);
//	cr->stroke ();
	cairo_new_path (ct);


	int tw, th;
	int toffset = 5*h;

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetBigBoldFontDesc() );

	 //Track #
	 //tracks will get a track#, this helps for recognizing in MMC or MCU controllers
	 if (_route->is_track() ||_route->mixbus() ) {
		cairo_save (ct);

			if (_route->is_track()) {

				if ( _rec_enable_ctrl && _rec_enable_ctrl->get_value()) {
					recording = true;
					
					uint32_t rec_color = UIConfiguration::instance().color ("transport recenable button: fill active");
					Gtkmm2ext::set_source_rgb_a (cr, rec_color, 1.0);
				} else {
					uint32_t bg_color = UIConfiguration::instance().color ("mixbus tracknum circle");
					Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
					cr->fill_preserve ();
				}

				cr->arc( 2*h + height/2, height/2, height/2-2, 0, TWO_PI);
				cr->fill_preserve ();
				cairo_set_source_rgba (ct, 0,0,0, 1);
				cr->set_line_width(h);
				cr->stroke ();

				toffset = 25*h;

			} else if (_route->mixbus()) {
				uint32_t bg_color = UIConfiguration::instance().color ("mixbus tracknum circle");
				Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
				Gtkmm2ext::rounded_rectangle( ct, 4*h, 3*h, height-4*h, height-6*h, 3*h);
				cr->fill_preserve ();
				cairo_set_source_rgba (ct, 0,0,0, 1);
				cr->set_line_width(h);
				cr->stroke ();
				toffset = 25*h;
			}

			//track #  (clip rect)
			cairo_new_path (ct);
			cairo_rectangle (ct, 1, 1, height-2, height-2);
			cairo_clip(ct);

			//track #  (text)
			cairo_new_path (ct);
			layout->set_text (buf);
			layout->get_pixel_size (tw, th);
			if (recording) {
				cairo_set_source_rgba (ct, 0,0,0, 1);
			} else {
				cairo_set_source_rgba (ct, 0.75,0.75,0.75, 1);
			}
			cairo_translate(ct, floor(2*h + (height/2) - tw/2), floor(height/2 - th/2));
			layout->show_in_cairo_context (cr);

		cairo_restore (ct);
	}

	//Track name - black text
	cairo_save (ct);
		//clip rect
		cairo_rectangle (ct, 1, 1, width-4, height-2);
		cairo_clip(ct);
		cairo_new_path (ct);

		//track name
		layout->set_text (_text);
		layout->get_pixel_size (tw, th);
		cairo_set_source_rgba(ct, 0,0,0, 1);
		cairo_translate(ct, floor(toffset), floor(height/2 - th/2 -0*h));
		layout->show_in_cairo_context (cr);
	cairo_restore (ct);

	//right shadow to fade-out text
	Cairo::RefPtr<Cairo::LinearGradient> r_shadow = Cairo::LinearGradient::create (width-8*h, 0, width, 0);
	r_shadow->add_color_stop_rgba ( 0.0, 0.75,0.75,0.72, 0.0);
	r_shadow->add_color_stop_rgba ( 1.0, 0.75,0.75,0.72, 1.0);  //TODO slate bg color
	cr->set_source (r_shadow);
	cr->rectangle( width-8*h, 0, 8*h, height-4*h );
	cr->fill ();

	bool flat = false;

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 4, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 4, height );
		cr->fill ();
	}

	//drop-shadow at top
	if (!flat) {
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 4);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 4 );
		cr->fill ();
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.2);
		Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, boxy ? 0 : 3*h );
		cr->fill ();
	}
}


bool
MixbusSlate::event_handler ( GdkEvent* ev)
{
	switch (ev->type) {
		case GDK_SCROLL:
			break;
		case GDK_BUTTON_PRESS:
			break;
		case GDK_2BUTTON_PRESS:
			break;
		case GDK_BUTTON_RELEASE:
			break;
		case GDK_ENTER_NOTIFY:
			_highlight = true;
			redraw();
			break;
		case GDK_LEAVE_NOTIFY:
			_highlight = false;
			redraw();
			break;
		case GDK_MOTION_NOTIFY:
			break;
		default:
			break;
	}
	return false;
}

//-----------------------------------------------


MixbusMeterBar::MixbusMeterBar ( Item* c)
	: MixbusWidget ( c )
{
       _l_val = _r_val = _l_hold = _r_hold = _l_red = _r_red = _stereo = 0;

	_redux = false;
}

void
MixbusMeterBar::meter( float l, float r, float hold_l, float hold_r, bool st, bool input )
{
	bool need_redraw = false;

	float max_fader_gain = Config->get_max_gain ();
	float metr_peak_levl = UIConfiguration::instance().get_meter_peak();

	float val = dB_to_coefficient(-60);
	float sig_pres = gain_to_slider_position_with_max (val, max_fader_gain);

	val = dB_to_coefficient(-80);
	float neg80 = gain_to_slider_position_with_max (val, max_fader_gain);

	val = dB_to_coefficient(6);
	float top = gain_to_slider_position_with_max (val, max_fader_gain);

	//note: main channel meter values come in as dB; we convert to the fader scale

	//LEFT PEAK
	val = dB_to_coefficient(l);
	val = gain_to_slider_position_with_max (val, max_fader_gain);
	if (val>top) val = top;
	if (val < neg80) {
		val = 0.0;
	} else if (val < sig_pres) {
		val = sig_pres;
	}
	if ( fabs(val - _l_val) > 0.002 ) {
		need_redraw = true;
		_l_val = val;
	}

	//LEFT hold
	if (input && hold_l  >= metr_peak_levl )
		_l_red = true;
	else
		_l_red = false;
	val = dB_to_coefficient(hold_l);
	val = gain_to_slider_position_with_max (val, max_fader_gain);
	if (val>top) val = top;
	if (val < neg80){
		 val = 0.0;
	} else if (val < sig_pres){
		 val = sig_pres;
	}
	if ( fabs(val - _l_hold) > 0.002 ) {
		need_redraw = true;
		_l_hold = val;
	}

	if (st) {

		//RIGHT PEAK
		val = dB_to_coefficient(r);
		val = gain_to_slider_position_with_max (val, max_fader_gain);
		if (val>top) val = top;
		if (val < neg80) {
			val = 0.0;
		} else if (val < sig_pres) {
			 val = sig_pres;
		}
		if ( fabs(val - _r_val) > 0.002 ) {
			need_redraw = true;
			_r_val = val;
		}

		//RIGHT hold
		if (input && hold_r >= metr_peak_levl)
			_r_red = true;
		else
			_r_red = false;
		val = dB_to_coefficient(hold_r);
		val = gain_to_slider_position_with_max (val, max_fader_gain);
		if (val>top) val = top;
		if (val < neg80) {
			val = 0.0;
		} else if (val < sig_pres) {
			 val = sig_pres;
		}
		if ( fabs(val - _r_hold) > 0.002 ) {
			need_redraw = true;
			_r_hold = val;
		}

	}

	//stereo
	if ( _stereo != st ) {
		need_redraw = true;
		_stereo = st;
	}

	//trigger a redraw if we haven't already drawn this meter value
	if ( need_redraw ) {
		redraw();
	}
}

void
MixbusMeterBar::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	//black border...this should be in draw_bg
	Gtkmm2ext::Color _active_color = rgba_to_color (0,0,0,0.8);
	set_source_rgba (cr, _active_color);
	cr->rectangle(0, 0, width, height);
	cr->fill ();

	//colored meter bar
	_active_color = rgba_to_color (0,1,0,0.55);  //mono should be dimmer, so it matches the brightness of the slighly smaller stereo meters
	if (_stereo )
		_active_color = rgba_to_color (0,1,0,0.8);
	set_source_rgba (cr, _active_color);
	cr->rectangle(2, 2, width-4, height-3);
	cr->fill ();

	//draw a gray overlay
	float travel = height - 1.5;
	_active_color = rgba_to_color (0,0,0,0.85);

	//if hold_target is 0,  don't draw peak holds
	uint32_t hold_target = (uint32_t) floor(UIConfiguration::instance().get_meter_hold());

	if (_stereo ) {
		float bwidth = (width-2)/3;

		//black line in middle
		cr->set_source_rgb (0,0,0);
		cr->rectangle(1+bwidth, 0, bwidth, height);
		cr->fill ();

		float tr = (1.0-_l_val)*travel;
		set_source_rgba (cr, _active_color);
		cr->rectangle(1, 1.5, bwidth, tr);
		cr->fill ();

		tr = (1.0-_r_val)*travel;
		set_source_rgba (cr, _active_color);
		cr->rectangle(width-1-bwidth, 1.5, bwidth, tr);
		cr->fill ();

		//peak hold ?
		if (hold_target) {
			tr = (1.0-_l_hold)*travel;
			set_source_rgba (cr, _l_red ? rgba_to_color (1,0,0,1) : rgba_to_color (0,1,0,0.8) );
			cr->rectangle(2, tr, bwidth-1, 2);
			cr->fill ();

			tr = (1.0-_r_hold)*travel;
			set_source_rgba (cr, _r_red ? rgba_to_color (1,0,0,1) : rgba_to_color (0,1,0,0.8) );
			cr->rectangle(width-1-bwidth, tr, bwidth-1, 2);
			cr->fill ();
		}

	} else {
		float tr = (1.0-_l_val)*travel;
		set_source_rgba (cr, _active_color);
		cr->rectangle(1, 1.5, width-2, tr);
		cr->fill ();

		//peak hold ?
		if (hold_target) {
			tr = (1.0-_l_hold)*travel;
			set_source_rgba (cr, _l_red ? rgba_to_color (1,0,0,1) : rgba_to_color (0,1,0,0.55) );
			cr->rectangle(2, tr, width-4, 2);
			cr->fill ();
		}
	}

}

//-----------------------------------------------


MixbusMeterDot::MixbusMeterDot ( Item* c)
	: MixbusWidget ( c )
{
	_color = rgba_to_color (0,1,0,0.8);

	_last_on = _redux = false;
}

void
MixbusMeterDot::set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::ReadOnlyControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;
	_ro_ctrl = ctrl;
}

void
MixbusMeterDot::meter()
{
	if (!_ro_ctrl) {
		return;
	}

	float val = _ro_ctrl->get_parameter ();

	bool _on = false;

	if (_redux) {
		_on = (val < _thresh);
	} else {
		_on = (val >= _thresh);
	}

	if (_on != _last_on)
		redraw();

	_last_on = _on;
}

void
MixbusMeterDot::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	Gtkmm2ext::Color _active_color = rgba_to_color (0,0,0,0.8);
	set_source_rgba (cr, _active_color);
	cr->rectangle(0, 0, width, height);
	cr->fill ();

	if (_last_on)
		_active_color = _color;
	else
		_active_color = color_at_alpha ( _color, 0.1);

	set_source_rgba (cr, _active_color);
	cr->rectangle(1, 1, width-2, height-2);
	cr->fill ();
}


//------------------------------------------

MixbusReduxDot::MixbusReduxDot ( Item* c)
	: MixbusMeterDot ( c )
{
	_color = rgba_to_color (1,1,0,0.8);
	_redux = true;
}

//------------------------------------------



MixbusPointerMeter::MixbusPointerMeter ( Item* c, bool k14) : MixbusWidget ( c )
{
	_last_val = 0;
	_k14_scale = k14;

	flicker_offset = random()%4000;

	if (!meter_texture) {
#ifdef MIXBUS32C
		meter_texture = ::get_icon ("strip_texture32c");
#else
		meter_texture = ::get_icon ("strip_texture");
#endif
	}
	if (!harrison_bug_img) {
		harrison_bug_img = ::get_icon ("harrison_bug");
	}
}

void
MixbusPointerMeter::set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::ReadOnlyControl> ctrl )
{
	_connection.disconnect();
	_auto_ctrl.reset();

	_stripable = rt;
	_ro_ctrl = ctrl;
}

void
MixbusPointerMeter::meter()
{
	if (_ro_ctrl) {

		float val = _ro_ctrl->get_parameter ();

		if (_k14_scale) {
			val = 3.01 + 10.0*log10( val+1.0e-12);
			val = 1.0 - (val / -28.0);
		} else {
			val = 20.0*log10( val+1.0e-12);
			val = 1.0+0.01211*val;
		}

		if( val > 1.0)val = 1.0;
		if( val < 0.01)
			val = 0.0;  //if we are close to zero, round to zero

		//trigger a redraw if we haven't already drawn this meter value
		if ( fabs(val - _last_val) > 0.005 ) {
			redraw();
			val = val*0.7 + _last_val*0.3;  // apply a little ballistic averaging
			_last_val = val;
		}

	}
}


static float SWING_DEG = 90;
static float SWING_START = ( 180-SWING_DEG/2.0);


void
MixbusPointerMeter::render_bg_top (Cairo::RefPtr<Cairo::Context> cr, bool ) const
{
#ifdef LAYOUT_HELPER
	return;
#endif

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	cairo_t *ct = cr->cobj();

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();
	Distance halfw = width/2.0;

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : 2;

	if (true /*!flat*/) {
		//shadow
		cr->translate (2, 2);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, width, height, _corner_radius);
		cr->fill ();
		cr->translate (-2, -2);
	}

	//texture
//	gdk_cairo_set_source_pixbuf (ct, meter_texture->gobj(), 0, 0);
//	cr->rectangle (3, 3, width-6, height-6);
//	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("mixbus vu bg");
	double bg_r,bg_g,bg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, bg_r, bg_g, bg_b, unused);

	//yellow bg
	cr->set_source_rgba (bg_r, bg_g, bg_b, 0.6);
//	cr->set_source_rgba (0.68, 0.66, 0.48, 0.7);
	Gtkmm2ext::rounded_rectangle (ct, 4, 4, width-8, height-8, _corner_radius );
	cr->fill ();

	//inner black border
	cr->set_line_width(2.0);
	cr->set_source_rgba (0,0,0, 0.8);
	Gtkmm2ext::rounded_rectangle (ct, 3, 3, width-6, height-6, _corner_radius );
	cr->stroke ();

	//outer black border
	cr->set_line_width(1.0);
	cr->set_source_rgba (0.3, 0.3, 0.3, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius );
	cr->stroke ();

	//harrison bug OR k14
	if (_k14_scale) {
		cr->set_source_rgba (0.4, 0.4, 0.4, 1.0);
		cairo_save (ct);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );
		layout->set_text ("K-14");
		int w, h; layout->get_pixel_size (w, h);
		cr->translate ( 0, height-1  );
//		cr->scale ( 0.75, 0.75 );
		cr->translate ( floor(6), floor(-h-4)  );
		layout->show_in_cairo_context (cr);
		cairo_restore (ct);
	} else if (false) {
		cairo_save (ct);
		gdk_cairo_set_source_pixbuf (ct, harrison_bug_img->gobj(), width/2-6, height*0.5);
		cr->translate (width/2-6, height*0.5);
		cr->rectangle (0, 0, 12, 12);
		cr->fill ();
		cairo_restore (ct);
	}

	//clip rect
	cairo_save (ct);
	cairo_rectangle (ct, 4, 4, width-8, height-8);
	cairo_clip(ct);

	//color arc(s)
	if (_k14_scale) {
		//green arc
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->set_source_rgba ( 0, 1, 0, 0.5 );
		cr->arc( halfw, height*1.2, height*0.85, (2*M_PI)*(225)/360, (2*M_PI)*(225+45)/360  );
		cr->set_line_width ( 1.0 );
		cr->stroke ();

		//yellow overload arc
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->set_source_rgba ( 1, 1, 0, 0.7 );
		cr->arc( halfw, height*1.2, height*0.85, (2*M_PI)*(270)/360.0, (2*M_PI)*(283)/360.0  );
		cr->set_line_width ( 1.0 );
		cr->stroke ();

		//red overload arc
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->set_source_rgba ( 1, 0, 0, 0.7 );
		cr->arc( halfw, height*1.2, height*0.85, (2*M_PI)*(283)/360.0, (2*M_PI)*(315)/360.0  );
		cr->set_line_width ( 1.0 );
		cr->stroke ();

//		cr->set_identity_matrix();
//		cr->translate (self.x0, self.y0-0.5);
//		cr->set_source_rgba ( 0, 0, 0, 0.7 );
//		cr->arc( halfw, height*1.2, height*0.85, (2*M_PI)*(225+55)/360, (2*M_PI)*(over)/360  );
//		cr->set_line_width ( 1.0 );
//		cr->stroke ();
	} else {

		float over = 300;  //arbitrary angle to turn "red"

		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->set_source_rgba ( 0, 0, 0, 0.7 );
		cr->arc( halfw, height*1.2, height*0.85, (2*M_PI)*(225)/360, (2*M_PI)*(over)/360  );
		cr->set_line_width ( 1.0 );
		cr->stroke ();

		//red overload arc
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		cr->set_source_rgba ( 1, 0, 0, 0.7 );
		cr->arc( halfw, height*1.2, height*0.85-1, (2*M_PI)*(over)/360.0, (2*M_PI)*(315)/360.0  );
		cr->set_line_width ( 2.0 );
		cr->stroke ();

	}

	//tickmarks
	if (_k14_scale) {

			cr->set_line_width ( 1.0 );
			cr->set_source_rgba ( 0, 0, 0, 0.7 );

/*			//-6 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55-20)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();

			//-4 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55-13)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();

			//-2 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55-6)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.9);
			cr->stroke ();
*/
			//0 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.9);
			cr->stroke ();

/*
			//+2 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+6)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();
*/

			//+4 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+13)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.9);
			cr->stroke ();

/*
 *			//+6 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+20)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();

			//+8 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+27)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();

			//+10 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+33)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();

			//+12 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+41)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.85);
			cr->stroke ();
*/
			//+14 tickmark
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);
			cr->rotate( (2*M_PI)*(125+55+45)/360 );
			cr->move_to( 0, height*0.75 );
			cr->line_to ( 0, height*0.9);
			cr->stroke ();

	} else {
		float numsteps = 10;  float range = 60;
		float step = range/(numsteps-1);
		float ang = 55 - range/2.0;
		for (int i = 0; i < numsteps; i++) {

			//move to base of meter
			cr->set_identity_matrix();
			cr->translate (self.x0, self.y0-0.5);
			cr->translate(halfw, height*1.2);

			cr->set_source_rgba ( 0, 0, 0, 0.7 );

			cr->rotate( (2*M_PI)*(125+ang)/360 );

			cr->move_to( 0, height*0.85 );
			cr->line_to ( 0, height*0.95);
			cr->set_line_width ( 1.0 );
			cr->stroke ();

			ang += step;
		}
	}

	//text
	if (_k14_scale) {
		cr->set_source_rgba ( 0, 0, 0, 0.4 );

		int w, h;

//		cr->set_identity_matrix();
//		cr->translate (self.x0, self.y0-0.5);
//		layout->set_text ("-3");
//		layout->get_pixel_size (w, h);
//		cr->translate ( width*0.36-0.5*w, 0.2*height-0.5*h );
//		cr->scale ( 0.7, 0.7 );
//		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		layout->set_text ("0");
		layout->get_pixel_size (w, h);
		cr->translate ( floor(0.5*width-0.5*w), floor(0.22*height-0.5*h) );
		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		layout->set_text ("+4");
		layout->get_pixel_size (w, h);
		cr->translate ( floor(width*0.64-0.5*w), floor(0.2*height-0.5*h) );
		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);
		layout->set_text ("+14");
		layout->get_pixel_size (w, h);
		cr->translate ( floor(width*0.84-0.5*w), floor(0.4*height-0.5*h) );
		layout->show_in_cairo_context (cr);
	} else {

		cr->set_source_rgba ( 0, 0, 0, 0.4 );

		//locate
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);

		//draw text
		layout->set_text ("-");
		int w, h; layout->get_pixel_size (w, h);
		cr->translate ( floor(width*0.16), 8 );
		layout->show_in_cairo_context (cr);

		//locate
		cr->set_identity_matrix();
		cr->translate (self.x0, self.y0-0.5);

		//draw text
		layout->set_text ("+");
		layout->get_pixel_size (w, h);
		cr->translate ( floor(width*0.84-w), 8 );
		layout->show_in_cairo_context (cr);

	}

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

/*	//right overload
	Cairo::RefPtr<Cairo::LinearGradient> r_red = Cairo::LinearGradient::create (0.0, 2.0, width, 0.0);
	r_red->add_color_stop_rgba ( 0.0, 1.0, 0.0, 0.0, 0.0);
	r_red->add_color_stop_rgba ( 0.6, 1.0, 0.0, 0.0, 0.0);
	r_red->add_color_stop_rgba ( 1.0, 1.0, 0.0, 0.0, 0.9);
	cr->set_source (r_red);
	cr->begin_new_path ();
	cr->arc( halfw, height, height-8, (2*M_PI)*(170+ang)/360, (2*M_PI)*(245+ang)/360  );
	cr->line_to( width/2.0, height );
	cr->close_path ();
	cr->fill ();
*/
	if (!flat) {
		//left shadow
		Cairo::RefPtr<Cairo::LinearGradient> l_shadow = Cairo::LinearGradient::create (0.0, height-8, width, height - 12);
		l_shadow->add_color_stop_rgba ( 0.0, 0.0,0.0,0.0, 0.3);
		l_shadow->add_color_stop_rgba ( 0.2, 0.0,0.0,0.0, 0.0);
		l_shadow->add_color_stop_rgba ( 1.0, 0.0,0.0,0.0, 0.0);
		cr->set_source (l_shadow);
		cr->rectangle( 4, 4, width-8, height-8 );
		cr->fill ();

		//right shadow
		Cairo::RefPtr<Cairo::LinearGradient> r_shadow = Cairo::LinearGradient::create (0.0, height/2, width, height);
		r_shadow->add_color_stop_rgba ( 0.0, 0.0,0.0,0.0, 0.0);
		r_shadow->add_color_stop_rgba ( 0.7, 0.0,0.0,0.0, 0.0);
		r_shadow->add_color_stop_rgba ( 1.0, 0.0,0.0,0.0, 0.4);
		cr->set_source (r_shadow);
		cr->rectangle( 4, 4, width-8, height-8 );
		cr->fill ();
	}

	if (!flat) {

		//btm shadow
		Cairo::RefPtr<Cairo::LinearGradient> b_shadow = Cairo::LinearGradient::create (2.0, height-6, 2.1, height);
		b_shadow->add_color_stop_rgba ( 0.0, 0.0,0.0,0.0, 0.0);
		b_shadow->add_color_stop_rgba ( 1.0, 0.0,0.0,0.0, 1.0);
		cr->set_source (b_shadow);
		cr->rectangle( 4, 4, width-8, height-8 );
		cr->fill ();

		//glow
		Cairo::RefPtr<Cairo::RadialGradient> glow_pattern = Cairo::RadialGradient::create ( halfw, height, 1, halfw, height, height*0.65  );
		glow_pattern->add_color_stop_rgba ( 0.0, 0.9, 0.7, 0.5, 0.6);
		glow_pattern->add_color_stop_rgba ( 0.3, 0.9, 0.7, 0.5, 0.4);
		glow_pattern->add_color_stop_rgba ( 1.0, 0.9, 0.7, 0.5, 0.0);
		cr->set_source (glow_pattern);
		cr->rectangle( 4, 3, width-8, height-7 );
		cr->fill ();
	}
	cairo_restore (ct);

}

static int flicker_counter;

void
MixbusPointerMeter::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}

	cairo_t *ct = cr->cobj();

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_save (ct);
	cairo_rectangle (ct, 4, 4, width-8, height-8);
	cairo_clip(ct);

	//move to base of meter
	cr->translate(width/2.0, height*1.2+3);
	//draw line SHADOW
	cr->set_source_rgba ( 0, 0, 0, 0.2 );
	cr->rotate( (2*M_PI)*SWING_START/360 );  //knob line swings from 125 to 235 degrees
	cr->rotate( (2*M_PI)*_last_val*SWING_DEG/360 ); //110 degree swing
	cr->move_to( 1, height*0.1 );
	cr->line_to ( 0, height*0.93 );
	cr->set_line_width ( 3.0 );
	cr->stroke ();

	//move to base of meter
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);
	cr->translate(width/2.0, height*1.2);

	//draw line
	cr->set_source_rgb ( 0, 0, 0 );
	cr->rotate( (2*M_PI)*SWING_START/360 );  //knob line swings from 125 to 235 degrees
	cr->rotate( (2*M_PI)*_last_val*SWING_DEG/360 ); //110 degree swing
	cr->move_to( 0, height*0.1 );
	cr->line_to ( 0, height*0.95 );
	cr->set_line_width ( 1 );
	cr->stroke ();

	cairo_restore (ct); //clear clip rect

	bool flat = UIConfiguration::instance().get_flat_buttons();
	if (false && !flat) {
		float halfw = width/2.0;

		float brightness = 0.2+(_last_val)/3;
		flicker_counter++;
		int flick = flicker_counter+flicker_offset;
		if (flicker_counter%75 < 15 && flick%4000 < 150) {
			brightness *= 0.5;
		}
		brightness += 0.4;

		//glow
		Cairo::RefPtr<Cairo::RadialGradient> glow_pattern = Cairo::RadialGradient::create ( halfw, height, 1, halfw, height, height*0.45  );
		glow_pattern->add_color_stop_rgba ( 0.0, 0.9, 0.7, 0.5, brightness);
		glow_pattern->add_color_stop_rgba ( 0.4, 0.9, 0.7, 0.5, brightness*0.9);
		glow_pattern->add_color_stop_rgba ( 1.0, 0.9, 0.7, 0.5, 0.0);
		cr->set_source (glow_pattern);
		cr->rectangle( 4, 3, width-8, height-7 );
		cr->fill ();

		//reflection
		Cairo::RefPtr<Cairo::RadialGradient> ref_pattern = Cairo::RadialGradient::create ( width*0.35, height*0.15, 1, width*0.15, height*0.45, width*0.65  );
		ref_pattern->add_color_stop_rgba ( 0.0, 1,1,1, 0.2);
		ref_pattern->add_color_stop_rgba ( 0.45, 0.5, 0.5, 0.5, 0.1);
		ref_pattern->add_color_stop_rgba ( 1.0, 1,1,1, 0.1);
		cr->set_source (ref_pattern);
		Gtkmm2ext::rounded_rectangle (ct, 7, 7, (width/2.0) - 7, height-15, 2);
		cr->fill ();
	}

	cairo_save (ct);
	cairo_rectangle (ct, 4, 4, width-8, height-8);
	cairo_clip(ct);

	//move to base of meter
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);
	cr->translate(width/2.0, height*1.2-1.5);

	//line reflection
	cr->set_source_rgba ( 1, 1, 1, 0.15 );
	cr->rotate( (2*M_PI)*SWING_START/360 );  //knob line swings from 125 to 235 degrees
	cr->rotate( (2*M_PI)*_last_val*SWING_DEG/360 ); //110 degree swing
	cr->move_to( 0, height*0.1 );
	cr->line_to ( 0, height*0.95 );
	cr->set_line_width ( 1 );
	cr->stroke ();

	cairo_restore (ct); //clear clip rect
}

// -------------------- -----------------------------


MixbusPhaseMeter::MixbusPhaseMeter ( Item* c) : MixbusWidget ( c )
{
//	_active_color = _color = rgba_to_color (1,1,1,1);

	_last_low_val = 0;
	_last_high_val = 0;

	if (!meter_texture) {
#ifdef MIXBUS32C
		meter_texture = ::get_icon ("strip_texture32c");
#else
		meter_texture = ::get_icon ("strip_texture");
#endif
	}
}

void
MixbusPhaseMeter::set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::ReadOnlyControl> low_ctrl, std::shared_ptr<ARDOUR::ReadOnlyControl> high_ctrl )
{
	_stripable = rt;
	_low_phase_ctrl = low_ctrl;
	_high_phase_ctrl = high_ctrl;
}

void
MixbusPhaseMeter::meter()
{
	if (_low_phase_ctrl && _high_phase_ctrl) {

		float l_val = (1.0+_low_phase_ctrl->get_parameter())/2.0;
		float h_val = (1.0+_high_phase_ctrl->get_parameter())/2.0;

		//range check
		if (l_val < 0) l_val = 0;  if (l_val > 1.0) l_val = 1.0;
		if (h_val < 0) h_val = 0;  if (h_val > 1.0) h_val = 1.0;

		//trigger a redraw if we haven't already drawn this meter value
		bool need_redraw = false;

		if ( fabs(l_val - _last_low_val ) > 0.005 ) {
			need_redraw = true;
			_last_low_val = l_val*0.7 + _last_low_val*0.3;
		}

		if ( fabs(h_val - _last_high_val ) > 0.005 ) {
			need_redraw = true;
			_last_high_val = h_val*0.7 + _last_high_val*0.3;
		}

		if (need_redraw)
			redraw();

	}
}


void
MixbusPhaseMeter::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool ) const
{
#ifdef LAYOUT_HELPER
	return;
#endif

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));

	cairo_t *ct = cr->cobj();

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : 2;

	if (!flat) {
		//shadow
		cr->translate (1, 1);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, width, height, _corner_radius);
		cr->fill ();
		cr->translate (-1, -1);
	}

	//texture
//	gdk_cairo_set_source_pixbuf (ct, meter_texture->gobj(), 0, 0);
//	cr->rectangle (3, 3, width-6, height-6);
//	cr->fill ();

	//black bg
	cr->set_source_rgba (0.0, 0.0, 0.0, 1.0);
	Gtkmm2ext::rounded_rectangle (ct, 3, 3, width-6, height-6, _corner_radius);
	cr->fill ();

	//color bands
	Cairo::RefPtr<Cairo::LinearGradient> color_pattern = Cairo::LinearGradient::create (5.0, 0.0, width-10, 0.0);
	color_pattern->add_color_stop_rgba ( 0.0, 1.0, 0.0, 0.0, 1.0);
	color_pattern->add_color_stop_rgba ( 0.4, 0.0, 1.0, 0.0, 0.6);
	color_pattern->add_color_stop_rgba ( 0.6, 0.0, 1.0, 0.0, 0.6);
	color_pattern->add_color_stop_rgba ( 1.0, 1.0, 1.0, 0.0, 1.0);
	cr->set_source (color_pattern);
	cr->rectangle (5, 5, width - 10, height-10); //make hourglass path here?
	cr->fill ();

	//inner grey border
	cr->set_line_width(2.0);
	cr->set_source_rgba (1,1,1, 0.3);
	Gtkmm2ext::rounded_rectangle (ct, 2, 2, width-4, height-4, _corner_radius);
	cr->stroke ();
}

void
MixbusPhaseMeter::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}

	cairo_t *ct = cr->cobj();

	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	//limit the ranges so there's always a color bar at the top or bottom showing
	float lowv = _last_low_val * 0.8;
	float highv = 0.2 + (_last_high_val*0.8);

	//color bar mask
	{
		cairo_set_source_rgba (ct, 0, 0, 0, 0.7);
		cairo_rectangle (ct, 4, 3, (width-8)*lowv, height-8);
		cairo_fill(ct);
		cairo_rectangle (ct, (width-8)*highv, 3, (width-4) - ((width-8)*highv), height-8);
		cairo_fill(ct);
	}

}
