/*
 * Copyright (C) 2016 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _harrison_mb_license_manager_h_
#define _harrison_mb_license_manager_h_

#include <vector>
#include <gtkmm.h>
#include "ardour/plugin.h"
#include "ardour/processor.h"

#include "ardour_window.h"

class LicenseManager : public ArdourWindow
{
public:
	LicenseManager ();
	void session_going_away ();
	void set_session(ARDOUR::Session *s);

	bool need_licenses () const { return !_uris.empty() || _mixbus_licensed != 0; }
	bool unlicensed_plugins () const { return !_unlicensed.empty() || _mixbus_licensed < 0; }
	bool session_has_unlicened_plugins () const { return _session_has_unlicened_plugins; }

	// return -1 if mixbus is unlicensed
	// return +1 if mixbus is licensed
	// return 0 if no license is needed or status cannot be determined (no master out LADSPA, no session, etc)
	static int mixbus_license_status (ARDOUR::Session* s);
	static bool download_license_dialog ( bool at_startup = false );
	static bool save_mixbus_license (std::string);

protected:
	bool on_delete_event (GdkEventAny*);

private:
	Gtk::Notebook pages;

	void setup ();
	void refiller (const ARDOUR::PluginInfoList&);
	void selection_changed ();
	void store_btn_clicked ();
	void home_btn_clicked ();

	class LicenseModelColumns : public Gtk::TreeModelColumnRecord
	{
		public:
			LicenseModelColumns ()
			{
				add (licensed);
				add (uri);
				add (name);
				add (licensee);
				add (store_url);
				add (license_file);
			}

			Gtk::TreeModelColumn<bool> licensed;
			Gtk::TreeModelColumn<std::string> uri;
			Gtk::TreeModelColumn<std::string> name;
			Gtk::TreeModelColumn<std::string> licensee;
			Gtk::TreeModelColumn<std::string> store_url;
			Gtk::TreeModelColumn<std::string> license_file;
	};

	Gtk::Button _store_button;
	Gtk::Button _home_button;

	Glib::RefPtr<Gtk::ListStore> _store;
	LicenseModelColumns _model;
	Gtk::TreeView _view;

	std::vector<std::string> _uris;
	std::vector<std::string> _unlicensed;
	int  _mixbus_licensed;
	bool _session_has_unlicened_plugins;

	void check_processor_license (std::weak_ptr<ARDOUR::Processor>);
};

#endif /* _harrison_mb_license_manager_h_ */
