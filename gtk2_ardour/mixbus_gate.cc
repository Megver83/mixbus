/*
    Copyright (C) 2023 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_comp.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------



//an MBCanvas for input knobs
MixbusGateCanvas::MixbusGateCanvas ()
{
	_knobs = new MixbusGate(root());
}

//-------------------------


MixbusGate::~MixbusGate ()
{
	_gate_md_connection.disconnect();
	_gate_md_ctrl.reset();
}

MixbusGate::MixbusGate (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_gate;

	std::string tl[NUM_KNOB_TICKS] = { "|", "|", "|", "|", "|", "|", "|", "|", "|", "|", "|" };
	std::string spd[NUM_KNOB_TICKS] = { "|", "Fst", "|", "|", "|", "|", "|", "|", "|", "Slo", "|" };

	gate_attk = new MixbusKnob(this);
	gate_attk->set_color("mixbus gate attack knob");
	widgets.push_front(gate_attk);
//	std::string ga[NUM_KNOB_TICKS] = { "|", ".2", "|", "|", "25", "|", "120", "|", "|", "20", "|" };
	gate_attk->set_ticks(spd);

	gate_rels = new MixbusKnob(this);
	widgets.push_front(gate_rels);
	gate_rels->set_color("mixbus gate release knob");
//	std::string gr[NUM_KNOB_TICKS] = { "|", "2", "|", "|", "25", "|", "150", "|", "|", " 1.5", "|" };
	gate_rels->set_ticks(spd);

	gate_hyst = new MixbusKnob(this);
	gate_hyst->set_color("mixbus gate hysteresis knob");
	widgets.push_front(gate_hyst);
	gate_hyst->set_ticks(tl);

	gate_knee = new MixbusKnob(this);
	gate_knee->set_color("mixbus gate knee knob");
	widgets.push_front(gate_knee);
	gate_knee->set_ticks(tl);

	gate_hold = new MixbusKnob(this);
	widgets.push_front(gate_hold);
	gate_hold->set_color("mixbus gate hold knob");
	gate_hold->set_ticks(tl);

	gate_ratio = new MixbusKnob(this);
	widgets.push_front(gate_ratio);
	gate_ratio->set_color("mixbus gate ratio knob");
	gate_ratio->set_ticks(tl);

	gate_emph = new MixbusKnob(this);
	gate_emph->set_color("mixbus gate emph freq knob");
	widgets.push_front(gate_emph);
	std::string hmft[NUM_KNOB_TICKS] = { "|", ".4", "|", ".9", "", "", "", "4", "|", "6", "|" };
	gate_emph->set_ticks(hmft);

	gate_depth = new MixbusKnob(this);
	gate_depth->set_color("mixbus gate depth knob");
	widgets.push_front(gate_depth);
	std::string gd[NUM_KNOB_TICKS] = { "|", "-", "|", "|", "|", "", "|", "|", "|", "+", "|" };
	gate_depth->set_ticks(gd);

	gate_thresh = new MixbusThresh(this);
	gate_thresh->set_color("mixbus gate threshold knob");
	widgets.push_front(gate_thresh);
//	std::string hft[7] = { "0.8", "|", "|", "|", "|", "|", "15" };
	gate_thresh->set_ticks(tl);

	gate_in = new MixbusToggle(this);
	widgets.push_front(gate_in);
	gate_in->set_text("Gate");
//	gate_in->set_color( 0.25, 0.9, 0.2 );

	gate_emph_in = new MixbusToggle(this);
	widgets.push_front(gate_emph_in);
	gate_emph_in->set_text("SC F");
//	gate_in->set_color( 0.25, 0.9, 0.2 );

	gate_exp = new MixbusToggle(this);
	widgets.push_front(gate_exp);
	gate_exp->set_text("Exp");
//	gate_in->set_color( 0.25, 0.9, 0.2 );

	gate_lstn = new MixbusToggle(this);
	widgets.push_front(gate_lstn);
	gate_lstn->set_text("Lstn");
//	gate_in->set_color( 0.25, 0.9, 0.2 );

	hyst_leg = new MixbusHystLegend(this);
	widgets.push_front(hyst_leg);

	hold_leg = new MixbusHoldLegend(this);
	widgets.push_front(hold_leg);

	//gate input meter leds
	float met = -0.2;
	for (int i = 0 ; i< GATE_METER_CNT; i++) {
		input_meterDots[i] = new MixbusMeterDot(this);
		widgets.push_front(input_meterDots[i]);
		input_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		met -= 5.0 + (float)i;
	}

	//gate redux meter leds
	met = -0.2;
	for (int i = 0 ; i< GATE_THRESH_CNT; i++) {
		redux_meterDots[i] = new MixbusReduxDot(this);
		widgets.push_front(redux_meterDots[i]);
		redux_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		redux_meterDots[i]->set_color( rgba_to_color (1,0,0,1) );  //TODO:  watch the meter color options
		met -= 2.0;
	}
	redux_meterDots[GATE_THRESH_CNT-2]->set_thresh( dB_to_coefficient(-14) );
	redux_meterDots[GATE_THRESH_CNT-1]->set_thresh( dB_to_coefficient(-24) );

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusGate::meter));

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusGate::config_changed, this, _1), gui_context());
}

void
MixbusGate::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scaling factor
	const float toggle_height = h*17.0;
	const float knob_sz = h*22;

// ------------ track gate knobs


	float l_offs = midp -34*h;
	float r_offs = midp + 14*h;

	float led_width = floor(Wide ? 9*h : 5*h);

	int stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	int size = floor(stride *0.75);
	for (int i = 0 ; i< GATE_METER_CNT; i++) {
		input_meterDots[i]->set_size(ArdourCanvas::Rect (0, 0, led_width, size));
		input_meterDots[i]->set_position(ArdourCanvas::Duple ( floor(39*h), floor(h*10 + stride*i) ));
		input_meterDots[i]->show();
	}

	stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	size = floor(stride *0.75);
	for (int i = 0 ; i< GATE_THRESH_CNT; i++) {
		redux_meterDots[i]->set_size(ArdourCanvas::Rect (0, 0, led_width, size));
		redux_meterDots[i]->set_position(ArdourCanvas::Duple ( r_offs+6*h, floor(h*10 + stride*i) ));
		redux_meterDots[i]->show();
	}

	gate_thresh->set_size(ArdourCanvas::Rect (0, 0, knob_sz, h*90));
	gate_thresh->set_position(ArdourCanvas::Duple (16*h, 4*h));

	gate_depth->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_depth->set_position(ArdourCanvas::Duple (r_offs, 56*h + 0*h));


	gate_attk->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_attk->set_position(ArdourCanvas::Duple (l_offs, 108*h));

	gate_rels->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_rels->set_position(ArdourCanvas::Duple (r_offs, 100*h));


	gate_hyst->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_hyst->set_position(ArdourCanvas::Duple (l_offs, h*150));

	gate_knee->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_knee->set_position(ArdourCanvas::Duple (l_offs, h*150));

	gate_hold->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_hold->set_position(ArdourCanvas::Duple (l_offs, h*196));

	gate_ratio->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_ratio->set_position(ArdourCanvas::Duple (l_offs, h*196));

	hyst_leg->set_size(ArdourCanvas::Rect (0, 0, h*42, h*14));
	hyst_leg->set_position(ArdourCanvas::Duple (l_offs-11*h, h*150 + 24*h));

	hold_leg->set_size(ArdourCanvas::Rect (0, 0, h*42, h*14));
	hold_leg->set_position(ArdourCanvas::Duple (l_offs-11*h, h*196 + 24*h));


	gate_emph->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	gate_emph->set_position(ArdourCanvas::Duple (r_offs, 145*h));

	gate_emph_in->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	gate_emph_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 166*h));


	gate_lstn->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	gate_lstn->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 183*h));


	gate_exp->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	gate_exp->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 203*h));

	gate_in->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	gate_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 218*h));

	end_visual_change();
}

void
MixbusGate::gate_md_changed ()
{
	if (_gate_md_ctrl) {
		if (_gate_md_ctrl->get_interface () > 0) {
			gate_hyst->hide();
			gate_hold->hide();
			gate_knee->show();
			gate_ratio->show();
		} else {
			gate_hyst->show();
			gate_hold->show();
			gate_knee->hide();
			gate_ratio->hide();
		}
	}
}

void
MixbusGate::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	_gate_md_connection.disconnect();
	_gate_md_ctrl.reset();
	_gate_md_ctrl = rt->gate_mode_controllable();
	if (_gate_md_ctrl ) {
		_gate_md_ctrl->Changed.connect (_gate_md_connection, invalidator (*this), boost::bind (&MixbusGate::gate_md_changed, this), gui_context ());
		gate_md_changed();
	}

	//now show and re-connect based on route type ?
	if (rt && !rt->mixbus() && !rt->is_master())  {

		gate_emph_in->show();
		gate_emph_in->set_route( rt, rt->gate_key_filter_enable_controllable() );

		gate_lstn->show();
		gate_lstn->set_route( rt, rt->gate_key_listen_controllable() );

		gate_exp->show();
		gate_exp->set_route( rt, rt->gate_mode_controllable() );

		gate_in->show();
		gate_in->set_route( rt, rt->gate_enable_controllable() );

		gate_attk->show();
		gate_attk->set_route( rt, rt->gate_attack_controllable() );

		gate_rels->show();
		gate_rels->set_route( rt, rt->gate_release_controllable() );

		gate_emph->show();
		gate_emph->set_route( rt, rt->gate_key_filter_freq_controllable() );

		gate_depth->show();
		gate_depth->set_route( rt,rt->gate_depth_controllable() );

		gate_hyst->set_route( rt, rt->gate_hysteresis_controllable() );
		gate_knee->set_route( rt, rt->gate_knee_controllable() );

		gate_hold->set_route( rt, rt->gate_hold_controllable() );
		gate_ratio->set_route( rt, rt->gate_ratio_controllable() );

		hyst_leg->show();
		hyst_leg->set_route( rt, rt->gate_mode_controllable() );

		hold_leg->show();
		hold_leg->set_route( rt, rt->gate_mode_controllable() );

		gate_thresh->show();
		gate_thresh->set_route( rt, rt->gate_threshold_controllable() );

		for (int i = 0 ; i< (GATE_METER_CNT); i++) {
			input_meterDots[i]->set_route( rt, rt->gate_meter_controllable() );
			input_meterDots[i]->show();
		}

		for (int i = 0 ; i< (GATE_THRESH_CNT); i++) {
			redux_meterDots[i]->set_route( rt, rt->gate_redux_controllable() );
			redux_meterDots[i]->show();
		}

	}

	end_visual_change();
}

void
MixbusGate::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();  float halfw = width/2.0;

	Cairo::Matrix m;

	bool is_mixbus = _route->mixbus() != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scaling factor

	int tw,  th;  //text width and height

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	/* theme colors */
	SVAModifier bord = UIConfiguration::instance().modifier ("strip border alpha");
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");

	//background fill ...
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t fg_color = UIConfiguration::instance().color ("gtk_texts");

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//sidechain puddle
	if (false) {
		Gtkmm2ext::set_source_rgb_a (cr, puddle_color, 1.0);
		Gtkmm2ext::rounded_rectangle (cr, midp, 139*h, width-midp, 61*h, boxy ? 0 : 5*h);
		cr->fill ();
	}

	//thresh puddle
	if (false) {
		cr->set_line_width (1.0*h);
		cr->move_to( 0*h, 0*h ); //start in the top left
		cr->rel_line_to( 0*h, 100*h );
		cr->rel_line_to( 41*h+6*h, 0*h );
		cr->rel_line_to( 10*h, -8*h );  //slant
		cr->rel_line_to( width-47*h-10*h, 0 );
		cr->line_to( width, 0 );
		Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
		cr->fill ();
	}

	//time separators
	if (true) {
		cr->set_line_width (1.0*h);
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, bord.a());
		cr->move_to( 6*h, 102*h ); //start in the top left
		cr->rel_line_to( 41*h, 0 );
		cr->rel_line_to( 10*h, -8*h );  //slant
		cr->rel_line_to( 38*h, 0 );
		cr->stroke();
	}

	//time separators
	if (true) {
		cr->set_line_width (1.0*h);
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, bord.a());
		cr->move_to( 6*h, 145*h ); //start in the top left
		cr->rel_line_to( 41*h, 0 );
		cr->rel_line_to( 10*h, -8*h );  //slant
		cr->rel_line_to( 38*h, 0 );
		cr->stroke();
	}

	//gate connector
	if (true) {
		cr->set_line_width (1.0*h);
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, 1.0);
		cr->move_to( 42*h, 180*h ); //start in the top left
		cr->rel_line_to( 4*h, 4*h );
		cr->rel_line_to( 0, 40*h );
		cr->rel_line_to( -4*h, 4*h );
		cr->stroke();
	}

	//gate attachment
	if (true) {
		cr->set_line_width (1.0*h);
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, bord.a());
		cr->move_to( 46*h, 211*h );
		cr->rel_line_to( 14*h, 0*h );
		cr->stroke();
	}

	//Input ticks
	{
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, 0.7);
		cr->set_identity_matrix();

		layout->set_text( "0" );
		cr->move_to( floor(6*h), 3*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "15" );
		cr->rel_move_to( 0, 19*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "30" );
		cr->rel_move_to( 0, 19*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "45" );
		cr->rel_move_to( 0, 20*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "60" );
		cr->rel_move_to( 0, 20*h );
		layout->show_in_cairo_context (cr);

		cr->begin_new_path();
	}


	//Redux ticks
	{
		int tw, th;
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, 0.7);
		cr->set_identity_matrix();

		layout->set_text( "0" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(3*h + 0*13*h) ); //start in the top left
		layout->show_in_cairo_context (cr);

		layout->set_text( "-4" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(3*h + 1*12*h) );
		layout->show_in_cairo_context (cr);

		layout->set_text( "-8" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(3*h + 2*12*h) );
		layout->show_in_cairo_context (cr);

		layout->set_text( "24" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(3*h + 3*12*h) );
		layout->show_in_cairo_context (cr);
		cr->begin_new_path();
	}

	//Depth decorator
	if (true) {
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Depth" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(75*h - (tw/2.)), floor(76*h) );
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Attk decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Attk" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(27*h - (tw/2.)), floor(129*h) );
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Rels decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Rels" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(75*h - (tw/2.)), floor(121*h) );
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	float drad = 7;  //decorator radius
	float offs = 146;  //32C offset ( allows widgets in the fader area to share position values with regular mb

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 6*h, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false && !flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 7);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 8 );
		cr->fill ();
	}

	if (false) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//section bounds
	if (true) {
		cr->set_line_width (1.0*h);
		Gtkmm2ext::set_source_rgb_a (cr, fg_color, bord.a());
		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width-2*h, height-2*h, 5*h);
		cr->stroke ();
	}

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}
}

void
MixbusGate::meter()
{
	if (!_route)
		return;

	//input meter
	for (int i = 0 ; i< GATE_METER_CNT; i++) {
		input_meterDots[i]->meter();
	}

	//redux meter
	for (int i = 0 ; i< GATE_THRESH_CNT; i++) {
		redux_meterDots[i]->meter();
	}
}

void
MixbusGate::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {}

	//nothing to do here?
}

void
MixbusGate::config_changed (string p)
{
}
