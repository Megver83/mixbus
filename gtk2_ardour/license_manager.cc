/*
 * Copyright (C) 2016 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef COMPILER_MSVC
#include <ardourext/misc.h>
#else
#include <regex.h>
#endif

#include <boost/algorithm/string/trim.hpp>

#include "gtkmm2ext/utils.h"
#include "pbd/openuri.h"
#include "ardour/plugin_manager.h"
#include "ardour/plugin_insert.h"
#include "ardour/session.h"

#include "license_manager.h"
#include "ardour_message.h"
#include "ardour_http.h"
#include "ardour_ui.h"
#include "pbd/i18n.h"

using namespace std;
using namespace Gtk;
using namespace ARDOUR;

LicenseManager::LicenseManager ()
	: ArdourWindow (_("License Manager"))
	, _store_button (_("Visit Online Store"))
	, _home_button (_("Open HOME Folder"))
	, _mixbus_licensed (0)
	, _session_has_unlicened_plugins (false)
{
	_store = ListStore::create (_model);
	_view.set_model (_store);
	_view.append_column (_("Name"), _model.name);
	_view.append_column (_("Licensee"), _model.licensee);
	_view.get_column(0)->set_expand (true);
	_view.get_column(1)->set_expand (true);
	_view.set_tooltip_column(5);

	Gtk::HBox* action_box = manage (new Gtk::HBox);
	action_box->set_spacing(3);
	action_box->pack_start (_store_button, true, true);
	action_box->pack_start (_home_button, true, true);

	_store_button.signal_clicked().connect (sigc::mem_fun(*this, &LicenseManager::store_btn_clicked));
	_home_button.signal_clicked().connect (sigc::mem_fun(*this, &LicenseManager::home_btn_clicked));
	_view.get_selection()->signal_changed().connect (sigc::mem_fun (*this, &LicenseManager::selection_changed));

	Gtk::VBox *vbox = manage (new VBox());
	vbox->pack_start (_view, false, false);
	vbox->pack_end (*action_box, false, false);
	vbox->set_spacing(3);
	vbox->show_all ();

	add (*vbox);

	setup ();
	selection_changed ();
}

bool
LicenseManager::on_delete_event (GdkEventAny*)
{
	return false;
}

void
LicenseManager::session_going_away ()
{
	ArdourWindow::session_going_away ();
	hide_all();
}

void
LicenseManager::check_processor_license (std::weak_ptr<Processor> p)
{
	std::shared_ptr<PluginInsert> pi;
	std::shared_ptr<Plugin> plugin;
	std::shared_ptr<Processor> pp = p.lock();
	if (!pp) {
		return;
	}
	if (!(pi = std::dynamic_pointer_cast<PluginInsert>(pp))) {
		return;
	}
	if (!(plugin = pi->plugin (0))) {
		return;
	}
	PluginInfoPtr pip = plugin->get_info();
	if (!pip->needs_license || pip->is_licensed) {
		return;
	}
	_session_has_unlicened_plugins = true;
}

/*static*/ int
LicenseManager::mixbus_license_status (ARDOUR::Session* s)
{
	if (s && s->master_out()) {
		std::shared_ptr<PluginInsert> pi = s->master_out()->ch_post();
		if (pi) {
			std::shared_ptr<ARDOUR::Plugin> plugin = pi->plugin();
			if (plugin) {
				const float value = plugin->get_parameter(0);
				if (value < 0) {
					return -1; //not licensed
				} else if (value > 0) {
					return 1; // licensed
				} else {
					return 0; // no license needed (chanstrip w/NO_LICENSE)
				}
			}
		}
	}
	// fall back to  ARDOUR_UI::instance()->is_licensed() ??
	return 0;
}

void
LicenseManager::set_session(ARDOUR::Session *s) {
	ArdourWindow::set_session (s);
	_session_has_unlicened_plugins = false;
	_mixbus_licensed = mixbus_license_status (s);
	setup ();

	if (s) {
		std::shared_ptr<RouteList const> rl = s->get_routes ();
		for (auto const& i : *rl) {
			i->foreach_processor (sigc::mem_fun (*this, &LicenseManager::check_processor_license));
			if (_session_has_unlicened_plugins) {
				break;
			}
		}
	}

	if (_session_has_unlicened_plugins && ARDOUR::Config->get_popup_license_dialog ()) {
		present ();
	}
}

void
LicenseManager::setup ()
{
	_store->clear ();
	_unlicensed.clear ();
	_uris.clear ();

	/* report from plugin
	 * > 0 licensed
	 * == 0 no licensed is neeeded (NOLICENSE)
	 * < 0 unlicensed
	 */
	if (_mixbus_licensed != 0) {
		TreeModel::Row r = *(_store->append ());
		r[_model.licensed] = _mixbus_licensed > 0;
		r[_model.uri] = "";
#ifdef MIXBUS32C
		r[_model.name] = "Mixbus 32C";
		r[_model.store_url] = "http://harrisonconsoles.com/site/mixbus32c.html";
#else
		r[_model.store_url] = "http://harrisonconsoles.com/site/mixbus.html";
		r[_model.name] = "Mixbus";
#endif

		if (_mixbus_licensed > 0) {
			// If this assert goes off, the plugin reports
			// "license file found and valid"
			// but Mixbus did not find the file.
			assert (ARDOUR_UI::instance()->is_licensed());
			r[_model.licensee] = ARDOUR_UI::instance()->get_licensee ();
		} else {
			r[_model.licensee] = _("-- No License Found --");
		}
	}

	PluginManager& manager (PluginManager::instance());
#ifdef LV2_SUPPORT
	refiller (manager.lv2_plugin_info());
#endif
}

void
LicenseManager::refiller (const PluginInfoList& plugs)
{
	for (PluginInfoList::const_iterator i = plugs.begin(); i != plugs.end(); ++i) {
		if (!(*i)->needs_license || (*i)->product_uri.empty()) { continue; }
		if (! (*i)->is_licensed) {
			_unlicensed.push_back ((*i)->unique_id);
		}

		if (std::find (_uris.begin(), _uris.end(), (*i)->product_uri) != _uris.end()) {
			continue;
		}

		TreeModel::Row r = *(_store->append ());
		r[_model.licensed] = (*i)->is_licensed;
		r[_model.uri] = (*i)->product_uri;
		r[_model.name] = (*i)->product_name;
		r[_model.store_url] = (*i)->store_url;

		if (! (*i)->is_licensed) {
			r[_model.licensee] = _("-- No License Found --");
			r[_model.license_file] = "";
		} else {
			r[_model.licensee] = (*i)->licensee;
			r[_model.license_file] = (*i)->current_license_file_path;
		}
		_uris.push_back((*i)->product_uri);
	}
}

void
LicenseManager::selection_changed ()
{
	TreeModel::Row row = *(_view.get_selection()->get_selected());
	if (row) {
		_store_button.set_sensitive (true);
	}
	else {
		_store_button.set_sensitive (false);
	}

	if (row && !row[_model.licensed]) {
		const std::string url = row[_model.store_url];
		_store_button.set_sensitive (!url.empty());
	} else {
		_store_button.set_sensitive (false);
	}
}

void
LicenseManager::store_btn_clicked ()
{
	TreeModel::Row row = *(_view.get_selection()->get_selected());
	assert (row);
	const std::string url = row[_model.store_url];
	PBD::open_uri (url);
}

void
LicenseManager::home_btn_clicked ()
{
	PBD::open_folder (Glib::get_home_dir());
}

bool
LicenseManager::download_license_dialog ( bool at_startup )
{
	ArdourDialog dialog (_("License Download + Install"));
	Gtk::Label label (_("Please find the license email that you received with your purchase.\n\nCopy/Paste the license URL here:"), ALIGN_LEFT, ALIGN_CENTER);
	Gtk::Entry entry;

	dialog.add_button (Gtk::StockID (Gtk::Stock::OK), Gtk::RESPONSE_OK);
	dialog.add_button (Gtk::StockID (Gtk::Stock::CANCEL), Gtk::RESPONSE_CANCEL);

	dialog.set_position (Gtk::WIN_POS_CENTER);

	dialog.get_vbox()->pack_start (label);
	dialog.get_vbox()->pack_start (entry);
	dialog.get_vbox()->set_spacing (3);
	dialog.show_all();

	std::string url;
	std::string lic;
	std::string msg;
	bool refresh = false;
	MessageType msg_type = Gtk::MESSAGE_ERROR;

	switch (dialog.run()) {
		case Gtk::RESPONSE_OK:
			url = entry.get_text ();
			boost::algorithm::trim (url);
			if (!strncmp ("https://harrisonconsolesstore.onfastspring.com", url.c_str(), 46)
			    || !strncmp ("https://sites.fastspring.com/harrisonconsoles", url.c_str(), 45)) {
				ArdourCurl::HttpGet h;
				lic = h.get (url);
				if (lic.empty ()) {
					msg = _("No license file was returned: Please email Harrison mixbus support (mixbus@harrisonconsoles.com)");
				} else {
					std::string cd = h.header()["Content-Disposition"];
					if (cd.empty()) {
						msg = _("Download failed: No valid license was returned.\n\nPlease email mixbus@harrisonconsoles.com to receive a new license link.");
					} else {
						regex_t regex;
						regmatch_t match[2];
						// $$ attachment; filename="license_key_harrison.txt"$$
						int rc = regcomp (&regex, "^attachment; filename=\"([^\"]*)\"", REG_EXTENDED);
						if (rc == 0 && 0 == regexec (&regex, cd.c_str(), 2, match, 0) && match[1].rm_so != -1) {
							std::string fn = cd.substr (match[1].rm_so, match[1].rm_eo - match[1].rm_so);

							/* compare to ARDOUR_UI::parse_license_file
							 *
							 * This is inverted! Don't allow to download 32C license with MB.
							 * (but do allow LV2 plugin licenses..)
							 */
#ifdef MIXBUS32C
							const string fnx = string_compose (X_("license_key_harrison_mixbus%1.txt"), atoi (X_(PROGRAM_VERSION)));
#else
							const string fnx = string_compose (X_("license_key_harrison_mixbus32c-%1.txt"), atoi (X_(PROGRAM_VERSION)));
#endif
							if (fn == fnx) {
								msg = string_compose(_("Invalid license URL/file for \n'%1'.\nMixbus version mismatch detected."), PROGRAM_NAME);
								break;
							}

							std::string path = Glib::build_filename (Glib::get_home_dir (), fn);
							bool pre_existing = (Glib::file_test(path, Glib::FILE_TEST_EXISTS));
							if (g_file_set_contents (path.c_str(), lic.c_str(), -1, NULL)) {
								if (pre_existing) {
									msg = string_compose(_("Replaced license file at:\n'%1'.\nRestart %2 to revalidate licenses."), path, PROGRAM_NAME);
								} else if (at_startup) {
									msg = string_compose(_("Installed license file to:\n'%1'."), path, PROGRAM_NAME);
								} else {
									msg = string_compose(_("Installed license file to:\n'%1'.\nRestart %2 to revalidate licenses."), path, PROGRAM_NAME);
								}
								msg_type = Gtk::MESSAGE_INFO;
								refresh = true;
							} else {
								msg = string_compose(_("Error saving file '%1'."), path);
							}
						} else {
							msg = _("Download failed: No valid license file name was returned.\n\nPlease email mixbus@harrisonconsoles.com to receive a new license link.");
						}
					}
				}
			} else {
				msg = _("Invalid URL: Given URL is not from the Harrison online store.");
			}
			break;
		default:
			break;
	}
	if (!msg.empty()) {
		dialog.hide ();
		ArdourMessageDialog m (msg, false, msg_type, Gtk::BUTTONS_OK, true);
		m.set_position (Gtk::WIN_POS_CENTER);
		m.get_vbox()->set_spacing (3);
		m.run();
	}
	return refresh;
}

bool
LicenseManager::save_mixbus_license (std::string lic)
{
#ifdef MIXBUS32C
	const string fn = string_compose (X_("license_key_harrison_mixbus32c-%1.txt"), atoi (X_(PROGRAM_VERSION)));
	const string pat = string_compose (X_("*32c%1*"), atoi (X_(PROGRAM_VERSION)));
#else
	const string fn = string_compose (X_("license_key_harrison_mixbus%1.txt"), atoi (X_(PROGRAM_VERSION)));
	const string pat = string_compose (X_("*mb%1*"), atoi (X_(PROGRAM_VERSION)));
#endif

	bool rv = false;
	std::string msg;
	MessageType msg_type = Gtk::MESSAGE_ERROR;
	boost::algorithm::trim (lic);

	if (string::npos == lic.find (pat) || std::count (lic.begin(), lic.end(), '|') != 2 || lic.at (lic.length() - 1) != '|') {
		msg = _("The given text does not look like a Harrison license for this product.\nPlease try again.\nIf the problem persists, please contact harrison support.");
	} else {
		if (lic.find ("License Key: ") == 0) {
			lic = lic.substr (14);
		}
		std::string path = Glib::build_filename (Glib::get_home_dir (), fn);
		bool pre_existing = (Glib::file_test(path, Glib::FILE_TEST_EXISTS));
		if (g_file_set_contents (path.c_str(), lic.c_str(), -1, NULL)) {
			if (pre_existing) {
				msg = string_compose(_("Replaced license file at:\n'%1'.\nRestart %2 to revalidate licenses."), path, PROGRAM_NAME);
			} else {
				msg = string_compose(_("Installed license file to:\n'%1'."), path, PROGRAM_NAME);
			}
			msg_type = Gtk::MESSAGE_INFO;
			rv = true;
		} else {
			msg = string_compose(_("Error saving file '%1'."), path);
		}
	}

	if (!msg.empty()) {
		ArdourMessageDialog m (msg, false, msg_type, Gtk::BUTTONS_OK, true);
		m.set_position (Gtk::WIN_POS_CENTER);
		m.get_vbox()->set_spacing (3);
		m.run();
	}
	return rv;
}
