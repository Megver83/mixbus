/*
    Copyright (C) 2014-2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "pbd/i18n.h"

#include "mixer_strip.h"
#include "mixer_ui.h"
#include "mixbus_widgets.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "gtkmm2ext/utils.h"

using namespace ArdourCanvas;
using namespace Gtkmm2ext;
using namespace std;

ScreenSizer* ScreenSizer::_instance = 0;

Pango::FontDescription _fontRegular;
Pango::FontDescription _fontBold;
Pango::FontDescription _fontBigBold;

int _fontScaleRegular;
int _fontScaleBold;
int _fontScaleBigBold;

ScreenSizer::ScreenSizer()
{
	_fontScaleRegular = 999;
	_fontScaleBold = 999;
	_fontScaleBigBold = 999;
	
	//find the -smallest- monitor used on the system
	gint height = 8192;
	GdkScreen* screen = gdk_display_get_screen (gdk_display_get_default(), 0);
	for (int m = 0; m< gdk_screen_get_n_monitors(screen); m++) {
		GdkRectangle rect;
		gdk_screen_get_monitor_geometry (screen, m, &rect);
		if (rect.height < height)
			height = rect.height;
	}

#ifdef MIXBUS32C
	height -= 100;  //allocate 100pix for menu/dock/doc, etc
	float recommended_min_height = 1100;  //1200pix monitor, but allocate 100pix for menu/dock/etc

	//between 768 & recommended_min_height, scale quickly from 0.80 to 1.0; after that, grow linearly with screen height.
	if (height <=recommended_min_height)
		_scale = 0.80 + ( 0.20 * (height-768)/(recommended_min_height-768) );
	else
		_scale = ((float)height/recommended_min_height);
#else
	float recommended_min_height = 1024;

	if (height <=recommended_min_height)
		_scale = 0.70 + ( 0.30 * (height-768)/(recommended_min_height-768) );
	else
		_scale = ((float)height/recommended_min_height);
#endif

	//limit scale values to something reasonable
	if (_scale < 0.7 ) _scale = 0.7;
	if (_scale > 1.5) _scale = 1.5;

	cout << "Mixbus:  Screen height is " << height << "; Font scale is " << UIConfiguration::instance().get_font_scale () / 102400. << "; User-scale is " << UIConfiguration::instance().get_mixbus_strip_scale () << "; Suggested scale: " << _scale << endl;
}

Pango::FontDescription
ScreenSizer::GetBigBoldFontDesc()
{
	float s_scale = UIConfiguration::instance().get_mixbus_strip_scale ();
	float font_base_size = 12;
	int pix = (int) floor(font_base_size * s_scale + 0.5 );

	if (pix != _fontScaleBigBold) {

		char fontBuf[64];  sprintf(fontBuf, "ArdourSans bold %dpx", pix );
		_fontBigBold = Pango::FontDescription (fontBuf);

		_fontScaleBigBold = pix;
	}
	
	return _fontBigBold;
}

Pango::FontDescription
ScreenSizer::GetBoldFontDesc()
{
	float s_scale = UIConfiguration::instance().get_mixbus_strip_scale ();
	float font_base_size = 10;
	int pix = (int) floor(font_base_size * s_scale + 0.5 );

	if (pix != _fontScaleBold) {

		char fontBuf[64];  sprintf(fontBuf, "ArdourSans bold %dpx", pix );
		_fontBold = Pango::FontDescription (fontBuf);

		_fontScaleBold = pix;
	}
	
	return _fontBold;
}

Pango::FontDescription
ScreenSizer::GetFontDesc()
{
	float s_scale = UIConfiguration::instance().get_mixbus_strip_scale ();
	float font_base_size = 10;
	int pix = (int) floor(font_base_size * s_scale + 0.5 );

	if (pix != _fontScaleRegular) {

		char fontBuf[64];  sprintf(fontBuf, "ArdourSans %dpx", pix );
		_fontRegular = Pango::FontDescription (fontBuf);

		_fontScaleRegular = pix;
	}
	
	return _fontRegular;
}

// ---------------------------------------------------------------------

MixbusSpillButton::MixbusSpillButton (Item* c)
	: MixbusButton (c)
{
	Mixer_UI::instance()->show_spill_change.connect (sigc::mem_fun (*this, &MixbusSpillButton::set_spill_state));

	_color = "mixbus spill button";

	button_press_event.connect (sigc::mem_fun(*this, &MixbusSpillButton::spill_press));
//	button_release_event.connect (sigc::mem_fun(*this, &MixbusStrip::spill_release));
}

void
MixbusSpillButton::set_stripable( std::shared_ptr<ARDOUR::Stripable> st )
{
	_stripable = st;
}

int
MixbusSpillButton::active_state () const
{
	if (Mixer_UI::instance()->showing_spill_for (_stripable)) {
		return Gtkmm2ext::ExplicitActive;
	}

	return 0;
}

void
MixbusSpillButton::set_spill_state (std::shared_ptr<ARDOUR::Stripable>)
{
	if (Mixer_UI::instance()->showing_spill_for (_stripable)) {
		set_tooltip (_("Click to show normal mixer"));
	} else {
		set_tooltip (_("Click to show assigned channels only"));
	}

	//this is called when mixer_ui calls "show_spill_change"; trigger a redraw
	redraw();
}

bool
MixbusSpillButton::spill_press ( GdkEventButton* )
{
	if (Mixer_UI::instance()->showing_spill_for (_stripable)) {
		Mixer_UI::instance()->show_spill (std::shared_ptr<ARDOUR::Stripable>());
	} else {
		Mixer_UI::instance()->show_spill (_stripable);
	}

	redraw();

	return true;
}

void
MixbusSpillButton::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance width = self.width();

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	cairo_t *ct = cr->cobj();

	//show implicit/explicit states based on solo/mute logic
	const int state = active_state ();

	//shadow
	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : 5.0;

	if ( !flat) {
		cr->translate (2, 2);
		cr->set_source_rgba (0, 0, 0, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 0, 0, width, height, _corner_radius);
		cr->fill ();
		cr->translate (-2, -2);
	}

	//implicit or explicit active
	if (state == 2) {
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
		cairo_fill (ct);
	} else if (state==1) {  //implicit active
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
		cairo_fill (ct);
		cairo_set_source_rgba (ct, _led_r, _led_g, _led_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 2, 2, width-4, height-4, _corner_radius - 1.5);
		cairo_set_line_width (ct, 2.0);
		cairo_stroke (ct);
	} else  {
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 1);
		Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
		cairo_fill (ct);
	}

	if (!flat) {

		if (state) {
			//gradient
			cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  height);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.5);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.0);
			cairo_set_source (ct, shade_pattern);
			Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
			cairo_fill (ct);
			cairo_pattern_destroy (shade_pattern);
		} else {
			//gradient
			cairo_pattern_t* shade_pattern = cairo_pattern_create_linear (0.0, 0, 0.0,  height);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 0.0, 0,0,0, 0.0);
			cairo_pattern_add_color_stop_rgba (shade_pattern, 1.0, 0,0,0, 0.7);
			cairo_set_source (ct, shade_pattern);
			Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
			cairo_fill (ct);
			cairo_pattern_destroy (shade_pattern);
		}
	}

	if (!state) {
		//flatter top
		cairo_set_source_rgba (ct, fill_r, fill_g, fill_b, 0.2);
		Gtkmm2ext::rounded_rectangle (ct, 3, 3, width-6, height-6, _corner_radius - 1.5);
		cairo_fill (ct);
	}

   { //text --------------

		Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
		layout->set_font_description ( ScreenSizer::GetFontDesc() );
		layout->set_text (_text);

		int w,  h;
		layout->get_pixel_size (w, h);

		cairo_save (ct);
		cairo_rectangle (ct, 2, 1, width-4, height-2);
		cairo_clip(ct);

/*		cairo_new_path (ct);
		if (state==2) {
			cairo_set_source_rgba (ct, 1, 1, 1, 0.4);
			cairo_move_to (ct, width/2 - w/2 +1, height/2.0 - h/2.0+1);
		} else {
			cairo_set_source_rgba (ct, 0,0,0, 0.4);
			cairo_move_to (ct, width/2 - w/2 -1, height/2.0 - h/2.0-1);
		}
		layout->show_in_cairo_context (cr);
*/
		cairo_new_path (ct);
		if (state==2)
			cairo_set_source_rgba (ct, 0,0,0, 1);
		else
			cairo_set_source_rgba (ct, fg_r,fg_g,fg_b, 1);

		cairo_move_to (ct, floor(width/2 - w/2), floor(height/2.0 - h/2.0 +1));
		layout->show_in_cairo_context (cr);

		cairo_restore (ct);
	} //-----------------------------


	//black border
	cairo_set_line_width(ct, OUTLINE_WIDTH);
	cairo_set_source_rgba (ct, 0, 0, 0, 1);
	Gtkmm2ext::rounded_rectangle (ct, 1, 1, width-2, height-2, _corner_radius - 1.5);
	cairo_stroke (ct);

	//user is currently pressing the button. dark outline helps to indicate this
	if (_grabbed ) {
		Gtkmm2ext::rounded_rectangle (ct, 2, 2, width - 4, height - 4, _corner_radius);
		cairo_set_line_width(ct, 3);
		cairo_set_source_rgba (ct, 0.0, 0.0, 0.0, 0.5);
		cairo_stroke (ct);
	}

	//highlight
	if ((UIConfiguration::instance().get_widget_prelight() && _highlight) || _grabbed) {
		cr->set_source_rgba (1,1,1, 0.3);
		Gtkmm2ext::rounded_rectangle (ct, 2, 2, width-4, height-4, _corner_radius-1.5);
		cr->fill ();
	}

	cr->set_identity_matrix();

	//NOTE:  need to destroy the cairo patterns!!
}
