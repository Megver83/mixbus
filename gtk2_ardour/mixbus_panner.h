/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_PANNER__
#define __MB_PANNER__

#include "mixbus_strips.h"

#ifdef MIXBUS32C
#define NATURAL_PANNER_HEIGHT 76
#else
#define NATURAL_PANNER_HEIGHT 64
#endif

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;

class MixbusStripPanner;

class MixbusGateSwitcher;
class MixbusCompSwitcher;
class MixbusBusSwitcher;
class MixbusEQSwitcher;
class MixbusCtrlSwitcher;

#define LIM_METER_CNT 6

class MixbusStripPannerCanvas : public MixbusStripCanvas
{
public:
	MixbusStripPannerCanvas();

	virtual float natural_width() {return NATURAL_STRIP_WIDTH;}

	float natural_height();
};

class MixbusStripPanner : public MixbusStrip
{
public:

	MixbusStripPanner (ArdourCanvas::Item *);
	~MixbusStripPanner ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void route_property_changed (const PBD::PropertyChange&);

	void meter();

protected:
	void set_button_names () {}

	MixbusKnob *master_pan;
	Mixbus_MasterToggle *master_asgn;
	Mixbus_MasterToggle *mb_master_asgn;

	MixbusToggle *lim_in;
	MixbusReduxDot	*lim_meter[LIM_METER_CNT];

	MixbusKnob *stereo_width;

	MixbusGateSwitcher *gate_switcher;
	MixbusCompSwitcher *comp_switcher;
	MixbusCompSwitcher *b_comp_switcher;
	MixbusBusSwitcher  *bus_switcher;
	MixbusEQSwitcher   *eq_switcher;
	MixbusCtrlSwitcher *ctrls_switcher;

	sigc::connection meter_connection;
};

#endif
