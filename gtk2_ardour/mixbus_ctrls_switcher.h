/*
    Copyright (C) 2016 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_SWITCHER__
#define __MB_SWITCHER__

#include <string>
#include <vector>

#include <pangomm/fontdescription.h>

#include <canvas/canvas.h>
#include <canvas/container.h>
#include <canvas/item.h>
#include <canvas/fill.h>
#include <canvas/outline.h>

#include <gtkmm/layout.h>

#include <ardour/route.h>
#include <ardour/vca.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/automation_control.h>

#include "mixer_ui.h"
#include "route_ui.h"

#include "mixbus_widgets.h"
#include "mixbus_strips.h"

class MixbusKnob;
class MixbusToggle;
class MixbusCtrlSwitcher;

class MixbusCtrlSwitcherCanvas : public MixbusSwitcherCanvas
{
public:
	MixbusCtrlSwitcherCanvas(RouteUI::RUIMessage m);

	float natural_height() {return NATURAL_SWITCHER_HEIGHT;}
};

class MixbusSwitcher : public MixbusStrip
{
public:

	MixbusSwitcher (ArdourCanvas::Item *, RouteUI::RUIMessage m);
	~MixbusSwitcher ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	bool event_handler ( GdkEvent* e);

	void HandleRUIMessage(PBD::ID id, RouteUI::RUIMessage msg, std::string extra);

	void route_property_changed (const PBD::PropertyChange&);

protected:
	bool _highlight;

	std::string _text;

	RouteUI::RUIMessage _vis_message;
	Mixer_UI::StripElemVis _vis_elem;
};

class MixbusCtrlSwitcher : public MixbusSwitcher
{
public:

	MixbusCtrlSwitcher (ArdourCanvas::Item *, RouteUI::RUIMessage m);
	~MixbusCtrlSwitcher ();

	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	bool event_handler ( GdkEvent* e);

	void something_changed ();
	void vca_changed (std::shared_ptr<ARDOUR::VCA>, bool);

private:
	std::shared_ptr<ARDOUR::AutomationControl> _rec_enable_ctrl;
	std::shared_ptr<ARDOUR::PhaseControl> _phase_ctrl;
};

#endif
