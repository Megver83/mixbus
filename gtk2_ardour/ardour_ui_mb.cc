/*
 * Copyright (C) 2019 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef PLATFORM_WINDOWS
# include <windows.h>
# include "pbd/windows_special_dirs.h"
# include <shlobj.h> // CSIDL_*
#endif

#include <gtkmm/messagedialog.h>

#include "pbd/openuri.h"

#include "ardour_ui.h"
#include "mixbus_nag.h"
#include "mixbus_strips.h"
#include "mixbus_widgets.h"

#include "pbd/i18n.h"

using namespace ARDOUR;
using namespace Gtk;
using namespace std;

static bool testfile (const char *fn) {
	return Glib::file_test(fn, Glib::FILE_TEST_EXISTS);
}

void
ARDOUR_UI::parse_license_file()
{
	_is_licensed = false;
	_license_string = _("The Harrison Mixbus plugin is <b>UNLICENSED</b> and will generate periodic noise.\nClick to register your license, or contact Harrison Consoles for more details.");
	_licensee = "";

	/* see also PluginManager::ladspa_refresh */
#ifdef MIXBUS32C
	const string fn = string_compose (X_("license_key_harrison_mixbus32c-%1.txt"), atoi (X_(PROGRAM_VERSION)));
#elif defined MIXBUS
	const string fn = string_compose (X_("license_key_harrison_mixbus%1.txt"), atoi (X_(PROGRAM_VERSION)));
#endif

	const char *filename = fn.c_str();

	/////////////////////////////////////////////////////////////////////////////
	// ladspa_license.load.cc

	char licensePathBuf[1024] = "";

#ifdef _WIN32
	{
		const char * homedrive = getenv ("HOMEDRIVE");
		const char * homepath = getenv ("HOMEPATH");
		if (homedrive && homepath && (strlen (homedrive) + strlen(homepath) + strlen(filename)) < 1020) {
			sprintf (licensePathBuf, "%s%s\\%s", homedrive, homepath, filename);
			licensePathBuf[1023] = 0;
		}
	}

	if (!testfile (licensePathBuf)) {
		char* hom = getenv ("USERPROFILE");
		if (hom && (strlen(hom) + strlen(filename) < 1020)) {
			sprintf (licensePathBuf, "%s\\%s", hom, filename );
			licensePathBuf[1023] = 0;
		}
	}

	if (!testfile (licensePathBuf) && strlen(filename) < 1000) {
		char keyfiletmp[1024];
		sprintf (keyfiletmp, "%%localappdata%%\\%s", filename);
		keyfiletmp[1023] = 0;
		ExpandEnvironmentStrings (keyfiletmp, licensePathBuf, 1024);
		licensePathBuf[1023] = 0;
	}

	if (!testfile (licensePathBuf)) {
		/* This is not 100% identical to harrison's license loader. The latter users
		 * WideCharToMultiByte(), PBD uses g_utf16_to_utf8() */
		std::string ccad (PBD::get_win_special_folder_path(CSIDL_COMMON_APPDATA));
		const char* cad = ccad.empty() ? NULL : ccad.c_str();
		if (cad && (strlen(cad) + strlen(filename) < 1010)) {
			sprintf (licensePathBuf, "%s\\harrisonconsoles\\%s", cad, filename);
			licensePathBuf[1023] = 0;
		}
	}

	if (!testfile (licensePathBuf)) {
		char VolumeName[MAX_PATH+1];
		char DriveLetter[] = "A:\\";

		char c;
		for (c = 'A'; c <= 'Z'; ++c) {
			DriveLetter[0] = c;
			if (GetVolumeInformation (DriveLetter, VolumeName, MAX_PATH+1, NULL, NULL, NULL, NULL, 0)) {
				if (0 == strcasecmp (VolumeName, "HARRISON")) {
					sprintf (licensePathBuf, "%s%s", DriveLetter, filename);
					licensePathBuf[1023] = 0;
					break;
				}
			}
		}
	}
#else // Unix

	const char * home = getenv ("HOME");
	// common Unix
	if (home) {
		snprintf (licensePathBuf, 1024, "%s/%s", home, filename);
		licensePathBuf[1023] = 0;
	}

#ifdef __APPLE__

	if (!testfile (licensePathBuf) && home) {
		snprintf (licensePathBuf, 1024, "%s/Library/Application Support/harrisonconsoles/%s", home, filename);
		licensePathBuf[1023] = 0;
	}

	if (!testfile (licensePathBuf)) { // check global Library folder
		snprintf (licensePathBuf, 1024, "/Library/Application Support/harrisonconsoles/%s", filename);
		licensePathBuf[1023] = 0;
	}

	if (!testfile (licensePathBuf)) {
		snprintf (licensePathBuf, 1024, "/Volumes/HARRISON/%s", filename);
		licensePathBuf[1023] = 0;
	}

#else // Linux/XDG

	const char * username = getenv ("USER");
	if (!testfile (licensePathBuf) && username) {
		snprintf (licensePathBuf, 1024, "/media/%s/HARRISON/%s", username, filename);
		licensePathBuf[1023] = 0;
	}

	const char * xdg_config_home = getenv ("XDG_CONFIG_HOME"); // OR  XDG_DATA_HOME ?? ~/.local/share/  ???

	if (!testfile (licensePathBuf) && xdg_config_home) {
		snprintf (licensePathBuf, 1024, "%s/harrisonconsoles/%s", xdg_config_home, filename);
		licensePathBuf[1023] = 0;
	}
	else if (!testfile (licensePathBuf) && home) {
		snprintf (licensePathBuf, 1024, "%s/.config/harrisonconsoles/%s", home, filename);
		licensePathBuf[1023] = 0;
	}
#endif

	// common Unix
	if (!testfile (licensePathBuf) && home) {
		snprintf (licensePathBuf, 1024, "%s/.%s", home, filename);
		licensePathBuf[1023] = 0;
	}
	if (!testfile (licensePathBuf)) {
		snprintf (licensePathBuf, 1024, "/usr/local/share/%s", filename);
		licensePathBuf[1023] = 0;
	}
#endif


	//printf("checking file : %s\n", licensePathBuf);

	//////////////////////////////////////////////////////////////////////

	FILE *file = fopen (licensePathBuf, "r");
	if (file == NULL) {
		cerr << "Mixbus license file " << filename << " was not found in either home folder or global path: " << licensePathBuf << endl;
		return;
	}

	//find the license's name (or lack of) for the New Session dialog
	if (file != NULL) {

		//read the license file ...
		char theBuf[2048];
		fseek ( file, 0 , SEEK_END);
		int size = ftell (file);
		rewind (file);
		if (size > 2048) size = 2048;
		size_t len = fread(theBuf, 1, size, file);
		fclose (file);
		if (len == 0) return;

		//dig out name
		char *name = strtok( theBuf, "*" );
		if (!name) return;
		//printf("expecting NAME, found token: %s\n", name);

		_license_string = string_compose (_("This copy of %1 is licensed to: "), PROGRAM_NAME);
		_license_string += "<span weight=\"bold\" size=\"large\">";
		_license_string += name;
		_license_string += "</span>";
		_licensee = name;
		_is_licensed = true;
	}

}

void
ARDOUR_UI::mixbus_starting ()
{
	parse_license_file();

	if (!ARDOUR_UI::instance()->is_licensed()) {
		MixbusNagScreen* ns = new MixbusNagScreen();
		ns->nag ();
		parse_license_file();
#ifndef NDEBUG
		if (!ARDOUR_UI_UTILS::running_from_source_tree ())
#endif
		if (!ARDOUR_UI::instance()->is_licensed()) {
			/* still not licensed?  must be in demo mode.  Open a browser window to our store. */
#ifdef MIXBUS32C
			PBD::open_uri ("http://harrisonconsoles.com/site/mixbus32c.html");
#else
			PBD::open_uri ("http://harrisonconsoles.com/site/mixbus.html");
#endif
		}
	}
}

void
ARDOUR_UI::mixbus_screensizing ()
{
	bool too_large = (ScreenSizer::suggested_scale () < ScreenSizer::user_scale ());
	if ( too_large && UIConfiguration::instance().get_mixbus_screen_height_warning ()) {
		ArdourDialog d (_("Mixer Strip Scale"), true);
		d.set_position (Gtk::WIN_POS_CENTER);

		Label l;

		if (too_large) {
			l.set_markup (_("\nMixbus has detected that your mixer strips\n"
						"might be too tall for your monitor resolution.\n\n"
						"A more suitable scale has been selected.\n\n"
						"You can later change this value in Preferences->Appearance.\n"));
		} else {
			l.set_markup (_("\nMixbus has detected that your mixer strips\n"
						"might be too small for your monitor resolution.\n\n"
						"A more suitable scale has been selected.\n\n"
						"You can later change this value in Preferences->Appearance.\n"));
		}


		l.set_justify (JUSTIFY_CENTER);

		d.get_vbox()->pack_start (l, true, true);

		d.add_button (_("OK, Select an appropriate mixer\nscale for me."), RESPONSE_OK);
		d.add_button (_("Never ask me about this again;\nI'll manage it myself in\nPreferences>Appearance>Mixer"), RESPONSE_CANCEL);
		d.show_all ();

		int ret = d.run();

		if ( ret == RESPONSE_CANCEL )
			UIConfiguration::instance().set_mixbus_screen_height_warning(false);
		else if ( ret == RESPONSE_OK ) {
			float scale  = ScreenSizer::suggested_scale ();
			scale *= 20;  scale = floor(scale);  scale /= 20;  //round down to 5% increments
			UIConfiguration::instance().set_mixbus_strip_scale(scale);
			ARDOUR_UI::instance()->main_window().resize (1,1);
		}
	}

}
