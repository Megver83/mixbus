/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include <pbd/compose.h>

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/phase_control.h"
#include "ardour/route_group.h"
#include "ardour/solo_isolate_control.h"
#include "ardour/utils.h"

#include <canvas/canvas.h>
#include <canvas/debug.h>
#include <canvas/utils.h>

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_strips.h"
#include "mixbus_ctrls_switcher.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"
#include "pbd/stacktrace.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;


// ---------------------------------------------------------------
static const float TWO_PI = (M_PI*2.0);


MixbusCtrlSwitcherCanvas::MixbusCtrlSwitcherCanvas (RouteUI::RUIMessage m)
{
	_knobs = new MixbusCtrlSwitcher(root(), m);
}

//-------------------------


MixbusSwitcher::~MixbusSwitcher ()
{
}

MixbusSwitcher::MixbusSwitcher (ArdourCanvas::Item *p, RouteUI::RUIMessage m)
	: MixbusStrip (p)
    , _vis_message(m)
{
	_bg_pattern_type = bg_pattern_switcher;

	_highlight = false;

	Event.connect (sigc::mem_fun (*this, &MixbusSwitcher::event_handler));
}

void
MixbusSwitcher::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	end_change();
}

void
MixbusSwitcher::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	_route->PropertyChanged.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::route_property_changed, this, _1), gui_context());

	RouteUI::RUIMessageReflector.connect_same_thread (*this, boost::bind(&MixbusSwitcher::HandleRUIMessage, this, _1, _2, _3));
}

void
MixbusSwitcher::HandleRUIMessage(PBD::ID id, RouteUI::RUIMessage msg, std::string extra)
{
	redraw();  //this allows us to follow the current visibility flags
}

void
MixbusSwitcher::route_property_changed (const PBD::PropertyChange& what_changed)
{
	redraw();
}

void
MixbusSwitcher::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_t *ct = cr->cobj();

	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : halfh;

//	if (!_auto_ctrl) return;

	//solid bg
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("mixbus switcher text");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

   { //text

		Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
		layout->set_font_description ( ScreenSizer::GetFontDesc() );
		layout->set_text (_text);

		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
//		cr->set_line_width(1);
//		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : (height-2)/2.);
//		cr->stroke ();

		int tw,  th;
		layout->get_pixel_size (tw, th);
		Cairo::Matrix m = cr->get_matrix();
		cairo_translate (ct, floor(width - tw - 4*h), floor(-1*h));
		layout->show_in_cairo_context (cr);
		cr->set_matrix(m); //translate back to widget pos

		//highlight if my element is already showing
		if (Mixer_UI::instance()->strip_elem_visible(_vis_elem)) {

			//LOCK
			if (Mixer_UI::instance()->strip_elem_locked(_vis_elem)) {
				Cairo::Matrix m = cr->get_matrix();
				cairo_translate (ct, floor(0*h), floor(1*h));
				layout->set_text(" *");
				layout->show_in_cairo_context (cr);
				cr->set_matrix(m); //translate back to widget pos
			}

			cr->set_source_rgba (1,1,1, 0.5);
			Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
			cr->set_line_width (1*h);
			cr->stroke ();
		}

		//highlight further on mouse-over
		if ((UIConfiguration::instance().get_widget_prelight() && _highlight)) {
			cr->set_source_rgba (1,1,1, 0.2);
			Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
			cr->fill ();
		}
   }
}

void
MixbusSwitcher::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
}

bool
MixbusSwitcher::event_handler ( GdkEvent* ev)
{
	switch (ev->type) {
	case GDK_SCROLL: {
	} break;
	case GDK_BUTTON_PRESS: {
		if ( ((GdkEventButton*)ev)->button == 3) {  //right-click to ???
		} else {
			transmit_rui_message( _vis_message );
		}
		return true;
	} break;
	case GDK_2BUTTON_PRESS: {
	} break;
	case GDK_BUTTON_RELEASE: {
//		if ( ((GdkEventButton*)ev)->button == 3) {
//			transmit_rui_message( ShowRouteMenu );
//		}
		return true;
	} break;
	case GDK_ENTER_NOTIFY:
		_highlight = true;
		redraw();
	break;
	case GDK_LEAVE_NOTIFY:
		_highlight = false;
		redraw();
	break;
	case GDK_MOTION_NOTIFY: {
//		GdkEventMotion *m = (GdkEventMotion *)ev;
	} break;
	default:
		break;
	}

	return false;
}


//-------------------------


MixbusCtrlSwitcher::~MixbusCtrlSwitcher ()
{
}

MixbusCtrlSwitcher::MixbusCtrlSwitcher (ArdourCanvas::Item *p, RouteUI::RUIMessage m)
	: MixbusSwitcher (p, m)
{
	_text = "Input";

	set_tooltip("Click to show Input Controls.");

	_vis_elem = Mixer_UI::strip_elem_ctrls;
}

void
MixbusCtrlSwitcher::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusSwitcher::set_strip_route (rt, mixer_owned);

	if (_route->presentation_info().flag_match (ARDOUR::PresentationInfo::Track)) {
		_rec_enable_ctrl = _route->rec_enable_control();
		_rec_enable_ctrl->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::something_changed, this), gui_context());
	}
	_phase_ctrl = _route->phase_control();
	_phase_ctrl->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::something_changed, this), gui_context());

	_route->AssignmentChange.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::vca_changed, this, _1, _2), gui_context());

	if (_route->solo_isolate_control ())
		_route->solo_isolate_control ()->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::something_changed, this), gui_context());
	if (_route->solo_safe_control ())
		_route->solo_safe_control ()->Changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::something_changed, this), gui_context());

	_route->active_changed.connect (_route_connections, invalidator (*this), boost::bind (&MixbusCtrlSwitcher::something_changed, this), gui_context());

//	trim->set_controllable( rt, rt->trim()->gain_control() );

	end_visual_change();
}

void
MixbusCtrlSwitcher::something_changed ()
{
	redraw();
}

void
MixbusCtrlSwitcher::vca_changed (std::shared_ptr<ARDOUR::VCA>,bool)
{
	redraw();
}

void
MixbusCtrlSwitcher::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	cr->set_identity_matrix();
	cr->translate (self.x0, self.y0-0.5);

	Distance height = self.height();
	Distance halfh = height/2;
	Distance width = self.width();

	float h = ScreenSizer::user_scale();  //scale factor

#ifdef LAYOUT_HELPER
	//black border...this should be in draw_bg
	set_source_rgba (cr, rgba_to_color (1,0,1,1));
	cr->set_line_width(1);
	cr->rectangle(0, 0, width, height);
	cr->stroke ();
	return;
#endif

	cairo_t *ct = cr->cobj();

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	const float _corner_radius = boxy ? 0 : halfh;

//	if (!_auto_ctrl) return;

	//solid bg
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("mixbus switcher text");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );
	int tw,  th;

   { //text

		layout->set_text (_text);

		if (!_route->active()) {
			layout->set_text ("Inactive");
			cr->set_source_rgba (0,0,0, 0.2);
			Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
			cr->fill ();
			cr->set_source_rgba (0.7, 0.2, 0.2, LEGEND_BRIGHTNESS);
		} else {
			cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		}


//		cr->set_line_width(1);
//		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : (height-2)/2.);
//		cr->stroke ();

		layout->get_pixel_size (tw, th);
		Cairo::Matrix m = cr->get_matrix();
		cairo_translate (ct, floor(width - tw - 4*h), floor(-1*h));
		layout->show_in_cairo_context (cr);
		cr->set_matrix(m); //translate back to widget pos

		//highlight if my element is already showing
		if (Mixer_UI::instance()->strip_elem_visible(_vis_elem)) {
			//LOCK
			if (Mixer_UI::instance()->strip_elem_locked(_vis_elem)) {
				Cairo::Matrix m = cr->get_matrix();
				cairo_translate (ct, floor(0*h), floor(1*h));
				layout->set_text(" *");
				layout->show_in_cairo_context (cr);
				cr->set_matrix(m); //translate back to widget pos
			}
			cr->set_source_rgba (1,1,1, 0.5);
			Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
			cr->set_line_width (1*h);
			cr->stroke ();
		}

		//highlight
		if ((UIConfiguration::instance().get_widget_prelight() && _highlight)) {
			cr->set_source_rgba (1,1,1, 0.2);
			Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width - 2*h, height-2*h, boxy ? 0 : 2*h);
			cr->fill ();
		}
   }

	cr->translate (self.x0, self.y0-0.5);

	cr->translate (self.x0, self.y0-0.5);
	Item::render_children(area, cr);  //now render any children in the needed area
}

void
MixbusCtrlSwitcher::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	//NOTE:  MixbusCtrlSwitcher needs to render a bg,  since it is the only widget on the canvas

	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	cr->set_identity_matrix();
	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	cr->set_identity_matrix();

	//TODO: offset this in fader (?)

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (false) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}
}
