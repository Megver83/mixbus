/*
    Copyright (C) 2023 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_comp.h"
#include "mixbus_k_meter.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------



//an MBCanvas for input knobs
MixbusCompCanvas::MixbusCompCanvas ()
{
	_knobs = new MixbusComp(root());
}

float
MixbusCompCanvas::natural_height ()
{
	float h = NATURAL_COMP_HEIGHT;

	MixbusStrip *child = (MixbusStrip *) _root.items ().front ();
	if (child && child->stripable() && child->stripable()->is_master()) {
		h -= NATURAL_K14_HEIGHT;  //master-bus compressor must be slightly smaller to line up
	}

	return h;
}


//-------------------------


MixbusComp::~MixbusComp ()
{
}

MixbusComp::MixbusComp (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_comp;

	std::string tl[NUM_KNOB_TICKS] = { "|", "|", "|", "|", "|", "|", "|", "|", "|", "|", "|" };
	std::string spd[NUM_KNOB_TICKS] = { "|", "Fst", "|", "|", "|", "|", "|", "|", "|", "Slo", "|" };

	comp_attk = new MixbusKnob(this);
	comp_attk->set_color("mixbus compressor attack knob");
	widgets.push_front(comp_attk);
//	std::string ga[NUM_KNOB_TICKS] = { "|", ".2", "|", "|", "25", "|", "120", "|", "|", "20", "|" };
	comp_attk->set_ticks(spd);

	comp_rels = new MixbusKnob(this);
	widgets.push_front(comp_rels);
	comp_rels->set_color("mixbus compressor release knob");
//	std::string gr[NUM_KNOB_TICKS] = { "|", "2", "|", "|", "25", "|", "150", "|", "|", " 1.5", "|" };
	comp_rels->set_ticks(spd);

	comp_ratio = new MixbusKnob(this);
	widgets.push_front(comp_ratio);
	comp_ratio->set_color("mixbus compressor ratio knob");
	std::string rat[NUM_KNOB_TICKS] = { "|", "-", "|", "|", "|", "|", "|", "|", "|", "+", "|" };
	comp_ratio->set_ticks(rat);

	comp_emph = new MixbusKnob(this);
	comp_emph->set_color("mixbus compressor emph freq knob");
	widgets.push_front(comp_emph);
	std::string hmft[NUM_KNOB_TICKS] = { "|", "-", "|", "|", "|", "|", "|", "|", "|", "+", "|" };
	comp_emph->set_ticks(hmft);

	comp_gain = new MixbusKnob(this);
	comp_gain->set_color("mixbus compressor gain knob");
	widgets.push_front(comp_gain);
	std::string gt[NUM_KNOB_TICKS] = { "|", "0", "|", "|", "|", "|", "|", "|", "|", "10", "|" };
	comp_gain->set_ticks(gt);

	comp_thresh = new MixbusThresh(this);
	comp_thresh->set_color("mixbus compressor threshold knob");
	widgets.push_front(comp_thresh);

	comp_thresh_knob = new MixbusKnob(this);
	comp_thresh_knob->set_color("mixbus compressor threshold knob");
	widgets.push_front(comp_thresh_knob);
	std::string hft[NUM_KNOB_TICKS] = { "|", "-20", "|", "|", "|", "|", "|", "|", "|", "0", "|" };
	comp_thresh_knob->set_ticks(hft);

	comp_in = new MixbusToggle(this);
	widgets.push_front(comp_in);
	comp_in->set_text("Comp");
//	comp_in->set_color( 0.25, 0.9, 0.2 );

	//comp input meter leds
	float met = -3.2;
	for (int i = 0 ; i< COMP_METER_CNT; i++) {
		input_meterDots[i] = new MixbusMeterDot(this);
		widgets.push_front(input_meterDots[i]);
		input_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		met -= 3.0 + (float)i;
	}

	//comp redux meter leds
	met = -0.2;
	for (int i = 0 ; i< COMP_THRESH_CNT; i++) {
		redux_meterDots[i] = new MixbusReduxDot(this);
		widgets.push_front(redux_meterDots[i]);
		redux_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		redux_meterDots[i]->set_color( rgba_to_color (1,0,0,1) );  //TODO:  watch the meter color options
		met -= 2.0;
	}
	redux_meterDots[COMP_THRESH_CNT-2]->set_thresh( dB_to_coefficient(-14) );
	redux_meterDots[COMP_THRESH_CNT-1]->set_thresh( dB_to_coefficient(-24) );

	comp_mode = new MixbusModeSelector(this);
	widgets.push_front(comp_mode);
	comp_mode->set_tooltip(_("Comp Mode"));


	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusComp::meter));

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusComp::config_changed, this, _1), gui_context());
}

void
MixbusComp::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scaling factor
	const float toggle_height = h*17.0;
	const float knob_sz = h*22;

// ------------ track gate knobs


	float l_offs = midp -34*h;
	float r_offs = midp + 14*h;

	float led_width = floor(Wide ? 9*h : 5*h);

	float master_offset = 0;
	if (_route && _route->is_master()) {
		master_offset = -NATURAL_K14_HEIGHT*h;
	}

	int stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	int size = floor(stride *0.75);
	for (int i = 0 ; i< COMP_METER_CNT; i++) {
		input_meterDots[i]->set_size(ArdourCanvas::Rect (0, 0, led_width, size));
		input_meterDots[i]->set_position(ArdourCanvas::Duple ( floor(39*h), floor(23*h + h*12 + stride*i) ));
	}

	stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	size = floor(stride *0.75);
	for (int i = 0 ; i< COMP_THRESH_CNT; i++) {
		redux_meterDots[COMP_THRESH_CNT-i-1]->set_size(ArdourCanvas::Rect (0, 0, led_width, size));
		redux_meterDots[COMP_THRESH_CNT-i-1]->set_position(ArdourCanvas::Duple ( r_offs+6*h, floor(23*h + h*12 + stride*i) ));
	}

	comp_thresh->set_size(ArdourCanvas::Rect (0, 0, knob_sz, h*90));
	comp_thresh->set_position(ArdourCanvas::Duple (16*h, 7*h + 20*h));

	comp_thresh_knob->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	comp_thresh_knob->set_position(ArdourCanvas::Duple (16*h, 10*h));

	comp_attk->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	comp_attk->set_position(ArdourCanvas::Duple (l_offs, 21*h + 116*h + master_offset));

	comp_rels->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	comp_rels->set_position(ArdourCanvas::Duple (r_offs, 21*h + 108*h + master_offset));

	comp_ratio->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	comp_ratio->set_position(ArdourCanvas::Duple (r_offs, 17*h + 68*h + master_offset));

	comp_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	comp_gain->set_position(ArdourCanvas::Duple (l_offs, 17*h + 168*h + master_offset));

	comp_emph->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	comp_emph->set_position(ArdourCanvas::Duple (r_offs, 17*h + 160*h + master_offset));


	comp_mode->set_size(ArdourCanvas::Rect (0, 0, inWidth-7*h, h*17));
	comp_mode->set_position(ArdourCanvas::Duple ( h*4, h*4 + master_offset));

	comp_in->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	comp_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 218*h + master_offset));

	end_visual_change();
}

void
MixbusComp::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	bool is_master = _route->is_master();

	_bg_pattern_type = is_master ? bg_pattern_comp_master : bg_pattern_comp;

	comp_in->show();
	comp_in->set_route( rt, rt->comp_enable_controllable() );

	comp_attk->show();
	comp_attk->set_route( rt, rt->comp_attack_controllable() );

	comp_rels->show();
	comp_rels->set_route( rt, rt->comp_release_controllable() );

	comp_emph->show();
	comp_emph->set_route( rt, rt->comp_key_filter_freq_controllable() );

	comp_ratio->show();
	comp_ratio->set_route( rt, rt->comp_ratio_controllable() );

	comp_gain->show();
	comp_gain->set_route( rt, rt->comp_makeup_controllable() );

	//now show and re-connect based on route type ?
	if ( is_master )  {  //all chans have a gate ... TODO  remove master?
		comp_thresh_knob->show();
		comp_thresh_knob->set_route( rt, rt->comp_threshold_controllable() );
	} else {
		comp_thresh->show();
		comp_thresh->set_route( rt, rt->comp_threshold_controllable() );

		comp_mode->show();
		comp_mode->set_route( rt, rt->comp_mode_controllable() );

		for (int i = 0 ; i< (COMP_METER_CNT); i++) {
			input_meterDots[i]->set_route( rt, rt->comp_meter_controllable() );
			input_meterDots[i]->show();
		}

		for (int i = 0 ; i< (COMP_THRESH_CNT); i++) {
			redux_meterDots[i]->set_route( rt, rt->comp_redux_controllable() );
			redux_meterDots[i]->show();
		}
	}

	end_visual_change();
}

void
MixbusComp::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();  float halfw = width/2.0;

	Cairo::Matrix m;

	bool is_mixbus = _route->mixbus() != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scaling factor

	int tw,  th;  //text width and height

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	float master_offset = 0;
	if (is_master) {
		master_offset = -NATURAL_K14_HEIGHT*h;
	}

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	/* puddle color */
	SVAModifier bord = UIConfiguration::instance().modifier ("strip border alpha");
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double puddle_r,puddle_g,puddle_b;
	Gtkmm2ext::color_to_rgba( puddle_color, puddle_r, puddle_g, puddle_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//makeup puddle
	if (true) {
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		Gtkmm2ext::rounded_rectangle (cr, 0, 173*h, width-midp, 67*h + master_offset, boxy ? 0 : 5*h);
		cr->fill ();
	}

	//thresh puddle
	if (false) {
		cr->set_line_width (1.0*h);
		cr->move_to( 0*h, 0*h + master_offset ); //start in the top left
		cr->rel_line_to( 0*h, 126*h );
		cr->rel_line_to( 47*h, 0*h );
		cr->rel_line_to( 10*h, -8*h );  //slant
		cr->rel_line_to( width-57*h, 0 );
		cr->line_to( width, 0 );
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		cr->fill ();
	}

	//time separators
	if (true) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		cr->move_to( 6*h, floor(17*h + 113*h + master_offset) ); //start in the top left
		cr->rel_line_to( 41*h, 0 );
		cr->rel_line_to( 10*h, -8*h );  //slant
		cr->rel_line_to( 38*h, 0 );
		cr->stroke();
	}

	//time separators
	if (true) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		cr->move_to( 6*h, floor(17*h + 158*h + master_offset) ); //start in the top left
		cr->rel_line_to( 41*h, 0 );
		cr->rel_line_to( 10*h, -8*h );  //slant
		cr->rel_line_to( 38*h, 0 );
		cr->stroke();
	}

	//Pgm-release connector
	if (false) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		cr->move_to( 42*h, 180*h + master_offset ); //start in the top left
		cr->rel_line_to( 4*h, 4*h );
		cr->rel_line_to( 0, 40*h );
		cr->rel_line_to( -4*h, 4*h );
		cr->stroke();
	}

	//Input ticks
	if (!is_master) {
		Gtkmm2ext::set_source_rgb_a (cr, font_color, 0.7);
		cr->set_identity_matrix();

		layout->set_text( "0" );
		cr->move_to( floor(6*h), 27*h ); //start in the top left
		layout->show_in_cairo_context (cr);

		layout->set_text( "10" );
		cr->rel_move_to( 0, 19*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "20" );
		cr->rel_move_to( 0, 19*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "30" );
		cr->rel_move_to( 0, 19*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "40" );
		cr->rel_move_to( 0, 19*h );
		layout->show_in_cairo_context (cr);

		cr->begin_new_path();
	}

	//Redux ticks
	if (!is_master) {
		int tw, th;
		Gtkmm2ext::set_source_rgb_a (cr, font_color, 0.7);
		cr->set_identity_matrix();

		layout->set_text( "24" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(27*h + 0*13*h) ); //start in the top left
		layout->show_in_cairo_context (cr);

		layout->set_text( "-8" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(27*h + 1*12*h) );
		layout->show_in_cairo_context (cr);

		layout->set_text( "-4" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(27*h + 2*12*h) );
		layout->show_in_cairo_context (cr);

		layout->set_text( "0" );
		layout->get_pixel_size (tw, th);
		cr->set_identity_matrix();
		cr->move_to( floor(width - 5*h) - tw, floor(27*h + 3*12*h) );
		layout->show_in_cairo_context (cr);
		cr->begin_new_path();
	}

	//Thresh decorator
	if (is_master) {
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Thresh" );
		layout->get_pixel_size (tw, th);
		cr->translate(  floor(27*h - (tw/2.)), floor(23*h + 89*h + master_offset) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Ratio decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Ratio" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(75*h - (tw/2.)), floor(17*h + 89*h + master_offset) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Attk decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Attk" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(27*h - (tw/2.)), floor(21*h + 138*h + master_offset) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Rels decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Rels" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(75*h - (tw/2.)), floor(21*h + 130*h + master_offset) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

/*
	//Knee decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Knee" );
		layout->get_pixel_size (tw, th);
		cr->translate( 27*h - (tw/2.), 166*h );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Ratio decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Ratio" );
		layout->get_pixel_size (tw, th);
		cr->translate( 27*h - (tw/2.), 196*h );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}
*/

	//Gain decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Emph" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(75*h - (tw/2.)), floor(17*h + 182*h + master_offset) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Emphasis decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Gain" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(27*h - (tw/2.)), floor(17*h + 190*h + master_offset) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}


	float drad = 7;  //decorator radius
	float offs = 146;  //32C offset ( allows widgets in the fader area to share position values with regular mb

	if ( false) {

		//eq decorator
		{
			cr->move_to( 1*h, 0*h ); //start in the top left
			cr->rel_line_to( width-2*h, 0);
			cr->rel_line_to( 0, 85*h );
			cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
			cr->rel_line_to( -width + drad*2*h, 0 );
			cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );//btm left corner
			cr->rel_line_to( 0, -85*h );
			cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
//			cr->set_line_width (1.0*h);
//			cr->set_source_rgba (1,1,1,0.7);
//			cr->stroke();
			cr->fill();
		}
		cr->set_identity_matrix();


	} else if (false) {

		//TONE decorator
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->move_to( 6*h, 58*h ); //start in the top left
		cr->rotate(M_PI*2*-0.041);
		cr->rel_curve_to( 0, drad*h, drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 83*h, 0*h );
		cr->rel_curve_to( drad*h, 0, drad*h, -drad*h, drad*h, -drad*h );
		cr->stroke ();
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//bug
	if (false){
		cr->set_identity_matrix();
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_logo_texture->gobj(), 83*h, 4*h);
		cr->rectangle (0, 0, 16*h, 16*h);
		cr->paint_with_alpha ( 0.65 );
		cr->begin_new_path();
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 6*h, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false && !flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 7);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 8 );
		cr->fill ();
	}

	if (false) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//section bounds
	if (true) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width-2*h, height-2*h, 5*h);
		cr->stroke ();
	}

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}
}

void
MixbusComp::meter()
{
	if (!_route)
		return;

	//input meter
	for (int i = 0 ; i< (COMP_METER_CNT); i++) {
		input_meterDots[i]->meter();
	}

	//redux meter
	for (int i = 0 ; i< COMP_THRESH_CNT; i++) {
		redux_meterDots[i]->meter();
	}
}

void
MixbusComp::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {}

	//nothing to do here?
}

void
MixbusComp::config_changed (string p)
{
}
