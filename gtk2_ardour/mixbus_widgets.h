/*
    Copyright (C) 2014 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_WIDGETS__
#define __MB_WIDGETS__

#include <string>
#include <vector>

#include <pangomm/fontdescription.h>

#include <canvas/canvas.h>
#include <canvas/container.h>
#include <canvas/item.h>
#include <canvas/fill.h>
#include <canvas/outline.h>

#include <gtkmm/layout.h>
#include <gtkmm/action.h>

#include "gtkmm2ext/activatable.h"

#include "pbd/stateful.h"

#include <ardour/route.h>
#include <ardour/vca.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/automation_control.h>

#include "ui_config.h"

#ifdef MIXBUS32C
# define LEGEND_BRIGHTNESS 1.0
# define NUM_KNOB_TICKS 11
#else
# define LEGEND_BRIGHTNESS 1.0
# define NUM_KNOB_TICKS 7
#endif

#define OUTLINE_WIDTH 1.5

#define NO_ROUND false  //a reminder to disable rounding (false) when calling item_to_window

class ScreenSizer {
public:

	static ScreenSizer *instance() { if (!_instance) _instance = new ScreenSizer(); return _instance; }

	static float user_scale() { return UIConfiguration::instance().get_mixbus_strip_scale(); }

	static float suggested_scale() { return instance()->_scale; }

	static Pango::FontDescription GetFontDesc();
	static Pango::FontDescription GetBoldFontDesc();
	static Pango::FontDescription GetBigBoldFontDesc();

private:

	ScreenSizer();
	static ScreenSizer *_instance;
	float _scale;
};


class MBContainer_InputKnobs;

class MixbusWidget : public ArdourCanvas::Item
{
public:
	MixbusWidget(ArdourCanvas::Item *);
	virtual ~MixbusWidget();
	virtual void ClearConnections();

	virtual void set_size (ArdourCanvas::Rect const&);
	virtual void compute_bounding_box () const;
	virtual void update_display();

	virtual void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const {}

	virtual void render_bg_top (Cairo::RefPtr<Cairo::Context>, bool wide) const {}  //special case for VU meters

protected:

	bool	_highlight;
	bool _grabbed;

	ArdourCanvas::Rect          _rect;

	PBD::ScopedConnection _connection;

	//connecty bits.  this connects the knob to the plugin's parameter
	ArdourWidgets::BindingProxy binding_proxy;
	std::shared_ptr<ARDOUR::AutomationControl> _auto_ctrl;  //NOTE:  this needs to go away, use binding_proxy in all cases
	std::shared_ptr<ARDOUR::ReadOnlyControl> _ro_ctrl;  //NOTE:  this needs to go away, use binding_proxy in all cases

	//stripable" is needed for some complicated widgets like Solo, Mute, compressor mode
	std::shared_ptr<ARDOUR::Stripable> _stripable;
};

class MixbusVcaFader : public MixbusWidget, public sigc::trackable, public PBD::ScopedConnectionList
{
public:
	MixbusVcaFader (ArdourCanvas::Item *);
	~MixbusVcaFader ();

	void set_vca_controllable( std::shared_ptr<ARDOUR::AutomationControl> ctrl, std::shared_ptr<ARDOUR::VCA> );
	void update_display();

	virtual void start_touch (std::shared_ptr<ARDOUR::AutomationControl> ctrl);
	virtual void stop_touch (std::shared_ptr<ARDOUR::AutomationControl> ctrl);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;
	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	bool vca_event ( GdkEvent* e);
private:
	std::shared_ptr<ARDOUR::VCA> _vca;

	float _grabbed_y;

	float _last_val;  //last value we received from the param
	float _last_drawn;  //last value we received from the param
	float _normal; //default val
};

typedef std::pair<float, std::string> KnobTickMark;
struct KnobTicks {
	std::vector<KnobTickMark> ticks;
};

class MixbusKnob : public MixbusWidget, public sigc::trackable, public PBD::ScopedConnectionList
{
public:
	MixbusKnob (ArdourCanvas::Item *);
	~MixbusKnob ();

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl );
	void set_controllable( std::shared_ptr<ARDOUR::Stripable> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl );  //used for trim
	void update_display();

	void set_concentric_controllables ( std::shared_ptr<ARDOUR::AutomationControl> enbl, std::shared_ptr<ARDOUR::AutomationControl> knb );

	void ClearConnections();

	virtual void slate_conversion_func( char* buf, float val ) const { if(_auto_ctrl) {sprintf(buf, "%s", _auto_ctrl->get_user_string().c_str());} }

	// call this when you want the knob to send its value to the strip's name slate,
	// unless specified otherwise, _auto_ctrl is used
	virtual void update_slate (std::shared_ptr<ARDOUR::AutomationControl> ac = std::shared_ptr<ARDOUR::AutomationControl>());

	virtual void start_touch (std::shared_ptr<ARDOUR::AutomationControl> ctrl);
	virtual void stop_touch (std::shared_ptr<ARDOUR::AutomationControl> ctrl);

	bool knob_event ( GdkEvent* e);
	virtual void right_double_click() {}
	virtual void right_move() {}

	void color_handler ();

	void set_color ( const char* c ) { _color = c; color_handler(); }

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;
	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	void set_ticks(std::string in[NUM_KNOB_TICKS]) { for (int i = 0; i<NUM_KNOB_TICKS; i++ ) ticks[i] = in[i];  }

protected:

	float _blue_desat;

	std::string _color;

	bool _rotary;

	bool _hovering;
	float _grabbed_y;

	KnobTicks _tickmarks;

	std::string ticks[NUM_KNOB_TICKS];

	float _last_val;  //last value we received from the param
	float _last_drawn;  //last value we received from the param
	float _normal; //default val

	std::shared_ptr<ARDOUR::AutomationControl> _concentric_enable_ctrl;
	PBD::ScopedConnection _concentric_enable_connection;

	std::shared_ptr<ARDOUR::AutomationControl> _concentric_knob_ctrl;
	PBD::ScopedConnection _concentric_knob_connection;

	float _last_concentric_knob_val, _last_concentric_enable_val;  //last values we received from the param
};

class MixbusWidthKnob : public MixbusKnob
{
public:
	MixbusWidthKnob (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;

	void slate_conversion_func( char* buf, float val ) const;
};

class MixbusPanKnob : public MixbusKnob
{
public:
	MixbusPanKnob (ArdourCanvas::Item *);
	virtual void slate_conversion_func( char* buf, float val ) const;
};

class MixbusSpeedKnob : public MixbusKnob
{
public:
	MixbusSpeedKnob (ArdourCanvas::Item *);
	virtual void slate_conversion_func( char* buf, float val ) const;
};

class Mixbus_EQ_Knob : public MixbusKnob
{
public:
	Mixbus_EQ_Knob (ArdourCanvas::Item *);
	~Mixbus_EQ_Knob () {_freq_ctrl.reset(); _gain_ctrl.reset(); _bell_ctrl.reset();}

	void set_slate_ctrls(    int idx,
						std::shared_ptr<ARDOUR::AutomationControl> f,
						std::shared_ptr<ARDOUR::AutomationControl> g,
						std::shared_ptr<ARDOUR::AutomationControl> b)
				   { _band_idx = idx; _freq_ctrl = f; _gain_ctrl = g; _bell_ctrl = b;}

private:
	int _band_idx;
	std::shared_ptr<ARDOUR::AutomationControl>  _freq_ctrl;
	std::shared_ptr<ARDOUR::AutomationControl>  _gain_ctrl;
	std::shared_ptr<ARDOUR::AutomationControl>  _bell_ctrl;
};

class Mixbus_Filter_Knob : public MixbusKnob
{
public:
	Mixbus_Filter_Knob (ArdourCanvas::Item* , int idx);

	virtual void slate_conversion_func( char* buf, float val ) const;
private:
	int _band_idx;
};

class MixbusGainKnob : public MixbusKnob
{
public:
	MixbusGainKnob (ArdourCanvas::Item *);
	virtual void slate_conversion_func( char* buf, float val ) const;
};

class MixbusSendKnob : public MixbusKnob
{
public:
	MixbusSendKnob (ArdourCanvas::Item *, int send_num);
	virtual void slate_conversion_func( char* buf, float val ) const;
private:
	uint32_t _send_number;
};

class MixbusFader : public MixbusKnob
{
public:
	MixbusFader (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;
	void render_bg_top (Cairo::RefPtr<Cairo::Context>, bool wide) const {}

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl>  ) {printf("DO NOT USE\n");}

	void set_fader_route( std::shared_ptr<ARDOUR::Route> rt );

	void update_display();

	virtual void update_slate (std::shared_ptr<ARDOUR::AutomationControl> ac = std::shared_ptr<ARDOUR::AutomationControl>()) {}

	void set_route_color (Gtkmm2ext::Color c) { _route_color = c;  redraw(); }

	void set_tickmarks(KnobTicks t) {_tickmarks = t;}
private:
	Gtkmm2ext::Color _route_color; //the route's color
};


class MixbusThresh : public MixbusKnob
{
public:
	MixbusThresh (ArdourCanvas::Item *);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg_top (Cairo::RefPtr<Cairo::Context>, bool wide) const {}

	void update_display();
};


class MixbusSlate : public MixbusWidget, public sigc::trackable
{
public:

	MixbusSlate (ArdourCanvas::Item *);
	~MixbusSlate ();

	void set_route( std::shared_ptr<ARDOUR::Route> rt );
	void set_color (Gtkmm2ext::Color c) { _color = c;  redraw(); }
	void set_text (std::string txt) { _text = txt;  end_visual_change(); }
	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	void update_display() {}

	bool event_handler ( GdkEvent* e);

	void something_changed () {redraw();}  //watch for track rec-arm

private:

	std::shared_ptr<ARDOUR::Route> _route;
	std::shared_ptr<ARDOUR::AutomationControl> _rec_enable_ctrl;
	PBD::ScopedConnectionList _route_connections;

	std::string			_text; //the route's name
	Gtkmm2ext::Color _color; //the route's color
};


class MixbusToggle : public MixbusWidget, public sigc::trackable, public PBD::ScopedConnectionList
{
public:

	MixbusToggle (ArdourCanvas::Item *);
	~MixbusToggle ();

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl );
	virtual void set_controllable( std::shared_ptr<ARDOUR::Stripable> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl );  //used for solo and mute

	void update_display();

	void color_handler_callback ();
	virtual void color_handler ();

	bool event_handler ( GdkEvent* e);

	sigc::signal<bool, GdkEventButton*> button_press_event;
	sigc::signal<bool, GdkEventButton*> button_release_event;

	void set_text ( std::string t ) { _text = t;  }
	void set_color ( const char* c ) { _color = c; color_handler(); }

	virtual void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;

	void set_mixbus_asgn_num(int n) {_mixbus_asgn_num=n;}
protected:

	std::string                 _text;
	Glib::RefPtr<Pango::Layout> _layout;
	int	_text_width, _text_height;

	std::string _color;
	double _led_r, _led_g, _led_b;
	double fill_r, fill_g, fill_b;

	float _tog_value;  //we'll toggle between _tog_value and 0;

	float _last_val;  //last value we received from the param
	float _last_drawn;  //last value we received from the param
	float _normal; //default val

	int _mixbus_asgn_num;
};

class MixbusSquareToggle : public MixbusToggle
{
public:

	MixbusSquareToggle (ArdourCanvas::Item *, int32_t tog_value);
	~MixbusSquareToggle () {}

	virtual void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const {}
private:
};

class MixbusTinyToggle : public MixbusToggle
{
public:

	MixbusTinyToggle (ArdourCanvas::Item *);
	~MixbusTinyToggle () {}

	virtual void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const {}
};

class MixbusButton : public MixbusToggle
{
public:
	MixbusButton (Item* c, float toggle_value = 1.0);
	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const {}

protected:
	virtual int active_state () const;
};

class MixbusSpillButton : public MixbusButton
{
public:

	MixbusSpillButton (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	void set_stripable( std::shared_ptr<ARDOUR::Stripable> st );

	void set_spill_state(std::shared_ptr<ARDOUR::Stripable>);
	bool spill_press ( GdkEventButton* );

protected:
	int active_state () const;

};

class MixbusBigToggle : public MixbusButton
{
public:

	MixbusBigToggle (ArdourCanvas::Item *, bool soloer);

	void set_controllable( std::shared_ptr<ARDOUR::Stripable> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl );  //used for solo and mute

	void update_display();
	void update_solo_display ();

protected:
	int active_state () const;

private:
	PBD::ScopedConnectionList _route_connections;
	bool _soloer;
};

class MixbusActionToggle : public MixbusButton, public Gtkmm2ext::Activatable
{
public:
	MixbusActionToggle (ArdourCanvas::Item *);
	void set_related_action (Glib::RefPtr<Gtk::Action>);

protected:
	int active_state () const;
	bool button_release (GdkEventButton* ev);

private:
	void update_display();
};

#ifdef MIXBUS32C
class Mixbus_BusToggle : public MixbusTinyToggle
#else
class Mixbus_BusToggle : public MixbusToggle
#endif
{
public:

	Mixbus_BusToggle (ArdourCanvas::Item *);
	~Mixbus_BusToggle ();

	void set_bus_route( std::shared_ptr<ARDOUR::Route> rt );

	virtual void color_handler ();  //we get color from the attached bus, not the widget
	void route_gui_changed (PBD::PropertyChange const&);
	void property_changed (const PBD::PropertyChange& what_changed);

	void update_display();

private:
	PBD::ScopedConnectionList _bus_connections;
	std::shared_ptr<ARDOUR::Route> _bus_route;
};

class Mixbus_BusLabel : public Mixbus_BusToggle
{
public:

	Mixbus_BusLabel (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;
};


class Mixbus_MasterToggle : public MixbusToggle
{
public:

	Mixbus_MasterToggle (ArdourCanvas::Item *);

	void update_display();
};

class MixbusModeSelector : public MixbusWidget, public sigc::trackable, public PBD::ScopedConnectionList
{
public:
	MixbusModeSelector (ArdourCanvas::Item *);
	~MixbusModeSelector();

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::AutomationControl> ctrl );
	void update_display();

	bool click_handler ( GdkEvent* e);

	void build_comp_mode_menu ();
	Gtk::Menu* comp_mode_menu;
	void set_comp_mode (int mode);
	gboolean comp_mode_button_press (GdkEvent*);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
protected:
	float _last_val;  //last value we received from the param
	float _last_drawn;  //last value we received from the param
	float _normal; //default val

	std::string _theme;

	sigc::connection event_connection;
};

class MixbusCompLegend : public MixbusModeSelector
{
public:
	MixbusCompLegend (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
};

// a label that displays either "Hyst"(eresis) or "Knee" depending on the state of a plugin param
class MixbusHystLegend : public MixbusModeSelector
{
public:
	MixbusHystLegend (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
};

// a label that displays either "Hold" or "Ratio" depending on the state of a plugin param
class MixbusHoldLegend : public MixbusModeSelector
{
public:
	MixbusHoldLegend (ArdourCanvas::Item *);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
};

// a text label
class MixbusLabel : public MixbusWidget
{
public:
	MixbusLabel (ArdourCanvas::Item *);

	void set_text(std::string txt) {_label = txt;}

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

protected:
	std::string _label;
};

class MixbusMeterBar : public MixbusWidget, public sigc::trackable, public PBD::ScopedConnectionList
{
public:

	MixbusMeterBar (ArdourCanvas::Item *);

	void meter(float, float, float, float, bool, bool);

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

	void update_display() {}

protected:
	float _l_val;
	float _r_val;

	float _l_hold;
	float _r_hold;

	float _l_red;
	float _r_red;

	bool _stereo;

	bool _redux;

};

class MixbusMeterDot : public MixbusWidget, public sigc::trackable, public PBD::ScopedConnectionList
{
public:

	MixbusMeterDot (ArdourCanvas::Item *);

	void update_display() {}

	void meter();  //gets called at a fixed rate, NOT driven by dsp callbacks

	void set_thresh(float thr) {_thresh = thr; }//if value is set above/below this, I will light up

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::ReadOnlyControl> ctrl );

	void set_color(Gtkmm2ext::Color c) { _color = c; }

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;

protected:
	float _thresh;

	bool _redux;

	bool _last_on;

	Gtkmm2ext::Color	_color;
};

class MixbusReduxDot : public MixbusMeterDot
{
public:

	MixbusReduxDot (ArdourCanvas::Item *);

	void update_display() {}
};


class MixbusPointerMeter : public MixbusWidget
{
public:

	MixbusPointerMeter (ArdourCanvas::Item *, bool k14 = false);

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::ReadOnlyControl> ctrl );
	void meter();  //gets called regularly

	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg_top (Cairo::RefPtr<Cairo::Context>, bool wide) const;

	void update_display() {}

private:
	float _last_val;  //last value we received from the param

	bool _k14_scale;
	int flicker_offset;
};

class MixbusPhaseMeter : public MixbusWidget
{
public:

	MixbusPhaseMeter (ArdourCanvas::Item *);

	void set_route( std::shared_ptr<ARDOUR::Route> rt, std::shared_ptr<ARDOUR::ReadOnlyControl> low_ctrl, std::shared_ptr<ARDOUR::ReadOnlyControl> high_ctrl );
	void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	void render_bg (Cairo::RefPtr<Cairo::Context>, bool wide) const;
	void meter();  //gets called regularly

	void update_display() {}

private:
	std::shared_ptr<ARDOUR::ReadOnlyControl> _low_phase_ctrl;
	std::shared_ptr<ARDOUR::ReadOnlyControl> _high_phase_ctrl;

	float _last_low_val;  //last value we received from the param
	float _last_high_val;  //last value we received from the param
};

#endif //__MB_WIDGETS__
