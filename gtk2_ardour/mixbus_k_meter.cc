/*
    Copyright (C) 2014 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include <pbd/compose.h>

#include "ardour/utils.h"
#include "ardour/amp.h"
#include "ardour/vca.h"
#include "ardour/meter.h"
#include "ardour/dB.h"
#include "ardour/route_group.h"
#include "ardour/automation_control.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/types.h"
#include "canvas/debug.h"
#include "canvas/utils.h"
#include "canvas/canvas.h"

#include "widgets/binding_proxy.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_k_meter.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"
#include "pbd/controllable.h"

#include "timers.h"

using namespace std;
using namespace PBD;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

Mixbus_K_Canvas::Mixbus_K_Canvas ()
{
	_knobs = new Mixbus_K_Meter(root());
}

//------------------------------


Mixbus_K_Meter::~Mixbus_K_Meter ()
{
	meter_connection.disconnect();
}

Mixbus_K_Meter::Mixbus_K_Meter (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_kmeter;

	k_meter = new MixbusPointerMeter(this, true);
	widgets.push_front(k_meter);
	k_meter->set_tooltip( _("Loudness Meter (K-14)") );

//polarity correlation meter
	c_meter = new MixbusPhaseMeter(this);
	widgets.push_front(c_meter);
	c_meter->set_tooltip( _("Stereo Correlation Meter") );

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	//TODO:  maybe consolidate this at some higher level, now that meter is in 2+ widgets
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &Mixbus_K_Meter::meter));
}

void
Mixbus_K_Meter::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	float h = ScreenSizer::user_scale();  //scale factor

	k_meter->set_size(ArdourCanvas::Rect (0, 0, inWidth - 1, h*54));
	k_meter->set_position(ArdourCanvas::Duple (0*h, h*3) );

	//stereo phase correlation meter
	c_meter->set_size(ArdourCanvas::Rect (0, 0, inWidth-4*h, 16*h));
	c_meter->set_position(ArdourCanvas::Duple (2*h, h*54 + 6*h));

	end_visual_change();
}

void
Mixbus_K_Meter::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	k_meter->set_route( rt, rt->master_k_mtr_controllable() );
	k_meter->show();

	c_meter->set_route (rt, rt->master_correlation_mtr_controllable (false), rt->master_correlation_mtr_controllable (true));
	c_meter->show();


	end_visual_change();
}

void
Mixbus_K_Meter::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	MixbusStrip::render(area, cr);
}

void
Mixbus_K_Meter::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect, NO_ROUND));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	bool flat = UIConfiguration::instance().get_flat_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	cr->set_identity_matrix();
	
//match the bottom color at the bus canvas
	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.10);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 8, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 8, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//special case for VU meter widgets (K-meter);  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
Mixbus_K_Meter::meter()
{
	if (!_route)
		return;

	//K meter
	k_meter->meter();

	//stereo correlation meter
	c_meter->meter();
}

