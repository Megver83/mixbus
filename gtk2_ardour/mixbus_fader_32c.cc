/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_fader.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------

//an MBCanvas for input knobs
MixbusStripFaderCanvas::MixbusStripFaderCanvas (bool small)
 : _small (small)
{
	if (small) {
		_knobs = new MixbusStripFader_Small(root());
	} else {
		_knobs = new MixbusStripFader(root());
	}
}

void
MixbusStripFaderCanvas::set_hold_count (int cnt)
{
	MixbusStripFader *f = (MixbusStripFader*) _knobs;
	f->set_hold_count (cnt);
}

void
MixbusStripFaderCanvas::display_slate_text (std::string txt)
{
	MixbusStripFader *f = (MixbusStripFader*) _knobs;
	f->display_slate_text (txt);
}

float
MixbusStripFaderCanvas::natural_height ()
{
	float h = NATURAL_FADER_HEIGHT;

	if (_small) h-= 100;
	
	return h;
}


//-------------------------


MixbusStripFader::~MixbusStripFader ()
{
	meter_connection.disconnect();
}

MixbusStripFader::MixbusStripFader (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_fader;

	hold_cnt = 0;
	hold_val = hold_val_r = -120;  //in dB

	slate = new MixbusSlate(this);
	widgets.push_front(slate);  //slate doesn't need this
	slate->set_tooltip("Right-click to rename");

	mute = new MixbusBigToggle(this, false);
	widgets.push_front(mute);
	mute->set_color("mute button");
	mute->set_text( "M" );
	std::string tip = string_compose( _("Mute this track\n"
							            "%2+Click to Override Group\n"
							            "%1+%3+Click to toggle ALL tracks\n"
							            "%4 for Momentary mute\n"
							            "Right-Click for Context menu")
									  , Keyboard::primary_modifier_short_name(), Keyboard::group_override_event_name(), Keyboard::tertiary_modifier_short_name(), Keyboard::momentary_push_name() );
	mute->set_tooltip(tip);

	mute->button_press_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_mute_press));
	mute->button_release_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_mute_release));

	solo = new MixbusBigToggle(this, true);
	widgets.push_front(solo);
	solo->set_color("solo button");
	solo->set_text( "S" );
	tip = string_compose( _("Solo this track\n"
										"%2+Click to Override Group\n"
										"%1+%5+Click for Exclusive solo\n"
										"%1+%3+Click to toggle ALL tracks\n"
										"%4 for Momentary solo\n"
										"Right-Click for Context menu")
										, Keyboard::primary_modifier_short_name(), Keyboard::group_override_event_name(), Keyboard::tertiary_modifier_short_name(), Keyboard::momentary_push_name(), Keyboard::secondary_modifier_short_name() );
	solo->set_tooltip(tip);

	solo->button_press_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_solo_press));
	solo->button_release_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_solo_release));

	fader = new MixbusFader(this);
	widgets.push_front(fader);

	trim = new MixbusKnob(this);
	widgets.push_front(trim);
	std::string tt[NUM_KNOB_TICKS] = { "|", "-20", "|", "|", "|", "|", "|", "|", "|", "20", "|" };
	trim->set_ticks(tt);
	trim->set_color( "mixbus trim knob" );
	trim->set_tooltip( _("Input Trim") );

	drive_in = new MixbusButton(this);
	widgets.push_front(drive_in);
	drive_in->set_color("mixbus sat org button");
	drive_in->set_text("");
	drive_in->set_tooltip(_("Drive Saturation On/Off"));

	thresh = new MixbusThresh(this);
	widgets.push_front(thresh);
	thresh->set_tooltip(_("Compressor Threshold"));

	gate_thresh = new MixbusThresh(this);
	widgets.push_front(gate_thresh);
	gate_thresh->set_tooltip(_("Gate Threshold"));

	comp_disp = new MixbusModeSelector(this);
	widgets.push_front(comp_disp);
	comp_disp->set_tooltip(_("Comp Mode"));

	//channelstrip meter
	strip_meter = new MixbusMeterBar(this);
	widgets.push_front(strip_meter);

	//channelstrip comp redux meter
	float met = -0.2;
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		comp_redux_meterDots[i] = new MixbusReduxDot(this);
		widgets.push_front(comp_redux_meterDots[i]);
		comp_redux_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		comp_redux_meterDots[i]->set_color( rgba_to_color (1,0,0,1) );  //TODO:  watch the meter color options

		gate_redux_meterDots[i] = new MixbusReduxDot(this);
		widgets.push_front(gate_redux_meterDots[i]);
		gate_redux_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		gate_redux_meterDots[i]->set_color( rgba_to_color (1,0,0,1) );  //TODO:  watch the meter color options

		met -= 2.0;
	}

	comp_redux_meterDots[COMP_THRESH_CNT-2]->set_thresh( dB_to_coefficient(-14) );
	comp_redux_meterDots[COMP_THRESH_CNT-1]->set_thresh( dB_to_coefficient(-24) );

	gate_redux_meterDots[COMP_THRESH_CNT-2]->set_thresh( dB_to_coefficient(-14) );
	gate_redux_meterDots[COMP_THRESH_CNT-1]->set_thresh( dB_to_coefficient(-24) );

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusStripFader::meter));
}

static const float eql = -415 -40;  //saved space

void
MixbusStripFader::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float h = ScreenSizer::user_scale();  //scale factor

	const float toggle_height = h*17.0;
	const float knob_sz = h*22;

	float offs = 146; //32c offset; this allows us to use the same numbers as regular mixbus, in the fader area

//colorbar and slate
	slate->set_size(ArdourCanvas::Rect (0, 0, inWidth, 21*h));
	slate->set_position(ArdourCanvas::Duple (0, 2*h));


//FADER SECTION

	float mute_w = 36*h;

/*	rec_toggle->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*20));
	rec_toggle->set_position(ArdourCanvas::Duple ( 4*h, 490*h));

	input_toggle->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*20));
	input_toggle->set_position(ArdourCanvas::Duple ( 26*h, 490*h));
*/

	mute->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*26));
	mute->set_position(ArdourCanvas::Duple ( 9*h, (484+eql)*h));

	solo->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*26));
	solo->set_position(ArdourCanvas::Duple ( 54*h, (484+eql)*h));

	trim->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	trim->set_position(ArdourCanvas::Duple ( 17*h, h*(347+20+offs+eql)));

	drive_in->set_size(ArdourCanvas::Rect (0, 0, 14*h, 14*h));
	drive_in->set_position(ArdourCanvas::Duple ( 75*h, 63*h));

	fader->set_size(ArdourCanvas::Rect (0, 0, 32*h, h*(218-20)));
	fader->set_position(ArdourCanvas::Duple (15*h, h*(376+18+offs+eql)));

	thresh->set_size(ArdourCanvas::Rect (0, 0, h*20, h*70));
	thresh->set_position(ArdourCanvas::Duple (61*h, h*100));

	gate_thresh->set_size(ArdourCanvas::Rect (0, 0, h*20, h*70));
	gate_thresh->set_position(ArdourCanvas::Duple (61*h, h*100 + 111*h));

	comp_disp->set_size(ArdourCanvas::Rect (0, 0, 45*h, h*17));
	comp_disp->set_position(ArdourCanvas::Duple ( 52*h, h*165));

	//main strip meter
	strip_meter->set_size(ArdourCanvas::Rect (0, 0, 8, 168*h ) );  //width is exactly eight, regardless of strip scale; this makes it easier to calc mono/stereo meters
	strip_meter->set_position(ArdourCanvas::Duple ( floor(51*h), (376+18+eql+17+offs)*h ));

	int stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	int size = floor(stride *0.75);
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		comp_redux_meterDots[THRESH_METER_CNT-i-1]->set_size(ArdourCanvas::Rect (0, 0, floor(Wide ? 9*h : 5*h), size));
		comp_redux_meterDots[THRESH_METER_CNT-i-1]->set_position(ArdourCanvas::Duple ( 71*h, h*116 + stride*i));
	}
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		gate_redux_meterDots[i]->set_size(ArdourCanvas::Rect (0, 0, floor(Wide ? 9*h : 5*h), size));
		gate_redux_meterDots[i]->set_position(ArdourCanvas::Duple ( 71*h, 85*h + 107*h  + stride*i));
	}

	end_visual_change();  //TODO ?
}

void
MixbusStripFader::set_hold_count (int cnt)
{
	hold_cnt = cnt;
	hold_val = hold_val_r = -120; //in dB
}

void
MixbusStripFader::display_slate_text (std::string txt)
{
	slate->set_text(txt);
}

void
MixbusStripFader::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	int mixbus_num = _route->mixbus();
	bool is_mixbus = mixbus_num != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	if (_bg_pattern_type != bg_pattern_fader_small) {
		_bg_pattern_type = is_track ? bg_pattern_fader : (is_master? bg_pattern_fader_master : bg_pattern_fader_bus);
	}

	route_color_changed ();

	slate->show();
	slate->set_text(rt->name());
	slate->set_route(rt);

	trim->set_controllable( rt, rt->trim_control() );
//	trim->set_route( rt, rt->ch_pre(), port_channel_pre_gain );
	trim->show();

	if (is_track) {
		drive_in->set_route( rt, rt->tape_drive_mode_controllable() );
		drive_in->show();
	}

	thresh->set_route( rt, rt->comp_threshold_controllable() );
//	thresh->show();

	gate_thresh->set_route( rt, rt->comp_threshold_controllable() );
//	gate_thresh->show();

	comp_disp->set_route( rt, rt->comp_mode_controllable() );
//	comp_disp->show();

	for (int i = 0 ; i< (THRESH_METER_CNT); i++) {
		comp_redux_meterDots[i]->set_route( rt, rt->comp_redux_controllable() );
		comp_redux_meterDots[i]->show();

		if (is_track) {
			gate_redux_meterDots[i]->set_route( rt, rt->gate_redux_controllable() );
			gate_redux_meterDots[i]->show();
		}
	}

	fader->set_fader_route( rt );
	fader->show();

//---------------------------------

	mute->set_controllable( rt, rt->mute_control() );
	mute->show();

	if ( ! rt->is_master() ) {
		solo->set_controllable( rt, rt->solo_control() );
		solo->show();
	}

	strip_meter->show();

	end_visual_change();
}

void
MixbusStripFader::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scale factor

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	float drad = 5;  //decorator radius

	int mixbus_num = _route->mixbus();
	bool is_mixbus = mixbus_num != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	SVAModifier bord = UIConfiguration::instance().modifier ("strip border alpha");
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double color_r,color_g,color_b;
	Gtkmm2ext::color_to_rgba( puddle_color, color_r, color_g, color_b, unused);

	//fader puddle
	if (true) {
		cr->set_source_rgba (color_r, color_g, color_b, mod.a());
		Gtkmm2ext::rounded_rectangle (cr, 0*h, 0*h, width, height, boxy ? 0 : (8)*h);
		cr->fill ();
	}

	Gtkmm2ext::color_to_rgba( bg_color, color_r, color_g, color_b, unused); //bg color

	//comp puddle
	if (true) {
		cr->set_source_rgba (color_r, color_g, color_b, mod.a());
		Gtkmm2ext::rounded_rectangle (cr, midp+(Wide ? 15*h:6*h), 59*h,   64*h, 217*h, boxy ? 0 : (5)*h);
		cr->fill ();
	}

	//Mute Solo Input Rec decorator
	if ( false ) {
		cr->set_identity_matrix();
		cr->move_to( 4*h, (487+eql)*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to(width-8*h-2*drad*h, 0);  //top right corner
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 6*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -(width-8*h-2*drad*h), 0 );  //btm left corner
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -6*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
	}
	cr->set_identity_matrix();

	//Trim decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Trim" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(27*h - (tw/2.)), floor((532+eql)*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Drive decorator
	if (is_track) {
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Drive" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(69*h), floor((532+eql)*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Comp decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Comp" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(80*h - (tw/2.)), floor(157*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b, 0.4);
		layout->show_in_cairo_context (cr);
	}

	//Gate decorator
	if (is_track) {
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Gate" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(80*h - (tw/2.)), floor(175*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b, 0.4);
		layout->show_in_cairo_context (cr);
	}

	//Comp Redux ticks
	{
		cr->set_source_rgba (fg_r,fg_g,fg_b,0.8);
		cr->set_identity_matrix();

		cr->move_to( floor(83*h), floor(110*h) );
		layout->set_text( "24" );
		layout->show_in_cairo_context (cr);

		layout->set_text( "8" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "4" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		cr->begin_new_path();
	}

	//Gate Redux ticks
	if (is_track) {
		cr->set_source_rgba (fg_r,fg_g,fg_b,0.8);
		cr->set_identity_matrix();

		cr->move_to( floor(83*h), floor(194*h) );
		layout->set_text( "4" );
		layout->show_in_cairo_context (cr);

		layout->set_text( "8" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "24" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		cr->begin_new_path();
	}

	//Fader decorator
	if (false) {
		cr->set_identity_matrix();
		cr->move_to( 4*h, (530+eql)*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to( 40 - 2*drad*h, 0);  //top right corner
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 235*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -(40-2*drad*h), 0 );  //btm left corner
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -235*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 6*h, height );
		cr->fill ();
	}
	
	//drop-shadow at top
	if (!flat) {
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 8);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 7 );
		cr->fill ();
	}

	//bug
	if (false){
		cr->set_identity_matrix();
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_logo_texture->gobj(), 74*h, 260*h);
		cr->rectangle (0, 0, 16*h, 16*h);
		cr->paint_with_alpha ( 0.65 );
		cr->begin_new_path();
	}

	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, width/2., height);  //IMPORTANT:  the gradient operates on the whole context
		btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
		btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.20);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

//	if (is_master) {
//		cr->set_identity_matrix();
//		layout->set_text( "MASTER" );
//		int tw, th; layout->get_pixel_size (tw, th);
//		cr->translate( width/2 - tw/2, 466*h - th/2. );
//		cr->set_source_rgba (1,1,1,1);
//		layout->show_in_cairo_context (cr);
//	}

	cr->set_identity_matrix();

	//section bounds
	if (true) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		Gtkmm2ext::rounded_rectangle (cr, 1*h, 25*h, width-2*h, height-27*h, 5*h);
		cr->stroke ();
	}

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//special case for VU meter widgets;  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
MixbusStripFader::meter()
{
	if (!_route)
		return;

	//we only want to show -audio- meters on the mixer page. for now.
	int offset = _route->shared_peak_meter()->input_streams().n_midi();

	//more than mono?
	bool st = _route->shared_peak_meter()->input_streams().n_audio() > 1;

	//todo:  different meter modes?
	float met = _route->peak_meter()->meter_level (offset, ARDOUR::MeterPeak);
	float met_r = st ? _route->peak_meter()->meter_level (offset+1, ARDOUR::MeterPeak) : 0.0;

	//calculate peak-hold decay
	hold_cnt--;
	uint32_t hold_target = (uint32_t) floor(UIConfiguration::instance().get_meter_hold());

	//LEFT peak grab
	if (met > hold_val) {
		hold_val = met;
	}

	//RIGHT peak grab
	if (met_r > hold_val_r) {
		hold_val_r = met_r;
	}

	//reset L&R peak hold after timeout
	if ( hold_cnt < 1 ) {
		hold_cnt = hold_target;
		hold_val = met;
		hold_val_r = met_r;
	}

	bool inp = ( _route->is_track() && (_route->meter_point()== ARDOUR::MeterInput) );

	strip_meter->meter(met, met_r, hold_val, hold_val_r, st, inp);

	//redux meter
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		comp_redux_meterDots[i]->meter();
		gate_redux_meterDots[i]->meter();
	}

}

void
MixbusStripFader::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name))
		if (_route)
			slate->set_text(_route->name());

	if (what_changed.contains (ARDOUR::Properties::color))
		route_color_changed();
}

void
MixbusStripFader::route_color_changed ()
{
	Gdk::Color gc = RouteUI::route_color ();
	Gtkmm2ext::Color c = gdk_color_to_rgba (gc);
	slate->set_color (c);
	fader->set_route_color (c);
}

bool
MixbusStripFader::mb_solo_press (GdkEventButton* ev)
{
	RouteUI::solo_press (ev);
	// don't continue with MixbusToggle::event_handler()
	return true;
}

bool
MixbusStripFader::mb_solo_release (GdkEventButton* ev)
{
	RouteUI::solo_release (ev);
	// don't call MixbusToggle::event_handler() pseudo-Latch and group-override
	return true;
}

bool
MixbusStripFader::mb_mute_press (GdkEventButton* ev)
{
	RouteUI::mute_press (ev);
	return true;
}

bool
MixbusStripFader::mb_mute_release (GdkEventButton* ev)
{
	RouteUI::mute_release (ev);
	return true;
}

/* ------------------------------------------------ */

MixbusStripFader_Small::MixbusStripFader_Small (ArdourCanvas::Item *p)
	: MixbusStripFader (p)
{
	_bg_pattern_type = bg_pattern_fader_small;

	//replace fader tickmarks with fewer tickmarks
	KnobTicks tickmarks;
	tickmarks.ticks.push_back(KnobTickMark(6, "6"));
	tickmarks.ticks.push_back(KnobTickMark(0, "0"));
	tickmarks.ticks.push_back(KnobTickMark(-6, "-6"));
	tickmarks.ticks.push_back(KnobTickMark(-12, "12"));
	tickmarks.ticks.push_back(KnobTickMark(-20, "20"));
	tickmarks.ticks.push_back(KnobTickMark(-40, "40"));
	tickmarks.ticks.push_back(KnobTickMark(-200, "90"));
	fader->set_tickmarks(tickmarks);
}

void
MixbusStripFader_Small::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float h = ScreenSizer::user_scale();  //scale factor

	const float toggle_height = h*17.0;
	const float knob_sz = h*21;  //shorty

	float offs = 146; //32c offset; this allows us to use the same numbers as regular mixbus, in the fader area

//colorbar and slate
	slate->set_size(ArdourCanvas::Rect (0, 0, inWidth, 21*h));
	slate->set_position(ArdourCanvas::Duple (0, (455+eql)*h));


//FADER SECTION

	float mute_w = 36*h;
	float shorty = 100;

/*	rec_toggle->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*20));
	rec_toggle->set_position(ArdourCanvas::Duple ( 4*h, 490*h));

	input_toggle->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*20));
	input_toggle->set_position(ArdourCanvas::Duple ( 26*h, 490*h));
*/
	mute->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*25));
	mute->set_position(ArdourCanvas::Duple ( 9*h, (480+eql)*h));

	solo->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*25));
	solo->set_position(ArdourCanvas::Duple ( 55*h, (480+eql)*h));

	trim->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	trim->set_position(ArdourCanvas::Duple ( 16*h, h*(347+18+offs+eql)));

	drive_in->set_size(ArdourCanvas::Rect (0, 0, 12*h, 12*h));
	drive_in->set_position(ArdourCanvas::Duple ( 45*h, 52*h));

	fader->set_size(ArdourCanvas::Rect (0, 0, 28*h, h*(106)));
	fader->set_position(ArdourCanvas::Duple (15*h, h*(390+offs+eql)));

	thresh->set_size(ArdourCanvas::Rect (0, 0, h*20, h*(80)));
	thresh->set_position(ArdourCanvas::Duple (61*h, h*(400+eql+offs)));

//	comp_disp->set_size(ArdourCanvas::Rect (0, 0, 45*h, h*17));
//	comp_disp->set_position(ArdourCanvas::Duple ( 52*h, h*(599+offs+eql-20-shorty)));
	comp_disp->hide();

	//main strip meter
	strip_meter->set_size(ArdourCanvas::Rect (0, 0, 8, h*(92) ) );  //width is exactly eight, regardless of strip scale; this makes it easier to calc mono/stereo meters
	strip_meter->set_position(ArdourCanvas::Duple ( floor(47*h), h*(396+eql+offs) ));


	int stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	int size = floor(stride *0.75);
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		comp_redux_meterDots[THRESH_METER_CNT-i-1]->set_size(ArdourCanvas::Rect (0, 0, floor(Wide ? 9*h : 5*h), size));
		comp_redux_meterDots[THRESH_METER_CNT-i-1]->set_position(ArdourCanvas::Duple ( 71*h, h*64 + stride*i));
	}
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		gate_redux_meterDots[i]->set_size(ArdourCanvas::Rect (0, 0, floor(Wide ? 9*h : 5*h), size));
		gate_redux_meterDots[i]->set_position(ArdourCanvas::Duple ( 71*h, 119*h + 17*h  + stride*i));
	}

	end_visual_change();  //TODO ?
}


void
MixbusStripFader_Small::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scale factor

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	float drad = 5;  //decorator radius

	//Comp color
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t generic_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double color_r,color_g,color_b;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);
	cr->set_source_rgba (color_r, color_g, color_b, mod.a());
	Gtkmm2ext::rounded_rectangle (cr, midp+(Wide ? 8*h:6*h), 55*h, 79*h, 128*h, boxy ? 0 : (5)*h);
	cr->fill ();

	//slate decorator
	if (false){
		cr->set_identity_matrix();
		Gtkmm2ext::rounded_rectangle (cr, 4*h, 40*h, width-8*h, 20*h, boxy ? 0 : 5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->stroke ();
	}

	cr->set_identity_matrix();

	//Trim decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Trim" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(27*h - (tw/2.)), floor((530+eql)*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Comp decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Comp" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(80*h - (tw/2.)), floor(105*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b, 0.4);
		layout->show_in_cairo_context (cr);
	}

	//Gate decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Gate" );
		layout->get_pixel_size (tw, th);
		cr->translate( floor(80*h - (tw/2.)), floor(119*h) );
		cr->set_source_rgba (fg_r,fg_g,fg_b, 0.4);
		layout->show_in_cairo_context (cr);
	}

	//Comp Redux ticks
	{
		cr->set_source_rgba (fg_r,fg_g,fg_b,0.8);
		cr->set_identity_matrix();

		cr->move_to( floor(83*h), floor(58*h) );
		layout->set_text( "12" );
		layout->show_in_cairo_context (cr);

		layout->set_text( "8" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "4" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		cr->begin_new_path();
	}

	//Gate Redux ticks
	{
		cr->set_source_rgba (fg_r,fg_g,fg_b,0.8);
		cr->set_identity_matrix();

		cr->move_to( floor(83*h), floor(138*h) );
		layout->set_text( "4" );
		layout->show_in_cairo_context (cr);

		layout->set_text( "8" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		layout->set_text( "12" );
		cr->rel_move_to( 0, 13*h );
		layout->show_in_cairo_context (cr);

		cr->begin_new_path();
	}

	//Fader decorator
	if (false) {
		cr->set_identity_matrix();
		cr->move_to( 4*h, (530+eql)*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to( 40 - 2*drad*h, 0);  //top right corner
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 235*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -(40-2*drad*h), 0 );  //btm left corner
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -235*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 8, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 8, height );
		cr->fill ();
	}
	
	//drop-shadow at top
	if (false) {
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 8);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 7 );
		cr->fill ();
	}
	
	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
		btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
		btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.15);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//special case for VU meter widgets;  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}
