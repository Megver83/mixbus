/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>
#include <cairomm/pattern.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_strips.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

//an MBCanvas for widgets
MixbusStripCanvas::MixbusStripCanvas ()
{
	_knobs = NULL;
	
	//our rendering speed suffers if we re-render knobs simply because they are in-between 2 meters that got invalidated (for example)
	set_single_exposure(false);
#ifdef __APPLE__
	use_intermediate_surface (false);
#endif
}

void
MixbusStripCanvas::on_size_request (Gtk::Requisition* req)
{
	float fscale = ScreenSizer::user_scale();

	req->width = natural_width()*fscale;
	req->height = natural_height()*fscale;
}


void
MixbusStripCanvas::on_size_allocate (Gtk::Allocation& alloc)
{
	float fscale = ScreenSizer::user_scale();

	_knobs->scale_strip_to(alloc.get_width(), alloc.get_height(), alloc.get_width() > natural_width()*fscale*0.9);

	//we must re-render our bg_patterns if the size of the bg needs to change
	_knobs->render_bg_pattern(alloc.get_width(), alloc.get_height(), alloc.get_width() > natural_width()*fscale*0.9);

	GtkCanvas::on_size_allocate(alloc);
}

void
MixbusStripCanvas::set_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	float fscale = ScreenSizer::user_scale();

	_knobs->set_strip_route (rt, mixer_owned);

	//we must re-render our bg_patterns if our route changes (editor-mixer-strip)
	_knobs->render_bg_pattern(width(), height(), width() > natural_width()*fscale*0.9);
}

//-------------------------

bool               strip_patterns_initialized = false;
cairo_pattern_t*   MixbusStrip::_patterns[NUM_BG_PATTERN_TYPES];
int                MixbusStrip::_pattern_widths[NUM_BG_PATTERN_TYPES];
void
MixbusStrip::initialize_mixbus_strip_patterns()
{
	if (!strip_patterns_initialized) {
		for (int i = 0; i < NUM_BG_PATTERN_TYPES; i++) {
			_patterns[(pattern_bg_types)i] = NULL;
			_pattern_widths[(pattern_bg_types)i] = 0;
		}
		strip_patterns_initialized = true;
	}
}

MixbusStrip::~MixbusStrip ()
{
	_route_connections.drop_connections();

//	cairo_pattern_destroy (_patterns[bg_pattern_fader]);  //TODO: how will we know when to clean these up?  maybe don't care
//	_patterns[bg_pattern_fader] = NULL;
}

MixbusStrip::MixbusStrip (ArdourCanvas::Item *p)
	: SessionHandlePtr ((ARDOUR::Session*)0)
	, RouteUI ((ARDOUR::Session*)0)
	, Item (p)
{
	initialize_mixbus_strip_patterns();
	_bg_pattern_type = bg_pattern_empty;

	set_tooltip("");

	UIConfiguration::instance().ColorsChanged.connect (sigc::mem_fun (*this, &MixbusStrip::color_handler));  //ToDo:: what happens if I'm not around?
	color_handler();
}

void
MixbusStrip::color_handler ()
{
	//theme changed; redraw me
	_patterns[_bg_pattern_type] = NULL;
	end_change();  //trigger a resize so my bg_pattern is rendered
}

void
MixbusStrip::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	set_session(&rt->session());
	RouteUI::init ();

	if (!mixer_owned) {
		/* the editor mixer strip: don't destroy it every time
		 * the underlying route goes away.
		 * RouteUI::init (); sets this to true by default,
		 * RouteUI::set_route (); will connect the signal.
		 *
		 * TODO check if it's valid for the route-ui to self-destruct
		 * (the canvas will delete the strip)
		 */
		self_destruct = false;
	}

	RouteUI::set_route (rt);
	_route_connections.drop_connections();

	route_color_changed ();

	//disconnect all, so mixbus knob don't continnue to invisbly control input stuff, etc ?
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		(*i)->ClearConnections();
	}

	//hide-all
	for (list<Item*>::iterator i = _items.begin(); i != _items.end(); i++ ) {
		(*i)->hide();
	}
}

void
MixbusStrip::render_bg_pattern (int inWidth, int inHeight, bool Wide)
{
	if (inWidth < 4 || inHeight < 4) {
		return;  //until we are allocated a reasonable size, don't render a bg pattern
	}

	cairo_pattern_t *pattern = _patterns[_bg_pattern_type];
	if (pattern && (_pattern_widths[_bg_pattern_type]==inWidth)) {
		return;  //a pattern already exists of the correct type and size. no need to render one
	}

	if (!_route && _bg_pattern_type!= bg_pattern_vca) {
		return;  //if we haven't yet been assigned a route, then we won't know what bg pattern to render
	}

	//destroy our old pattern and make a new one
	cairo_pattern_destroy (_patterns[_bg_pattern_type]);
	cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, inWidth, inHeight);
	cairo_t* ct = cairo_create (surface);

	//stash the pattern in a local static variable;  we only need ONE bg pattern of each type, for each scale
	_patterns[_bg_pattern_type] = cairo_pattern_create_for_surface (surface);
	_pattern_widths[_bg_pattern_type] = inWidth;

	//create a context for the pattern, and let widgets render their background to the pattern (i.e. backing store)
	Cairo::RefPtr<Cairo::Context> cc( new Cairo::Context (ct) );
	render_bg (cc, Wide);
	cairo_destroy (ct);
	cairo_surface_destroy (surface);

}

void
MixbusStrip::render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context> cr) const
{
	ArdourCanvas::Rect self (_rect);
	boost::optional<ArdourCanvas::Rect> i = self.intersection (area);
	if (!i) {
		return;
	}
	ArdourCanvas::Rect overlap (i.get());

	cr->set_identity_matrix();

	//blit a portion of our bg_pattern behind the widget being updated
	cairo_pattern_t *pattern = _patterns[_bg_pattern_type];
	if (pattern) {
		//just blit the portion we need
		Cairo::RefPtr<Cairo::Pattern> p (new Cairo::Pattern (pattern, false));
		cr->set_source (p);
		cr->rectangle (overlap.x0, overlap.y0, overlap.width(), overlap.height());
		cr->fill ();
	}

	Item::render_children(area, cr);  //now render any children in the needed area
}

void
MixbusStrip::set_slate_text (std::string txt)
{
	transmit_rui_message( RouteUI::SetSlateText, txt);
}

void
MixbusStrip::set_size (ArdourCanvas::Rect const & area)
{
	if (_rect != area) {
		begin_change ();
		_rect = area;
		set_bbox_dirty ();
		end_change ();
	}
}

void
MixbusStrip::compute_bounding_box () const
{
	if (!_rect.empty()) {
		_bounding_box = _rect;
	}

	set_bbox_clean ();
}

std::string
MixbusStrip::state_id() const
{
	if (_route) {
		return string_compose ("mixbus %1", _route->id().to_s());
	} else {
		return string ();
	}
}
