/*
 * Copyright (C) 2016 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include "ardour/route.h"
#include "ardour/phase_control.h"

#include "gtkmm2ext/gui_thread.h"

#include "widgets/tooltips.h"

#include "polmax_ui.h"
#include "pbd/i18n.h"

using namespace Gtk;
using namespace ARDOUR;
using namespace ArdourWidgets;

static const char* labels[POLMAX_OPTIONS] = {
	_("N/N"),
	_("Peak"),
	_("RMS"),
	_("L:N/N"),
	_("L:Peak"),
	_("L:RMS")
};

static const char* tooltips[POLMAX_OPTIONS] = {
	_("Not normalized"),
	_("Peak normalized"),
	_("RMS normalized"),
	_("Low emphasis, not normalized"),
	_("Low emphasis, peak normalized"),
	_("Low emphasis, RMS normalized")
};

PolMaxResult::PolMaxResult (PolarityMaximizer::SrcList src, std::vector<bool> pol[POLMAX_OPTIONS], const float gain[POLMAX_OPTIONS])
	: ArdourWindow (_("Polarity Optimization Results"))
	, _srclist (src)
	, _use_old (_("Old"), ArdourButton::default_elements)
{
	for (uint32_t o = 0; o < POLMAX_OPTIONS; ++o) {
		_opt[o] = pol [o];
		_gain[o] = gain [o];
		_use_new[o] = manage (new ArdourButton (labels[o], ArdourButton::default_elements));
		set_tooltip (*_use_new[o], tooltips[o]);

	}

	for (PolarityMaximizer::SrcList::const_iterator i = _srclist.begin (); i != _srclist.end(); ++i) {
		i->route->DropReferences.connect (
				_route_connections, invalidator (*this), boost::bind (&PolMaxResult::route_going_away, this), gui_context()
				);
	}

	VBox *vbox = manage (new VBox ());
	add (*vbox);
	vbox->show ();


	pol_model = ListStore::create (pol_columns);
	pol_sort = TreeModelSort::create (pol_model);

	pol_display.set_model (pol_sort);
	pol_display.append_column (_("Name"), pol_columns.name);
	pol_display.append_column (_("Old"),  pol_columns.p_old);
	pol_display.append_column (labels[0], pol_columns.p_n0);
	pol_display.append_column (labels[1], pol_columns.p_n1);
	pol_display.append_column (labels[2], pol_columns.p_n2);
	pol_display.append_column (labels[3], pol_columns.p_n3);
	pol_display.append_column (labels[4], pol_columns.p_n4);
	pol_display.append_column (labels[5], pol_columns.p_n5);
	pol_display.get_column (0)->set_expand(true);
	pol_display.get_column (1)->set_expand(false);
	pol_display.get_selection()->set_mode (Gtk::SELECTION_SINGLE);
	pol_display.set_reorderable (false);
	pol_display.set_headers_visible (true);
	pol_display.set_headers_clickable (true);
	pol_display.set_can_focus(false);

	pol_sort->set_sort_column (pol_columns.order, SORT_ASCENDING);

	uint32_t chn = 0;
	bool match = true;
	for (PolarityMaximizer::SrcList::const_iterator i = _srclist.begin (); i != _srclist.end(); ++i) {
		const uint32_t nc = i->route->n_inputs().n_audio();
		for (uint32_t channel = 0; channel < nc; ++channel) {
			bool piv = i->route->phase_control()->inverted (channel);
			_orig.push_back (piv);

			TreeModel::Row row = *(pol_model->append());
			if (nc > 1) {
				row[pol_columns.name] = string_compose (_("%1 chn %2"), i->route->name (), channel + 1);
			} else {
				row[pol_columns.name] = i->route->name ();
			}
			row[pol_columns.order] = i->route->presentation_info().order();
			row[pol_columns.chn]   = chn;
			row[pol_columns.p_old] = (piv ? "\u00d8" : "+");
			row[pol_columns.p_n0]  = (_opt[0][chn] ? "\u00d8" : "+");
			row[pol_columns.p_n1]  = (_opt[1][chn] ? "\u00d8" : "+");
			row[pol_columns.p_n2]  = (_opt[2][chn] ? "\u00d8" : "+");
			row[pol_columns.p_n3]  = (_opt[3][chn] ? "\u00d8" : "+");
			row[pol_columns.p_n4]  = (_opt[4][chn] ? "\u00d8" : "+");
			row[pol_columns.p_n5]  = (_opt[5][chn] ? "\u00d8" : "+");

			for (uint32_t o = 0; o < POLMAX_OPTIONS; ++o) {
				if (piv != _opt[o][chn]) { match = false; }
			}
			++chn;
		}
	}

	Box* box = manage (new HBox ());
	box->set_border_width (6);
	box->set_spacing (6);

	if (match) {
		box->pack_start (*manage (new Label (_("Polarities are already optimal."))), true, true);
		vbox->pack_start (*box, false, false);
	} else {
		TreeModel::Row row = *(pol_model->append());
		row[pol_columns.order] = INT32_MAX;
		row[pol_columns.chn] = UINT32_MAX;
		row[pol_columns.name] = _("-- approx. gain (RMS):");

		pol_display.get_column (1)->set_widget (_use_old);
		pol_display.get_column (1)->set_alignment (.5);
		pol_display.get_column (1)->set_expand(false);
		pol_display.get_column (1)->get_first_cell_renderer ()->set_alignment (0.5, 0.5);
		pol_display.get_column (1)->signal_clicked().connect
			(sigc::bind (sigc::mem_fun (*this, &PolMaxResult::switch_polarities), -1));
		_use_old.show ();

		row[pol_columns.p_old] = string_compose (_("%1%2%3 dB"), std::setprecision (2), std::fixed, _gain[POLMAX_OPTIONS]);

		for (uint32_t o = 0; o < POLMAX_OPTIONS; ++o) {
			TreeModelColumn<std::string>* mcol = column (o);
			assert (mcol);
			row[*mcol] = string_compose (_("%1%2%3 dB"), std::setprecision (2), std::fixed, _gain[o]);
			TreeViewColumn* col = pol_display.get_column (o + 2);
			col->set_widget (*_use_new[o]);
			col->set_alignment (.5);
			col->set_expand(false);
			col->get_first_cell_renderer ()->set_alignment (0.5, 0.5);
			col->signal_clicked().connect (sigc::bind (sigc::mem_fun (*this, &PolMaxResult::switch_polarities), o));
			_use_new[o]->show ();
		}

		switch_polarities (0);
		pol_display.get_selection()->signal_changed().connect (sigc::mem_fun (*this, &PolMaxResult::flip_selected));
	}

	vbox->pack_start (pol_display, false, false);
	show_all ();
}

void
PolMaxResult::switch_polarities (int use)
{
	_active_option = use;
	const std::vector<bool>& pol = use < 0 ? _orig : _opt[use];
	_use_old.set_active (use < 0);
	for (int o = 0; o < POLMAX_OPTIONS; ++o) {
		_use_new[o]->set_active (use == o);
	}

	uint32_t chn = 0;
	for (PolarityMaximizer::SrcList::const_iterator i = _srclist.begin (); i != _srclist.end(); ++i) {
		const uint32_t nc = i->route->n_inputs().n_audio();
		for (uint32_t channel = 0; channel < nc; ++channel) {
			i->route->phase_control ()->set_phase_invert (channel, pol[chn]);
			++chn;
		}
	}
}

bool
PolMaxResult::on_delete_event (GdkEventAny*)
{
	delete this;
	return true;
}

void
PolMaxResult::route_going_away ()
{
	_srclist.clear ();
	delete this;
}

void
PolMaxResult::set_session (ARDOUR::Session *s)
{
	if (!s) {
		_srclist.clear ();
		delete this;
	}
}

void
PolMaxResult::flip_selected ()
{
	TreeModel::Row row = *(pol_display.get_selection()->get_selected());
	if (!row) {
		return;
	}
	bool need_update = false;
	uint32_t chn = row[pol_columns.chn];
	if (chn == UINT32_MAX) {
		return;
	}
	for (int o = 0; o < POLMAX_OPTIONS; ++o) {
		if (!_opt[o][chn]) {
			// not inverted
			continue;
		}
		// currently inverted -> flip all
		if (o == _active_option) {
			need_update = true;
		}
		for (std::vector<bool>::iterator i = _opt[o].begin (); i != _opt[o].end(); ++i) {
			(*i) = !(*i);
		}
		// update display
		TreeModelColumn<std::string>* col = column (o);
		assert (col);

		typedef Gtk::TreeModel::Children type_children;
		type_children children = pol_model->children();
		for(type_children::iterator iter = children.begin(); iter != children.end(); ++iter) {
			Gtk::TreeModel::Row r = *iter;
			uint32_t c = r[pol_columns.chn];
			if (c == UINT32_MAX) {
				continue;
			}
			r[*col]  = (_opt[o][c] ? "\u00d8" : "+");
		}
	}

	if (need_update) {
		switch_polarities (_active_option);
	}
}
