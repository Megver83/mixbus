/*
    Copyright (C) 2016 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include <pbd/compose.h>

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/phase_control.h"
#include "ardour/route_group.h"
#include "ardour/solo_isolate_control.h"
#include "ardour/utils.h"

#include <canvas/canvas.h>
#include <canvas/debug.h>
#include <canvas/utils.h>

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_strips.h"

#include "mixbus_eq_switcher.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"
#include "pbd/stacktrace.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;


static const float TWO_PI = (M_PI*2.0);


MixbusEQSwitcherCanvas::MixbusEQSwitcherCanvas (RouteUI::RUIMessage m)
{
	_knobs = new MixbusEQSwitcher(root(),m);
}


//-------------------------


MixbusEQSwitcher::~MixbusEQSwitcher ()
{
}

MixbusEQSwitcher::MixbusEQSwitcher (ArdourCanvas::Item *p, RouteUI::RUIMessage m)
	: MixbusSwitcher (p, m)
{
	_text = "32C EQ";

	set_tooltip("Click to show EQ controls.");

	_vis_elem = Mixer_UI::strip_elem_eq;
}
