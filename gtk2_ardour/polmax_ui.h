/*
 * Copyright (C) 2016 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __gtkardour_polmax_ui_h__
#define __gtkardour_polmax_ui_h__

#include <gtkmm/liststore.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/treemodelsort.h>
#include <gtkmm/treestore.h>
#include <gtkmm/treeview.h>

#include "ardour/polarity_maximizer.h"

#include "widgets/ardour_dropdown.h"
#include "widgets/ardour_button.h"

#include "ardour_dialog.h"
#include "ardour_window.h"

#define POLMAX_OPTIONS 6

class PolMaxResult : public ArdourWindow
{
public:
	PolMaxResult (ARDOUR::PolarityMaximizer::SrcList, std::vector<bool>[POLMAX_OPTIONS], const float [POLMAX_OPTIONS + 1]);
	void set_session (ARDOUR::Session *);

protected:
	bool on_delete_event (GdkEventAny*);

private:
	void flip_selected ();
	void switch_polarities (int);

	ARDOUR::PolarityMaximizer::SrcList _srclist;
	std::vector<bool> _orig;
	std::vector<bool> _opt[POLMAX_OPTIONS];
	float             _gain[POLMAX_OPTIONS + 1];

	ArdourWidgets::ArdourButton _use_old;
	ArdourWidgets::ArdourButton *_use_new[POLMAX_OPTIONS];

	void route_going_away ();
	PBD::ScopedConnectionList _route_connections;

	int _active_option;

	struct PolarityModelColumns : public Gtk::TreeModel::ColumnRecord {
	    PolarityModelColumns () {
		    add (order);
		    add (chn);
		    add (name);
		    add (p_old);
		    add (p_n0);
		    add (p_n1);
		    add (p_n2);
		    add (p_n3);
		    add (p_n4);
		    add (p_n5);
	    }
	    Gtk::TreeModelColumn<int> order;
	    Gtk::TreeModelColumn<uint32_t> chn;
	    Gtk::TreeModelColumn<std::string> name;
	    Gtk::TreeModelColumn<std::string> p_old;
	    Gtk::TreeModelColumn<std::string> p_n0;
	    Gtk::TreeModelColumn<std::string> p_n1;
	    Gtk::TreeModelColumn<std::string> p_n2;
	    Gtk::TreeModelColumn<std::string> p_n3;
	    Gtk::TreeModelColumn<std::string> p_n4;
	    Gtk::TreeModelColumn<std::string> p_n5;
	};

	PolarityModelColumns pol_columns;
	Gtk::TreeView pol_display;
	Glib::RefPtr<Gtk::ListStore> pol_model;
	Glib::RefPtr<Gtk::TreeModelSort> pol_sort;

	Gtk::TreeModelColumn<std::string>* column (uint32_t opt) {
		switch (opt) {
			case 0: return &pol_columns.p_n0;
			case 1: return &pol_columns.p_n1;
			case 2: return &pol_columns.p_n2;
			case 3: return &pol_columns.p_n3;
			case 4: return &pol_columns.p_n4;
			case 5: return &pol_columns.p_n5;
		}
		return 0;
	}
};

#endif

