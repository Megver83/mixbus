/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_eq.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------



//an MBCanvas for input knobs
MixbusEQCanvas::MixbusEQCanvas ()
{
	_knobs = new MixbusEQ(root());
}

//-------------------------


MixbusEQ::~MixbusEQ ()
{
}

MixbusEQ::MixbusEQ (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	hp_freq = new MixbusKnob(this);
	widgets.push_front(hp_freq);
	hp_freq->set_color("mixbus hp filter freq knob");
//	hp_freq->set_line_color( 0.1, 0.1, 0.1);
	std::string ft[7] = { ".02", "|", "|", "|", "|", "|", "1k" };
	hp_freq->set_ticks(ft);

	std::string gt[7] = { "-15", "|", "|", "|", "|", "|", "15" };

	eq_low_gain = new MixbusKnob(this);
	eq_low_gain->set_color("mixbus eq low gain knob");
	widgets.push_front(eq_low_gain);
	eq_low_gain->set_ticks(gt);

	eq_low_freq = new MixbusKnob(this);
	eq_low_freq->set_color("mixbus eq low freq knob");
	widgets.push_front(eq_low_freq);
	std::string lft[7] = { ".04", "|", "|", "|", "|", "|", ".4k" };
	eq_low_freq->set_ticks(lft);

	eq_mid_freq = new MixbusKnob(this);
	widgets.push_front(eq_mid_freq);
	eq_mid_freq->set_color("mixbus eq mid freq knob");
	std::string mft[7] = { "0.2", "|", "|", "|", "|", "|", "4k" };
	eq_mid_freq->set_ticks(mft);

	eq_mid_gain = new MixbusKnob(this);
	widgets.push_front(eq_mid_gain);
	eq_mid_gain->set_color("mixbus eq mid gain knob");
	eq_mid_gain->set_ticks(gt);

	eq_hi_gain = new MixbusKnob(this);
	eq_hi_gain->set_color("mixbus eq hi gain knob");
	widgets.push_front(eq_hi_gain);
	eq_hi_gain->set_ticks(gt);

	eq_hi_freq = new MixbusKnob(this);
	eq_hi_freq->set_color("mixbus eq hi freq knob");
	widgets.push_front(eq_hi_freq);
	std::string hft[7] = { "0.8", "|", "|", "|", "|", "|", "15" };
	eq_hi_freq->set_ticks(hft);

	eq_in = new MixbusToggle(this);
	widgets.push_front(eq_in);
	eq_in->set_text("In");
//	eq_in->set_color( 0.25, 0.9, 0.2 );

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusEQ::config_changed, this, _1), gui_context());
}

void
MixbusEQ::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scaling factor
	const float toggle_height = h*17.0;
	const float knob_sz = h*22;

// ------------ track EQ knobs


	float l_offs = midp + (Wide ? -42*h : -35*h);
	float m_offs = (midp - knob_sz/2.0) + (Wide ? 0 : 1);
	float r_offs = midp + (Wide ? 22*h : 16*h);

	eq_in->set_size(ArdourCanvas::Rect (0, 0, h*33, toggle_height));
	eq_in->set_position(ArdourCanvas::Duple (l_offs - 4*h, h*4));

	eq_hi_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_hi_gain->set_position(ArdourCanvas::Duple (r_offs, h*8));

	eq_hi_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_hi_freq->set_position(ArdourCanvas::Duple (r_offs, h*36));

	eq_mid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_mid_gain->set_position(ArdourCanvas::Duple (m_offs, h*20));

	eq_mid_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_mid_freq->set_position(ArdourCanvas::Duple (m_offs, h*48));

	eq_low_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_low_gain->set_position(ArdourCanvas::Duple (l_offs, h*32));

	eq_low_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_low_freq->set_position(ArdourCanvas::Duple (l_offs, h*60));

	hp_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	hp_freq->set_position(ArdourCanvas::Duple (r_offs, h*67));

	end_visual_change();
}

void
MixbusEQ::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	//now show and re-connect based on route type ?
	if ( rt->is_master() ) {
	} else if ( rt->mixbus() ) {
	} else {

		eq_in->show();
		eq_in->set_route( rt, rt->eq_enable_controllable() );

		eq_low_gain->show();
		eq_low_gain->set_route( rt, rt->eq_gain_controllable(0) );
//		eq_low_gain->set_slate_ctrls( 0, rt->eq_freq_controllable(0), rt->eq_gain_controllable(0), rt->eq_shape_controllable(0) );

		eq_mid_gain->show();
		eq_mid_gain->set_route( rt, rt->eq_gain_controllable(1) );
//		eq_mid_gain->set_slate_ctrls( 2, rt->eq_freq_controllable(2), rt->eq_gain_controllable(2), std::shared_ptr<ARDOUR::AutomationControl>() );

		eq_hi_gain->show();
		eq_hi_gain->set_route( rt,rt->eq_gain_controllable(2) );
//		eq_hi_gain->set_slate_ctrls( 3, rt->eq_freq_controllable(3), rt->eq_gain_controllable(3), rt->eq_shape_controllable(3) );

		eq_low_freq->show();
		eq_low_freq->set_route( rt, rt->eq_freq_controllable(0) );
//		eq_low_freq->set_slate_ctrls( 0, rt->eq_freq_controllable(0), rt->eq_gain_controllable(0), rt->eq_shape_controllable(0) );

		eq_mid_freq->show();
		eq_mid_freq->set_route( rt, rt->eq_freq_controllable(1) );
//		eq_mid_freq->set_slate_ctrls( 2, rt->eq_freq_controllable(2), rt->eq_gain_controllable(2), std::shared_ptr<ARDOUR::AutomationControl>() );

		eq_hi_freq->show();
		eq_hi_freq->set_route( rt, rt->eq_freq_controllable(2) );
//		eq_hi_freq->set_slate_ctrls( 3, rt->eq_freq_controllable(3), rt->eq_gain_controllable(3), rt->eq_shape_controllable(3) );

		hp_freq->show();
		hp_freq->set_route (rt, rt->filter_freq_controllable (true));
	}

	end_visual_change();
}

void
MixbusEQ::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();  float halfw = width/2.0;

	Cairo::Matrix m;

	bool is_mixbus = _route->mixbus() != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scaling factor

	int tw,  th;  //text width and height

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	//mid eq outline
	if (false && is_track) {
		cr->set_source_rgba (0,0,0, 0.2);
		if (Wide)
			Gtkmm2ext::rounded_rectangle (cr, midp - 15*h, 17*h, 30*h, 61*h, boxy ? 0 : 5*h);
		else
			Gtkmm2ext::rounded_rectangle (cr, midp - 12*h, 17*h, 25*h, 61*h, boxy ? 0 : 5*h);
	//	cr->set_line_width (0.7);
	//	cr->stroke ();
		cr->fill ();
	}

	float drad = 7;  //decorator radius
	float offs = 146;  //32C offset ( allows widgets in the fader area to share position values with regular mb

	if ( is_track) {

		SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
		uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");
		double puddle_r,puddle_g,puddle_b,unused;
		Gtkmm2ext::color_to_rgba( puddle_color, puddle_r, puddle_g, puddle_b, unused);

		//eq decorator
		{
			cr->move_to( 1*h, 0*h ); //start in the top left
			cr->rel_line_to( width-2*h, 0);
			cr->rel_line_to( 0, 85*h );
			cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
			cr->rel_line_to( -width + drad*2*h, 0 );
			cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );//btm left corner
			cr->rel_line_to( 0, -85*h );
			cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
//			cr->set_line_width (1.0*h);
//			cr->set_source_rgba (1,1,1,0.7);
//			cr->stroke();
			cr->fill();
		}
		cr->set_identity_matrix();


	} else if (false) {

		//TONE decorator
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->move_to( 6*h, 58*h ); //start in the top left
		cr->rotate(M_PI*2*-0.041);
		cr->rel_curve_to( 0, drad*h, drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 83*h, 0*h );
		cr->rel_curve_to( drad*h, 0, drad*h, -drad*h, drad*h, -drad*h );
		cr->stroke ();
	}

	if ( is_mixbus ) {
		cr->set_identity_matrix();
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->set_text( string_compose( "%1", _route->mixbus()));
		layout->get_pixel_size (tw, th); tw*=1.5;
#ifdef MIXBUS32C
		cr->translate( width- 4*h - tw, (149+offs)*h );
#else
		cr->translate( width- 4*h - tw, (154+offs)*h );
#endif
		cr->scale(1.5, 1.5);
		layout->show_in_cairo_context (cr);
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//bug
	if (false && is_track){
		cr->set_identity_matrix();
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_logo_texture->gobj(), 83*h, 4*h);
		cr->rectangle (0, 0, 16*h, 16*h);
		cr->paint_with_alpha ( 0.65 );
		cr->begin_new_path();
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 8, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 8, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false && !flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 7);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 8 );
		cr->fill ();
	}

	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}

	//scratches
	if (!flat) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}

}

void
MixbusEQ::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {}

	//nothing to do here?
}

void
MixbusEQ::config_changed (string p)
{
}
