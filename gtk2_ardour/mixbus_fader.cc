/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_fader.h"

#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------

//an MBCanvas for input knobs
MixbusStripFaderCanvas::MixbusStripFaderCanvas (bool small)
{
	_knobs = new MixbusStripFader(root());
}

void
MixbusStripFaderCanvas::set_hold_count (int cnt)
{
	MixbusStripFader *f = (MixbusStripFader*) _knobs;
	f->set_hold_count (cnt);
}

void
MixbusStripFaderCanvas::display_slate_text (std::string txt)
{
	MixbusStripFader *f = (MixbusStripFader*) _knobs;
	f->display_slate_text (txt);
}

float
MixbusStripFaderCanvas::natural_height ()
{
	float h = NATURAL_FADER_HEIGHT;
	
	return h;
}



//-------------------------


MixbusStripFader::~MixbusStripFader ()
{
	meter_connection.disconnect();
}

MixbusStripFader::MixbusStripFader (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	hold_cnt = 0;
	hold_val = hold_val_r = -120;  //in dB

	slate = new MixbusSlate(this);
	widgets.push_front(slate);  //slate doesn't need this
	slate->set_tooltip("Right-click to rename");

	mute = new MixbusBigToggle(this, false);
	widgets.push_front(mute);
	mute->set_color("mute button");
	mute->set_text( "M" );
	mute->set_tooltip(_("Mute"));

	mute->button_press_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_mute_press));
	mute->button_release_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_mute_release));

	solo = new MixbusBigToggle(this, true);
	widgets.push_front(solo);
	solo->set_color("solo button");
	solo->set_text( "S" );
	solo->set_tooltip( _("Solo") );

	solo->button_press_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_solo_press));
	solo->button_release_event.connect (sigc::mem_fun(*this, &MixbusStripFader::mb_solo_release));

	fader = new MixbusFader(this);
	widgets.push_front(fader);

	trim = new MixbusKnob(this);
	widgets.push_front(trim);
	std::string tt[NUM_KNOB_TICKS] = { "-20", "|", "|", "|", "|", "|", "20" };
	trim->set_ticks(tt);
	trim->set_color( "mixbus trim knob" );
	trim->set_tooltip( _("Input Trim") );

	makeup = new MixbusKnob(this);
	widgets.push_front(makeup);
	std::string mt[NUM_KNOB_TICKS] = { "0", "|", "|", "|", "|", "|", "+" };
	makeup->set_color( "mixbus compressor gain knob");
	makeup->set_ticks(mt);
	makeup->set_tooltip(_("Makeup Gain"));

	speed = new MixbusSpeedKnob(this);
	widgets.push_front(speed);
	std::string st[NUM_KNOB_TICKS] = { "|", "|", "|", "|", "|", "|", "|" };
	speed->set_color( "mixbus compressor speed knob");
	speed->set_ticks(st);
	speed->set_tooltip(_("Compressor Speed/Ratio"));

	thresh = new MixbusThresh(this);
	widgets.push_front(thresh);
	thresh->set_tooltip(_("Compressor Threshold"));

	comp_in = new MixbusToggle(this);
	widgets.push_front(comp_in);
	comp_in->set_color("mixbus compressor button");
	comp_in->set_text("Comp");
	comp_in->set_tooltip(_("Compressor On/Off"));

	speed_leg = new MixbusCompLegend(this);
	widgets.push_front(speed_leg);

	comp_disp = new MixbusModeSelector(this);
	widgets.push_front(comp_disp);
	comp_disp->set_tooltip(_("Comp Mode"));

	//channelstrip meter
	strip_meter = new MixbusMeterBar(this);
	widgets.push_front(strip_meter);

	//channelstrip comp redux meter
	float met = -0.2;
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		redux_meterDots[i] = new MixbusReduxDot(this);
		widgets.push_front(redux_meterDots[i]);
		redux_meterDots[i]->set_thresh( dB_to_coefficient(met) );
		redux_meterDots[i]->set_color( rgba_to_color (1,0,0,1) );  //TODO:  watch the meter color options
		met -= 2.0;
	}

	//this connection is a callback for the meters to poll and redraw rapidly (from the GUI thread)
	meter_connection = Timers::super_rapid_connect (sigc::mem_fun(*this, &MixbusStripFader::meter));

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusStripFader::config_changed, this, _1), gui_context());
}

static const float eql = -415-40;  //saved space

void
MixbusStripFader::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scale factor

	const float toggle_height = h*17.0;
	const float toggle_width =  Wide ? 44.0 : 42;
	const float knob_sz = h*23;

	float offs = 146; //32c offset; this allows us to use the same numbers as regular mixbus, in the fader area
	float side_offs = 13;

//colorbar and slate
	slate->set_size(ArdourCanvas::Rect (0, 0, inWidth, 21*h));
	slate->set_position(ArdourCanvas::Duple (0, (455+eql)*h));


//FADER SECTION

	float mute_w = Wide ? 42 : 28;
	side_offs = Wide ? 14 : 6;

/*	rec_toggle->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*20));
	rec_toggle->set_position(ArdourCanvas::Duple ( 4*h, 490*h));

	input_toggle->set_size(ArdourCanvas::Rect (0, 0, mute_w, h*20));
	input_toggle->set_position(ArdourCanvas::Duple ( 26*h, 490*h));
*/
	mute->set_size(ArdourCanvas::Rect (0, 0, mute_w*h, h*21));
	mute->set_position(ArdourCanvas::Duple ( 9*h, (484+eql)*h));

	solo->set_size(ArdourCanvas::Rect (0, 0, mute_w*h, h*21));
	solo->set_position(ArdourCanvas::Duple ( inWidth - mute_w*h - 9*h, (484+eql)*h));

	trim->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	trim->set_position(ArdourCanvas::Duple ( (side_offs+2)*h, h*(347+18+offs+eql)));

	makeup->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	makeup->set_position(ArdourCanvas::Duple (inWidth - knob_sz*h - (side_offs)*h, h*(377+15+offs+eql)));

	speed->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	speed->set_position(ArdourCanvas::Duple ( inWidth - knob_sz*h - (side_offs)*h, h*(546+18+offs+eql-20)));

	speed_leg->set_size(ArdourCanvas::Rect (0, 0, knob_sz, h*10));
	speed_leg->set_position(ArdourCanvas::Duple (inWidth - knob_sz*h - (side_offs)*h, h*(570+18+offs+eql-20)));

	fader->set_size(ArdourCanvas::Rect (0, 0, 32*h, h*(218-20)));
	fader->set_position(ArdourCanvas::Duple ( side_offs*h, h*(376+18+offs+eql)));

	side_offs = Wide ? 6 : 2;

	comp_in->set_size(ArdourCanvas::Rect (0, 0, toggle_width*h, toggle_height));
	comp_in->set_position(ArdourCanvas::Duple ( inWidth - toggle_width*h - side_offs*h, h*(367+offs+eql)));

	comp_disp->set_size(ArdourCanvas::Rect (0, 0, toggle_width*h, h*17));
	comp_disp->set_position(ArdourCanvas::Duple ( inWidth - toggle_width*h - side_offs*h, h*(599+offs+eql-20)));

	side_offs = Wide ? 15 : 0;

	thresh->set_size(ArdourCanvas::Rect (0, 0, h*20, h*(130-20)));
	thresh->set_position(ArdourCanvas::Duple (side_offs*h+50*h, h*(412+18+offs+eql)));

	side_offs = Wide ? 12 : 2;

	//main strip meter
	strip_meter->set_size(ArdourCanvas::Rect (0, 0, 8, 218*h -35*h -20*h ) );  //width is exactly eight, regardless of strip scale; this makes it easier to calc mono/stereo meters
	strip_meter->set_position(ArdourCanvas::Duple ( floor(side_offs*h+38*h), (376+18+eql+17+offs)*h ));

	int stride = floor(h*7);  // pixels each meter dot, in "normalized" space
	int size = floor(stride *0.75);
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		redux_meterDots[i]->set_size(ArdourCanvas::Rect (0, 0, floor(Wide ? 9*h : 5*h), size));
		redux_meterDots[i]->set_position(ArdourCanvas::Duple ( floor(inWidth - side_offs*h - (Wide ? 12*h : 8*h)), h*(429+18+offs+eql) + stride*i));
	}

	end_visual_change();  //TODO ?
}

void
MixbusStripFader::set_hold_count (int cnt)
{
	hold_cnt = cnt;
	hold_val = hold_val_r = -120; //in dB
}

void
MixbusStripFader::display_slate_text (std::string txt)
{
	slate->set_text(txt);
}

void
MixbusStripFader::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	route_color_changed ();

	//now show and re-connect based on route type ?
	slate->show();
	slate->set_text(rt->name());
	slate->set_route(rt);

	trim->set_controllable( rt, rt->trim_control() );
//	trim->set_route( rt, rt->ch_pre(), port_channel_pre_gain );
	trim->show();

	makeup->set_route( rt, rt->comp_makeup_controllable() );
	makeup->show();

	speed->set_route( rt, rt->comp_speed_controllable() );
	speed->show();

	thresh->set_route( rt, rt->comp_threshold_controllable() );
	thresh->show();

	comp_in->set_route( rt, rt->comp_enable_controllable() );
	comp_in->show();

	speed_leg->set_route( rt, rt->comp_mode_controllable() );
	speed_leg->show();

	comp_disp->set_route( rt, rt->comp_mode_controllable() );
	comp_disp->show();

	for (int i = 0 ; i< (THRESH_METER_CNT); i++) {
		redux_meterDots[i]->set_route( rt, rt->comp_redux_controllable() );
		redux_meterDots[i]->show();
	}

	fader->set_fader_route( rt );
	fader->show();

//---------------------------------

	mute->set_controllable( rt, rt->mute_control() );
	mute->show();

	if ( ! rt->is_master() ) {
		solo->set_controllable( rt, rt->solo_control() );
		solo->show();
	}

	strip_meter->show();

	end_visual_change();
}

void
MixbusStripFader::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();

	Cairo::Matrix m;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scale factor

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	bool is_master = _route->is_master();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	float drad = 5;  //decorator radius

	//comp decorator
	if (false){
		cr->set_identity_matrix();
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->move_to( 81*h, (526+eql)*h ); //start in the top left
		cr->rel_line_to( 11*h, 0 );
		cr->rel_curve_to( drad*h, 0, drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 223*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );
		cr->stroke ();
	}

	//Comp color
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t generic_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double color_r,color_g,color_b;
	Gtkmm2ext::color_to_rgba( generic_color, color_r, color_g, color_b, unused);
	cr->set_source_rgba (color_r, color_g, color_b, mod.a());
	Gtkmm2ext::rounded_rectangle (cr, midp+(Wide ? 8*h:6*h), 105*h, 64*h, 230*h, boxy ? 0 : (5)*h);
	cr->fill ();
//	Gtkmm2ext::rounded_rectangle (cr, midp+(Wide ? 10*h:8*h), 106*h, 64*h, 230*h, boxy ? 0 : (5)*h);
//	cr->set_line_width (1.0*h);
//	cr->set_source_rgba (1,1,1,0.6);
//	cr->stroke ();

	//slate decorator
	if (false){
		cr->set_identity_matrix();
		Gtkmm2ext::rounded_rectangle (cr, 4*h, 40*h, width-8*h, 20*h, boxy ? 0 : 5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->stroke ();
	}

	//Mute Solo Input Rec decorator
	if ( false ) {
		cr->set_identity_matrix();
		cr->move_to( 4*h, (487+eql)*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to(width-8*h-2*drad*h, 0);  //top right corner
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 6*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -(width-8*h-2*drad*h), 0 );  //btm left corner
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -6*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
	}
	cr->set_identity_matrix();

	//Comp decorator
	if (false){
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Comp" );
		layout->get_pixel_size (tw, th);
		cr->translate( 68*h, (511+eql)*h );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Trim decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Trim" );
		layout->get_pixel_size (tw, th);
		cr->translate( (Wide ? 27*h : 19*h) - (tw/2.), (538+eql)*h );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Makeup decorator
	{
		int tw, th;
		cr->set_identity_matrix();
		layout->set_text( "Gain" );
		layout->get_pixel_size (tw, th);
		cr->translate( width - (Wide ? 27*h : 19*h) - (tw/2.), (563+eql)*h );
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->show_in_cairo_context (cr);
	}

	//Fader decorator
	if (false) {
		cr->set_identity_matrix();
		cr->move_to( 4*h, (530+eql)*h ); //start in the top left
		cr->rel_move_to( drad*h, 0 );
		cr->rel_line_to( 40 - 2*drad*h, 0);  //top right corner
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );
		cr->rel_line_to( 0, 235*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -(40-2*drad*h), 0 );  //btm left corner
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );
		cr->rel_line_to( 0, -235*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //back to top left
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 8, height );
		cr->fill ();
	}

	//drop-shadow at top
	if (false) {
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 8);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 7 );
		cr->fill ();
	}
	
	if (true) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
		btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
		btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.15);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

//	if (is_master) {
//		cr->set_identity_matrix();
//		layout->set_text( "MASTER" );
//		int tw, th; layout->get_pixel_size (tw, th);
//		cr->translate( width/2 - tw/2, 466*h - th/2. );
//		cr->set_source_rgba (1,1,1,1);
//		layout->show_in_cairo_context (cr);
//	}

	cr->set_identity_matrix();

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
	}

	//special case for VU meter widgets;  we want these to draw on top of noise/shading
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg_top(cr, Wide);
	}
}

void
MixbusStripFader::meter()
{
	if (!_route)
		return;

	//we only want to show -audio- meters on the mixer page. for now.
	int offset = _route->shared_peak_meter()->input_streams().n_midi();

	//more than mono?
	bool st = _route->shared_peak_meter()->input_streams().n_audio() > 1;

	//todo:  different meter modes?
	float met = _route->peak_meter()->meter_level (offset, ARDOUR::MeterPeak);
	float met_r = st ? _route->peak_meter()->meter_level (offset+1, ARDOUR::MeterPeak) : 0.0;

	//calculate peak-hold decay
	hold_cnt--;
	uint32_t hold_target = (uint32_t) floor(UIConfiguration::instance().get_meter_hold());

	//LEFT peak grab
	if (met > hold_val) {
		hold_val = met;
	}

	//RIGHT peak grab
	if (met_r > hold_val_r) {
		hold_val_r = met_r;
	}

	//reset L&R peak hold after timeout
	if ( hold_cnt < 1 ) {
		hold_cnt = hold_target;
		hold_val = met;
		hold_val_r = met_r;
	}

	bool inp = ( _route->is_track() && (_route->meter_point()== ARDOUR::MeterInput) );

	strip_meter->meter(met, met_r, hold_val, hold_val_r, st, inp);

	//redux meter
	for (int i = 0 ; i< THRESH_METER_CNT; i++) {
		redux_meterDots[i]->meter_redux();
	}
}

void
MixbusStripFader::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name))
		if (_route)
			slate->set_text(_route->name());

	if (what_changed.contains (ARDOUR::Properties::color))
		route_color_changed();
}

void
MixbusStripFader::route_color_changed ()
{
	Gdk::Color gc = RouteUI::route_color ();
	Gtkmm2ext::Color c = gdk_color_to_rgba (gc);
	slate->set_color (c);
	fader->set_route_color (c);
}

bool
MixbusStripFader::mb_solo_press (GdkEventButton* ev)
{
	RouteUI::solo_press (ev);
	// don't continue with MixbusToggle::event_handler()
	return true;
}

bool
MixbusStripFader::mb_solo_release (GdkEventButton* ev)
{
	RouteUI::solo_release (ev);
	// don't call MixbusToggle::event_handler() pseudo-Latch and group-override
	return true;
}

bool
MixbusStripFader::mb_mute_press (GdkEventButton* ev)
{
	RouteUI::mute_press (ev);
	return true;
}

bool
MixbusStripFader::mb_mute_release (GdkEventButton* ev)
{
	RouteUI::mute_release (ev);
	return true;
}

void
MixbusStripFader::config_changed (string p)
{
}
