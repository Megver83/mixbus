/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_GATE__
#define __MB_GATE__

#include "mixbus_strips.h"

#ifdef MIXBUS32C
# define NATURAL_GATE_HEIGHT (235)
#else
# define NATURAL_GATE_HEIGHT (94)
#endif

#define GATE_METER_CNT 10
#define GATE_THRESH_CNT 6

class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;

class MixbusGate;

class MixbusGateCanvas : public MixbusSwitcherCanvas
{
public:
	MixbusGateCanvas();

	float natural_height() {return NATURAL_GATE_HEIGHT;}

	void set_hold_count (int cnt);
};

class MixbusGate : public MixbusStrip
{
public:

	MixbusGate (ArdourCanvas::Item *);
	~MixbusGate ();

	void scale_strip_to (float inWidth, float inHeight, bool Wide);
	void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide);

	void route_property_changed (const PBD::PropertyChange&);

	void meter();

	void gate_md_changed();

private:
	void set_button_names () {}

	std::shared_ptr<ARDOUR::AutomationControl> _gate_md_ctrl;
	PBD::ScopedConnection _gate_md_connection;

	MixbusKnob *gate_attk;
	MixbusKnob *gate_rels;
	MixbusKnob *gate_hyst;
	MixbusKnob *gate_knee;
	MixbusKnob *gate_hold;
	MixbusKnob *gate_ratio;
	MixbusKnob *gate_emph;

	MixbusHystLegend *hyst_leg;
	MixbusHoldLegend *hold_leg;

	MixbusThresh *gate_thresh;
	MixbusKnob *gate_depth;

	MixbusToggle *gate_in;
	MixbusToggle *gate_exp;
	MixbusToggle *gate_emph_in;
	MixbusToggle *gate_lstn;

	MixbusMeterDot *input_meterDots[GATE_METER_CNT];
	MixbusReduxDot *redux_meterDots[GATE_THRESH_CNT];

	PBD::ScopedConnectionList _config_connection;
	void config_changed (std::string);

	sigc::connection meter_connection;
};

#endif
