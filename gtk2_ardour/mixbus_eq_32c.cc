/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <algorithm>
#include <cairomm/context.h>

#include <pangomm/layout.h>

#include <gtkmm/menu.h>
#include <gtkmm/menu_elems.h>

#include "pbd/compose.h"

#include "ardour/amp.h"
#include "ardour/automation_control.h"
#include "ardour/dB.h"
#include "ardour/meter.h"
#include "ardour/route_group.h"
#include "ardour/utils.h"

#include "gtkmm2ext/keyboard.h"
#include "gtkmm2ext/utils.h"

#include "canvas/canvas.h"
#include "canvas/debug.h"
#include "canvas/utils.h"

#include "widgets/binding_proxy.h"

#include "actions.h"
#include "gui_thread.h"
#include "utils.h"
#include "mouse_cursors.h"
#include "ardour_ui.h"
#include "editor_xpms"

#include "mixbus_textures.h"
#include "mixbus_widgets.h"
#include "mixbus_eq.h"

#include "mixer_strip.h"  //needed to do wide/narrow switching
#include "mixer_ui.h" //needed to do wide/narrow switching
#include "group_tabs.h"

#include "pbd/i18n.h"

#include "timers.h"

using namespace std;
using namespace ArdourCanvas;
using namespace Gtk;
using namespace Gtkmm2ext;
using namespace ARDOUR_UI_UTILS;

// ---------------------------------------------------------------



//an MBCanvas for input knobs
MixbusEQCanvas::MixbusEQCanvas ()
{
	_knobs = new MixbusEQ(root());
}

//-------------------------


MixbusEQ::~MixbusEQ ()
{
}

MixbusEQ::MixbusEQ (ArdourCanvas::Item *p)
	: MixbusStrip (p)
{
	_bg_pattern_type = bg_pattern_eq;

	hp_freq = new Mixbus_Filter_Knob(this, 0);
	widgets.push_front(hp_freq);
	hp_freq->set_color("mixbus hp filter freq knob");
	std::string ft[NUM_KNOB_TICKS] = { "|", ".02", "|", ".05", "", ".25", "", "1.2", "|", "3.1", "|" };
	hp_freq->set_ticks(ft);

	lp_freq = new Mixbus_Filter_Knob(this, 1);
	widgets.push_front(lp_freq);
	lp_freq->set_color("mixbus lp filter freq knob");
	std::string lpft[NUM_KNOB_TICKS] = { "|", ".16", "|", ".4", "", "1.6", "", "8", "|", "20", "|" };
//	std::string lpft[NUM_KNOB_TICKS] = { "0","0","0","0","0","0","0","0","0","0","0" };
	lp_freq->set_ticks(lpft);

	std::string gt[NUM_KNOB_TICKS] = { "|", "12", "|", "4", "|", "0", "|", "4", "|", "12", "|" };

	eq_low_gain = new Mixbus_EQ_Knob(this);
	eq_low_gain->set_color("mixbus eq low gain knob");
	widgets.push_front(eq_low_gain);
	eq_low_gain->set_ticks(gt);

	eq_low_freq = new Mixbus_EQ_Knob(this);
	eq_low_freq->set_color("mixbus eq low freq knob");
	widgets.push_front(eq_low_freq);
	std::string lft[NUM_KNOB_TICKS] = { "|", ".04", "|",".09",  "", ".28", "", ".4", "|", ".6", "|" };
	eq_low_freq->set_ticks(lft);

	eq_lomid_freq = new Mixbus_EQ_Knob(this);
	widgets.push_front(eq_lomid_freq);
	eq_lomid_freq->set_color("mixbus eq lomid freq knob");
	std::string mft[NUM_KNOB_TICKS] = { "|", "0.2", "|", ".45", "", "1.4", "", "2", "|", "3.1", "|" };
	eq_lomid_freq->set_ticks(mft);

	eq_lomid_gain = new Mixbus_EQ_Knob(this);
	widgets.push_front(eq_lomid_gain);
	eq_lomid_gain->set_color("mixbus eq lomid gain knob");
	eq_lomid_gain->set_ticks(gt);

	eq_himid_freq = new Mixbus_EQ_Knob(this);
	widgets.push_front(eq_himid_freq);
	eq_himid_freq->set_color("mixbus eq himid freq knob");
	std::string hmft[NUM_KNOB_TICKS] = { "|", "0.4", "|", ".9", "", "2.8", "", "4", "|", "6", "|" };
	eq_himid_freq->set_ticks(hmft);

	eq_himid_gain = new Mixbus_EQ_Knob(this);
	widgets.push_front(eq_himid_gain);
	eq_himid_gain->set_color("mixbus eq himid gain knob");
	eq_himid_gain->set_ticks(gt);

	eq_hi_gain = new Mixbus_EQ_Knob(this);
	eq_hi_gain->set_color("mixbus eq hi gain knob");
	widgets.push_front(eq_hi_gain);
	eq_hi_gain->set_ticks(gt);

	eq_hi_freq = new Mixbus_EQ_Knob(this);
	eq_hi_freq->set_color("mixbus eq hi freq knob");
	widgets.push_front(eq_hi_freq);
	std::string hft[NUM_KNOB_TICKS] = { "|", "0.9", "|", "1.8", "|", "6", "|", "8", "|", "13", "|" };
	eq_hi_freq->set_ticks(hft);

	eq_in = new MixbusToggle(this);
	widgets.push_front(eq_in);
	eq_in->set_text("EQ");

	eq_hi_bell_in = new MixbusToggle(this);
	widgets.push_front(eq_hi_bell_in);
	eq_hi_bell_in->set_color("mixbus compressor button");
	eq_hi_bell_in->set_text("Hi");

	eq_lo_bell_in = new MixbusToggle(this);
	widgets.push_front(eq_lo_bell_in);
	eq_lo_bell_in->set_color("mixbus compressor button");
	eq_lo_bell_in->set_text("Lo");

	filt_in = new MixbusToggle(this);
	widgets.push_front(filt_in);
	filt_in->set_text("FLT");

	ARDOUR::Config->ParameterChanged.connect (_config_connection, MISSING_INVALIDATOR, boost::bind (&MixbusEQ::config_changed, this, _1), gui_context());
}

void
MixbusEQ::scale_strip_to (float inWidth, float inHeight, bool Wide)
{
	set_size(ArdourCanvas::Rect (0, 0, inWidth, inHeight));

	//CHECK:  if last_scale == this, already done; bail out.

	float midp = inWidth/ 2.0;
	float h = ScreenSizer::user_scale();  //scaling factor
	const float toggle_height = h*17.0;
	const float knob_sz = h*22;

// ------------ track EQ knobs

//eq switches
	eq_hi_bell_in->set_size(ArdourCanvas::Rect (0, 0, 48*h, toggle_height));
	eq_hi_bell_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 2*h));

	eq_lo_bell_in->set_size(ArdourCanvas::Rect (0, 0, 48*h, toggle_height));
	eq_lo_bell_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 174*h));

	eq_in->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	eq_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 195*h));

	filt_in->set_size(ArdourCanvas::Rect (0, 0, h*48, toggle_height));
	filt_in->set_position(ArdourCanvas::Duple (Wide ? 51*h : 44*h, 216*h));

//eq knobs
	float offs = -6*h - 12*h;

	eq_hi_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_hi_gain->set_position(ArdourCanvas::Duple (61*h, 46*h+offs));

	eq_hi_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_hi_freq->set_position(ArdourCanvas::Duple (17*h, 34*h+offs));

	eq_himid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_himid_gain->set_position(ArdourCanvas::Duple (61*h, 86*h+offs));

	eq_himid_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_himid_freq->set_position(ArdourCanvas::Duple (17*h, 74*h+offs));

	eq_lomid_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_lomid_gain->set_position(ArdourCanvas::Duple (61*h, 126*h+offs));

	eq_lomid_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_lomid_freq->set_position(ArdourCanvas::Duple (17*h, 114*h+offs));

	eq_low_gain->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_low_gain->set_position(ArdourCanvas::Duple (61*h, 166*h+offs));

	eq_low_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	eq_low_freq->set_position(ArdourCanvas::Duple (17*h, 154*h+offs));


	lp_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	lp_freq->set_position(ArdourCanvas::Duple (17*h, 192*h+offs));

	hp_freq->set_size(ArdourCanvas::Rect (0, 0, knob_sz, knob_sz));
	hp_freq->set_position(ArdourCanvas::Duple (17*h, 224*h+offs));

	end_visual_change();
}

void
MixbusEQ::set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned)
{
	MixbusStrip::set_strip_route(rt, mixer_owned);

	//now show and re-connect based on route type ?
	if ( rt->is_master() ) {
	} else if ( rt->mixbus() ) {
	} else {

		eq_in->show();
		eq_in->set_route( rt, rt->eq_enable_controllable() );

		eq_hi_bell_in->show();
		eq_hi_bell_in->set_route( rt, rt->eq_shape_controllable(3) );

		eq_lo_bell_in->show();
		eq_lo_bell_in->set_route( rt, rt->eq_shape_controllable(0) );

		filt_in->show();
		filt_in->set_route( rt, rt->filter_enable_controllable(false) );  //note: we only have one enable for both hp+lp

		eq_low_gain->show();
		eq_low_gain->set_route( rt, rt->eq_gain_controllable(0) );
		eq_low_gain->set_slate_ctrls( 0, rt->eq_freq_controllable(0), rt->eq_gain_controllable(0), rt->eq_shape_controllable(0) );

		eq_lomid_gain->show();
		eq_lomid_gain->set_route( rt, rt->eq_gain_controllable(1) );
		eq_lomid_gain->set_slate_ctrls( 1, rt->eq_freq_controllable(1), rt->eq_gain_controllable(1), std::shared_ptr<ARDOUR::AutomationControl>() );

		eq_himid_gain->show();
		eq_himid_gain->set_route( rt, rt->eq_gain_controllable(2) );
		eq_himid_gain->set_slate_ctrls( 2, rt->eq_freq_controllable(2), rt->eq_gain_controllable(2), std::shared_ptr<ARDOUR::AutomationControl>() );

		eq_hi_gain->show();
		eq_hi_gain->set_route( rt,rt->eq_gain_controllable(3) );
		eq_hi_gain->set_slate_ctrls( 3, rt->eq_freq_controllable(3), rt->eq_gain_controllable(3), rt->eq_shape_controllable(3) );

		eq_low_freq->show();
		eq_low_freq->set_route( rt, rt->eq_freq_controllable(0) );
		eq_low_freq->set_slate_ctrls( 0, rt->eq_freq_controllable(0), rt->eq_gain_controllable(0), rt->eq_shape_controllable(0) );

		eq_lomid_freq->show();
		eq_lomid_freq->set_route( rt, rt->eq_freq_controllable(1) );
		eq_lomid_freq->set_slate_ctrls( 1, rt->eq_freq_controllable(1), rt->eq_gain_controllable(1), std::shared_ptr<ARDOUR::AutomationControl>() );

		eq_himid_freq->show();
		eq_himid_freq->set_route( rt, rt->eq_freq_controllable(2) );
		eq_himid_freq->set_slate_ctrls( 2, rt->eq_freq_controllable(2), rt->eq_gain_controllable(2), std::shared_ptr<ARDOUR::AutomationControl>() );

		eq_hi_freq->show();
		eq_hi_freq->set_route( rt, rt->eq_freq_controllable(3) );
		eq_hi_freq->set_slate_ctrls( 3, rt->eq_freq_controllable(3), rt->eq_gain_controllable(3), rt->eq_shape_controllable(3) );

		hp_freq->show();
		hp_freq->set_route (rt, rt->filter_freq_controllable (true));

		lp_freq->show();
		lp_freq->set_route (rt, rt->filter_freq_controllable (false));
	}

	end_visual_change();
}

void
MixbusEQ::render_bg (Cairo::RefPtr<Cairo::Context> cr, bool Wide)
{
	if (!_route) {  //too early, the route hasn't been set yet
		return;
	}

	ArdourCanvas::Rect self (item_to_window (_rect));
	Distance height = self.height();
	Distance width = self.width();  float halfw = width/2.0;

	Cairo::Matrix m;

	bool is_mixbus = _route->mixbus() != 0;
	bool is_master = _route->is_master();
	bool is_track = !is_master && !is_mixbus;

	float midp = width / 2;
	float h = ScreenSizer::user_scale();  //scaling factor

	int tw,  th;  //text width and height

	bool flat = UIConfiguration::instance().get_flat_buttons();
	bool boxy = UIConfiguration::instance().get_boxy_buttons();

	//bg color
	uint32_t bg_color = UIConfiguration::instance().color ("mixbus strip bg");
	Gtkmm2ext::set_source_rgb_a (cr, bg_color, 1.0);
	cr->rectangle (0, 0, width, height);
	cr->fill ();

	/* fg (font) color */
	uint32_t font_color = UIConfiguration::instance().color ("gtk_texts");
	double fg_r,fg_g,fg_b, unused;
	Gtkmm2ext::color_to_rgba( font_color, fg_r, fg_g, fg_b, unused);

	SVAModifier bord = UIConfiguration::instance().modifier ("strip border alpha");
	SVAModifier mod = UIConfiguration::instance().modifier ("strip puddle alpha");
	uint32_t puddle_color = UIConfiguration::instance().color ("mixbus puddle: fill");
	double puddle_r,puddle_g,puddle_b;
	Gtkmm2ext::color_to_rgba( puddle_color, puddle_r, puddle_g, puddle_b, unused);

	//Text setup
	Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create (cr);
	layout->set_font_description ( ScreenSizer::GetFontDesc() );

	float drad = 5;  //decorator radius
	float offs = 146;  //32C offset ( allows widgets in the fader area to share position values with regular mb

	cr->set_line_width (1.0*h);

	//bell decorators
	if (false) {
		cr->move_to( 66*h, 192*h ); //start in the top left
		cr->rel_move_to( 22*h, 0 );
		cr->rel_move_to( drad*h, 0 );
		cr->rel_move_to( 0, drad*h );
		cr->rel_line_to( 0, 3*h );
		cr->rel_curve_to( 0, drad*h, -drad*h, drad*h, -drad*h, drad*h );  //btm right corner
		cr->rel_line_to( -22*h, 0 );
		cr->rel_curve_to( -drad*h, 0, -drad*h, -drad*h, -drad*h, -drad*h );  //btm left corner
		cr->rel_line_to( 0, -3*h );
		cr->rel_move_to( 0, -drad*h );
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
		cr->set_source_rgba (0.75, 0.5, 0.25, 0.1);
//			cr->fill();
	}
	if (false) {
		cr->move_to( 7*h, 15*h ); //start in the btm left
		cr->rel_move_to( 0, -drad*h );
		cr->rel_line_to( 0, -3*h );
		cr->rel_curve_to( 0, -drad*h, drad*h, -drad*h, drad*h, -drad*h );  //top left
		cr->rel_line_to( 22*h, 0 );
		cr->rel_curve_to( drad*h, 0 , drad*h, drad*h, drad*h, drad*h );  //top rt
		cr->rel_line_to( 0, 3*h );
		cr->rel_move_to( 0, drad*h );
		cr->rel_move_to( -22*h, 0 );
		cr->set_source_rgba (1,1,1,0.7);
		cr->stroke ();
		cr->set_source_rgba (0.75, 0.5, 0.25, 0.1);
//			cr->fill();
	}

	//bell symbols
	if (true) {

		float xrad = 3.5;
		float rad = 3.5;
		cr->move_to( 82*h, 186*h ); //start in the top left
		cr->set_source_rgba (fg_r, fg_g, fg_b,1);
		cr->rel_curve_to( xrad*h, 0, xrad*h, -rad*h, xrad*h, -rad*h );
		cr->rel_curve_to( 0, -rad*h, xrad*h, -rad*h, xrad*h, 0 );
		cr->rel_curve_to( 0, rad*h, xrad*h, rad*h, xrad*h, rad*h );
		cr->stroke();

		cr->move_to( 82*h, 14*h ); //start in the top left
		cr->set_source_rgba (fg_r, fg_g, fg_b,1);
		cr->rel_curve_to( xrad*h, 0, xrad*h, -rad*h, xrad*h, -rad*h );
		cr->rel_curve_to( 0, -rad*h, xrad*h, -rad*h, xrad*h, 0 );
		cr->rel_curve_to( 0, rad*h, xrad*h, rad*h, xrad*h, rad*h );
		cr->stroke();
	}

	//puddle
	if (true) {
		cr->set_identity_matrix();
		cr->set_source_rgba (puddle_r, puddle_g, puddle_b, mod.a());
		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width-2*h, height-2*h, 5*h);
		cr->fill ();
	}

	if (false) { //EQ IN
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);

		cr->set_identity_matrix();
		cr->translate( floor(72*h), floor(4*h) );
		layout->set_text( "IN" );
		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->translate( floor(19*h), floor(211*h) );
		layout->set_text( "IN" );
		layout->show_in_cairo_context (cr);
	}

	if (false) { //EQ bands
		cr->set_source_rgba (fg_r*0.8,fg_g*0.8,fg_b*0.8,LEGEND_BRIGHTNESS);

		cr->set_identity_matrix();
		cr->translate( floor(5*h), floor(5*h) );
		layout->set_text( "HI" );
		layout->show_in_cairo_context (cr);

//			cr->set_identity_matrix();
//			cr->translate( 5*h, 88*h );
//			layout->set_text( "MID" );
//			layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->translate( floor(83*h), floor(174*h) );
		layout->set_text( "LO" );
		layout->show_in_cairo_context (cr);

//			cr->set_identity_matrix();
//			cr->translate( 5*h, 160*h );
//			layout->set_text( "LO" );
//			layout->show_in_cairo_context (cr);
	}

	if (false) { //Filter squiggly
		float rad = 5;

		cr->set_identity_matrix();
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->move_to( 30*h, 222*h );
		cr->rel_line_to( 2*h, 0*h );
		cr->rel_curve_to( rad*h, 0, rad*h, -rad*h, rad*h, -rad*h );
		cr->rel_line_to( 0, -26*h);
		cr->rel_curve_to( 0, -rad*h, rad*h, -rad*h, rad*h, -rad*h );
		cr->stroke ();

		cr->set_identity_matrix();
		cr->set_line_width (1.5*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b,0.7);
		cr->move_to( 32*h + rad*h, 215*h - rad*h );
		cr->rel_curve_to( 0, rad*h, rad*h, rad*h, rad*h, rad*h );
		cr->stroke ();

		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);

		cr->set_identity_matrix();
		cr->translate( floor(44*h), floor(181*h) );
		layout->set_text( "LP" );
		layout->show_in_cairo_context (cr);

		cr->set_identity_matrix();
		cr->translate( floor(44*h), floor(211*h) );
		layout->set_text( "HP" );
		layout->show_in_cairo_context (cr);
	}


	//band separators
	cr->set_identity_matrix();
	if (true) {
		float drad = 5;  //smaller decorator radius

		for (int i = 0; i < 3; i++ ) {
			cr->set_line_width (1.0*h);
			cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
			cr->move_to( 14*h, 42*h + 40*h*i  ); //start in the top left
			cr->rel_line_to( 25*h, 0);  //slant start
			cr->rel_line_to( 16*h, 13*h);  //slant end
			cr->rel_line_to( 31*h, 0*h);
			cr->stroke ();
			cr->set_identity_matrix();
		}

		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		cr->move_to( 14*h, 42*h + 40*h*3  ); //start in the top left
		cr->rel_line_to( 25*h, 0);  //slant start
		cr->rel_line_to( 14*h, 13*h);  //slant end
//			cr->rel_line_to( 0*h, 4*h);
		cr->stroke ();
		cr->set_identity_matrix();
	}

	if ( false ) {
		cr->set_identity_matrix();
		cr->set_source_rgba (fg_r,fg_g,fg_b,LEGEND_BRIGHTNESS);
		layout->set_text( string_compose( "%1", _route->mixbus()));
		layout->get_pixel_size (tw, th); tw*=1.5;
#ifdef MIXBUS32C
		cr->translate( width- 4*h - tw, (149+offs)*h );
#else
		cr->translate( width- 4*h - tw, (154+offs)*h );
#endif
		cr->scale(1.5, 1.5);
		layout->show_in_cairo_context (cr);
	}

	//some widgets draw some background tickmarks, or whatever
	for (list<MixbusWidget*>::iterator i = widgets.begin(); i != widgets.end(); i++ ) {
		cr->set_identity_matrix();
		if ((*i)->visible())
			(*i)->render_bg(cr, Wide);
	}

	//bug
	if (false && is_track){
		cr->set_identity_matrix();
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_logo_texture->gobj(), 83*h, 4*h);
		cr->rectangle (0, 0, 16*h, 16*h);
		cr->paint_with_alpha ( 0.65 );
		cr->begin_new_path();
	}

	cr->set_identity_matrix();

	if (!flat) {
		//drop-shadow at left
		Cairo::RefPtr<Cairo::LinearGradient> left_pattern = Cairo::LinearGradient::create (0.0, 0.0, 6*h, 0);
		left_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.5);
		left_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (left_pattern);
		cr->rectangle(0, 0, 6*h, height );
		cr->fill ();
	}

	cr->set_identity_matrix();

	if (false && !flat) {
		//drop-shadow at top
		Cairo::RefPtr<Cairo::LinearGradient> shine_pattern = Cairo::LinearGradient::create (0.0, 0.0, 0.0, 7);
		shine_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.7);
		shine_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.0);
		cr->set_source (shine_pattern);
		cr->rectangle(0, 0, width, 8 );
		cr->fill ();
	}

	if (false) {
		//slow-shadow at bottom
		Cairo::RefPtr<Cairo::LinearGradient> btm_pattern = Cairo::LinearGradient::create (0.0, 0, 0.0, height);  //IMPORTANT:  the gradient operates on the whole context
			btm_pattern->add_color_stop_rgba (0,	0,	0,	0,	0.0);
			btm_pattern->add_color_stop_rgba (1,	0,	0,	0,	0.10);
		cr->set_source (btm_pattern);
		cr->rectangle(0, 0, width, height  );
		cr->fill ();
	}

	cr->set_identity_matrix();

	//section bounds
	if (true) {
		cr->set_line_width (1.0*h);
		cr->set_source_rgba (fg_r, fg_g, fg_b, bord.a());
		Gtkmm2ext::rounded_rectangle (cr, 1*h, 1*h, width-2*h, height-2*h, 5*h);
		cr->stroke ();
	}

	//noise
	if (true){
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_strip_texture->gobj(), 0, 0);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip texture alpha");
		cr->paint_with_alpha ( mod.a()*0.40 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}

	//scratches
	if (!flat && is_mixbus) {
		cairo_t *ct = cr->cobj();
		gdk_cairo_set_source_pixbuf (ct, MixbusTextures::mb_scratch_texture->gobj(), -_route->mixbus()*25, -_route->mixbus()*10);
		cr->rectangle (0, 0, width, height);
		SVAModifier mod = UIConfiguration::instance().modifier ("strip scratch alpha");
		cr->paint_with_alpha ( mod.a() * 0.20 );
		cr->begin_new_path();
//		cr->rectangle (0, 234, width, 253);
//		cr->paint_with_alpha (0.5);
	}

}

void
MixbusEQ::route_property_changed (const PBD::PropertyChange& what_changed)
{
	if (what_changed.contains (ARDOUR::Properties::name)) {}

	//nothing to do here?
}

void
MixbusEQ::config_changed (string p)
{
}
