/*
    Copyright (C) 2015 Paul Davis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifdef WAF_BUILD
#include "gtk2ardour-config.h"
#endif

#include <gtkmm.h>
#include <boost/algorithm/string/trim.hpp>

#include "license_manager.h"
#include "ui_config.h"
#include "mixbus_nag.h"

#include "pbd/compose.h"
#include "pbd/i18n.h"

using namespace std;
using namespace Gtk;
using namespace ArdourWidgets;

MixbusNagScreen::MixbusNagScreen ()
	: ArdourDialog    ("", true)
	, _license_button (_("Apply the License"))
	, _demo_button    (_("Continue in Demo Mode"))
	, _first_focus (false)
{
	set_title(string_compose(_("%1 is UNLICENSED"), PROGRAM_NAME));

	Label* msg = manage (new Label (
				string_compose(
				"%1 is currently unlicensed.\n"
				"\n"
				"Please visit Harrison's online store\n"
#ifdef __APPLE__
				"to purchase a license, and copy it with Cmd+C.\n"
#else
				"to purchase a license, and copy it with Ctrl+C.\n"
#endif
				"\n"
				"Click PASTE to insert the license text here:\n"
				, PROGRAM_NAME)
				, ALIGN_CENTER, ALIGN_CENTER));

	msg->set_justify (JUSTIFY_CENTER);
	msg->modify_font (UIConfiguration::instance().get_BigFont());

	_license_button.set_layout_font (UIConfiguration::instance().get_BigFont());
	_demo_button.set_layout_font (UIConfiguration::instance().get_BigFont());

	Button* paste_btn = manage (new Button (Stock::PASTE));

	_entry_lic.set_text ("Your Name*XX*HAR...678==|");
	_entry_lic.set_name ("LicenseKeyGhostEntry");

	Table* tbl = manage (new Table());
	tbl->attach (*msg,            0, 2, 0, 1, EXPAND|FILL, SHRINK,      0, 10);
	tbl->attach (*paste_btn,      0, 1, 1, 2, SHRINK,      SHRINK,      4, 2);
	tbl->attach (_entry_lic,      1, 2, 1, 2, EXPAND|FILL, SHRINK|FILL, 4, 2);
	tbl->attach (_license_button, 0, 2, 2, 3, EXPAND|FILL, SHRINK,      0, 2);
	tbl->attach (_demo_button,    0, 2, 3, 4, EXPAND|FILL, SHRINK,      0, 10);

	HBox* hbox = manage (new HBox());
	hbox->pack_start (*tbl, true, false);
	get_vbox()->pack_start (*hbox, false, false);

	paste_btn->signal_clicked().connect (sigc::mem_fun (this, &MixbusNagScreen::get_pastebuffer));
	_entry_lic.signal_focus_in_event().connect (sigc::mem_fun (*this, &MixbusNagScreen::entry_focus_in), false);
	_entry_lic.signal_changed().connect (sigc::mem_fun (*this, &MixbusNagScreen::update_sensitivity));
	_license_button.signal_clicked.connect (sigc::bind (sigc::mem_fun (this, &ArdourDialog::response), RESPONSE_ACCEPT));
	_demo_button.signal_clicked.connect (sigc::bind (sigc::mem_fun (this, &ArdourDialog::response), RESPONSE_CANCEL));

	set_size_request (540, -1);
	set_position (WIN_POS_CENTER);
	show_all ();
	update_sensitivity ();
}

void
MixbusNagScreen::update_sensitivity ()
{
	string lic = _entry_lic.get_text ();
	boost::algorithm::trim (lic);

#ifdef MIXBUS32C
	const string pat = string_compose (X_("*32c%1*"), atoi (X_(PROGRAM_VERSION)));
#else
	const string pat = string_compose (X_("*mb%1*"), atoi (X_(PROGRAM_VERSION)));
#endif
	if (string::npos == lic.find (pat) || std::count (lic.begin(), lic.end(), '|') != 2 || lic.at (lic.length() - 1) != '|') {
		_license_button.set_sensitive (false);
		_demo_button.set_sensitive (true);
	} else {
		_demo_button.set_sensitive (false);
		_license_button.set_sensitive (true);
	}
}

bool
MixbusNagScreen::entry_focus_in (GdkEventFocus*)
{
	if (!_first_focus) {
		_first_focus = true;
		_entry_lic.set_name ("gtkmm__GtkEntry");
		_entry_lic.set_text ("");
	}
	return false;
}

void
MixbusNagScreen::get_pastebuffer ()
{
	GtkClipboard *clip = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
	gchar* text = gtk_clipboard_wait_for_text (clip);
	if (!text) {
		std::string msg (_(
					"The clipboard is empty or does not contain text.\n\n"
					"Please select and copy the license text from the order confirmation web-page.\n"
					"(it usually starts with your name, and ends with a '|')\n"));
		MessageDialog m (msg, false, Gtk::MESSAGE_ERROR, BUTTONS_OK, true);
		m.get_vbox()->set_spacing (3);
		m.run();
		return;
	}

	_first_focus = true;
	_entry_lic.set_name ("gtkmm__GtkEntry");
	_entry_lic.set_text (Glib::locale_to_utf8 (text));
	g_free (text);
}

void
MixbusNagScreen::nag ()
{
	while (true) {
		show ();
		int response = run ();
		hide ();

		if (response != RESPONSE_ACCEPT) {
			/* demo-mode or install was cancelled */
			return;
		}
		std::string lic = _entry_lic.get_text ();
		if (LicenseManager::save_mixbus_license (lic)) {
			/* success */
			return;
		}
	}
}
