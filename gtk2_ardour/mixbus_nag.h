/*
    Copyright (C) 2015 Paul Davis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef __gtk_ardour_nag_h__
#define __gtk_ardour_nag_h__

#include <gtkmm/entry.h>

#include "widgets/ardour_button.h"
#include "ardour_dialog.h"

class MixbusNagScreen : public ArdourDialog
{
public:
	MixbusNagScreen ();
	~MixbusNagScreen() {};

	void nag ();

private:
	void update_sensitivity ();
	void get_pastebuffer ();
	bool entry_focus_in (GdkEventFocus*);

	ArdourWidgets::ArdourButton _license_button;
	ArdourWidgets::ArdourButton _demo_button;
	Gtk::Entry                  _entry_lic;

	bool _first_focus;
};

#endif /* __gtk_ardour_nag_h__ */
