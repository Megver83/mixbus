/*
    Copyright (C) 2019 Paul Davis
    Author: Ben Loftis <ben@harrisonconsoles.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __MB_STRIPS__
#define __MB_STRIPS__

#include <string>
#include <vector>

#include <pangomm/fontdescription.h>

#include <cairo/cairo.h>

#include <cairomm/context.h>
#include <cairomm/pattern.h>

#include <canvas/canvas.h>
#include <canvas/container.h>
#include <canvas/item.h>
#include <canvas/fill.h>
#include <canvas/outline.h>

#include <gtkmm/layout.h>

#include <ardour/route.h>
#include <ardour/plugin.h>
#include <ardour/plugin_insert.h>
#include <ardour/automation_control.h>

#include "mixbus_widgets.h"
#include "route_ui.h"

#ifdef MIXBUS32C
# define NATURAL_SWITCHER_HEIGHT (14.0)
#else
# define NATURAL_SWITCHER_HEIGHT (14.0)
#endif

#ifdef MIXBUS32C
# define NATURAL_STRIP_WIDTH 104.0
#else
# define NATURAL_STRIP_WIDTH 114.0
#endif

class MixbusWidget;
class MixbusKnob;
class MixbusFader;
class MixbusThresh;
class MixbusToggle;
class MixbusMeterDot;
class MixbusPointerMeter;

class MixbusStrip;


class MixbusStripCanvas : public ArdourCanvas::GtkCanvas  //this is the gtk canvas
{
public:
	MixbusStripCanvas();

	void on_size_request (Gtk::Requisition* req);
	void on_size_allocate (Gtk::Allocation& alloc);

	void set_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	virtual float natural_width() {return NATURAL_STRIP_WIDTH;}
	virtual float natural_height() = 0;

protected:
	MixbusStrip *_knobs;
};

class MixbusSwitcherCanvas : public MixbusStripCanvas
{
public:
	MixbusSwitcherCanvas() {}

	virtual float natural_width() {return NATURAL_STRIP_WIDTH;}
};

class MixbusStrip : public RouteUI, public ArdourCanvas::Item  //and this is the root Canvas item that will hold our widgets
{
public:

	MixbusStrip (ArdourCanvas::Item *);
	~MixbusStrip ();

	virtual void scale_strip_to (float inWidth, float inHeight, bool Wide) = 0;
	virtual void set_strip_route (std::shared_ptr<ARDOUR::Route> rt, bool mixer_owned);

	virtual void render (ArdourCanvas::Rect const & area, Cairo::RefPtr<Cairo::Context>) const;
	virtual void render_bg (Cairo::RefPtr<Cairo::Context>, bool Wide) {}
	void render_bg_pattern (int w, int h, bool Wide);

	std::shared_ptr<ARDOUR::Stripable> stripable() const { return RouteUI::stripable(); }

	/* as the top-level widget, I'm a RouteUI and responsible for retransmitting some GUI events via transmit_rui_message */
	void set_slate_text (std::string txt);

	PBD::ScopedConnectionList _route_connections;

	virtual void set_size (ArdourCanvas::Rect const&);
	virtual void compute_bounding_box () const;
	
	virtual void set_button_names() {}

	enum pattern_bg_types {
		bg_pattern_empty = 0,

		bg_pattern_fader,
		bg_pattern_fader_bus,
		bg_pattern_fader_master,
		bg_pattern_fader_small,

		bg_pattern_eq,

		bg_pattern_sends,
		bg_pattern_sends_mixbus,
		bg_pattern_sends_fxbus,  //mixbuses 9..12 don't have the extra knobs
		bg_pattern_sends_master,

		bg_pattern_comp,
		bg_pattern_comp_master,

		bg_pattern_gate,

		bg_pattern_panner,
		bg_pattern_panner_bus,
		bg_pattern_panner_master,

		bg_pattern_switcher,
		bg_pattern_kmeter,
		bg_pattern_vca,
		bg_pattern_spiller,
		NUM_BG_PATTERN_TYPES
	};
	static void initialize_mixbus_strip_patterns();

protected:
	ArdourCanvas::Rect          _rect;
	pattern_bg_types            _bg_pattern_type;

	std::string state_id() const;

	//our own list of widgets; we use this to globally hide stuff
	std::list<MixbusWidget*> widgets;

	void color_handler ();  //theme_changed

protected:
	//our offscreen bitmaps
	static cairo_pattern_t* _patterns[NUM_BG_PATTERN_TYPES];
	static int				_pattern_widths[NUM_BG_PATTERN_TYPES];
};

#endif
