#ifdef WAF_BUILD
#include "libardour-config.h"
#endif

#ifdef MIXBUS

#include "ardour/plugin_insert.h"
#include "ardour/route.h"

using namespace ARDOUR;
using namespace PBD;

/* ****************************************************************************
 * Mixbus v3-6 common ports (EQ, Comp)
 */

enum channel_comp_ports {
	port_channel_comp_channelstrip = 0,
	port_channel_comp_in,
	port_channel_comp_thresh,
	port_channel_comp_speed,
	port_channel_comp_mode,
	port_channel_comp_makeup,
	port_channel_comp_redux,
	port_channel_comp_input1,
	port_channel_comp_input2,
	port_channel_comp_input3,
	port_channel_comp_input4,
	port_channel_comp_output1,
	port_channel_comp_output2,
	port_channel_comp_output3,
	port_channel_comp_output4,
	CHAN_COMP_NUMPORTS
};

#ifdef MIXBUS32C
enum channel_eq_ports {
	port_channel_eq_channelstrip = 0,
	port_channel_eq_in,
	port_channel_filt_in,
	port_channel_hi_bell_in,
	port_channel_lo_bell_in,
	port_channel_hp_freq,
	port_channel_lp_freq,
	port_channel_eq_hi_freq,
	port_channel_eq_hi_gain,
	port_channel_eq_himid_freq,
	port_channel_eq_himid_gain,
	port_channel_eq_lomid_freq,
	port_channel_eq_lomid_gain,
	port_channel_eq_lo_freq,
	port_channel_eq_lo_gain,
	port_channel_eq_input1,
	port_channel_eq_input2,
	port_channel_eq_input3,
	port_channel_eq_input4,
	port_channel_eq_output1,
	port_channel_eq_output2,
	port_channel_eq_output3,
	port_channel_eq_output4,
	CHAN_EQ_NUMPORTS
};

#else

enum channel_eq_ports {
	port_channel_eq_channelstrip = 0,
	port_channel_eq_in,
	port_channel_hp_freq,
	port_channel_eq_hi_freq,
	port_channel_eq_hi_gain,
	port_channel_eq_mid_freq,
	port_channel_eq_mid_gain,
	port_channel_eq_lo_freq,
	port_channel_eq_lo_gain,
	port_channel_eq_input1,
	port_channel_eq_input2,
	port_channel_eq_input3,
	port_channel_eq_input4,
	port_channel_eq_output1,
	port_channel_eq_output2,
	port_channel_eq_output3,
	port_channel_eq_output4,
	CHAN_EQ_NUMPORTS
};


#endif

enum master_comp_ports {
	port_master_comp_channelstrip = 0,
	port_master_comp_in,
	port_master_comp_thresh,
	port_master_comp_speed,
	port_master_comp_mode,
	port_master_comp_makeup,
	port_master_comp_redux,
	port_master_comp_input1,
	port_master_comp_input2,
	port_master_comp_input3,
	port_master_comp_input4,
	port_master_comp_output1,
	port_master_comp_output2,
	port_master_comp_output3,
	port_master_comp_output4,
	MASTER_COMP_NUMPORTS
};

enum master_eq_ports {
	port_master_eq_channelstrip = 0,
	port_master_eq_in,
	port_master_eq_hi_gain,
	port_master_eq_mid_gain,
	port_master_eq_lo_gain,
	port_master_eq_input1,
	port_master_eq_input2,
	port_master_eq_input3,
	port_master_eq_input4,
	port_master_eq_output1,
	port_master_eq_output2,
	port_master_eq_output3,
	port_master_eq_output4,
	MASTER_EQ_NUMPORTS
};

enum master_post_ports {
	port_master_post_channelstrip = 0,
	port_master_post_limiter_in,
	port_master_post_limiter_redux,
	port_master_post_phase_lowside,
	port_master_post_phase_highside,
	port_master_post_k14,
	port_master_post_latency,
	port_master_post_input1,
	port_master_post_input2,
	port_master_post_input3,
	port_master_post_input4,
	port_master_post_output1,
	port_master_post_output2,
	port_master_post_output3,
	port_master_post_output4,
	MASTER_POST_NUMPORTS
};

enum mixbus_comp_ports {
	port_mixbus_comp_channelstrip = 0,
	port_mixbus_comp_in,
	port_mixbus_comp_thresh,
	port_mixbus_comp_speed,
	port_mixbus_comp_mode,
	port_mixbus_comp_makeup,
	port_mixbus_comp_redux,
	port_mixbus_comp_input1,
	port_mixbus_comp_input2,
	port_mixbus_comp_input3,
	port_mixbus_comp_input4,
	port_mixbus_comp_output1,
	port_mixbus_comp_output2,
	port_mixbus_comp_output3,
	port_mixbus_comp_output4,
	MIXBUS_COMP_NUMPORTS
};

enum mixbus_eq_ports {
	port_mixbus_eq_channelstrip = 0,
	port_mixbus_eq_in,
	port_mixbus_eq_hi_gain,
	port_mixbus_eq_mid_gain,
	port_mixbus_eq_lo_gain,
	port_mixbus_eq_input1,
	port_mixbus_eq_input2,
	port_mixbus_eq_input3,
	port_mixbus_eq_input4,
	port_mixbus_eq_output1,
	port_mixbus_eq_output2,
	port_mixbus_eq_output3,
	port_mixbus_eq_output4,
	MIXBUS_EQ_NUMPORTS
};

/* ****************************************************************************
 * Mixbus v6 ports
 */

/* common with mixbus 3-5 */
#define m6_channel_comp_ports channel_comp_ports
#define m6_channel_eq_ports channel_eq_ports
#define m6_mixbus_comp_ports mixbus_comp_ports
#define m6_mixbus_eq_ports mixbus_eq_ports
#define m6_master_comp_ports master_comp_ports;
#define m6_master_eq_ports master_eq_ports
#define m6_master_post_ports master_post_ports

// NO chan_post_ports in v6
// NO mixbus_post_ports in v6.

#define M6_LADSPA_ID_channel_pre  (9300)
#define M6_LADSPA_ID_channel_eq   (9399)
#define M6_LADSPA_ID_channel_comp (9302)
#define M6_LADSPA_ID_mixbus_pre   (9304)
#define M6_LADSPA_ID_mixbus_eq    (9305)
#define M6_LADSPA_ID_mixbus_comp  (9306)
#define M6_LADSPA_ID_master_pre   (9336)
#define M6_LADSPA_ID_master_eq    (9337)
#define M6_LADSPA_ID_master_comp  (9338)
#define M6_LADSPA_ID_master_post  (9339)
#define M6_LADSPA_ID_pan_amp      (9396)

/* v5-ID = 9300 */
/* v6-ID = 9300 */
enum m6_channel_pre_ports {
	m6_port_channel_pre_input1 = 0,
	m6_port_channel_pre_output1,
	M6_CHAN_PRE_NUMPORTS
};

/* v5-ID: (Index%4 == 0) && (Index > 3 && Index < 36) || (Index > 39 && Index < 57)
 * v6-ID: 9304
 */
enum m6_mixbus_pre_ports {
	m6_port_mixbus_pre_tape_drive = 0,
	m6_port_mixbus_pre_stereo_width,
	m6_port_mixbus_pre_sat_meter,
	m6_port_mixbus_pre_input1,
	m6_port_mixbus_pre_input2,
	m6_port_mixbus_pre_output1,
	m6_port_mixbus_pre_output2,
	M6_MIXBUS_PRE_NUMPORTS
};

/* v5-ID = 9336
 * v6-ID = 9336 */
enum m6_master_pre_ports {
	m6_port_master_pre_tape_drive = 0,
	m6_port_master_pre_sat_meter,
	m6_port_master_pre_input1,
	m6_port_master_pre_input2,
	m6_port_master_pre_output1,
	m6_port_master_pre_output2,
	M6_MASTER_PRE_NUMPORTS
};

/* v6-ID = 9396 */
enum m6_pan_amp_ports {
  port_pamamp_panstereo = 0,
  port_pamamp_target_gain,
  port_pamamp_target_pan,
  port_pamamp_silent,
  port_pamamp_input1,
  port_pamamp_input2,
  port_pamamp_output1,
  port_pamamp_output2,
  PAN_AMP_NUMPORTS
};

#define COMP_MODE_CNT 4


/* ****************************************************************************
 * Mixbus v2 ports  (for loading old sessions )
 */

enum masterPorts {
	eMstrPort_Drive = 0,
	eMstrPort_LookAhead,
	eMstrPort_EQ1_Level,
	eMstrPort_EQ2_Level,
	eMstrPort_EQ3_Level,
	eMstrPort_EQ_In,
	eMstrPort_Comp_in,
	eMstrPort_Comp_thresh,
	eMstrPort_Comp_makeup,
	eMstrPort_Mode,
	eMstrPort_Speed,
	eMstrPort_L_Input,
	eMstrPort_R_Input,
	eMstrPort_L_Output,
	eMstrPort_R_Output,
	eMstrPort_Drive_Meter,
	eMstrPort_Output_Meter,
	eMstrPort_Comp_redux,
	eMstrPort_Lim_redux,
	eMstrPort_Lim_Defeat,
	eMstrPort_Input_trim,
	eMstrPort_Correlation_low,
	eMstrPort_Correlation_hi,
	eMstrPort_Num_Ports
};

enum eAuxPorts {
	eAuxPort_Drive = 0,
	eAuxPort_Balance,
	eAuxPort_EQ1_Level,
	eAuxPort_EQ2_Level,
	eAuxPort_EQ3_Level,
	eAuxPort_EQ_In,
	eAuxPort_Comp_in,
	eAuxPort_Comp_thresh,
	eAuxPort_Comp_makeup,
	eAuxPort_Comp_Mode,
	eAuxPort_Comp_Speed,
	eAuxPort_Comp_Sidechain,
	eAuxPort_ToMaster,
	eAuxPort_L_Input,
	eAuxPort_R_Input,
	eAuxPort_L_Output,
	eAuxPort_R_Output,
	eAuxPort_Output_Meter,
	eAuxPort_Comp_redux,
	eAuxPort_Num_Ports
};

enum chanPorts {
	eChanPort_Input_trim = 0,
	eChanPort_Pan,
	eChanPort_Main_On,
	eChanPort_Aux1_Level,
	eChanPort_Aux1_Assign,
	eChanPort_Aux2_Level,
	eChanPort_Aux2_Assign,
	eChanPort_Aux3_Level,
	eChanPort_Aux3_Assign,
	eChanPort_Aux4_Level,
	eChanPort_Aux4_Assign,
	eChanPort_HPF_Freq,
	eChanPort_Band1_Level,
	eChanPort_Band1_Freq,
	eChanPort_Band2_Level,
	eChanPort_Band2_Freq,
	eChanPort_Band3_Level,
	eChanPort_Band3_Freq,
	eChanPort_EQ_In,
	eChanPort_Comp_in,
	eChanPort_Comp_thresh,
	eChanPort_Comp_makeup,
	eChanPort_Comp_Mode,
	eChanPort_Comp_Speed,
	eChanPort_L_Input,
	eChanPort_R_Input,
	eChanPort_L_Output,
	eChanPort_R_Output,
	eChanPort_Comp_redux,
	eChanPort_Aux5_Level,
	eChanPort_Aux5_Assign,
	eChanPort_Aux6_Level,
	eChanPort_Aux6_Assign,
	eChanPort_Aux7_Level,
	eChanPort_Aux7_Assign,
	eChanPort_Aux8_Level,
	eChanPort_Aux8_Assign,
	eChanPort_Num_Ports
};

static double
db_to_coeff (double db)
{
	if (db <= -80) { return 0; }
	else if (db >=  20) { return 10; }
	return pow (10., .05 * db);
}

static void
set_automation_control_value (XMLNode *node, int v2_id, std::shared_ptr<ARDOUR::AutomationControl> ac, double add = 0.0, double div = 1.0)
{
	XMLNodeList innerList = node->children ();
	XMLNodeConstIterator innerIter;
	for (innerIter = innerList.begin(); innerIter != innerList.end(); ++innerIter) {
		if ((*innerIter)->name() == "port") {
			const XMLProperty *prop;
			if ((prop = (*innerIter)->property ("number")) != 0) {
				if (atoi(prop->value().c_str()) == v2_id) {
					if ((prop = (*innerIter)->property ("value")) != 0) {
						float val = atof(prop->value().c_str());
						if (div == 0) {
							val = db_to_coeff (val);
						} else {
							val = (val + add) / div;
						}
						ac->set_value (val, Controllable::NoGroup);
					}
				}
			}
		}
	}
}

static void
set_controllable_val (XMLNode *node, int v2_id, std::shared_ptr<PluginInsert> plugin, int id)
{
	std::shared_ptr<ARDOUR::AutomationControl> c = std::dynamic_pointer_cast<ARDOUR::AutomationControl>(plugin->control(Evoral::Parameter(PluginAutomation, 0, id)));
	if (c) {
		set_automation_control_value (node, v2_id, c);
	}
}

void
Route::convert_trim (XMLNode *node, int v2_id)
{
	XMLNodeList innerList = node->children ();
	XMLNodeConstIterator innerIter;
	for (innerIter = innerList.begin(); innerIter != innerList.end(); ++innerIter) {
		if ((*innerIter)->name() == "port") {
			const XMLProperty *prop;
			if ((prop = (*innerIter)->property ("number")) != 0) {
				if (atoi (prop->value().c_str()) == v2_id) {
					if ((prop = (*innerIter)->property ("value")) != 0) {
						float val = atof(prop->value().c_str());
						if (_trim)
							_trim_control->set_value (dB_to_coefficient(val), Controllable::NoGroup);
					}
				}
			}
		}
	}
}

void
Route::convert_mb2_ladspa (XMLNode *node)
{
	if (is_master()) {
		convert_trim(node, eMstrPort_Input_trim);
		set_controllable_val(node, eMstrPort_Drive,       _ch_pre,  m6_port_master_pre_tape_drive);
		set_controllable_val(node, eMstrPort_EQ3_Level,   _ch_eq,   port_master_eq_lo_gain);
		set_controllable_val(node, eMstrPort_EQ2_Level,   _ch_eq,   port_master_eq_mid_gain);
		set_controllable_val(node, eMstrPort_EQ1_Level,   _ch_eq,   port_master_eq_hi_gain);
		set_controllable_val(node, eMstrPort_EQ_In,       _ch_eq,   port_master_eq_in);
		set_controllable_val(node, eMstrPort_Comp_thresh, _ch_comp, port_master_comp_thresh);
		set_controllable_val(node, eMstrPort_Comp_makeup, _ch_comp, port_master_comp_makeup);
		set_controllable_val(node, eMstrPort_Mode,        _ch_comp, port_master_comp_mode);
		set_controllable_val(node, eMstrPort_Speed,       _ch_comp, port_master_comp_speed);
		set_controllable_val(node, eMstrPort_Comp_in,     _ch_comp, port_master_comp_in);
		set_controllable_val(node, eMstrPort_Lim_Defeat,  _ch_post, port_master_post_limiter_in);
	} else if (_mixbus) {
		set_controllable_val(node, eAuxPort_Drive,       _ch_pre,  m6_port_mixbus_pre_tape_drive);
		set_controllable_val(node, eAuxPort_EQ1_Level,   _ch_eq,   port_mixbus_eq_lo_gain);
		set_controllable_val(node, eAuxPort_EQ2_Level,   _ch_eq,   port_mixbus_eq_mid_gain);
		set_controllable_val(node, eAuxPort_EQ3_Level,   _ch_eq,   port_mixbus_eq_hi_gain);
		set_controllable_val(node, eAuxPort_EQ_In,       _ch_eq,   port_mixbus_eq_in);
		set_controllable_val(node, eAuxPort_Comp_thresh, _ch_comp, port_mixbus_comp_thresh);
		set_controllable_val(node, eAuxPort_Comp_makeup, _ch_comp, port_mixbus_comp_makeup);
		set_controllable_val(node, eAuxPort_Comp_Mode,   _ch_comp, port_mixbus_comp_mode);
		set_controllable_val(node, eAuxPort_Comp_Speed,  _ch_comp, port_mixbus_comp_speed);
		set_controllable_val(node, eAuxPort_Comp_in,     _ch_comp, port_mixbus_comp_in);

		set_automation_control_value (node, eAuxPort_Balance,      pan_azimuth_control (), 90, 180);
		set_automation_control_value (node, eAuxPort_ToMaster,     master_send_enable_controllable ());
	} else {
		convert_trim(node, eChanPort_Input_trim);
		set_controllable_val(node, eChanPort_HPF_Freq,    _ch_eq,   port_channel_hp_freq);
#ifndef MIXBUS32C // NOTE: in 32C we ignore the EQ settings from MB.  You are using the 32C EQ now.
		set_controllable_val(node, eChanPort_Band1_Level, _ch_eq,   port_channel_eq_lo_gain);
		set_controllable_val(node, eChanPort_Band1_Freq,  _ch_eq,   port_channel_eq_lo_freq);
		set_controllable_val(node, eChanPort_Band2_Level, _ch_eq,   port_channel_eq_mid_gain);
		set_controllable_val(node, eChanPort_Band2_Freq,  _ch_eq,   port_channel_eq_mid_freq);
		set_controllable_val(node, eChanPort_Band3_Level, _ch_eq,   port_channel_eq_hi_gain);
		set_controllable_val(node, eChanPort_Band3_Freq,  _ch_eq,   port_channel_eq_hi_freq);
		set_controllable_val(node, eChanPort_EQ_In,       _ch_eq,   port_channel_eq_in);
#endif
		set_controllable_val(node, eChanPort_Comp_in,     _ch_comp, port_channel_comp_in);
		set_controllable_val(node, eChanPort_Comp_thresh, _ch_comp, port_channel_comp_thresh);
		set_controllable_val(node, eChanPort_Comp_makeup, _ch_comp, port_channel_comp_makeup);
		set_controllable_val(node, eChanPort_Comp_Mode,   _ch_comp, port_channel_comp_mode);
		set_controllable_val(node, eChanPort_Comp_Speed,  _ch_comp, port_channel_comp_speed);

		set_automation_control_value (node, eChanPort_Pan,         pan_azimuth_control (), 90, 180);
		set_automation_control_value (node, eChanPort_Main_On,     master_send_enable_controllable ());
		set_automation_control_value (node, eChanPort_Aux1_Level,  send_level_controllable (0), 0, 0);
		set_automation_control_value (node, eChanPort_Aux1_Assign, send_enable_controllable (0));
		set_automation_control_value (node, eChanPort_Aux2_Level,  send_level_controllable (1), 0, 0);
		set_automation_control_value (node, eChanPort_Aux2_Assign, send_enable_controllable (1));
		set_automation_control_value (node, eChanPort_Aux3_Level,  send_level_controllable (2), 0, 0);
		set_automation_control_value (node, eChanPort_Aux3_Assign, send_enable_controllable (2));
		set_automation_control_value (node, eChanPort_Aux4_Level,  send_level_controllable (3), 0, 0);
		set_automation_control_value (node, eChanPort_Aux4_Assign, send_enable_controllable (3));
		set_automation_control_value (node, eChanPort_Aux5_Level,  send_level_controllable (4), 0, 0);
		set_automation_control_value (node, eChanPort_Aux5_Assign, send_enable_controllable (4));
		set_automation_control_value (node, eChanPort_Aux6_Level,  send_level_controllable (5), 0, 0);
		set_automation_control_value (node, eChanPort_Aux6_Assign, send_enable_controllable (5));
		set_automation_control_value (node, eChanPort_Aux7_Level,  send_level_controllable (6), 0, 0);
		set_automation_control_value (node, eChanPort_Aux7_Assign, send_enable_controllable (6));
		set_automation_control_value (node, eChanPort_Aux8_Level,  send_level_controllable (7), 0, 0);
		set_automation_control_value (node, eChanPort_Aux8_Assign, send_enable_controllable (7));

	}
}
#endif
