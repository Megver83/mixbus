#ifndef _ardour_mixbus_send_h_
#define _ardour_mixbus_send_h_

#include <boost/array.hpp>

#include "ardour/automation_control.h"
#include "ardour/ardour.h"
#include "ardour/processor.h"
#include "ardour/send.h"

namespace ARDOUR {

class MuteMaster;
class Pannable;
class Delayline;
#ifdef MIXBUS32C
class MixbusPanAmp;
#else
class MixbusAmp;
class MixbusPan;
#endif

class LIBARDOUR_API MixbusSend : public Processor, public LatentSend
{
public:
	enum Role {
		Channel,
		Mixbus,
		FXBus
	};

	MixbusSend (Session& s, Role r) : Processor (s, "Mixbus Send", Temporal::AudioTime), _role (r) {}
	virtual ~MixbusSend () {}

	virtual std::shared_ptr<AutomationControl> send_gain_ctrl (size_t mb) const = 0;
	virtual std::shared_ptr<AutomationControl> send_enable_ctrl (size_t mb) const = 0;
	virtual std::shared_ptr<AutomationControl> send_pan_enable_ctrl (size_t mb) const = 0;
	virtual std::shared_ptr<AutomationControl> send_pan_ctrl (size_t mb) const = 0;
	virtual std::shared_ptr<AutomationControl> master_pan_ctrl () const = 0;
	virtual std::shared_ptr<AutomationControl> master_send_enable_ctrl () const = 0;

	virtual size_t n_busses () const = 0;
	virtual BufferSet const& get_buffers (size_t bus = 0) const = 0;

	Role role () const { return _role; }

	/* Processor */
	virtual void run (BufferSet& bufs, samplepos_t start_sample, samplepos_t end_sample, double speed, pframes_t nframes, bool) = 0;
	virtual samplecnt_t signal_latency () const {
		if (!_pending_active) {
			return 0;
		}
		if (_delay_out > _delay_in) {
			return _delay_out - _delay_in;
		}
		return 0;
	}
	virtual samplecnt_t send_delay (size_t) const = 0;
	virtual samplecnt_t master_delay () const = 0;

	std::string describe_parameter (Evoral::Parameter);

	void update_delaylines (bool) {}

	ChannelStripType channelstrip () const { return Processor::MBSend ; }

private:
	/* disallow copy construction */
	MixbusSend (const MixbusSend&);

	Role _role;
};

template<std::size_t N>
class LIBARDOUR_API MixbusSendImpl : public MixbusSend
{
public:
	MixbusSendImpl (Session&, std::shared_ptr<MuteMaster>, std::shared_ptr<Pannable>, MixbusSend::Role role = Channel, size_t busoffset = 1);
	virtual ~MixbusSendImpl ();

	XMLNode& state () const;
	int set_state (const XMLNode&, int version);

	size_t n_busses () const { return N; }

	/* Processor */
	bool can_support_io_configuration (const ChanCount& in, ChanCount& out);
	bool configure_io (ChanCount in, ChanCount out);
	int  set_block_size (pframes_t);

	void run (BufferSet& bufs, samplepos_t start_sample, samplepos_t end_sample, double speed, pframes_t nframes, bool);

	samplecnt_t send_delay (size_t b) const;
	samplecnt_t master_delay () const;

	/* Send API */
	void set_delay_in (samplecnt_t);
	void set_delay_out (samplecnt_t, size_t);

	/* Control API */
	std::shared_ptr<AutomationControl> send_gain_ctrl (size_t mb) const {
		return _send_gain_ctrl[mb - 1];
	}
	std::shared_ptr<AutomationControl> send_enable_ctrl (size_t mb) const {
		return _send_enable_ctrl[mb - 1];
	}
	std::shared_ptr<AutomationControl> send_pan_enable_ctrl (size_t mb) const {
#ifdef MIXBUS32C
		return _send_pan_enable_ctrl[mb - 1];
#else
		return std::shared_ptr<AutomationControl> ();
#endif
	}
	std::shared_ptr<AutomationControl> send_pan_ctrl (size_t mb) const {
#ifdef MIXBUS32C
		return _send_pan_ctrl[mb - 1];
#else
		return std::shared_ptr<AutomationControl> ();
#endif
	}
	std::shared_ptr<AutomationControl> master_send_enable_ctrl () const {
		return _master_send_enable_ctrl;
	}
	std::shared_ptr<AutomationControl> master_pan_ctrl () const {
		return _master_pan_ctrl;
	}

	/* Internal Return Access */
	BufferSet const& get_buffers (size_t mb) const;

private:
	/* disallow copy construction */
	MixbusSendImpl (const MixbusSendImpl&);

	void setup_controls (std::shared_ptr<Pannable>);
	void update_delaylines (bool unused = true);

	size_t _busoffset;

	std::shared_ptr<MuteMaster> _mute_master;
	std::shared_ptr<DelayLine>  _thru_delay;

	std::shared_ptr<DelayLine>         _master_delay;
	samplecnt_t                          _master_delay_out;
	BufferSet                            _master_buf;
#ifdef MIXBUS32C
	std::shared_ptr<MixbusPanAmp>      _thru_pan;
	std::shared_ptr<MixbusPanAmp>      _master_amp;
#else
	std::shared_ptr<MixbusPan>         _master_pan;
	std::shared_ptr<MixbusAmp>         _master_amp;
#endif
	std::shared_ptr<AutomationControl> _master_send_enable_ctrl;
	std::shared_ptr<AutomationControl> _master_pan_ctrl;

	boost::array<std::shared_ptr<AutomationControl>, N> _send_gain_ctrl;
	boost::array<std::shared_ptr<AutomationControl>, N> _send_enable_ctrl;

	boost::array<std::shared_ptr<AutomationControl>, N> _send_pan_enable_ctrl;
	boost::array<std::shared_ptr<AutomationControl>, N> _send_pan_ctrl;

	boost::array<BufferSet, N>                       _mixbufs;
#ifdef MIXBUS32C
	boost::array<std::shared_ptr<MixbusPanAmp>, N> _amps;
#else
	boost::array<std::shared_ptr<MixbusAmp>, N>    _amps;
#endif
	boost::array<std::shared_ptr<DelayLine>, N>    _send_delays;
	boost::array<samplecnt_t, N>                     _delay_out_bus;
};

} // namespace ARDOUR
#endif
