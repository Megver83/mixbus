/*
 * Copyright (C) 2019 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef _ardour_mixbus_return_h_
#define _ardour_mixbus_return_h_

#include "ardour/ardour.h"
#include "ardour/processor.h"
#include "ardour/buffer_set.h"

namespace ARDOUR {

#ifdef MIXBUS
class MixbusSend;
#endif

class LIBARDOUR_API MixbusReturn : public Processor
{
public:
	enum Role {
		Mixbus,
		FXBus,
		Master
	};

	MixbusReturn (Session&, Role r, size_t mixbus);

	/* Processor */
	void run (BufferSet& bufs, samplepos_t start_sample, samplepos_t end_sample, double speed, pframes_t nframes, bool);
	bool configure_io (ChanCount, ChanCount);
	bool can_support_io_configuration (const ChanCount& in, ChanCount& out);
	void set_playback_offset (samplecnt_t cnt);

	/* Mixbus API */
	Role role () const { return _role; }
	int mixbus () const { return _mixbus; }

	void set_mute_chn_cycle () {
		_mute_channel_assigns = true;
	}

protected:
	XMLNode& state () const;

private:
	Role   _role;
	size_t _mixbus; /* 0 for master, but 1 .. NUM_MIXBUSES  for mixbus */

	/* mixbus solo-logic, master-bus: ignore sends for non-mixusses */
	bool _mute_channel_assigns;
#if 0 // TODO cache
	std::list<MixbusSend*> _mb_sends;
	Glib::Threads::Mutex _sends_mutex;
#endif
};

} // namespace ARDOUR

#endif
