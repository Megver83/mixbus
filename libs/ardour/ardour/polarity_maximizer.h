/*
    Copyright (C) 2016 Robin Gareus

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef __ardour_polarity_maximizer_h__
#define __ardour_polarity_maximizer_h__

#include <list>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#include "pbd/signals.h"
#include <vamp-hostsdk/Plugin.h>

#include "ardour/audioanalyser.h"
#include "ardour/libardour_visibility.h"
#include "ardour/types.h"

namespace ARDOUR {

class Route;
class AudioPlaylist;

namespace DSP {
	class Biquad;
}

class LIBARDOUR_API PolarityMaximizer : public boost::noncopyable
{
public:
	PolarityMaximizer (float sample_rate = 48000);
	~PolarityMaximizer();

	struct Src {
		public:
			Src (std::shared_ptr<AudioPlaylist> pl, std::shared_ptr<Route> r)
				: playlist (pl), route (r) {}
			std::shared_ptr<AudioPlaylist> playlist;
			std::shared_ptr<Route> route;
	};

	typedef std::list<Src> SrcList;

	int run (const SrcList&, const std::list<TimelineRange>& range);

	std::vector<bool> results () const { return _results; }
	float result_gain () const { return _result_gain; }

	void cancel () { _canceled = true; }
	bool canceled () const { return _canceled; }

	void set_total_frames (samplecnt_t p) { _samples_end = p; }
	void set_total_progress (float f, uint32_t o) { _prog_factor = f; _prog_offset = o; }
	PBD::Signal2<void, samplecnt_t, samplecnt_t> Progress;

	void set_hpf (float f, float q)
	{ _hp_freq = f; _hp_qual = q; }

	void set_lsf (float f, float q, float g)
	{ _ls_gain = g; _ls_freq = f; _ls_qual = q; }

	void set_mode (uint32_t m) { _mode = m ;}
	void set_norm (uint32_t n) { _norm = n ;}

	float calc_gain (const std::vector<bool>&) const;

private:
	int initialize_plugin (std::string key);

	typedef std::shared_ptr<DSP::Biquad> BiquadPtr;
	typedef std::vector<BiquadPtr> Filt;

	Vamp::Plugin*      _plugin;
	float              _sample_rate;
	samplecnt_t        _bufsize;
	samplecnt_t        _samples_read;
	samplecnt_t        _samples_end;
	float              _prog_factor;
	uint32_t           _prog_offset;
	bool               _canceled;
	std::vector<bool>  _results;
	std::vector<float> _matrix_terms;
	float              _result_gain;

	float              _hp_freq;
	float              _hp_qual;

	float              _ls_freq;
	float              _ls_qual;
	float              _ls_gain;

	uint32_t           _mode;
	uint32_t           _norm;

};

} /* namespace */

#endif /* __ardour_polarity_maximizer_h__ */

