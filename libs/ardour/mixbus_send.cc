
#include "ardour/audio_buffer.h"
#include "ardour/audioengine.h"
#include "ardour/dB.h"
#include "ardour/delayline.h"
#include "ardour/internal_return.h"
#include "ardour/mixbus_send.h"
#include "ardour/mute_master.h"
#include "ardour/pannable.h"
#include "ardour/plugin.h"
#include "ardour/route.h"
#include "ardour/session.h"
#include "ardour/types.h"

#include "pbd/i18n.h"

using namespace PBD;
using namespace ARDOUR;
using namespace std;

namespace ARDOUR {
#ifdef MIXBUS32C
class MixbusPanAmp {
public:
	MixbusPanAmp (Session& s)
	{
		_p = find_plugin (s, "9396", ARDOUR::LADSPA);
		static bool warned = false;
		if (!warned && !_p) {
			warned = true;
			printf("harrison channelstrip 9396 missing\n");
		}
		reset ();
	}

	void reset () {
		if (_p) {
			_p->flush ();
		}
	}

	void run (BufferSet& bufs, pframes_t n_samples, bool stereo, gain_t target_gain, float target_pan)
	{
		if (!_p) {
			bufs.silence (n_samples, 0);
			return;
		}

		_p->set_parameter(0, stereo ? 1.f : 0.f, 0);
		_p->set_parameter(1, target_gain, 0);
		_p->set_parameter(2, target_pan, 0);

		ChanMapping id (ChanCount (DataType::AUDIO, 2));
		_p->connect_and_run (bufs, 0, n_samples, 1.0, id, id, n_samples, 0);

		if (_p->get_parameter(3) > 0) {
			bufs.silence (n_samples, 0);
		}
	}
private:
	std::shared_ptr<Plugin> _p;
};

#else

class MixbusAmp {
public:
	MixbusAmp (Session& s)
	{
		_p = find_plugin (s, "9391", ARDOUR::LADSPA);
		static bool warned = false;
		if (!warned && !_p) {
			warned = true;
			printf("harrison channelstrip 9391 missing\n");
		}
		reset ();
	}

	void reset () {
		if (_p) {
			_p->flush ();
		}
	}

	void run (BufferSet& bufs, pframes_t n_samples, bool stereo, gain_t target_gain)
	{
		if (!_p) {
			bufs.silence (n_samples, 0);
			return;
		}

		_p->set_parameter(0, target_gain, 0);

		ChanMapping id (ChanCount (DataType::AUDIO, 2));
		_p->connect_and_run (bufs, 0, n_samples, 1.0, id, id, n_samples, 0);

		if (_p->get_parameter(1) > 0) {
			bufs.silence (n_samples, 0);
		}
	}
private:
	std::shared_ptr<Plugin> _p;
};

class MixbusPan {
public:
	MixbusPan (Session& s)
	{
		_p = find_plugin (s, "9390", ARDOUR::LADSPA);
		static bool warned = false;
		if (!warned && !_p) {
			warned = true;
			printf("harrison channelstrip 9390 missing\n");
		}
		reset ();
	}

	void reset () {
		if (_p) {
			_p->flush ();
		}
	}

	void run (BufferSet& bufs, pframes_t n_samples, bool stereo, float target_pan)
	{
		if (!_p) {
			bufs.silence (n_samples, 0);
			return;
		}

		_p->set_parameter(0, stereo ? 1.f : 0.f, 0);
		_p->set_parameter(1, target_pan, 0);

		ChanMapping id (ChanCount (DataType::AUDIO, 2));
		_p->connect_and_run (bufs, 0, n_samples, 1.0, id, id, n_samples, 0);
	}
private:
	std::shared_ptr<Plugin> _p;
};

#endif

}

/* ****************************************************************************/

std::string
MixbusSend::describe_parameter (Evoral::Parameter param)
{
	if (param.type() == PluginAutomation) {
		std::shared_ptr<AutomationControl> c (automation_control (param));
		if (c && !c->desc().label.empty()) {
			return c->desc().label;
		}
	}
	return Automatable::describe_parameter(param);
}

/* ****************************************************************************/

class MixbusControl : public AutomationControl
{
public:

	MixbusControl(ARDOUR::Session&                          session,
	              const Evoral::Parameter&                  parameter,
	              const ParameterDescriptor&                desc,
	              std::shared_ptr<ARDOUR::AutomationList> list,
	              const std::string&                        name,
	              PBD::Controllable::Flag                   flags)
		: AutomationControl (session, parameter, desc, list, name, flags)
	{}

	double internal_to_interface (double val, bool rotary) const
	{
		if (_desc.type == BusSendLevel) {
			val = std::min (upper (), std::max (lower (), val));
			if (val == 0) {
				return 0;
			} else if (val < 1.0) {
				/* power mapping  [-75dB .. 0dB] -> [0 .. 0.5] */
				float db = 20 * log10f (val);
				db = .5 + (db / 150); // [-75 .. 0] -> [0 .. 0.5]
				val = (db * db) / (2 + 2 * db * db - 4 * db); // [0 .. 0.5] -> [0 .. 0.5]
#ifndef MIXBUS32C
				val *= 5.0 / 3.0; // [0 .. 0.5] ->  [0 .. 5/6]
#endif
			} else {
				/* dB linear [0dB .. +15dB] -> [0.5 .. 1.0]*/
				float db = 20 * log10f (val);
				val = 0.5 + db / 30;
#ifndef MIXBUS32C
				val = (val + 2.0) / 3.0; // scale [.5 .. 1.0] -> [5/6 .. 6/6]
#endif
			}
			val = std::max (0.0, std::min (1.0, val));
			return val;
		}
		return AutomationControl::internal_to_interface (val, rotary);
	}

	double interface_to_internal (double val, bool rotary) const
	{
		if (_desc.type == BusSendLevel) {
			val = std::max (0.0, std::min (1.0, val));
			if (
#ifdef MIXBUS32C
			    val <= .5
#else
			    val <= 5.0 / 6.0
#endif
			   ) {
#ifndef MIXBUS32C
				val *= 3.0 / 5.0; // scale [ 0 .. 5/6] -> [0 .. 0.5]
#endif
				/* exp mapping [0.0 .. 0.5] -> [-75dB .. 0dB]  (lower .. 1.0) */
				val = sqrt(val) / (sqrt(.5) + sqrt(val)); // [0 .. 0.5] -> [0 .. 0.5]
				val = 150 * (val - .5); // [0 .. 0.5] -> [-75 .. 0]
				val = pow (10.0, .05 * val);
			} else {
#ifndef MIXBUS32C
				val = 3 * val - 2.0; // scale [5/6 .. 6/6] -> [.5 .. 1.0]
#endif
				/* dB linear [0.5 .. 1.0] -> [0dB .. +15dB]  (1 .. upper) */
				val = pow (10.0, 1.5 * (val - .5));
			}
			val = std::min (upper (), std::max (lower (), val));
			return val;
		}
		return AutomationControl::interface_to_internal (val, rotary);
	}

	std::string get_user_string () const
	{
		if (_desc.type == PanAzimuthAutomation) {
			const double v = get_value();
			char buf[32];
			if (v == .5) {
				snprintf (buf, sizeof (buf), "Center");
			} else if (v == 0) {
				snprintf (buf, sizeof (buf), "Left");
			} else if (v == 1.0) {
				snprintf (buf, sizeof (buf), "Right");
			} else {
				snprintf (buf, sizeof (buf), "%.1f \u00B0", 180. * v - 90.);
			}
			return buf;
		}
		return  AutomationControl::get_user_string ();
	}

	void actually_set_value (double v, Controllable::GroupControlDisposition group_override)
	{
		v = std::min (upper (), std::max (lower (), v));
		AutomationControl::actually_set_value (v, group_override);
	}
};

/* ****************************************************************************/

template<std::size_t N>
MixbusSendImpl<N>::MixbusSendImpl (Session& s, std::shared_ptr<MuteMaster> mm, std::shared_ptr<Pannable> p, MixbusSend::Role role, size_t busoffset)
	: MixbusSend (s, role)
	, _busoffset (busoffset)
	, _mute_master (mm)
{
	_display_to_user = false;

	set_block_size (_session.engine ().samples_per_cycle ());
	setup_controls (p);

	for (uint32_t b = 0; b < N; ++b) {
		_delay_out_bus[b] = 0;
	}
	_master_delay_out = 0;
	_delay_in = 0;
}

template<std::size_t N>
MixbusSendImpl<N>::~MixbusSendImpl ()
{
}

static std::shared_ptr<AutomationControl>
forge_control (Session& s, uint32_t idx, int what, std::string const& name)
{
	Evoral::Parameter param (PluginAutomation, 0, idx);
	ParameterDescriptor desc;
	Controllable::Flag flag = Controllable::Flag (0);

	switch (what) {
		default:
		case 0:
			desc.type         = BusSendLevel;
			desc.lower        = pow (10.0, .05 * -75.0);
			desc.upper        = pow (10.0, .05 *  15.0);
			desc.normal       = 1.0;
			desc.logarithmic  = true;
			flag              = Controllable::GainLike;
			break;
		case 1:
			desc.type         = BusSendEnable;
			desc.lower        = 0.0;
			desc.upper        = 1.0;
			desc.normal       = 0.0;
			desc.toggled      = true;
			flag              = Controllable::Toggle;
			break;
		case 2:
			desc.type         = PanAzimuthAutomation;
			desc.lower        = 0.0;
			desc.upper        = 1.0;
			desc.normal       = 0.5;
			break;
	}

	desc.label = name;
	desc.update_steps ();

	std::shared_ptr<AutomationList> list (new AutomationList(param, desc, Temporal::AudioTime));
	std::shared_ptr<AutomationControl> c (new MixbusControl (s, param, desc, list, name, flag));
	return c;
}

template<std::size_t N>
void
MixbusSendImpl<N>::setup_controls (std::shared_ptr<Pannable> p)
{
	int idx = 0;

	_master_send_enable_ctrl = forge_control (_session, idx, 1, "Master Assign");
	_master_pan_ctrl         = forge_control (_session, ++idx, 2, "Master Pan");

	if (p) {
		p->replace_azimuth_control (_master_pan_ctrl);
	}

	add_control (_master_send_enable_ctrl);
	add_control (_master_pan_ctrl);

	_master_send_enable_ctrl->set_value_unchecked (1.0);
	_master_pan_ctrl->set_value_unchecked (0.5);

	for (uint32_t b = 0; b < N; ++b) {

		std::string mbn = string_compose (_("Mixbus %1"), b + _busoffset) + ": ";

		_send_enable_ctrl[b]     = forge_control (_session, ++idx, 1, mbn + _("Assign"));
		_send_gain_ctrl[b]       = forge_control (_session, ++idx, 0, mbn + _("Level"));
		_send_pan_enable_ctrl[b] = forge_control (_session, ++idx, 1, mbn + _("Pan Enable"));
		_send_pan_ctrl[b]        = forge_control (_session, ++idx, 2, mbn + _("Pan"));

		_send_gain_ctrl[b]->set_dependent_in_control (_send_enable_ctrl[b]);
		_send_pan_ctrl[b]->set_dependent_in_control (_send_pan_enable_ctrl[b]);

#ifdef MIXBUS32C
		_amps[b].reset (new MixbusPanAmp (_session));
#else
		_amps[b].reset (new MixbusAmp (_session));
		_send_pan_enable_ctrl[b]->set_flag (Controllable::HiddenControl);
		_send_pan_ctrl[b]->set_flag (Controllable::HiddenControl);
#endif

		add_control (_send_enable_ctrl[b]);
		add_control (_send_gain_ctrl[b]);
		add_control (_send_pan_enable_ctrl[b]);
		add_control (_send_pan_ctrl[b]);

		_send_gain_ctrl[b]->set_value_unchecked (1.0);
		_send_enable_ctrl[b]->set_value_unchecked (0);
		_send_pan_ctrl[b]->set_value_unchecked (0.5);
		_send_pan_enable_ctrl[b]->set_value_unchecked (0);

		_send_delays[b].reset (new DelayLine (_session, mbn + "Delay"));
		_send_delays[b]->configure_io (ChanCount (DataType::AUDIO, 2), ChanCount (DataType::AUDIO, 2));
	}

#ifdef MIXBUS32C
	_master_amp.reset (new MixbusPanAmp (_session));
	_thru_pan.reset   (new MixbusPanAmp (_session));
#else
	_master_pan.reset (new MixbusPan (_session));
	_master_amp.reset (new MixbusAmp (_session));
#endif
	_thru_delay.reset (new DelayLine (_session, "Thru-" + name ()));
	_master_delay.reset (new DelayLine (_session, "Master-" + name ()));
	_master_delay->configure_io (ChanCount (DataType::AUDIO, 2), ChanCount (DataType::AUDIO, 2));
}


template<std::size_t N>
bool
MixbusSendImpl<N>::can_support_io_configuration (const ChanCount& in, ChanCount& out)
{
	out = in;
	return true;
}

template<std::size_t N>
bool
MixbusSendImpl<N>::configure_io (ChanCount in, ChanCount out)
{
	set_block_size (_session.engine ().samples_per_cycle ());
	if (!_thru_delay->configure_io (in, out)) {
		return false;
	}
	return Processor::configure_io (in, out);
}

template<std::size_t N>
int
MixbusSendImpl<N>::set_block_size (pframes_t n_samples)
{
	_master_buf.ensure_buffers (ChanCount (DataType::AUDIO, 2), n_samples);
	for (uint32_t i = 0; i < N; ++i) {
		_mixbufs[i].ensure_buffers (ChanCount (DataType::AUDIO, 2), n_samples);
	}
	return 0;
}

template<std::size_t N>
BufferSet const&
MixbusSendImpl<N>::get_buffers (size_t mb) const {
	assert (mb < N + _busoffset);
	if (mb == 0) {
		return _master_buf;
	}
	return _mixbufs[mb - _busoffset];
}

template<std::size_t N>
void
MixbusSendImpl<N>::run (BufferSet& bufs, samplepos_t start_sample, samplepos_t end_sample, double speed, pframes_t n_samples, bool)
{
#if 0 // always active !
	if (!_active && !_pending_active) {
		return;
	}
#endif
	_active = _pending_active;

	for (BufferSet::audio_iterator b = _master_buf.audio_begin(); b != _master_buf.audio_end(); ++b) {
		b->prepare ();
	}
	for (uint32_t i = 0; i < N; ++i) {
		for (BufferSet::audio_iterator b = _mixbufs[i].audio_begin(); b != _mixbufs[i].audio_end(); ++b) {
			b->prepare ();
		}
	}

	std::shared_ptr<ControlList const> cl = _automated_controls.reader ();
	for (auto const& ci : *cl) {
		AutomationControl& c = *(ci.get());
		std::shared_ptr<const Evoral::ControlList> clist (c.list());
		/* we still need to check for Touch and Latch */
		if (clist && (static_cast<AutomationList const&> (*clist)).automation_playback ()) {
			bool valid;
			const float val = c.list()->rt_safe_eval (timepos_t(start_sample), valid);
			if (valid) {
				c.set_value_unchecked(val);
			}
		}
	}

	const gain_t mute_gain   = _mute_master->mute_gain_at (MuteMaster::Main);
	const gain_t master_gain = _master_send_enable_ctrl->get_value() == 0 ? GAIN_COEFF_ZERO : mute_gain;
	const float  master_pan  = _master_pan_ctrl->get_value();

	const uint32_t n_audio = bufs.count().n_audio ();

	/* prepare buffers */
	switch (n_audio) {
		case 0:
			/* midi only, no audio */
			_master_buf.silence (n_samples, 0);
			for (uint32_t b = 0; b < N; ++b) {
				_mixbufs[b].silence (n_samples, 0);
			}
			break;
		case 1:
			/* mono */
#ifdef MIXBUS32C
			for (uint32_t b = 0; b < N; ++b) {
				_mixbufs[b].get_audio (0).read_from (bufs.get_audio (0), n_samples);
				_mixbufs[b].get_audio (1).read_from (bufs.get_audio (0), n_samples);
			}
#endif
			/* Mono: leave through path alone -- except for mute */
			_master_buf.get_audio (0).read_from (bufs.get_audio (0), n_samples);
			_master_buf.get_audio (1).read_from (bufs.get_audio (0), n_samples);
#ifdef MIXBUS32C
			/* 32C: pan and apply master gain */
			_master_amp->run (_master_buf, n_samples, false, master_gain, master_pan);
#else
			/* Mixbus: pan only - do not apply master-gain here, the pre-panned
			 * _master_buf is used for mixbus-assign sends */
			_master_pan->run (_master_buf, n_samples, false, master_pan);
#endif
			break;
		default:
		case 2:
			/* stereo */
#ifdef MIXBUS32C
			/* 32C: just copy data to mixbus-sends. Gain-statging and panning
			 * happens separately per send */
			for (uint32_t b = 0; b < N; ++b) {
				_mixbufs[b].get_audio (0).read_from (bufs.get_audio (0), n_samples);
				_mixbufs[b].get_audio (1).read_from (bufs.get_audio (1), n_samples);
			}
			/* 32C: cannot reuse PanAmp for master pan+level vs. thru pan-only */
			_master_buf.get_audio (0).read_from (bufs.get_audio (0), n_samples);
			_master_buf.get_audio (1).read_from (bufs.get_audio (1), n_samples);
			_thru_pan->run (bufs, n_samples, true, /*mute_gain*/ 1.0, master_pan);
			/* 32C: pan and apply master gain */
			_master_amp->run (_master_buf, n_samples, true, master_gain, master_pan);
#else
			/* MB: directly pan bufs, re-use for master-out (and sends) */
			_master_pan->run (bufs, n_samples, n_audio > 1, master_pan);
			_master_buf.get_audio (0).read_from (bufs.get_audio (0), n_samples);
			_master_buf.get_audio (1).read_from (bufs.get_audio (1), n_samples);
			/* do not apply master-gain here, the pre-panned _master_buf is
			 * used for mixbus-assign sends */
#endif
			break;
	}

	/* copy data to all mixbus-assigns */
	for (uint32_t b = 0; b < N; ++b) {
#ifndef MIXBUS32C
		/* MB: tap off pre-panned data */
		_mixbufs[b].get_audio (0).read_from (_master_buf.get_audio (0), n_samples);
		_mixbufs[b].get_audio (1).read_from (_master_buf.get_audio (1), n_samples);
#endif

		const gain_t tg = (_send_enable_ctrl[b]->get_value() == 0) ? GAIN_COEFF_ZERO : (mute_gain * _send_gain_ctrl[b]->get_value());
#ifdef MIXBUS32C
		/* 32C: per bus-assign panning + bus send level*/
		const float  tp = (_send_pan_enable_ctrl[b]->get_value() == 0) ? master_pan : _send_pan_ctrl[b]->get_value();
		_amps[b]->run (_mixbufs[b], n_samples, n_audio > 1, tg, tp);
#else
		/* MB: bus send level only */
		_amps[b]->run (_mixbufs[b], n_samples, n_audio > 1, tg);
#endif
		_send_delays[b]->run (_mixbufs[b], start_sample, end_sample, speed, n_samples, true);
	}

#ifndef MIXBUS32C
	/* master-gain - Mixbus only */
	_master_amp->run (_master_buf, n_samples, n_audio > 1, master_gain);
#endif

	_master_delay->run (_master_buf, start_sample, end_sample, speed, n_samples, true);
	_thru_delay->run (bufs, start_sample, end_sample, speed, n_samples, true);
}

/* *****************************************************************************
 * Latency Compensation
 */

template<std::size_t N>
samplecnt_t
MixbusSendImpl<N>::send_delay (size_t b) const
{
	return _send_delays[b]->delay ();
}

template<std::size_t N>
samplecnt_t
MixbusSendImpl<N>::master_delay () const
{
	return _master_delay->delay ();
}

template<std::size_t N>
void
MixbusSendImpl<N>::set_delay_in (samplecnt_t delay)
{
	if (_delay_in == delay) {
		return;
	}
	_delay_in = delay;
	update_delaylines ();
}

template<std::size_t N>
void
MixbusSendImpl<N>::set_delay_out (samplecnt_t delay, size_t mb)
{
	assert (mb < N + _busoffset);
	if (mb == 0) {
		if (_master_delay_out == delay) {
			return;
		}
		_master_delay_out = delay;
	} else {
		mb -= _busoffset;
		if (_delay_out_bus[mb] == delay) {
			return;
		}
		_delay_out_bus[mb] = delay;
	}

	samplecnt_t max_delay = _master_delay_out;
	for (uint32_t b = 0; b < N; ++b) {
		max_delay = std::max (max_delay, _delay_out_bus[b]);
	}
	_delay_out = max_delay;
	update_delaylines ();
}

template<std::size_t N>
void
MixbusSendImpl<N>::update_delaylines (bool)
{
	bool changed;
	if (_delay_out > _delay_in) {
		changed = _thru_delay->set_delay (_delay_out - _delay_in);
		_master_delay->set_delay (_delay_out - _master_delay_out);
		for (uint32_t b = 0; b < N; ++b) {
			assert (_delay_out_bus[b] <= _delay_out);
			_send_delays[b]->set_delay (_delay_out - _delay_out_bus[b]);
		}
	} else {
		changed = _thru_delay->set_delay (0);
		_master_delay->set_delay (_delay_in - _master_delay_out);
		for (uint32_t b = 0; b < N; ++b) {
			assert (_delay_out_bus[b] <= _delay_in);
			_send_delays[b]->set_delay (_delay_in - _delay_out_bus[b]);
		}
	}

	if (changed) {
		ChangedLatency (); /* EMIT SIGNAL */
	}
}

/* *****************************************************************************
 * Mixbus 3,4,5 -> Mixbus 6 state translation
 */

// XXX needs update Mixbus vs 32C

static int map_mb_parameter_id (uint32_t pid) {
	switch (pid) {
		case  1: return -1; // hidden
		case 20: return -1; // Sidechain Assign

		case  2: return  1; // Pan
		case 19: return  0; // Master Assign

		case  3: return  2; // MB 1 Assign
		case  4: return  3; // MB 1 Level
		case  5: return  6; // MB 2 Assign
		case  6: return  7; // MB 2 Level
		case  7: return 10; // MB 3 Assign
		case  8: return 11; // MB 3 Level
		case  9: return 14; // MB 4 Assign
		case 10: return 15; // MB 4 Level
		case 11: return 18; // MB 5 Assign
		case 12: return 19; // MB 5 Level
		case 13: return 22; // MB 6 Assign
		case 14: return 23; // MB 6 Level
		case 15: return 26; // MB 7 Assign
		case 16: return 27; // MB 7 Level
		case 17: return 30; // MB 8 Assign
		case 18: return 31; // MB 8 Level

		case 21: return 34; // MB 9 Assign
		case 22: return 35; // MB 9 Level
		case 23: return 38; // MB 10 Assign
		case 24: return 39; // MB 10 Level
		case 25: return 42; // MB 11 Assign
		case 26: return 43; // MB 11 Level
		case 27: return 46; // MB 12 Assign
		case 28: return 47; // MB 12 Level

		case 29: return  4; // MB 1 Pan Enable
		case 30: return  5; // MB 1 Pan
		case 31: return  8; // MB 2 Pan Enable
		case 32: return  9; // MB 2 Pan
		case 33: return 12; // MB 3 Pan Enable
		case 34: return 13; // MB 3 Pan
		case 35: return 16; // MB 4 Pan Enable
		case 36: return 17; // MB 4 Pan
		case 37: return 20; // MB 5 Pan Enable
		case 38: return 21; // MB 5 Pan
		case 39: return 24; // MB 6 Pan Enable
		case 40: return 25; // MB 6 Pan
		case 41: return 28; // MB 7 Pan Enable
		case 42: return 29; // MB 7 Pan
		case 43: return 32; // MB 8 Pan Enable
		case 44: return 33; // MB 8 Pan
		case 45: return 36; // MB 9 Pan Enable
		case 46: return 37; // MB 9 Pan
		case 47: return 40; // MB 10 Pan Enable
		case 48: return 41; // MB 10 Pan
		case 49: return 44; // MB 11 Pan Enable
		case 50: return 45; // MB 11 Pan
		case 51: return 48; // MB 12 Pan Enable
		case 52: return 49; // MB 12 Pan
		default: assert (0); break;
	}
	return pid;
}

static bool map_mb_is_pan_id (int pid) {
	return (pid > 0 && (pid % 4) == 1);
}

static bool map_mb_is_gain_id (int pid) {
	return (pid > 0 && (pid % 4) == 3);
}

static bool map_mb_is_pan (std::string const& str) {
	if (str.length() > 10 && str.substr(0, 10) == "parameter-") {
		int pid = map_mb_parameter_id (atoi (str.c_str() + 10));
		return map_mb_is_pan_id (pid);
	}
	return false;
}

static bool map_mb_is_gain (std::string const& str) {
	if (str.length() > 10 && str.substr(0, 10) == "parameter-") {
		int pid = map_mb_parameter_id (atoi (str.c_str() + 10));
		return map_mb_is_gain_id (pid);
	}
	return false;
}

static std::string map_mb_parameter (std::string const& str, bool mixbus) {
	if (str.length() > 10 && str.substr(0, 10) == "parameter-") {
		int pid = map_mb_parameter_id (atoi (str.c_str() + 10));
		if (pid < 0 || (mixbus && pid != 1)) {
			return "ignore";
		} else {
			char buf[64];
			sprintf (buf, "parameter-%d", pid);
			return buf;
		}
	}
	return str;
}

static double db_to_coeff (double db)
{
	if (db <= -80) { return 0; }
	else if (db >=  20) { return 10; }
	return pow (10., .05 * db);
}

static bool mab_mb_param_value (uint32_t& p, double& v, bool mixbus) {
	int pm = map_mb_parameter_id (p);
	if (pm < 0 || (mixbus && pm != 1)) {
		return false;
	}
	p = pm;
	if (map_mb_is_gain_id (p)) {
		v = db_to_coeff (v);
	}
	else if (map_mb_is_pan_id (p)) {
		v = (v + 90.) / 180.0;
	}
	return true;
}

/* *****************************************************************************
 * State
 */

template<std::size_t N>
XMLNode&
MixbusSendImpl<N>::state () const
{
	XMLNode& node (Processor::state ());
	node.set_property ("type", "mbsend");

	for (Controls::const_iterator c = controls().begin(); c != controls().end(); ++c) {
		std::shared_ptr<AutomationControl> ac = std::dynamic_pointer_cast<AutomationControl> ((*c).second);
		if (!ac) {
			continue;
		}
		XMLNode& n (ac->get_state());
		n.set_property (X_("parameter"), ac->parameter().id());
		node.add_child_nocopy (n);
	}

	return node;
}

template<std::size_t N>
int
MixbusSendImpl<N>::set_state (const XMLNode& n, int version)
{
	XMLNode node (n);

	XMLProperty const* ladspa_id = node.property("unique-id");
	assert (version > 5990 || ladspa_id);
	unsigned long ladspa_uid = 0;
	if (ladspa_id) {
		ladspa_uid = atoi (ladspa_id->value());
		node.set_property ("name", name()); // don't use "POST"
	}

	std::map <uint32_t, float> lv1_value_map;

	if (version < 5990 && ladspa_uid == 9303 && node.child ("ladspa")) {
		XMLNodeList lv1 = node.child ("ladspa")->children ();
		for (XMLNodeIterator iter = lv1.begin(); iter != lv1.end(); ++iter) {
			if ((*iter)->name() != "Port") {
				continue;
			}
			uint32_t port_id;
			float value;
			if (!(*iter)->get_property ("number", port_id)) {
				continue;
			}
			if (!(*iter)->get_property ("value", value)) {
				continue;
			}

			lv1_value_map[port_id] = value;
		}
	}

	XMLNodeList nlist = node.children();
	for (XMLNodeIterator iter = nlist.begin(); iter != nlist.end(); ++iter) {

		if (version < 5990 && (*iter)->name() == X_("Automation")) {
			XMLNodeList n2list = (*iter)->children();
			XMLNodeIterator n2iter;
			for (n2iter = n2list.begin(); n2iter != n2list.end(); ++n2iter) {
				if ((*n2iter)->name() != "AutomationList") {
					continue;
				}
				XMLProperty const* id_prop = (*n2iter)->property("automation-id");
				if (!id_prop) {
					continue;
				}
				std::string pname (id_prop->value());
				(*n2iter)->set_property ("automation-id", map_mb_parameter (pname, ladspa_uid != /*chan post*/ 9303));
				XMLNode* el = (*n2iter)->child ("events");
				if (el && map_mb_is_gain (pname)) {
					el->set_property ("convert_from_db", true);
				}
				if (el && map_mb_is_pan (pname)) {
					el->set_property ("convert_from_pan", true);
				}
			}
		}

		if ((*iter)->name() != Controllable::xml_node_name) {
			continue;
		}
		uint32_t p;
		if (!(*iter)->get_property (X_("parameter"), p)) {
			continue;
		}

		if (version < 5990) {
			double val;
			if (!(*iter)->get_property (X_("value"), val)) {
				continue;
			}
			if (lv1_value_map.find (p) != lv1_value_map.end ()) {
				val = lv1_value_map[p];
			}
			if (!mab_mb_param_value (p, val, ladspa_uid != /*chan post*/ 9303)) {
				continue;
			}
			(*iter)->set_property ("parameter", p);
			(*iter)->set_property ("value", val);
		}

		std::shared_ptr<Evoral::Control> c = control (Evoral::Parameter (PluginAutomation, 0, p));
		if (!c) {
			continue;
		}
		std::shared_ptr<AutomationControl> ac = std::dynamic_pointer_cast<AutomationControl> (c);
		if (ac) {
			ac->set_state (**iter, version);
#ifdef MIXBUS32C
			ac->clear_flag (Controllable::HiddenControl);
#else
			for (uint32_t b = 0; b < N; ++b) {
				if (ac == _send_pan_enable_ctrl[b] || ac == _send_pan_ctrl[b]) {
					ac->set_flag (Controllable::HiddenControl);
				}
			}
#endif
		}
	}

	//node.dump (std::cout, "mbs: ");

	/* THIS SETS AUTOMATION
	 * -> Processor::set_state
	 * -> Automatable::set_automation_xml_state
	 * -> AutomationList::set_state
	 * -> AutomationList::deserialize_events
	 */
	return Processor::set_state (node, version);
}

/* Specialization */

namespace ARDOUR {
template class MixbusSendImpl<NUM_MIXBUSES>;
template class MixbusSendImpl<4>;
template class MixbusSendImpl<0>;
}
