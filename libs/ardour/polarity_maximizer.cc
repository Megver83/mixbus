/*
    Copyright (C) 2016 Robin Gareus

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#include <cmath>
#include <cstring>
#include <vamp-hostsdk/PluginLoader.h>

#include "ardour/audioplaylist.h"
#include "ardour/polarity_maximizer.h"
#include "ardour/route.h"
#include "ardour/dsp_filter.h"

#include "pbd/i18n.h"

using namespace Vamp;
using namespace ARDOUR;
using namespace std;

PolarityMaximizer::PolarityMaximizer (float sample_rate)
	: _sample_rate (sample_rate)
	, _bufsize (8192)
	, _samples_read (0)
	, _samples_end (0)
	, _prog_factor (1.f)
	, _prog_offset (0)
	, _canceled (false)
	, _result_gain (0)
	, _hp_freq (0)
	, _ls_freq (0)
	, _mode (0)
	, _norm (0)
{
	if (initialize_plugin (X_("harrison_vamp:polmax2"))) {
		throw failed_constructor();
	}
}

PolarityMaximizer::~PolarityMaximizer()
{
	delete _plugin;
}

int
PolarityMaximizer::initialize_plugin (std::string key)
{
	using namespace Vamp::HostExt;

	PluginLoader* loader (PluginLoader::getInstance());
	_plugin = loader->loadPlugin (key, _sample_rate, PluginLoader::ADAPT_ALL_SAFE);

	if (!_plugin) {
		PBD::error << string_compose (_("VAMP Plugin \"%1\" could not be loaded"), key) << endmsg;
		return -1;
	}

	if (_plugin->getMinChannelCount() > 1) {
		delete _plugin;
		_plugin = 0;
		return -1;
	}

	return 0;
}

int
PolarityMaximizer::run (const SrcList& src, const std::list<TimelineRange>& range)
{
	int ret = -1;
	Plugin::FeatureSet features;
	uint32_t n_channels = 0;

	_results.clear ();
	_result_gain = 0;
	_samples_read = 0;

	for (SrcList::const_iterator i = src.begin (); i != src.end(); ++i) {
		n_channels += i->route->n_inputs ().n_audio ();
	}

	_plugin->reset ();
	if (!_plugin->initialise (n_channels, _bufsize, _bufsize)) {
		return -1;
	}

	_plugin->setParameter ("norm_t", _norm);
	_plugin->setParameter ("algo_t", _mode);

	Filt hpf;
	Filt lsf;

	if (_hp_freq > 0) {
		for (uint32_t c = 0; c < n_channels; ++c) {
			BiquadPtr bq (new DSP::Biquad (_sample_rate));
			bq->compute (DSP::Biquad::HighPass, _hp_freq, _hp_qual, 0);
			hpf.push_back (bq);
		}
	}

	if (_ls_freq > 0) {
		for (uint32_t c = 0; c < n_channels; ++c) {
			BiquadPtr bq (new DSP::Biquad (_sample_rate));
			bq->compute (DSP::Biquad::LowShelf, _ls_freq, _ls_qual, _ls_gain);
			lsf.push_back (bq);
		}
	}

	float** bufs = (float**) malloc (n_channels * sizeof(float*));
	for (uint32_t c = 0; c < n_channels; ++c) {
		bufs[c] = (float*) malloc (_bufsize * sizeof(float));
	}

	// unused, needed for playlist read
	ARDOUR::Sample* mixbuf  = (Sample *) malloc(sizeof(Sample) * _bufsize);
	float*          gainbuf = (float *)  malloc(sizeof(float)  * _bufsize);

	for (std::list<TimelineRange>::const_iterator j = range.begin(); j != range.end(); ++j) {

		samplecnt_t x = 0;
		const samplecnt_t rlen = j->length().samples();
		const samplepos_t rpos = j->start().samples();

		while (x < rlen) {
			samplecnt_t chunk = std::min (_bufsize, rlen - x);
			uint32_t c = 0;
			for (SrcList::const_iterator i = src.begin (); i != src.end(); ++i) {
				const uint32_t nc = i->route->n_inputs().n_audio();
				for (uint32_t channel = 0; channel < nc; ++channel) {
					samplecnt_t n;
					n = i->playlist->read (bufs[c], mixbuf, gainbuf, timepos_t (rpos + x), timecnt_t (chunk), channel).samples();
					if (n < chunk) {
						// XXX can this happen?
						// TODO handle gracefully?, zero buffers
						cerr << "Polarity Analysis: short read: " << n << " / " << chunk << "\n";
						goto out;
					}
					++c;
				}
			}
			assert (c == n_channels);

			if (_hp_freq > 0) {
				for (uint32_t c = 0; c < n_channels; ++c) {
					hpf[c]->run (bufs[c], chunk);
				}
			}

			if (_ls_freq > 0) {
				for (uint32_t c = 0; c < n_channels; ++c) {
					lsf[c]->run (bufs[c], chunk);
				}
			}

			if (chunk < _bufsize) {
				for (uint32_t c = 0; c < n_channels; ++c) {
					for (uint32_t s = chunk; s < _bufsize; ++s) {
						bufs[c][s] = 0;
					}
				}
			}

			_plugin->process (bufs, RealTime::fromSeconds ((double) _samples_read / _sample_rate));

			_samples_read += chunk;
			x += chunk;

			Progress (_prog_factor * (_samples_read + _samples_end * _prog_offset), _samples_end);
			if (_canceled) {
				goto out;
			}
		}
	}

	features = _plugin->getRemainingFeatures ();

	if (features.empty() || features.size() != 3) {
		goto out;
	}

	for (std::vector<float>::const_iterator i = features[0][0].values.begin();
			i != features[0][0].values.end(); ++i) {
		if (*i < 0) {
			_results.push_back (true);
		} else {
			_results.push_back (false);
		}
	}

	_result_gain = features[1][0].values[0];
	_matrix_terms = features[2][0].values;

	ret = 0;

out:
	for (uint32_t c = 0; c < n_channels; ++c) {
		free (bufs[c]);
	}
	free (bufs);
	free (mixbuf);
	free (gainbuf);
	return ret;
}

float
PolarityMaximizer::calc_gain (const std::vector<bool>& pol) const
{
	size_t N = pol.size ();
	if (_matrix_terms.size () != 1 + N * (N - 1) / 2) {
		return -1;
	}
	float sum0 = _matrix_terms[0]; // squared terms / 2
	float sum1 = _matrix_terms[0]; // squared terms / 2
	//Cross terms
	uint32_t k = 1;
	for (size_t i = 0; i < N; ++i) {
		const float ci = pol[i] ? -1.f : 1.f;
		for (size_t j = 0; j < i; ++j) {
			const float cj = pol[j] ? -1.f : 1.f;
			sum0 += _matrix_terms[k];
			sum1 += ci * cj * _matrix_terms[k];
			++k;
		}
	}
	if (sum1 < 0) { sum1 = 0; }
	return 10.f * log10f (sum1 / sum0);
}
