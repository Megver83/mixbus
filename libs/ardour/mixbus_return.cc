/*
 * Copyright (C) 2019 Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glibmm/threads.h>

#include "ardour/audio_buffer.h"
#include "ardour/mixbus_send.h"
#include "ardour/mixbus_return.h"
#include "ardour/route.h"
#include "ardour/session.h"

using namespace std;
using namespace ARDOUR;

MixbusReturn::MixbusReturn (Session& s, Role role, size_t mixbus)
	: Processor (s, "Mixbus Return", Temporal::AudioTime)
	, _role (role)
	, _mixbus (mixbus)
	, _mute_channel_assigns (false)
{
	assert (mixbus > 0 || role == Master);
	assert (mixbus < 9 || role == FXBus);
	_display_to_user = false;
}

void
MixbusReturn::run (BufferSet& bufs, samplepos_t /*start_sample*/, samplepos_t /*end_sample*/, double /*speed*/, pframes_t nframes, bool)
{
#if 0 // always active
	if (!_active && !_pending_active) {
		return;
	}
#endif
	_active = _pending_active;

	for (BufferSet::audio_iterator b = bufs.audio_begin(); b != bufs.audio_end(); ++b) {
		b->prepare ();
		b->silence (nframes);
	}

	/* this is used by mixbus-solo - mute master assigns */
	const bool mute_channel_assigns = _mute_channel_assigns;
	_mute_channel_assigns = false;

	std::shared_ptr<RouteList const> routes = _session.get_routes();
	for (auto const& i : *routes) {
		if (!i->active ()) {
			continue;
		}
		std::shared_ptr<MixbusSend> mbs = i->mixbus_send();
		if (!mbs) {
			continue;
		}
		if (mute_channel_assigns && _mixbus == 0 && mbs->role() == MixbusSend::Channel) {
			continue;
		}
		if ((_role == Master)
		    || (_role == FXBus && mbs->role() == MixbusSend::Mixbus)
		    || (mbs->role() == MixbusSend::Channel && mbs->n_busses() >= _mixbus)
		   )
		{
			bufs.merge_from (mbs->get_buffers(_mixbus), nframes);
		}
	}
}

void
MixbusReturn::set_playback_offset (samplecnt_t cnt)
{
	Processor::set_playback_offset (cnt);

	std::shared_ptr<RouteList const> routes = _session.get_routes();
	for (auto const& i : *routes) {
		std::shared_ptr<MixbusSend> mbs = i->mixbus_send();
		if (!mbs) {
			continue;
		}
		if ((_role == Master)
		    || (_role == FXBus && mbs->role() == MixbusSend::Mixbus)
		    || (mbs->role() == MixbusSend::Channel && mbs->n_busses() >= _mixbus)
		   )
		{
			mbs->set_delay_out (cnt, _mixbus);
		}
	}
}

bool
MixbusReturn::can_support_io_configuration (const ChanCount& in, ChanCount& out)
{
	out = ChanCount (DataType::AUDIO, 2);
	return true;
}

bool
MixbusReturn::configure_io (ChanCount in, ChanCount out)
{
	return Processor::configure_io (in, out);
}

XMLNode&
MixbusReturn::state () const
{
	XMLNode& node (Processor::state ());
	/* override type */
	node.set_property("type", "mbreturn");
	return node;
}
